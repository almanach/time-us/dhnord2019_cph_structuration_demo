# dhnord2019_cph_structuration_demo

Démonstration de la pipeline de structuration des comptes-rendus des conseils de prud'hommes parisiens mise en place dans le cadre du projet Time Us et présentée à l'occasion du colloque DHNord2019.

---


[🪶 Aller voir le notebook sur Google Colab](https://colab.research.google.com/drive/1UeDdrqXBm4DsaSl8KjRKsf8yQ2MxsTwA?usp=sharing)
