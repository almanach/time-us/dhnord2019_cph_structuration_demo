# Structuration logique des Comptes-rendus des séances du Conseil des Prud'hommes de Paris pour l'industrie du textile (TIME US)

## License

[CC BY](https://creativecommons.org/licenses/by/2.0/fr/)

```
Chagué, Alix & Le Fourner, Victoria, \"Structuration logique des Comptes-rendus des séances du Conseil des Prud'hommes de Paris pour l'industrie du textile (TIME US)\", 2021 [Jupyter Notebook]
```

## 0. Préambule

Ce scénario de transformation a été mis en place dans le cadre du projet ANR Time Us. Il a été élaboré dans le cadre du stage de Victoria Le Fourner, conduit au sein de l'équipe ALMAnaCH du centre de recherche Inria Paris entre avril et juillet 2019. Il a été adapté sous la forme d'un Notebook Jupyter en juillet 2021 par Alix Chagué, ingénieure de recherche et développement au sein de l'équipe ALMAnaCH.



### 0.1 Scénario 1

Le scénario initial s'inscrivait dans un environnement adossé au logiciel Transkribus, dans lequel des images d'archives numérisées étaient transcrite automatique ou manuellement. \n",


1. Le programme [ExportFromTranskribus](https://gitlab.inria.fr/almanach/time-us/ExportFromTranskribus) s'appuyait sur la fonctionnalité d'export de Transkribus vers XML TEI et permettait de récupérer un fichier XML TEI unique pour un ensemble d'image.
2. Le script [StructurationMinute](https://gitlab.inria.fr/almanach/time-us/schema-tei/-/blob/master/E%20-%20Structuration%20automatique/E.1%20-%20StructurationMinute/StructurationMinute.py) permettait ensuite de modifier cet arbre XML de manière à retrouver la structure logique de chaque compte-rendu. 
  - pour voir un prototype du résultat : http://timeusage.paris.inria.fr/prudhommes-paris-19e/content/PHD-Paris-ensemble_manuscrit_2_audience_1.html
  - pour mieux comprendre la modélisation de la structure, voir :  *Le Fourner, Victoria. 2019. « Étude de la structuration automatique et de l'éditorialisation d'un corpus hétérogène, l'exemple des sources du conseil des prud'hommes pour le textile du XIXe siècle ». Mémoire de master. Technologies numériques appliquées à l'histoire. Paris : École nationale des chartes.*

###  0.2 Scénario 2
Depuis la mise en place du scénario initial, les données du projet TIME US ont été basculées vers la [plateforme eScriptorium déployée par Inria](http://escriptorium.inria.fr). D'une part, eScriptorium ne permet pas (encore) d'exporter un fichier XML TEI, d'autre part, les évolutions de Transkribus ne garantissent plus la réussite de l'exécution du script ExportFromTranskribus. Il était donc nécessaire de réadapter la scénario de structuration automatique à l'état des données disponibles. 

En l'occurrence, on peut facilement exporter d'eScriptorium les transcriptions au format TXT, ce qui correspond à peu près à l'état de structuration du fichier à l'étape des données à la [ligne 373](https://gitlab.inria.fr/almanach/time-us/schema-tei/-/blob/master/E%20-%20Structuration%20automatique/E.1%20-%20StructurationMinute/StructurationMinute.py#L373) du programme StructurationMinute.

L'adaptation du scénario au nouvel environnement logiciel est aussi l'occasion d'optimiser la formulation des REGEX et la construction de l'arbre XML. 

###  0.3 Structure

Chaque audience est structurée de la manière suivante : 

- `div @type=courtHearing`
  - `div @type=case`
    - `opener`
    - `div @type=identificationParties`
      - `span @type=part1` (× n)
      - `span @type=part2` (× n)
    - `div @type=pointDeFait`
    - `div @type=pointDeDroit`
    - `div @type=judgement`

###  0.4 Données
On s'est appuyé sur les données des Conseils des Prud'hommes (CPH) de Paris de 1847-49, 1858 et 1878, disponibles ici : 

#### 1858:
- http://escriptorium.inria.fr/media/users/3/export_doc417_dataset_cph_paris_1858_fini_text_202107191516.txt

#### 1847-1849: 
- http://escriptorium.inria.fr/media/users/3/export_doc536_timeus_ad75_d1u10_379_prud_homme_text_202107201913.txt

#### 1878:
- http://escriptorium.inria.fr/media/users/3/export_doc538_timeus_ad75_d1u10_405_prud_homme_text_202107201917.txt\n"


## 1. Installation de l'environnement et des dépendances


```py
import os
import bs4
from bs4 import BeautifulSoup, NavigableString

import re
from datetime import datetime
```
Output
```txt

```

## 2. Récupération des données sources

```py
sources = ["https://escriptorium.inria.fr/media/users/3/export_doc417_dataset_cph_paris_1858_fini_text_202107191516.txt",  # tout 1858
           "https://escriptorium.inria.fr/media/users/3/export_doc536_timeus_ad75_d1u10_379_prud_homme_text_202107201913.txt", # tout 1847-49
           "https://escriptorium.inria.fr/media/users/3/export_doc538_timeus_ad75_d1u10_405_prud_homme_text_202107280912.txt", # pp 1-19 : janvier 1878
           "https://escriptorium.inria.fr/media/users/3/export_doc538_timeus_ad75_d1u10_405_prud_homme_text_202108250949.txt"]  # pp 375-462 : juin 1878 

titles = ["AD75 D1U10 386 Prud'hommes 1858",  # tout 1858
          "AD75 D1U10 379 Prud'hommes 1847-1849",  # tout 1847-49
          "AD75 D1U10 405 Prud'hommes 1878 - janvier",  # pp 1-19 : janvier 1878
          "AD75 D1U10 405 Prud'hommes 1878 - juin"]  # pp 375-462 : juin 1878
```
Output
```txt

```

### 2.1 Téléchargement des fichiers sources avec WGET

Pour lancer cette commande localement, vous devez préalablement installer [`wget`](https://doc.ubuntu-fr.org/wget).

Pour chaque URL dirigeant vers un export TXT disponible, on télécharge le fichier et on calcule le nom du fichier une fois téléchargé, en le racourcissant si besoin.

```py
source_bases = []
for source_url in sources:
    source_base = source_url.split("/")[-1]

    # on peut racourcir le nom du fichier si besoin ;
    #if len(source_base.split(".")[0]) > 20:
    #    source_rebase = source_base[:21] + "." + source_base.split(".")[-1] 
    #else:
    source_rebase = False
    
    !wget --no-check-certificate -N $source_url

    if source_rebase:
        !mv $source_base $source_rebase
        source_base = source_rebase
    
    source_bases.append(source_base)
```
Output
```txt
    --2021-10-25 16:36:50--  https://escriptorium.inria.fr/media/users/3/export_doc417_dataset_cph_paris_1858_fini_text_202107191516.txt
    Resolving escriptorium.inria.fr (escriptorium.inria.fr)... 128.93.101.11
    Connecting to escriptorium.inria.fr (escriptorium.inria.fr)|128.93.101.11|:443... connected.
    WARNING: cannot verify escriptorium.inria.fr's certificate, issued by ‘CN=GEANT OV RSA CA 4,O=GEANT Vereniging,C=NL’:
      Unable to locally verify the issuer's authority.
    HTTP request sent, awaiting response... 200 OK
    Length: 285069 (278K) [text/plain]
    Saving to: ‘export_doc417_dataset_cph_paris_1858_fini_text_202107191516.txt’
    
    export_doc417_datas 100%[===================>] 278.39K   686KB/s    in 0.4s    
    
    2021-10-25 16:36:52 (686 KB/s) - ‘export_doc417_dataset_cph_paris_1858_fini_text_202107191516.txt’ saved [285069/285069]
    
    --2021-10-25 16:36:52--  https://escriptorium.inria.fr/media/users/3/export_doc536_timeus_ad75_d1u10_379_prud_homme_text_202107201913.txt
    Resolving escriptorium.inria.fr (escriptorium.inria.fr)... 128.93.101.11
    Connecting to escriptorium.inria.fr (escriptorium.inria.fr)|128.93.101.11|:443... connected.
    WARNING: cannot verify escriptorium.inria.fr's certificate, issued by ‘CN=GEANT OV RSA CA 4,O=GEANT Vereniging,C=NL’:
      Unable to locally verify the issuer's authority.
    HTTP request sent, awaiting response... 200 OK
    Length: 593313 (579K) [text/plain]
    Saving to: ‘export_doc536_timeus_ad75_d1u10_379_prud_homme_text_202107201913.txt’
    
    export_doc536_timeu 100%[===================>] 579.41K  1.05MB/s    in 0.5s    
    
    2021-10-25 16:36:53 (1.05 MB/s) - ‘export_doc536_timeus_ad75_d1u10_379_prud_homme_text_202107201913.txt’ saved [593313/593313]
    
    --2021-10-25 16:36:53--  https://escriptorium.inria.fr/media/users/3/export_doc538_timeus_ad75_d1u10_405_prud_homme_text_202107280912.txt
    Resolving escriptorium.inria.fr (escriptorium.inria.fr)... 128.93.101.11
    Connecting to escriptorium.inria.fr (escriptorium.inria.fr)|128.93.101.11|:443... connected.
    WARNING: cannot verify escriptorium.inria.fr's certificate, issued by ‘CN=GEANT OV RSA CA 4,O=GEANT Vereniging,C=NL’:
      Unable to locally verify the issuer's authority.
    HTTP request sent, awaiting response... 200 OK
    Length: 205499 (201K) [text/plain]
    Saving to: ‘export_doc538_timeus_ad75_d1u10_405_prud_homme_text_202107280912.txt’
    
    export_doc538_timeu 100%[===================>] 200.68K   742KB/s    in 0.3s    
    
    2021-10-25 16:36:54 (742 KB/s) - ‘export_doc538_timeus_ad75_d1u10_405_prud_homme_text_202107280912.txt’ saved [205499/205499]
    
    --2021-10-25 16:36:54--  https://escriptorium.inria.fr/media/users/3/export_doc538_timeus_ad75_d1u10_405_prud_homme_text_202108250949.txt
    Resolving escriptorium.inria.fr (escriptorium.inria.fr)... 128.93.101.11
    Connecting to escriptorium.inria.fr (escriptorium.inria.fr)|128.93.101.11|:443... connected.
    WARNING: cannot verify escriptorium.inria.fr's certificate, issued by ‘CN=GEANT OV RSA CA 4,O=GEANT Vereniging,C=NL’:
      Unable to locally verify the issuer's authority.
    HTTP request sent, awaiting response... 200 OK
    Length: 185647 (181K) [text/plain]
    Saving to: ‘export_doc538_timeus_ad75_d1u10_405_prud_homme_text_202108250949.txt’
    
    export_doc538_timeu 100%[===================>] 181.30K   671KB/s    in 0.3s    
    
    2021-10-25 16:36:55 (671 KB/s) - ‘export_doc538_timeus_ad75_d1u10_405_prud_homme_text_202108250949.txt’ saved [185647/185647]
```

### 2.2 Récupération du schéma RNG
Pour la validation des fichiers XML TEI générés (non géré dans ce notebook).


```py
#schema_url = "https://gitlab.inria.fr/almanach/time-us/schema-tei/-/raw/master/D%20-%20Sch%C3%A9ma%20de%20validation/D.2%20-%20ODD/D.2.1%20-%20ODDmanuscrit/ODDmanuscrit.rng"
#base_url_schema = schema_url.split("/")[-1]

#!wget -N $schema_url
```
Output
```txt

```

## 3\. Fonctions

L'ensemble des traitements effectués sur le texte peut être décomposé en 3 grandes étapes :
- la construction de l'en-tête avec métadonnées
- la construction de la structure logique spécifique aux comptes-rendus des CPH et aux informations qui intéressent le projet ANR TIME US, sur la base de motifs textuels récurrents (expressions régulières)\n",
- le nettoyage et l'ajout d'une couche d'annotation des dates d'audiences

Une fonction principale (`main()`) gère l'application de ces trois ensembles de modifications dans l'ordre souhaité.


### 3.1 Fonctions TeiHeader\n",
on construit un gabarit contenant un body vide, qui sera rempli ensuite, et une en-tête avec les métadonnées propres au projet TIME US et au corpus des CPH de Paris. "
 

```py
# structure du header
def build_tei_header(tei_title):
    return "".join(["<teiHeader>","<fileDesc>","<titleStmt>",
                      f"<title>{tei_title} - Transcription</title>",
                      "<editor>Alix Chagué</editor>",
                      "<respStmt>",
                      "<name>Alix Chagué</name><resp>Gestion des données, encodage de la structure logique</resp>",
                      "</respStmt>",
                      "<respStmt>",
                      "<name>Victoria Le Fourner</name><resp>Encodage de la structure des documents</resp>",
                      "</respStmt>",
                      "<respStmt>",
                      "<name>Kevin Champougny</name><resp>Correction de la transcription et prise de vues</resp>",
                      "</respStmt>","</titleStmt>",
                      "<publicationStmt>",
                      "<p>Time Us (2017-2020) : http://timeusage.paris.inria.fr/mediawiki/index.php/Accueil</p>",
                      "</publicationStmt>","<sourceDesc>","<p>No description</p>",
                      "</sourceDesc>", "</fileDesc>", "<encodingDesc>", "<projectDesc>", 
                      "<p>TIME US est un projet ANR dont le but est de reconstituer les rémunérations et les budgets temps des travailleur⋅ses du textile dans quatre villes industrielles française (Lille, Paris, Lyon, Marseille) dans une perspective européenne et de longue durée. Il réunit une équipe pluridisciplinaire d'historiens des techniques, de l'économie et du travail, des spécialistes du traitement automatique des langues et des sociologues spécialistes des budgets familiaux. Il vise à donner des clés pour comprendre le gender gap en analysant les mutations du travail et la répartition du temps et des tâches au sein des ménages pendant la première industrialisation. Pour ce faire, le projet met en place une action de transcription et d'annotation de documents d'archives datés de la fin du XVIIe au début du XXe siècle.</p>",
                      "</projectDesc>", "<editorialDecl>", 
                      "<p>Les transcriptions et leur annotations sont réalisées à l'aide de la plate-forme Transkribus.</p>",
                      "<p>Les transcriptions ont été transférées importées dans eScriptorium (escriptorium.inria.fr) et la validité de leur segmentation controlée manuellement.</p>",
                      "</editorialDecl>", "</encodingDesc>", "<revisionDesc>",
                      '<change type="Created">2018-12-19T23:03:09.528+01:00</change>',
                      '<change type="LastChange">2019-01-27T15:58:22.906+01:00</change>',
                      '<change type="toeScriptorium">2021-01</change>',
                      f'<change type="ToTEI">{str(datetime.now()).replace(" ", "T") + "+2:00"}</change>',
                      "</revisionDesc>", "</teiHeader>"])
    


def make_tei_container(title):
    container = """<TEI xmlns="http://www.tei-c.org/ns/1.0"></TEI>"""
    parsed_container = BeautifulSoup(container, "xml")
    parsed_tei_header = BeautifulSoup(build_tei_header(title), "xml").extract()
    parsed_container.TEI.append(parsed_tei_header)
    parsed_container.TEI.append(parsed_container.new_tag("text_blob"))
    return parsed_container
```
Output
```txt

```

### 3.2 Fonctions structure 

La construction de la structure logique s'appuie sur des indices textuels récurrents tels que "Audience du" ou "Entre... d'une part ;" ou "... d'autre part ;" ou encore "Point de fait", "Point de droit", etc. L'utilisation d'expressions régulières permet de gagner en souplesse face aux variations dans la redaction et dans la transcription.

```py
def add_courthearing_clues(text):
    """Ajoute des div pour repérer le début de chaque nouvelle audience"""
    #PATTERN_DIV_COURTHEARING =re.compile(r"((?=(Audience)).*?((?= Audience|$)))") 
    # <- impossible de faire marcher cette regex dans le script, pourtant correcte
    text = text.replace("Audience", '</div><div type="courtHearing">Audience')
    return text


def add_case_clues(text):
    """Ajoute des div pour repérer le début de chaque nouvelle affaire"""
    # Target: Du jour ... / Du dit jour ... / Dudit jour ...
    text = re.sub(r"(Du ?(?:dit)? {0,}jour? ?v?.+?)(dessus|[eE]ntre)?", 
                  r'</div><div type="case">\1\2', text)
    # Target: Audience Du Lundi/Mardi/Mercredi/Jeudi/Vendredi
    # Noter que cibler seulement Vendredi serait suffisant
    text = re.sub(r"(Audience\s{0,})(\s{0,}[dD]u\s{0,}\s{0,}[A-z]+di )(.+)",
                  r'\1</div><div type="case">\2\3', text)
    return text


def remove_extra_divs(text):
    """Retire les div fermantes qui n'ont pas de div ouvrante correspondant"""
    if text.startswith("</div>"):
        text = text[6:]
    text = text.replace('Audience</div><div type="case">', 'Audience<div type="case">')
    return text


def force_clean_manusrit_div(text):
    text = re.sub(r"""^[\w\s\n]+</div><div type="courtHearing">""",
                  r"""<div type="courtHearing">""", text)
    return text


def build_tree(text):
    """Crée un arbre XML à partir d'une chaîne de caractères"""
    text = '<body><div type="manuscrit">ph</div></body>'.replace("ph", text)
    tree = BeautifulSoup(text, "xml")
    return tree


def add_n_counters_to_div(tree, type):
    """Ajoute des attributs "n" avec valeur incrémentée dans les div ciblées"""
    i = 0
    for tag in tree.find_all("div", type=type):
        tag.attrs["n"] = i
        i += 1
    return tree


def finely_structure_cases(tree):
    """Afine la structure des affaires en repérant les différents points"""
    for tag in tree.find_all("div", type="case"):
        tag_text = " ".join([seg for seg in tag.get_text().split("\n")])
        tag_text = re.sub(r" {2,}", " ", tag_text)

        # Ajout des opener et des div pour les identifications des parties
        text = re.sub(r"([Dd]u [A-z]+di)(.+)(Si[eèé]geaient ?:)(.+)(Entre )", 
                    r'<opener>\1\2\3\4</opener><div type="identificationParties">\5</div>', 
                    tag_text)
        text = re.sub(r"([dD]u ?(?:dit)? jour? v?)(.+?)([eE]ntre .+)", 
                    r'<opener>\1\2</opener><div type="identificationParties">\3</div>', 
                    tag_text)
        if not text.startswith("<opener>"):
            if re.search(r"Entre", text):
                text = "".join(["<opener>", text.replace('Entre ', '</opener><div type="identificationParties">Entre '), "</div>"])
            else:
                text = "".join(['<div type="undefined">', text, '</div>'])

        # Ajout des div pour les Points de fait
        text = re.sub(r"([pP]oint [Dd]e [Ff]ait.+)", 
                    r'</div><div type="pointDeFait">\1', 
                    text)
        # Ajout des div pour les Points de droit
        text = re.sub(r"([pP]oi?nt [Dd]e [Dd]r?oit.+)", 
                    r'</div><div type="pointDeDroit">\1', 
                    text)
        # Ajout des div pour les Jugements
        text = re.sub(r"([Aa]près avoir entendu|Ainsi jugé)(.+)", 
                    r'</div><div type="judgement">\1\2', 
                    text)
        # On s'assure que toutes les div et les p sont fermées
        if not text.endswith("</div>"):
            text = text + "</div>"
        # On réintègre tout cela dans l'arbre XML
        tag.contents = []
        new_content = BeautifulSoup(f"<blob>{text}</blob>", "xml").extract()
        for subtag in new_content.find_all(["div", "opener"]):
            tag.append(subtag)
    return tree


def identification_parties_et_date_audience(tree):
    """Afine la structure de l'identification des parties en repérant les différentes parties"""
    for tag in tree.find_all("div", type="identificationParties"):
        text = tag.get_text()
    
        # Il y a parfois plusieurs parties "d'une part"
        # notre approche change s'il n'y a qu'une partie d'une part ou plusieurs
        # et on essaie d'avoir un mécanisme de log quand on n'en trouve aucune
        # TODO : il y a beaucoup de répétitions, on pourrait utiliser des variables
        #        pour les motifs qui se répètent
        nmatches = len([x for x in re.finditer(r"([dD][’'4]?u(?:[un]e)? part)([\s;]+)", text)])
        if nmatches > 1:
            text = re.sub(r"(Entre)(.+?)([dD][’'4]?u(?:[un]e)? part)([\s;]+)", 
                        r'<span type="part1">\1\2\3\4</span>', text)
            for i in range(nmatches - 1):
                text = re.sub(r"([dD][’'4]?u(?:[un]e)? part[ ;]+</span>)(.+?)([dD][’'4]?u(?:ne)? part)([\s;]+)", 
                        r'\1<span type="part1">\2\3\4</span>', text)
        elif nmatches == 1:
            text = re.sub(r"(Entre)(.+)([dD][’'4]?u(?:[un]e)? part)([\s;]+)", 
                        r'<span type="part1">\1\2\3\4</span>', text)
        else:
            print("[" +str(nmatches) + "] Unable to find something like \"D'une part ;\" in:\n \"" + text + "\"\n")
    
        # Il y a parfois plusieurs parties "d'autre part"
        # notre approche change en fonction de s'il y a une partie d'autre part ou bien plusieurs
        # et on essaie d'avoir un mécanisme de log quand on n'en trouve aucune
        # TODO : on pourrait utiliser des variables pour les motifs qui se répètent
        nmatches = len([x for x in re.finditer(r"([dD][’'4]? ?autre )(part)?([\s,;]+)", text)])
        if nmatches > 1:
            text = re.sub(r"([dD][’'4] ?u(?:ne)? part[\s;]+</span>)(.+?)([dD][’'4]? ?autre )(part)?([\s,;]+)", 
                        r'\1<span type="part2">\2\3\4\5</span>', text)
            text = re.sub(r"([dD][’'4]? ?autre (?:part)?[\s;]+</span>)(.+?)([dD][’'4]? ?autre )(part)?([\s,;]+)", 
                        r'\1<span type="part2">\2\3\4\5</span>', text)
            text = re.sub(r"([dD][’'4]? ?autre (?:part)?[\s;]+</span>)([^<].+?)([dD][’'4]? ?autre )(part)?([\s,;]+)", 
                        r'\1<span type="part2">\2\3\4\5</span>', text)
        elif nmatches == 1:
            text = re.sub(r"([dD][’'4]?u(?:ne)? part[\s;]+</span>)(.+)([dD][’'4]? ?autre )(part)?([\s,;]+)", 
                        r'\1<span type="part2">\2\3\4\5</span>', text) 
        else:
            print("[" +str(nmatches) + "] Unable to find something like \"D'autre part ;\" in:\n \"" + text + "\"\n")
    
        # On réintègre tout ça dans l'arbre XML, de manière plus ou moins élégante...
        tag.clear()
        new_content = BeautifulSoup(f"<rs>{text}</rs>", "xml").extract()
        tag.append(new_content)
    return tree
```
Output
```txt

```

### 3.3 Fonction nettoyage et annotation

Pour s'assurer que le fichier qui sera enregistré est valide et conforme aux guidelines de la TEI, on ajoute les éléments manquants comme les `<p>` dans les éléments `<div>`.

On peut également ajouter des éléments `<date>` autour d'un certain nombre de dates. Il est difficile de les cibler toutes dans la mesure où elles sont exprimées entièrement en toute lettres et il existe de nombreuses irrégularités dans le texte.

```py
def clean_up(xml_tree):
    """Mostly add p elements in div where it's missing"""
    types = ["courtHearing", "identificationParties", 
           "pointDeFait", "pointDeDroit", "judgement",
           "undefined"]  #"manuscrit",
    for typ in types:
        for div in xml_tree.find_all("div", type=typ):
            new_p = xml_tree.new_tag("p")
            for c in div.contents:
                new_p.append(c.extract())
            div.append(new_p)
    return xml_tree
```
Output
```txt

```

```py
def build_date_regex():
    """Fonction séparée pour lisibilité"""
    # on va chercher les jours
    PAT_DAY = "|".join(["[Ll]undi", "[Mm]ardi", "[Mm]ercredi", "[Jj]eudi", "[Vv]endredi", "[Ss]amedi", "[Dd]imanche"])
    # il reste des scories dans la transcription qu'on gère comme des exceptions pour éviter de prendre trop de mots
    DATE_EXCP =  "|".join(["seijiliet","Viugeaient","Monsieure","conant","Courant","en demn se touer","par saite","\. légitime","\. Cité"])
    # Motif Regex des dates:
    pat_date = re.compile("".join(["([Dd]u )(",PAT_DAY, ")(.+?)( )([Ss]i?[éeè]g[aient]+|", DATE_EXCP, ")"]))
    # mémo captures : $1="du ", $2="vendredi", $3=" Dix neuf Janvier mil huit Cent quarante huit", $4=" ", $5="Siégeaient"
    return pat_date

def find_dates(xml_tree):
    """find dates and add <date> tags"""
    # On essaie de mettre des tags "date" seulement quand on est sûr que c'est une date
    # donc on a va manquer des éléments.
    date_regex = build_date_regex()
    # On récupère des bouts de texte dans lesquels on va chercher les dates
    scope = []
    for elem in xml_tree.body.find_all(["p", "span", "opener"]):
        for content in elem.contents:
            if content.name:
                if content.name == "rs" or content.name == "span":
                    for cont in content.contents:
                        if cont.name == "span":
                            scope.append(cont)
                        else:
                            scope.append(content)
                #else:
                #    print(content.name)
            else:
                scope.append(elem)
    # à ce moment-là, on n'a que des objets de type bs4.element.NavigableString dans scope
    #for elem in scope:
    #    if not isinstance(elem, bs4.element.NavigableString):
    #        print(type(elem))

    # on cherche les dates et on insère les dates
    for elem in scope:
        if elem.string:
            elem.string.replace_with(re.sub(date_regex, r"\1<date>\2\3</date>\4\5", elem.string, 0))
        #else:  # on a du mal à atteindre les string dans les elemn rs
        #    print(elem.name)
    return xml_tree


def fix_date_tags(tree_as_text):
    return tree_as_text.replace("&lt;date&gt;", "<date>").replace("&lt;/date&gt;", "</date>")
```
Output
```txt

```

### 3.4 Fonction principale

La fonction principale applique l'ensemble des transformations expliquées plus haut, en procédant pas à pas pour faciliter la lisibilité de l'enchaînement des transformations. A la fin, un fichier `.tei.xml` est créé.
        
```py
def main(source, target, title, show_output=False):
    # On ouvre le fichier source
    with open(source, "r", encoding="utf8") as fh:
        source_as_text = fh.read()
    # suppression des césures et régularisation des espaces
    pattern_cesure = re.compile(r"[¬]+[ \\n]+")
    source_as_text = " ".join(re.sub(pattern_cesure, "", source_as_text).split("\\n"))
    source_as_text = re.sub(r" {2,}", r" ", source_as_text)

    # ajout d'indices de structure
    source_w_clues = add_courthearing_clues(source_as_text)
    source_w_clues = add_case_clues(source_w_clues)

    # nettoyage avant de parser le XML
    clean_text = force_clean_manusrit_div(remove_extra_divs(source_w_clues))

    # parsing et ajout d'éléments de structuration logique
    tree = build_tree(clean_text)
    tree = add_n_counters_to_div(tree, type="courtHearing")
    tree = add_n_counters_to_div(tree, type="case")
    tree = finely_structure_cases(tree)
    tree = identification_parties_et_date_audience(tree)

    # on remballe tout
    complete_tree = make_tei_container(title)
    complete_tree.text_blob.append(tree.extract())
    complete_tree = clean_up(complete_tree)
    complete_tree = find_dates(complete_tree)
    complete_tree.text_blob.name = "text"

    # enregistrement du fichier
    with open(target, "w", encoding="utf8") as fh:
        fh.write(fix_date_tags(complete_tree.prettify()))
  
    if show_output:
        print(fix_date_tags(complete_tree.prettify()))
```
Output
```txt

```

## 4. Execution

Après avoir calculé le nom du fichier de sortie, on exécute la fonction `main()` pour créer un fichier XML structuré !"

```py
for source, title in zip(source_bases, titles):
    # construction du nom du fichier cible
    ext = f".{source.split('.')[-1]}"
    target = source.replace(ext, ".tei.xml")
    target = re.sub(r"_{2,}", "_", target)

    # vérification du nom du fichier cible 
    print("# # # # # # # # #", target, "# # # # # # # # #\\n")

    # Ajouter "show_output=True" pour afficher l'arbre XML
    main(source=source_base, target=target, title=title)#, show_output=True)"
```
Output
```txt
# # # # # # # # # export_doc417_dataset_cph_paris_1858_fini_text_202107191516.tei.xml # # # # # # # # #

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Bréchot, ouvrier Galochier, demeurant à Paris, rue de Meaux, numéro trente sept, passage de la Brie ; Demandeur ; Comparant ; D'une part ; </span>Et Monsieur Vermérot, fabricant de Galoches, demeurant et domicilié à Paris deux mots rayés nuls Lecucq — "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Madame veuve Bernard, ouvrière demeurant à Joinville le Pont, près Paris, rue du canal, numéro onze ; Demanderesse ; Comparant ; D'une part ; </span><span type="part1">Et Monsieur Bardin, fabricant plumassier, demeurant et domicilié à Joinville le Pont, rue des Réservoirs ; Défendeur ; Comparant ; D'une part ; </span>"

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Levonte, anvner lapeemntier, demeurant à Boles, au rondipement de Clermont leisa j Demandur, Comperant ; D'une part ; </span><span type="part1">Et Monpeur Curtet Jeun Marie, ouvrier vailleur d’hobits, demeurant à Paris, passage Montes quien, numéro cinq ; Demander ; Comparant ; D’une part ; </span>L Monsieur Lacembe,; tute dailleur d’habits, demeurant et domicilié à Paris rue lai sa numéro quinze ; Défendeur ; Comparant ; Dutelurt, Paint de fait - Par lettres du secrétaire du Conseil de srur ha du Département de la Seine pour l’industrie des tissus en dtates des Vendredi trente un Mai et lundi trois juin mil huit cent soixante dix huit, curtet fit citer Lacombe à Comparatre par devant ledit conseil de Prud’homme séant en Bureaux Particuliers les emble trois et Mardi quatre Cin mil huit cent soixante dix huit pour sin concilier à faire se pouvait sur la demande qu’il entendit forixen contre lui devant le dit Conseil en paiement de la somme de Pi de sit es arés nil Mn ec e Dre q u 2 i ié a q i i q du soixante denx francs pour selaire. La coulée n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six pis par lettre du secrétairé en date du quatre juin mil huit ent soixante dix huit à la réquité de Cortet Lecomle comparut; A l'appel de la cause Curet se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner lacole à lui payer avec intérêts suivant la loi la somme de soinante deux francs qu'il lui doit pour prix de travaux de son état et le condonner aux dépens. De son coté Laconbe se présenta et conclut à ce q’il plut au Bureau Général du Conseil ettendu qu'il reconnit devoir la soume de soixante deux francs ; mois attendu qu'an dêtenant le fière qu'il encère entre les moins turtet lui ceuse préjudice Luisque cette frèce n’ayant pa être livrée en temps la cestire pour comple Par ces motifs — Lire Cortet untucevable, en rademand e tavu débouter eile condamner aux dépens. foit de doit - Doit on condamner Lacoute à payer à cortets la somme de soixante de soixante deux froncs contre la remisé d’un ditent qu'il salire à Paton dit en du leis reant lut la demande ; l'en débouter ecevoir Lécombe reonvou tionnellement demandent ; Condamne Tortet a remitre le vêtement en psus eu disitre ou mis d sant au eu du du sent pu tie ueapsite d etito u ell t tent lerse lu de lete de e e e t l peiue en leurs demandes et conclusions repeetivement et encvord et e t te e te ite u ede ur tn du t esur purt la pre des ioise les our are e e e e t due a ler s r se te e dte t e poi i en e p e ten u reit e de u te enpe ente e pe de iq ount quilt eten de ses uee de de e titer duit en ue se andurs deuen der de aun u ant e e reie uar e e t enede tain e ti qu iq e te see inps e Mée minute, Cnformément à la loi du sept aût mil huit cent cinquante rue, non compris le cout du présent jugement, la signification d’iceluit et ses suites. "

[0] Unable to find something like "D'une part ;" in:
 "Entre Mademoiselle Désirée Sanson, se disont ouvrière contrière demeurant à Paris, rue Fesnnier, numéro dix Demanderesle ; Comparant ; D’une dort ; t Monsieur et Madame Magnen, le sieur Magnen tant en son nom personnel que pur epaiter et autoriser la dame son épouse, Mantrelte contirre, demeurant et domiciliés, entembrle, le dits épout à Paris, avenie des taris, nméré qatre suit cinq Défendeurs ; Comparant ; D’autre part ; Poit de fit Par lettre du secrétaire du Conseil de Prud’homme du Dépastement de la Seine pour l'industrie des tissus en dates des Mercredi vingt neuf a vendredi trente Mai mil huit cent huit li e t le te de se le e u dapers prde ts tel a sid en ue u Anseu té te eontritat d u umes e les epord s le er en p p t ente unde ete dais poir tetre qunt les e e pre s la o e e en per due mne u deant ens un epe e pe le deu le deur du ti ens ene n en pement dei e e der e paur edu due au de juemne ede t e e e e es lute ede e e que der eun, duedoint e e e e e e en e se e eit e aen e den e eser pr e enqu les Bourrnie de se sonfais ; que si elle lui à enteigne grctutenant le protique de la cuture s'eit qu'elle la veutoir endre expabla de complir l'umploi d’une smme de ciembre au lien de cettedeu borne d’enfans qu'elle avait occupée jusque la ; Que s'elle sa nourrie c’esrt qu'elle prit, l'engagement de lui payer sa nouriter par ena somme de se francs int ciq cntiomes pas jounr qu'en sai elle est sa débitrie et hou se trénière poureni et e ecinq frcès de l’appele devant Monsieur lejage de Pair la su arcondipement qui les ontorisé à détenir sa malle jusque jour ou elle de serapoiquittée envers eur ; Par ces motifs — Le déclarer en conpétut pour comaitre de la demande la demoiselle Sandon, l'en débouter et la condamner aux dépens. Deton coté la demoiselle San son se présenta et conclut à ce qu’il plaut au Bureau Général du conseil attendu que la dame Jourson l’a occuhze pendont cinq mois à des travaux de contire après quoi elle lui delivra un certfiret dant lequel elle est qualitée d’ouvrière conturière ; Par ces motifs — Dire les époux Mégaenu non recevables enteur demante ; les en débouter. Le déclarer compétent, retenir la cause et oedonner qu’il sera explique sur le fand et condamner les époux Magnon aux dépens. Point de troit - Doit-on se déclarer on complent pour comastre de la demande de la demoiselle sanson? Ou bien doit-on de déclarer compétent, retenir la cause et ordonner que les parties seront tenue de s'expliquer sur le fundi pour être statué ce qu'il appartiendeu?? Que doit-il être statué à l'égard es dépens ? ? "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monpeur Louis Voubert soupeur d’habités, demeurant à Paris, rue de la bente tir numéro cinquante ; Demandeur ; Comparant ; D'une part ; </span>Monsieur Purant ; confectionneur d’habits, demeurant et domicilié à Paris, rue croir des Patits chemps, numéro cinquante ; Défendeur ; Comparant ; D'utre part ; Dinq de fait - Par letres du secrétaire du Conseil de Prud'homes du Département de la Seine pour l’industrie des tissus en dates des Mardi quatre et vendredi sept jaunvier mil huit cent soixente dix huit Joulert fpt citer sinant à comparaître par devant le dit conseil de Prud’homme demil au Bureaux Particulier les Vendredi sept et Murdi onze vun mil huit censoixmmmte dix fait sur se concilier si faire se pouvait sur lu demende qu'it entendait former cente lui devant le dit conseil en delai conge domme dusage Denait- n’ayant pas comparu la caute fut renvayée devans le Bureau Général du Conseil séant le jeudi toize juin mil huit cent prbte de li di e le dt es hun sem pr lettre du secrétaire du Conseil en date du onte juin nif huit Cent sixante dix huit à la reate de outant denans aplels; Ail le det dte sudi épaut de eue sut an en et oncsue que det hui e e e e e qui edte pue t e e e fitr de de afur l contilous es re de lui poarseue udits pos l l se e er pl de onos qnr i d it fi t M t de dommages-intérêts et le condamner pans les deur ense dépens. De son coé dement se présenta et conclut à ci qu’il plut au Bureau Général du Conseil attendu qu’il embouche pouler, pour un cop de moins et qu'en sontaires le conseisa à cajournée de quatre francs cinquante centimes de non con mis sonners le prétend ; qu'il la anpe dé poue qu'il étant en capable de bien faire la latijue ; Devait motifs ; Dire Soulert non recevablien su semante l'en déboiter et le condanner ux dépens. Panit, de droit Dait-on dire que Finand sera tenu de recevoir etae lui peutert et de lui continuer pendant le reste du mill de juin du travail de son état de coupeur d’habits enten servant les appointement de cent vingt francs pour le dit mois, sinon et faute par lui de ce faire le condamner à payerau dit Toulert la somme de cent vingt francs à titre de dammaser intrêts ? Ou bien dait-on dire Toulert non recevable ses demandes, l'en débouter ? Que doit-il être statué à l’égard des dépens ? "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Charles Fétemett, ouvrier tordonner peurs à Paris, rue Paint souveur, numéro vingt huit ; Demanders comparant ; D'une part ; </span>Et Monsieur Gavail, Mntre cordonnier, demeurant et domicilié à Paris, rue Vayevir numéro quatre ; Défendeur ; Défaillant ;D’autre hart ; ores de fait – Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l’industrie des tissus en dates des Lundi trois et Mardi quatre juin mil huit cent sixante dix huit étemill fit citer Lavail à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les mordi quel et vendredi sept juin mil huit, cent soixante dix huit pour se concilier si faire se pouvait sur la demande qusil entendu former contre lui devant le dit conseil en paiement de la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour lalaire . Lavail n'ayant pas comprru la cause ft renvayée devant le Bureau Général du conseil séant le jeudi vingt et ajournée au putte Vingt sept juin mil huit cent soixante six temp Cité pour le dit suivant exploif de champron, luipier à Paris, en date du vingt un juin mil huit cent soixante dix huit, visé pour timbre et enregistré à Paris, le vingt deux Juin mil huit cent soirante dix huit, etmitte fit citer ravail à comparaître par devant le dit Conseil de Prud’hommes séant en Bureau Général le jeudi vingt sept juin mil huit cent soixante dix lui pour s'entendre condamner u lui payer avec intérêts suivant la loi la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour salaire plus, telte indemnité qu'il plaira au Conseil frer pour perte de temps devant les Bureaux du Conseil et les dépens. A l'’appel de la cause Lavail ne comparut pas. De son coté schunt se présenta et conclutz à ce qu’il plut au Bureau Général du Conseil donner défau contre Lavail non comparant ni personn pour lui quoique dumet appelé et pour le profit lui adjuger le bénifice des conclusions par lui prises dans le citation en plois de Champour, lui par enregistré. Poin de troit — Doit-on donner défaut contre lavail non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précédemment prises ? Que doit-il etre statué à l’égard des dépens ? "

# # # # # # # # # export_doc536_timeus_ad75_d1u10_379_prud_homme_text_202107201913.tei.xml # # # # # # # # #

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Bréchot, ouvrier Galochier, demeurant à Paris, rue de Meaux, numéro trente sept, passage de la Brie ; Demandeur ; Comparant ; D'une part ; </span>Et Monsieur Vermérot, fabricant de Galoches, demeurant et domicilié à Paris deux mots rayés nuls Lecucq — "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Madame veuve Bernard, ouvrière demeurant à Joinville le Pont, près Paris, rue du canal, numéro onze ; Demanderesse ; Comparant ; D'une part ; </span><span type="part1">Et Monsieur Bardin, fabricant plumassier, demeurant et domicilié à Joinville le Pont, rue des Réservoirs ; Défendeur ; Comparant ; D'une part ; </span>"

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Levonte, anvner lapeemntier, demeurant à Boles, au rondipement de Clermont leisa j Demandur, Comperant ; D'une part ; </span><span type="part1">Et Monpeur Curtet Jeun Marie, ouvrier vailleur d’hobits, demeurant à Paris, passage Montes quien, numéro cinq ; Demander ; Comparant ; D’une part ; </span>L Monsieur Lacembe,; tute dailleur d’habits, demeurant et domicilié à Paris rue lai sa numéro quinze ; Défendeur ; Comparant ; Dutelurt, Paint de fait - Par lettres du secrétaire du Conseil de srur ha du Département de la Seine pour l’industrie des tissus en dtates des Vendredi trente un Mai et lundi trois juin mil huit cent soixante dix huit, curtet fit citer Lacombe à Comparatre par devant ledit conseil de Prud’homme séant en Bureaux Particuliers les emble trois et Mardi quatre Cin mil huit cent soixante dix huit pour sin concilier à faire se pouvait sur la demande qu’il entendit forixen contre lui devant le dit Conseil en paiement de la somme de Pi de sit es arés nil Mn ec e Dre q u 2 i ié a q i i q du soixante denx francs pour selaire. La coulée n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six pis par lettre du secrétairé en date du quatre juin mil huit ent soixante dix huit à la réquité de Cortet Lecomle comparut; A l'appel de la cause Curet se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner lacole à lui payer avec intérêts suivant la loi la somme de soinante deux francs qu'il lui doit pour prix de travaux de son état et le condonner aux dépens. De son coté Laconbe se présenta et conclut à ce q’il plut au Bureau Général du Conseil ettendu qu'il reconnit devoir la soume de soixante deux francs ; mois attendu qu'an dêtenant le fière qu'il encère entre les moins turtet lui ceuse préjudice Luisque cette frèce n’ayant pa être livrée en temps la cestire pour comple Par ces motifs — Lire Cortet untucevable, en rademand e tavu débouter eile condamner aux dépens. foit de doit - Doit on condamner Lacoute à payer à cortets la somme de soixante de soixante deux froncs contre la remisé d’un ditent qu'il salire à Paton dit en du leis reant lut la demande ; l'en débouter ecevoir Lécombe reonvou tionnellement demandent ; Condamne Tortet a remitre le vêtement en psus eu disitre ou mis d sant au eu du du sent pu tie ueapsite d etito u ell t tent lerse lu de lete de e e e t l peiue en leurs demandes et conclusions repeetivement et encvord et e t te e te ite u ede ur tn du t esur purt la pre des ioise les our are e e e e t due a ler s r se te e dte t e poi i en e p e ten u reit e de u te enpe ente e pe de iq ount quilt eten de ses uee de de e titer duit en ue se andurs deuen der de aun u ant e e reie uar e e t enede tain e ti qu iq e te see inps e Mée minute, Cnformément à la loi du sept aût mil huit cent cinquante rue, non compris le cout du présent jugement, la signification d’iceluit et ses suites. "

[0] Unable to find something like "D'une part ;" in:
 "Entre Mademoiselle Désirée Sanson, se disont ouvrière contrière demeurant à Paris, rue Fesnnier, numéro dix Demanderesle ; Comparant ; D’une dort ; t Monsieur et Madame Magnen, le sieur Magnen tant en son nom personnel que pur epaiter et autoriser la dame son épouse, Mantrelte contirre, demeurant et domiciliés, entembrle, le dits épout à Paris, avenie des taris, nméré qatre suit cinq Défendeurs ; Comparant ; D’autre part ; Poit de fit Par lettre du secrétaire du Conseil de Prud’homme du Dépastement de la Seine pour l'industrie des tissus en dates des Mercredi vingt neuf a vendredi trente Mai mil huit cent huit li e t le te de se le e u dapers prde ts tel a sid en ue u Anseu té te eontritat d u umes e les epord s le er en p p t ente unde ete dais poir tetre qunt les e e pre s la o e e en per due mne u deant ens un epe e pe le deu le deur du ti ens ene n en pement dei e e der e paur edu due au de juemne ede t e e e e es lute ede e e que der eun, duedoint e e e e e e en e se e eit e aen e den e eser pr e enqu les Bourrnie de se sonfais ; que si elle lui à enteigne grctutenant le protique de la cuture s'eit qu'elle la veutoir endre expabla de complir l'umploi d’une smme de ciembre au lien de cettedeu borne d’enfans qu'elle avait occupée jusque la ; Que s'elle sa nourrie c’esrt qu'elle prit, l'engagement de lui payer sa nouriter par ena somme de se francs int ciq cntiomes pas jounr qu'en sai elle est sa débitrie et hou se trénière poureni et e ecinq frcès de l’appele devant Monsieur lejage de Pair la su arcondipement qui les ontorisé à détenir sa malle jusque jour ou elle de serapoiquittée envers eur ; Par ces motifs — Le déclarer en conpétut pour comaitre de la demande la demoiselle Sandon, l'en débouter et la condamner aux dépens. Deton coté la demoiselle San son se présenta et conclut à ce qu’il plaut au Bureau Général du conseil attendu que la dame Jourson l’a occuhze pendont cinq mois à des travaux de contire après quoi elle lui delivra un certfiret dant lequel elle est qualitée d’ouvrière conturière ; Par ces motifs — Dire les époux Mégaenu non recevables enteur demante ; les en débouter. Le déclarer compétent, retenir la cause et oedonner qu’il sera explique sur le fand et condamner les époux Magnon aux dépens. Point de troit - Doit-on se déclarer on complent pour comastre de la demande de la demoiselle sanson? Ou bien doit-on de déclarer compétent, retenir la cause et ordonner que les parties seront tenue de s'expliquer sur le fundi pour être statué ce qu'il appartiendeu?? Que doit-il être statué à l'égard es dépens ? ? "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monpeur Louis Voubert soupeur d’habités, demeurant à Paris, rue de la bente tir numéro cinquante ; Demandeur ; Comparant ; D'une part ; </span>Monsieur Purant ; confectionneur d’habits, demeurant et domicilié à Paris, rue croir des Patits chemps, numéro cinquante ; Défendeur ; Comparant ; D'utre part ; Dinq de fait - Par letres du secrétaire du Conseil de Prud'homes du Département de la Seine pour l’industrie des tissus en dates des Mardi quatre et vendredi sept jaunvier mil huit cent soixente dix huit Joulert fpt citer sinant à comparaître par devant le dit conseil de Prud’homme demil au Bureaux Particulier les Vendredi sept et Murdi onze vun mil huit censoixmmmte dix fait sur se concilier si faire se pouvait sur lu demende qu'it entendait former cente lui devant le dit conseil en delai conge domme dusage Denait- n’ayant pas comparu la caute fut renvayée devans le Bureau Général du Conseil séant le jeudi toize juin mil huit cent prbte de li di e le dt es hun sem pr lettre du secrétaire du Conseil en date du onte juin nif huit Cent sixante dix huit à la reate de outant denans aplels; Ail le det dte sudi épaut de eue sut an en et oncsue que det hui e e e e e qui edte pue t e e e fitr de de afur l contilous es re de lui poarseue udits pos l l se e er pl de onos qnr i d it fi t M t de dommages-intérêts et le condamner pans les deur ense dépens. De son coé dement se présenta et conclut à ci qu’il plut au Bureau Général du Conseil attendu qu’il embouche pouler, pour un cop de moins et qu'en sontaires le conseisa à cajournée de quatre francs cinquante centimes de non con mis sonners le prétend ; qu'il la anpe dé poue qu'il étant en capable de bien faire la latijue ; Devait motifs ; Dire Soulert non recevablien su semante l'en déboiter et le condanner ux dépens. Panit, de droit Dait-on dire que Finand sera tenu de recevoir etae lui peutert et de lui continuer pendant le reste du mill de juin du travail de son état de coupeur d’habits enten servant les appointement de cent vingt francs pour le dit mois, sinon et faute par lui de ce faire le condamner à payerau dit Toulert la somme de cent vingt francs à titre de dammaser intrêts ? Ou bien dait-on dire Toulert non recevable ses demandes, l'en débouter ? Que doit-il être statué à l’égard des dépens ? "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Charles Fétemett, ouvrier tordonner peurs à Paris, rue Paint souveur, numéro vingt huit ; Demanders comparant ; D'une part ; </span>Et Monsieur Gavail, Mntre cordonnier, demeurant et domicilié à Paris, rue Vayevir numéro quatre ; Défendeur ; Défaillant ;D’autre hart ; ores de fait – Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l’industrie des tissus en dates des Lundi trois et Mardi quatre juin mil huit cent sixante dix huit étemill fit citer Lavail à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les mordi quel et vendredi sept juin mil huit, cent soixante dix huit pour se concilier si faire se pouvait sur la demande qusil entendu former contre lui devant le dit conseil en paiement de la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour lalaire . Lavail n'ayant pas comprru la cause ft renvayée devant le Bureau Général du conseil séant le jeudi vingt et ajournée au putte Vingt sept juin mil huit cent soixante six temp Cité pour le dit suivant exploif de champron, luipier à Paris, en date du vingt un juin mil huit cent soixante dix huit, visé pour timbre et enregistré à Paris, le vingt deux Juin mil huit cent soirante dix huit, etmitte fit citer ravail à comparaître par devant le dit Conseil de Prud’hommes séant en Bureau Général le jeudi vingt sept juin mil huit cent soixante dix lui pour s'entendre condamner u lui payer avec intérêts suivant la loi la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour salaire plus, telte indemnité qu'il plaira au Conseil frer pour perte de temps devant les Bureaux du Conseil et les dépens. A l'’appel de la cause Lavail ne comparut pas. De son coté schunt se présenta et conclutz à ce qu’il plut au Bureau Général du Conseil donner défau contre Lavail non comparant ni personn pour lui quoique dumet appelé et pour le profit lui adjuger le bénifice des conclusions par lui prises dans le citation en plois de Champour, lui par enregistré. Poin de troit — Doit-on donner défaut contre lavail non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précédemment prises ? Que doit-il etre statué à l’égard des dépens ? "

# # # # # # # # # export_doc538_timeus_ad75_d1u10_405_prud_homme_text_202107280912.tei.xml # # # # # # # # #

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Bréchot, ouvrier Galochier, demeurant à Paris, rue de Meaux, numéro trente sept, passage de la Brie ; Demandeur ; Comparant ; D'une part ; </span>Et Monsieur Vermérot, fabricant de Galoches, demeurant et domicilié à Paris deux mots rayés nuls Lecucq — "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Madame veuve Bernard, ouvrière demeurant à Joinville le Pont, près Paris, rue du canal, numéro onze ; Demanderesse ; Comparant ; D'une part ; </span><span type="part1">Et Monsieur Bardin, fabricant plumassier, demeurant et domicilié à Joinville le Pont, rue des Réservoirs ; Défendeur ; Comparant ; D'une part ; </span>"

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Levonte, anvner lapeemntier, demeurant à Boles, au rondipement de Clermont leisa j Demandur, Comperant ; D'une part ; </span><span type="part1">Et Monpeur Curtet Jeun Marie, ouvrier vailleur d’hobits, demeurant à Paris, passage Montes quien, numéro cinq ; Demander ; Comparant ; D’une part ; </span>L Monsieur Lacembe,; tute dailleur d’habits, demeurant et domicilié à Paris rue lai sa numéro quinze ; Défendeur ; Comparant ; Dutelurt, Paint de fait - Par lettres du secrétaire du Conseil de srur ha du Département de la Seine pour l’industrie des tissus en dtates des Vendredi trente un Mai et lundi trois juin mil huit cent soixante dix huit, curtet fit citer Lacombe à Comparatre par devant ledit conseil de Prud’homme séant en Bureaux Particuliers les emble trois et Mardi quatre Cin mil huit cent soixante dix huit pour sin concilier à faire se pouvait sur la demande qu’il entendit forixen contre lui devant le dit Conseil en paiement de la somme de Pi de sit es arés nil Mn ec e Dre q u 2 i ié a q i i q du soixante denx francs pour selaire. La coulée n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six pis par lettre du secrétairé en date du quatre juin mil huit ent soixante dix huit à la réquité de Cortet Lecomle comparut; A l'appel de la cause Curet se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner lacole à lui payer avec intérêts suivant la loi la somme de soinante deux francs qu'il lui doit pour prix de travaux de son état et le condonner aux dépens. De son coté Laconbe se présenta et conclut à ce q’il plut au Bureau Général du Conseil ettendu qu'il reconnit devoir la soume de soixante deux francs ; mois attendu qu'an dêtenant le fière qu'il encère entre les moins turtet lui ceuse préjudice Luisque cette frèce n’ayant pa être livrée en temps la cestire pour comple Par ces motifs — Lire Cortet untucevable, en rademand e tavu débouter eile condamner aux dépens. foit de doit - Doit on condamner Lacoute à payer à cortets la somme de soixante de soixante deux froncs contre la remisé d’un ditent qu'il salire à Paton dit en du leis reant lut la demande ; l'en débouter ecevoir Lécombe reonvou tionnellement demandent ; Condamne Tortet a remitre le vêtement en psus eu disitre ou mis d sant au eu du du sent pu tie ueapsite d etito u ell t tent lerse lu de lete de e e e t l peiue en leurs demandes et conclusions repeetivement et encvord et e t te e te ite u ede ur tn du t esur purt la pre des ioise les our are e e e e t due a ler s r se te e dte t e poi i en e p e ten u reit e de u te enpe ente e pe de iq ount quilt eten de ses uee de de e titer duit en ue se andurs deuen der de aun u ant e e reie uar e e t enede tain e ti qu iq e te see inps e Mée minute, Cnformément à la loi du sept aût mil huit cent cinquante rue, non compris le cout du présent jugement, la signification d’iceluit et ses suites. "

[0] Unable to find something like "D'une part ;" in:
 "Entre Mademoiselle Désirée Sanson, se disont ouvrière contrière demeurant à Paris, rue Fesnnier, numéro dix Demanderesle ; Comparant ; D’une dort ; t Monsieur et Madame Magnen, le sieur Magnen tant en son nom personnel que pur epaiter et autoriser la dame son épouse, Mantrelte contirre, demeurant et domiciliés, entembrle, le dits épout à Paris, avenie des taris, nméré qatre suit cinq Défendeurs ; Comparant ; D’autre part ; Poit de fit Par lettre du secrétaire du Conseil de Prud’homme du Dépastement de la Seine pour l'industrie des tissus en dates des Mercredi vingt neuf a vendredi trente Mai mil huit cent huit li e t le te de se le e u dapers prde ts tel a sid en ue u Anseu té te eontritat d u umes e les epord s le er en p p t ente unde ete dais poir tetre qunt les e e pre s la o e e en per due mne u deant ens un epe e pe le deu le deur du ti ens ene n en pement dei e e der e paur edu due au de juemne ede t e e e e es lute ede e e que der eun, duedoint e e e e e e en e se e eit e aen e den e eser pr e enqu les Bourrnie de se sonfais ; que si elle lui à enteigne grctutenant le protique de la cuture s'eit qu'elle la veutoir endre expabla de complir l'umploi d’une smme de ciembre au lien de cettedeu borne d’enfans qu'elle avait occupée jusque la ; Que s'elle sa nourrie c’esrt qu'elle prit, l'engagement de lui payer sa nouriter par ena somme de se francs int ciq cntiomes pas jounr qu'en sai elle est sa débitrie et hou se trénière poureni et e ecinq frcès de l’appele devant Monsieur lejage de Pair la su arcondipement qui les ontorisé à détenir sa malle jusque jour ou elle de serapoiquittée envers eur ; Par ces motifs — Le déclarer en conpétut pour comaitre de la demande la demoiselle Sandon, l'en débouter et la condamner aux dépens. Deton coté la demoiselle San son se présenta et conclut à ce qu’il plaut au Bureau Général du conseil attendu que la dame Jourson l’a occuhze pendont cinq mois à des travaux de contire après quoi elle lui delivra un certfiret dant lequel elle est qualitée d’ouvrière conturière ; Par ces motifs — Dire les époux Mégaenu non recevables enteur demante ; les en débouter. Le déclarer compétent, retenir la cause et oedonner qu’il sera explique sur le fand et condamner les époux Magnon aux dépens. Point de troit - Doit-on se déclarer on complent pour comastre de la demande de la demoiselle sanson? Ou bien doit-on de déclarer compétent, retenir la cause et ordonner que les parties seront tenue de s'expliquer sur le fundi pour être statué ce qu'il appartiendeu?? Que doit-il être statué à l'égard es dépens ? ? "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monpeur Louis Voubert soupeur d’habités, demeurant à Paris, rue de la bente tir numéro cinquante ; Demandeur ; Comparant ; D'une part ; </span>Monsieur Purant ; confectionneur d’habits, demeurant et domicilié à Paris, rue croir des Patits chemps, numéro cinquante ; Défendeur ; Comparant ; D'utre part ; Dinq de fait - Par letres du secrétaire du Conseil de Prud'homes du Département de la Seine pour l’industrie des tissus en dates des Mardi quatre et vendredi sept jaunvier mil huit cent soixente dix huit Joulert fpt citer sinant à comparaître par devant le dit conseil de Prud’homme demil au Bureaux Particulier les Vendredi sept et Murdi onze vun mil huit censoixmmmte dix fait sur se concilier si faire se pouvait sur lu demende qu'it entendait former cente lui devant le dit conseil en delai conge domme dusage Denait- n’ayant pas comparu la caute fut renvayée devans le Bureau Général du Conseil séant le jeudi toize juin mil huit cent prbte de li di e le dt es hun sem pr lettre du secrétaire du Conseil en date du onte juin nif huit Cent sixante dix huit à la reate de outant denans aplels; Ail le det dte sudi épaut de eue sut an en et oncsue que det hui e e e e e qui edte pue t e e e fitr de de afur l contilous es re de lui poarseue udits pos l l se e er pl de onos qnr i d it fi t M t de dommages-intérêts et le condamner pans les deur ense dépens. De son coé dement se présenta et conclut à ci qu’il plut au Bureau Général du Conseil attendu qu’il embouche pouler, pour un cop de moins et qu'en sontaires le conseisa à cajournée de quatre francs cinquante centimes de non con mis sonners le prétend ; qu'il la anpe dé poue qu'il étant en capable de bien faire la latijue ; Devait motifs ; Dire Soulert non recevablien su semante l'en déboiter et le condanner ux dépens. Panit, de droit Dait-on dire que Finand sera tenu de recevoir etae lui peutert et de lui continuer pendant le reste du mill de juin du travail de son état de coupeur d’habits enten servant les appointement de cent vingt francs pour le dit mois, sinon et faute par lui de ce faire le condamner à payerau dit Toulert la somme de cent vingt francs à titre de dammaser intrêts ? Ou bien dait-on dire Toulert non recevable ses demandes, l'en débouter ? Que doit-il être statué à l’égard des dépens ? "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Charles Fétemett, ouvrier tordonner peurs à Paris, rue Paint souveur, numéro vingt huit ; Demanders comparant ; D'une part ; </span>Et Monsieur Gavail, Mntre cordonnier, demeurant et domicilié à Paris, rue Vayevir numéro quatre ; Défendeur ; Défaillant ;D’autre hart ; ores de fait – Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l’industrie des tissus en dates des Lundi trois et Mardi quatre juin mil huit cent sixante dix huit étemill fit citer Lavail à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les mordi quel et vendredi sept juin mil huit, cent soixante dix huit pour se concilier si faire se pouvait sur la demande qusil entendu former contre lui devant le dit conseil en paiement de la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour lalaire . Lavail n'ayant pas comprru la cause ft renvayée devant le Bureau Général du conseil séant le jeudi vingt et ajournée au putte Vingt sept juin mil huit cent soixante six temp Cité pour le dit suivant exploif de champron, luipier à Paris, en date du vingt un juin mil huit cent soixante dix huit, visé pour timbre et enregistré à Paris, le vingt deux Juin mil huit cent soirante dix huit, etmitte fit citer ravail à comparaître par devant le dit Conseil de Prud’hommes séant en Bureau Général le jeudi vingt sept juin mil huit cent soixante dix lui pour s'entendre condamner u lui payer avec intérêts suivant la loi la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour salaire plus, telte indemnité qu'il plaira au Conseil frer pour perte de temps devant les Bureaux du Conseil et les dépens. A l'’appel de la cause Lavail ne comparut pas. De son coté schunt se présenta et conclutz à ce qu’il plut au Bureau Général du Conseil donner défau contre Lavail non comparant ni personn pour lui quoique dumet appelé et pour le profit lui adjuger le bénifice des conclusions par lui prises dans le citation en plois de Champour, lui par enregistré. Poin de troit — Doit-on donner défaut contre lavail non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précédemment prises ? Que doit-il etre statué à l’égard des dépens ? "

# # # # # # # # # export_doc538_timeus_ad75_d1u10_405_prud_homme_text_202108250949.tei.xml # # # # # # # # #

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Bréchot, ouvrier Galochier, demeurant à Paris, rue de Meaux, numéro trente sept, passage de la Brie ; Demandeur ; Comparant ; D'une part ; </span>Et Monsieur Vermérot, fabricant de Galoches, demeurant et domicilié à Paris deux mots rayés nuls Lecucq — "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Madame veuve Bernard, ouvrière demeurant à Joinville le Pont, près Paris, rue du canal, numéro onze ; Demanderesse ; Comparant ; D'une part ; </span><span type="part1">Et Monsieur Bardin, fabricant plumassier, demeurant et domicilié à Joinville le Pont, rue des Réservoirs ; Défendeur ; Comparant ; D'une part ; </span>"

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Levonte, anvner lapeemntier, demeurant à Boles, au rondipement de Clermont leisa j Demandur, Comperant ; D'une part ; </span><span type="part1">Et Monpeur Curtet Jeun Marie, ouvrier vailleur d’hobits, demeurant à Paris, passage Montes quien, numéro cinq ; Demander ; Comparant ; D’une part ; </span>L Monsieur Lacembe,; tute dailleur d’habits, demeurant et domicilié à Paris rue lai sa numéro quinze ; Défendeur ; Comparant ; Dutelurt, Paint de fait - Par lettres du secrétaire du Conseil de srur ha du Département de la Seine pour l’industrie des tissus en dtates des Vendredi trente un Mai et lundi trois juin mil huit cent soixante dix huit, curtet fit citer Lacombe à Comparatre par devant ledit conseil de Prud’homme séant en Bureaux Particuliers les emble trois et Mardi quatre Cin mil huit cent soixante dix huit pour sin concilier à faire se pouvait sur la demande qu’il entendit forixen contre lui devant le dit Conseil en paiement de la somme de Pi de sit es arés nil Mn ec e Dre q u 2 i ié a q i i q du soixante denx francs pour selaire. La coulée n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six pis par lettre du secrétairé en date du quatre juin mil huit ent soixante dix huit à la réquité de Cortet Lecomle comparut; A l'appel de la cause Curet se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner lacole à lui payer avec intérêts suivant la loi la somme de soinante deux francs qu'il lui doit pour prix de travaux de son état et le condonner aux dépens. De son coté Laconbe se présenta et conclut à ce q’il plut au Bureau Général du Conseil ettendu qu'il reconnit devoir la soume de soixante deux francs ; mois attendu qu'an dêtenant le fière qu'il encère entre les moins turtet lui ceuse préjudice Luisque cette frèce n’ayant pa être livrée en temps la cestire pour comple Par ces motifs — Lire Cortet untucevable, en rademand e tavu débouter eile condamner aux dépens. foit de doit - Doit on condamner Lacoute à payer à cortets la somme de soixante de soixante deux froncs contre la remisé d’un ditent qu'il salire à Paton dit en du leis reant lut la demande ; l'en débouter ecevoir Lécombe reonvou tionnellement demandent ; Condamne Tortet a remitre le vêtement en psus eu disitre ou mis d sant au eu du du sent pu tie ueapsite d etito u ell t tent lerse lu de lete de e e e t l peiue en leurs demandes et conclusions repeetivement et encvord et e t te e te ite u ede ur tn du t esur purt la pre des ioise les our are e e e e t due a ler s r se te e dte t e poi i en e p e ten u reit e de u te enpe ente e pe de iq ount quilt eten de ses uee de de e titer duit en ue se andurs deuen der de aun u ant e e reie uar e e t enede tain e ti qu iq e te see inps e Mée minute, Cnformément à la loi du sept aût mil huit cent cinquante rue, non compris le cout du présent jugement, la signification d’iceluit et ses suites. "

[0] Unable to find something like "D'une part ;" in:
 "Entre Mademoiselle Désirée Sanson, se disont ouvrière contrière demeurant à Paris, rue Fesnnier, numéro dix Demanderesle ; Comparant ; D’une dort ; t Monsieur et Madame Magnen, le sieur Magnen tant en son nom personnel que pur epaiter et autoriser la dame son épouse, Mantrelte contirre, demeurant et domiciliés, entembrle, le dits épout à Paris, avenie des taris, nméré qatre suit cinq Défendeurs ; Comparant ; D’autre part ; Poit de fit Par lettre du secrétaire du Conseil de Prud’homme du Dépastement de la Seine pour l'industrie des tissus en dates des Mercredi vingt neuf a vendredi trente Mai mil huit cent huit li e t le te de se le e u dapers prde ts tel a sid en ue u Anseu té te eontritat d u umes e les epord s le er en p p t ente unde ete dais poir tetre qunt les e e pre s la o e e en per due mne u deant ens un epe e pe le deu le deur du ti ens ene n en pement dei e e der e paur edu due au de juemne ede t e e e e es lute ede e e que der eun, duedoint e e e e e e en e se e eit e aen e den e eser pr e enqu les Bourrnie de se sonfais ; que si elle lui à enteigne grctutenant le protique de la cuture s'eit qu'elle la veutoir endre expabla de complir l'umploi d’une smme de ciembre au lien de cettedeu borne d’enfans qu'elle avait occupée jusque la ; Que s'elle sa nourrie c’esrt qu'elle prit, l'engagement de lui payer sa nouriter par ena somme de se francs int ciq cntiomes pas jounr qu'en sai elle est sa débitrie et hou se trénière poureni et e ecinq frcès de l’appele devant Monsieur lejage de Pair la su arcondipement qui les ontorisé à détenir sa malle jusque jour ou elle de serapoiquittée envers eur ; Par ces motifs — Le déclarer en conpétut pour comaitre de la demande la demoiselle Sandon, l'en débouter et la condamner aux dépens. Deton coté la demoiselle San son se présenta et conclut à ce qu’il plaut au Bureau Général du conseil attendu que la dame Jourson l’a occuhze pendont cinq mois à des travaux de contire après quoi elle lui delivra un certfiret dant lequel elle est qualitée d’ouvrière conturière ; Par ces motifs — Dire les époux Mégaenu non recevables enteur demante ; les en débouter. Le déclarer compétent, retenir la cause et oedonner qu’il sera explique sur le fand et condamner les époux Magnon aux dépens. Point de troit - Doit-on se déclarer on complent pour comastre de la demande de la demoiselle sanson? Ou bien doit-on de déclarer compétent, retenir la cause et ordonner que les parties seront tenue de s'expliquer sur le fundi pour être statué ce qu'il appartiendeu?? Que doit-il être statué à l'égard es dépens ? ? "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monpeur Louis Voubert soupeur d’habités, demeurant à Paris, rue de la bente tir numéro cinquante ; Demandeur ; Comparant ; D'une part ; </span>Monsieur Purant ; confectionneur d’habits, demeurant et domicilié à Paris, rue croir des Patits chemps, numéro cinquante ; Défendeur ; Comparant ; D'utre part ; Dinq de fait - Par letres du secrétaire du Conseil de Prud'homes du Département de la Seine pour l’industrie des tissus en dates des Mardi quatre et vendredi sept jaunvier mil huit cent soixente dix huit Joulert fpt citer sinant à comparaître par devant le dit conseil de Prud’homme demil au Bureaux Particulier les Vendredi sept et Murdi onze vun mil huit censoixmmmte dix fait sur se concilier si faire se pouvait sur lu demende qu'it entendait former cente lui devant le dit conseil en delai conge domme dusage Denait- n’ayant pas comparu la caute fut renvayée devans le Bureau Général du Conseil séant le jeudi toize juin mil huit cent prbte de li di e le dt es hun sem pr lettre du secrétaire du Conseil en date du onte juin nif huit Cent sixante dix huit à la reate de outant denans aplels; Ail le det dte sudi épaut de eue sut an en et oncsue que det hui e e e e e qui edte pue t e e e fitr de de afur l contilous es re de lui poarseue udits pos l l se e er pl de onos qnr i d it fi t M t de dommages-intérêts et le condamner pans les deur ense dépens. De son coé dement se présenta et conclut à ci qu’il plut au Bureau Général du Conseil attendu qu’il embouche pouler, pour un cop de moins et qu'en sontaires le conseisa à cajournée de quatre francs cinquante centimes de non con mis sonners le prétend ; qu'il la anpe dé poue qu'il étant en capable de bien faire la latijue ; Devait motifs ; Dire Soulert non recevablien su semante l'en déboiter et le condanner ux dépens. Panit, de droit Dait-on dire que Finand sera tenu de recevoir etae lui peutert et de lui continuer pendant le reste du mill de juin du travail de son état de coupeur d’habits enten servant les appointement de cent vingt francs pour le dit mois, sinon et faute par lui de ce faire le condamner à payerau dit Toulert la somme de cent vingt francs à titre de dammaser intrêts ? Ou bien dait-on dire Toulert non recevable ses demandes, l'en débouter ? Que doit-il être statué à l’égard des dépens ? "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Charles Fétemett, ouvrier tordonner peurs à Paris, rue Paint souveur, numéro vingt huit ; Demanders comparant ; D'une part ; </span>Et Monsieur Gavail, Mntre cordonnier, demeurant et domicilié à Paris, rue Vayevir numéro quatre ; Défendeur ; Défaillant ;D’autre hart ; ores de fait – Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l’industrie des tissus en dates des Lundi trois et Mardi quatre juin mil huit cent sixante dix huit étemill fit citer Lavail à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les mordi quel et vendredi sept juin mil huit, cent soixante dix huit pour se concilier si faire se pouvait sur la demande qusil entendu former contre lui devant le dit conseil en paiement de la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour lalaire . Lavail n'ayant pas comprru la cause ft renvayée devant le Bureau Général du conseil séant le jeudi vingt et ajournée au putte Vingt sept juin mil huit cent soixante six temp Cité pour le dit suivant exploif de champron, luipier à Paris, en date du vingt un juin mil huit cent soixante dix huit, visé pour timbre et enregistré à Paris, le vingt deux Juin mil huit cent soirante dix huit, etmitte fit citer ravail à comparaître par devant le dit Conseil de Prud’hommes séant en Bureau Général le jeudi vingt sept juin mil huit cent soixante dix lui pour s'entendre condamner u lui payer avec intérêts suivant la loi la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour salaire plus, telte indemnité qu'il plaira au Conseil frer pour perte de temps devant les Bureaux du Conseil et les dépens. A l'’appel de la cause Lavail ne comparut pas. De son coté schunt se présenta et conclutz à ce qu’il plut au Bureau Général du Conseil donner défau contre Lavail non comparant ni personn pour lui quoique dumet appelé et pour le profit lui adjuger le bénifice des conclusions par lui prises dans le citation en plois de Champour, lui par enregistré. Poin de troit — Doit-on donner défaut contre lavail non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précédemment prises ? Que doit-il etre statué à l’égard des dépens ? "
```

## 5. Aperçu du contenu des fichiers

Cette section permet de suivre le détail des transformations à travers l'exemple d'un fichier dont on affiche l'état à différentes étapes du processus.

### 5.1 URL du fichier TXT sur eScriptorium

```py
print(sources[-1])
```
Output
```txt
https://escriptorium.inria.fr/media/users/3/export_doc538_timeus_ad75_d1u10_405_prud_homme_text_202108250949.txt
```

### 5.2 Contenu du fichier TXT exporté de l'application eScriptorium

```py
with open(source_bases[-1], "r", encoding="utf8") as fh:
    txt_content = fh.read()

print(txt_content)
```
Output
```txt
Audience
du
Jeudi six Juin mil huit cent soixante dix huit.
Siégeaient : Messieurs
Pinaud, Chevalier de la Légion d'honneur, vice
Président du conseil de Prud'homme du Département
de la Seine pour l'Industrie des tissus, Lerroy,
Larcher, Platiau, Broutin, Godfrin
et Meslien, Prud'hommes assistés de Monsieur
Lecucq, secrétaire dudit conseil
Entre Monsieur et Madame Lablanche,
le sieur Lablanche tant en son nom personnel que pour
assister et autoriser la dame son épouse ouvrière passementière,
demeurant ensemble, les dits époux, à Paris, rue Besfroid,
numéro dix ; Demandeur ; Comparant ; D'une part ; et
Monsieur Nisson, fabricant passementier, demeurant et
domicilié à Paris, rue Rambuteau, numéro trente huit ;
Défendeur ; Défaillant ; D'autre part ; Point de fait = 
Par lettres du secrétaire du conseil de Prud'hommes du Département
de la Seine pour l'industrie des tissus en dates des Lundi vingt
et Mardi vingt un Mai mil huit cent soixante dix huit les époux
Lablanche firent citer nisson à comparaître par devant le dit
conseil de Prud'hommes séant en Bureaux Particulier les Mardi
Vingt un et vendredi vingt trois Mai mil huit cent soixante dix huit
pour se concilier si faire se pouvait sur la demande qu'ils entendaient
former contre lui devant le dit conseil en paiement de la somme de
douze francs pour prix de travaux de la dame Lablanche. 
A l'appel de la cause le vingt trois Mai mil, nisson n'ayant pas comparu
le vingt un, la dame Lablanche se présenta et exposa comme
cidessous. De son coté Nisson se présenta et exposa au 
Conseil que la dame Lablanche qui avait entrepris un travail qu'elle
devait coudre le vendredi l'obligea à l'aller chercher chez elle le 
Lendemain samedi ; pour ce fait il entendit, Quoique le travail
fut bien exécuté, ne payer que moitié prix de façon . Les
parties n'ayant pu être conciliées la cause fut renvoyée devant le
Bureau Général du Conseil séant le Jeudi six Juin mil huit 
Cent soixante dix huit. Cité pour le dit jour six Juin par
lettre du secrétaire du conseil en date du vingt cinq Mai
mil huit cent soixante dix huit à la requête des époux Lablanche
quatre mots rayés nuls
Lecucq
Nisson ne comparut pas. A l'appel de la cause les époux Lablanche
se présentèrent et conclurent à ce qu'il plut au Bureau Général
du Conseil donner défaut contre Nisson non comparant
ni personne pour lui quoique dument appelé et pour le profit
le condamner à leur payer avec intérêts suivant la loi la somme
de douze francs qu'il leur doit pour prix de travaux de la dame
Lablanche, plus, telle indemnité qu'il plaira au conseil fixer
pour perte de temps devant les Bureaux du Conseil et les dépens
Point de droit = Doit-on donner défaut contre Nisson non
comparant ni personne pour lui quoique dument appelé et pour le
profit le condamner à lui adjuger aux demandeurs les conclusions
par eux précèdemment prises ? Que doit-il être statué à l'égard
des dépens ? Après avoir entendu les époux Lablanche en leurs demande
et conclusions et en avoir délibéré conformément à la loi ; Attendu
que la demande des époux Lablanche parait juste et fondée ;
Que d'ailleurs elle n'est plus contestée par Nisson non comparant
ni personne pour lui quoique dument appelé ; Attendu que
Nisson a fait perdre du temps aux époux Lablanche devant les Bureaux
du conseil, ce qui leur cause un préjudice dont il leur doit réparation
Par ces motifs = Le Bureau Général jugeant en dernier ressort ; vu
l'article quarante un du décret du onze Juin mil huit cent neuf
portant règlement pour les Conseils de Prud'hommes, Donne
défaut contre Nisson non comparant ni personne pour lui quoique
dument appelé ; Et adjugeant profit du dit défaut ;
Condamne Nisson à payer avec intérêts suivant la loi aux époux
Lablanche la somme de douze francs qu'il leur doit pour prix de
travaux de la dame Lablanche, plus une indemnité de deux
francs pour temps perdu ; Le condamne en outre aux dépens taxés
et liquidés envers les demandeurs à la somme de un franc, et à celle
due au Trésor Public, pour le papier timbré de la présente minute,
Conformément à la loi du sept août mil huit cent cinquante, ence,
non compris le coût du présent jugement, la signification d'icelui
et ses suites. Et vu les articles 437 du code de procédure civile, 27 et
42 du décret du onze Juin 1809, pour signifier au défendeur le
présent jugement, Commet Champion, l'un de ses huissiers
audienciers. Ainsi jugé les jour mois et  que dessus
Lecucq secrétaire
Du dit jour six Juin —
Entre Monsieur Gutz Willer, ouvrier cordonnier, demeurant
à Paris, rue de Jouy, impasse Guepin, numéro quatre ;
Demandeur ; Comparant ; D'une part ; Et Monsieur Caillier
Maître cordonnier, demeurant et domicilié à Paris, rue Notre
dame de Nazareth, numéro soixante dix ; Défendeur ;
Défaillant ; D'autre part ; Point de fait = Par lettre du secrétaire
du Conseil de Prud'hommes du Département de la seine pour l'industrie
des tissus en date du Mardi vingt un Mai mil huit cent soixante dix
huit Gutz willer fit citer Caillier à comparaître par devant le dit
Conseil de Prud'hommes séant en Bureau Particulier le Vendredi
vingt quatre Mai mil huit cent soixante dix huit pour se concilier
si faire se pouvait sur la demande qu'il entendait former contre lui
devant le dit conseil en remboursement de cinquante centimes pour
pareille somme qu'il a payée au Bureau de placement qui
l'adressa chez lui au moyen d'une carte et en celle de cinq francs
pour perte de temps. A l'appel de la cause Gutz Willer
se présenta et exposa au conseil que s'étant présenté au Bureau
de placement pour y demander en carte fut envoyé à Caillier
qui ne le reçut pas prétendant qu'il arrivait alors que la place
était prises ; or, il résulte d'une clause qui régit les Bureaux de
placement que le cas échéant le Patron rembourse le prix de 
la carte qui est de cinquante centimes et l'ouvrier n'a alors 
qu'à se retirer ; Mais Caillier s'est refusé à rembourser et
fut cause qu'il perdait sa journée. De son coté Caillier 
se présenta et exposa au Conseil qu'il a en effet demandé au
Bureau de Placement des ouvriers cordonniers un ouvrier, mais
ce Bureau le lui envoyant près cinq semaines rien détonnant
qu'il n'en ait plus besoin au jour de l'envoi. C'est pourquoi
il s'opposa au remboursement du coût de la carte. Le Bureau
Particulier fut d'avis que Caillier qui reconnut n'avoir pas
contremandé la demande au Bureau d'un ouvrier aurait dû
rembourser le coût de la caste, que s'étant refusé à le faire il
devait les cinquante centimes et une indemnité de temps
perdu qu'il fixa à quatre francs. Sur le refus de Caillier
de se rendre à l'avis du Bureau Particulier la cause fut renvoyée
devant le Bureau Général du conseil séant le Jeudi six juin
mil huit cent soixante dix huit. Cité pour le dit jour six Juin par
lettre du secrétaire du conseil en date du vingt quatre Mai mil huit
Cent soixante dix huit à la requête de Gutzwiller Caillier ne
comparut pas. A l'appel de la cause Gutzwiller se présenta
et Conclut à ce qu'il plut au Bureau Général du conseil donner
défaut contre Caillier non comparant ni personne pour lui quoique
dument appelé et pour le profit le condamner à lui payer
avec intérêts suivant la loi la somme de quatre francs cinquante
Centimes se composant de celle de cinquante centimes pour le prix
d'une carte émanant du Bureau de Paiement et de celle de quatr
francs pour indemnité du temps qu'il a fait perdre devant les
Bureaux du conseil et les dépens. Point de droit = Doit-on
donner défaut contre Caillier non comparant ni personne
pour lui quoique dument appelé et pour le profit adjuger au
demandeur les conclusions par lui précèdemment prises ? Que
doit-il être statué à l'égard des dépens ? Après avoir entendu
Gutzwiller en ses demandes et conclusions et en avoir délibéré conformément
à la loi ; Attendu que la demande de Gutzwiller parait juste et
fondée ; Que d'ailleurs elle n'est plus contestée par Caillier non comparant
ni personne pour lui quoique dument appelé ; Par ces motifs = Le
Bureau Général jugeant en dernier ressort ; vu l'article quarante
un du décret du onze Juin mil huit cent neuf, portant règlement po
les Conseils de Prud'hommes ; Donne défaut conter Caillier
non comparant ni personne pour lui quoique dument appelé ;
Et adjugeant le profit du dit défaut ; Condamne Caillier à
payer avec intérêts suivant la loi à Gutzwiller la somme de
quatre francs cinquante centimes pour remboursement d'un coût d'un
carte de Bureau de Placement et indemnité de temps perdu ;
Le condamne en outre aux dépens taxés et liquidés envers le demandeur
à la somme de soixante cinq centimes, et à celle dûe au Trésor Public
pour le papier timbré de la présente minute, conformément à la loi du
sept août mil huit cent cinquante non compris le coût du 
présent jugement, la signification d'icelui et ses suites. Ainsi
jugé les jour mois et an que dessus.
Lecucq secrétaire
Du dit jour six Juin
Entre Monsieur et Madame Poitier, le sieur Poitier
tant en son nom personnel que pour assister et autoriser la
dame son épouse ouvrière brodeuse, demeurant  ensemble,
les dits époux, à Paris, rue Beauregard, numéro neuf
Demandeurs ; Comparant ; D'une part ; Et Monsieur Dieu
fabricant de broderies, demeurant et domicilié à Paris, rue
Turbigo, numéro cinquante trois ; Défendeur ; Défaillant ;
D'autre part ; Point de fait = Par lettres du secrétaire du
Conseil de Prud'hommes du Département de la Seine pour
l'industrie des Tissus en dates des Mardi vingt un et Vendredi
Vingt quatre Mai mil huit cent soixante dix huit les époux Poitier
firent citer Dieu à comparaître par devant ledit conseil de
Prud'hommes séant en Bureaux Particuliers les vendredi vingt
quatre et Mardi vingt huit Mai mil huit cent soixante
dix huit pour se concilier si faire se pouvait sur la demande
qu'ils entendaient former contre lui devant le dit conseil en paiement
de la somme de vingt quatre francs qu'il leur doit pour prix
de travaux de la dame Poitier. Dieu n'ayant pas comparu
la cause fut renvoyée devant le Bureau Général du conseil
séant le Jeudi six Juin mil huit cent soixante dix huit. Cité
pour le dit jour six Juin par lettre du secrétaire du conseil
en date du vingt neuf Mais mil huit cent soixante dix huit à 
la requête des époux Poitier Dieu ne comparut pas. A l'appel
de la cause les époux Poitier se présentèrent et conclurent à ce qu'il 
plut a Bureau Général du conseil donner défaut contre Dieu
non comparant ni personne pour lui quoique dument appelé et
pour le profit le condamner à leur payer avec intérêts suivant
la loi la somme de vingt quatre francs qu'il leur doit pour
prix de travaux de la dame Poirier, plus telle indemnité
qu'il plaira au conseil fixer pour perte de temps devant
les Bureaux du conseil et les dépens. Point de droit = Doit-on 
donner défaut contre Dieu non comparant ni personne pour 
lui quoique ce dument appelé et pour le profit adjuger aux
demandeurs les conclusions pour eux précèdemment prises ? Que
doit-il être statué à l'égard des dépens ? Après avoir entendu
les époux Poitier en leurs demandes et conclusions et en avoir
délibéré conformément à la loi ; Attendu que la demande
des époux Poitier parait juste et fondée ; Que d'ailleurs elle
n'est pas contestée par Dieu non comparant ni personne 
pour lui quoique dument appelé ; Attendu qu'en ne comparaissant
pas devant les Bureaux du conseil Dieu a causé aux époux
Poitier un préjudice de perte de temps, ce qu'il doit réparer ;
Pour ces motifs = Le Bureau Général jugeant en dernier ressort ;
Donne défaut contre Dieu non comparant ni personne pour lui
quoique dument appelé ; Attendu qu'en ne comparaissant pas
devant les Bureaux du conseil Dieu a causé aux époux Poitiers
un préjudice de perte de temps, ce qu'il doit réparer ; Par ces motifs =
Le Bureau Général jugeant en dernier ressort ; Donne défaut
contre Dieu non comparant ni personne pour lui quoique dument
appelé ; Et adjugeant le profit du dit défaut ; Condamne Dieu
à payer avec intérêts suivant la loi aux époux Poirier la somme
de vingt quatre francs qu'il leur doit pour prix de travaux de la
dame Poirier, plus une indemnité de quatre francs pour temps
perdu ; Le condamne en outre aux dépens taxés et liquidés
envers les demandeurs à la somme de un franc, et à celle dûe au
Trésor Public, pour le papier timbré de la présente minute,
conformément à la loi du sept août mil huit cent cinquante, ence,
non compris le coût du présent jugement, la signification d'icelui et
ses suites. Et vu les articles 437 du code de procèdure civile, 27
et 42 du décret du onze Juin 1804, pour signifier au défendeur le
présent jugement, commet champion, l'un de ses huissiers-
audienciers. Ainsi jugé les jour mois et an que dessus.
Lecucq secrétaire
Dudit jour six Juin —
Entre Monsieur Afchain, (Edouard) ouvrier passementier,
demeurant à Paris, rue des cendriers, numéro trente six ; Demandeur ; 
Comparant ; D'une part ; Et Monsieur Bailleux, fabricant
passementier, demeurant et domicilié à Paris, rue d'angoulême,
numéro cinquante ; Défendeur ; Défaillant ; D'autre part ; point
de fait = Par lettres du secrétaire du conseil de Prud'hommes du
Département de la Seine pour l'industrie des tissus en dates des
Mercredi vingt deux et vendredi vingt quatre Mai mil huit cent
soixante dix huit Afchain fit citer Bailleux à comparaître par devant
le dit conseil de Prud'hommes séant en Bureaux Particuliers les
Vendredi vingt quatre et Lundi vingt sept Mai mil huit cent
soixante dix huit pour se concilier si faire se pouvait sur la
demande qu'il entendait former contre lui devant le dit conseil en
paiement de la somme de cinquante huit francs cinquante centimes qu'il lui
doit pour salaire et en celle de vingt sept francs cinquante centimes
pour perte de temps par la faute Bailleux n'ayant pas comparu
la cause fut renvoyée devant le Bureau Général du conseil
séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour 
le dit jour six Juin par lettre du secrétaire du conseil en date du
trente un Mais mil huit cent soixante dix huit à la requête d'Afchain
Bailleux ne comparant pas. A l'appel de la cause Afchain se
présenta et conclut à ce qu'il plut au Bureau Général du conseil
donner défaut contre Bailleux non comparant ni personne pour
lui quoique dument appelé et pour le profit le condamne à lui
payer avec intérêts suivant la loi la somme de cinquante huit
francs cinquante centimes qu'il lui doit pour salaire et celle de
vingt sept francs cinquante centimes pour temps perdu et le condamner
aux dépens. Point de droit = Doit-on donner défaut contre Bailleux
non comparant ni personne pour lui quoique dument appelé et pour
le profit adjuger au demandeur les conclusions pour lui précedemment
prises ? Que doit-il être statué à l'égard des dépens ? Après
avoir entendu Afchain en ses demandes et conclusions et en
avoir délibéré conformément à la loi ; Attendu que la demande
d'Afchain parait juste et fondée ; Que d'ailleurs elle n'est pas
contestée par Bailleux non comparant ni personne pour lu
Quoique dument appelé ; Par ces motifs = Le Bureau Général
jugeant en dernier ressort ; vu l'article quarante un du décret
du onze Juin mil huit cent neuf, portant règlement pour les conseils
de Prud'hommes ; Donne défaut contre Bailleux non comparant ni 
personne pour lui quoique dument appelé ; Et adjugeant le
profit du dit dépens ; condamne Bailleux à payer avec intérêts
suivant la loi à Afchain la somme de cinquante huit francs
cinquante centimes qu'il lui doit pour salaire, puis celle de vingt
sept francs cinquante centimes pour temps perdu ; Le condamne
en outre aux dépens taxés et liquidés envers le demandeur à la somme
de un franc, et à celle dûe au Trésor Public, pour le papier timbré de
la présente minute, conformément à la loi du sept août mil huit
cent cinquante, ence, non compris le coût du présent jugement, la
signification d'icelui et ses suites. Et vu les articles 437 du code
de procèdure civile, 27 et 42 du décret du onze Juin 1809, pour
signifier au défendeur le présent jugement, commet champion,
l'un de ses huissiers audienciers. Ainsi jugé les jour mois et
an que dessus.
Lecucq secrétaire
Dudit jour six Juin
Entre Monsieur Zephir Dignoire, ouvrier passementier,
demeurant à Paris, Cité Popincourt, numéro six ; Demandeur ;
Comparant ; D'une part ; Et Monsieur Bailleux, fabricant
passementier, demeurant et domicilié à Paris, rue d'Angoulême,
numéro cinquante ; Défendeur ; Défaillant ; D'autre part ;
Point de fait = Par lettres du secrétaire du conseil de Prud'hommes
du Département de la seine pour l'industrie des tissus en dates des
Mercredi vingt deux et vendredi vingt quatre Mai mil huit cent
soixante dx huit Dignoire fit citer Bailleux à comparaître
par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers
les Vendredi vingt quatre et Lundi sept Mai mil huit cent
soixante dix huit pour se concilier si faire se pouvait sur la 
demande qu'il entendait former contre lui devant le dit conseil
en paiement de la somme de cinq cent soixante treize francs cinquante
centimes qu'il lui doit pour salaire. Bailleux n'ayant pas
comparu la cause fut renvoyée devant le Bureau Général du
Conseil séant le jeudi six Juin mil huit cent soixante dix huit.
Cité pour le dit jour six Juin par lettre du secrétaire du conseil
en date du vingt neuf Mai mil huit cent soixante dix huit à
la requête de Dignoire Bailleux ne comparut pas. A
l'appel de la cause Dignoire se présenta et conclut à  ce qu'il plut
au Bureau Général du conseil donner défaut contre Bailleux
non comparant ni personne pour lui quoique dument appelé et
pour le profit le condamner à lui payer avec intérêts suivant
la loi la somme de cinq cent soixante treize francs cinquante centimes
qu'il lui doit pour salaire, plus, telle indemnité qu'il plaira
au Conseil fixer pour perte de temps devant les Bureaux du
Conseil et les dépens, ordonner l'exécution provisoire du payement
à intervenir, nonobstant appel et sans qu'il soit besoin par lui
de fournir caution, conformément à l'article quatorze de la loi
du premier Juin mil huit cent cinquante trois. Point de droit =
Doit-on donner défaut contre Bailleux non comparant ni 
personne pour lui quoique dument appelé et pour le profit
adjuger au demandeur les conclusions par lui précèdemment
prises ? Que doit-il être statué à l'égard des dépens ? Après
avoir entendu Dignoire en ses demandes et Conclusions et
en avoir délibéré conformément à la loi ; Attendu que les demandes
de Dignoire paraissent justes et fondées ; Que d'ailleurs elles
ne sont pas contestée par Bailleux non comparant ni \trsonne pour
lui quoique dument appelé ; Attendu qu'en ne comparaissant pas
devant les Bureaux du conseil Bailleux a causé à Dignoire un
préjudice de perte de temps, ce qu'il doit réparer ; Par ces 
motifs = Le Bureau Général jugeant en premier ressort ; vu
l'article quarante un du décret du onze Juin mil huit cent neuf,
portant règlement pour les Conseils de Prud'hommes ; Donne
défaut contre Bailleux non comparant ni personne pour lui
quoique dument appelé ; Et adjugeant le profit du dit défaut ;
Condamne Bailleux à payer avec intérêts suivant la loi à 
Dignoire la somme de Cinq cent soixante treize francs cinquante
centimes qu'il lui doit pour salaire, plus une indemnité de
six francs pour temps perdu ; Le condamne en outre aux dépens
taxés et liquidés envers le demandeur à la somme de un franc, et à
celle dûe au Trésor Public, pour la papier timbré de la présente
minute, conformément à la loi du sept août mil huit cent cinquante ;
ence, non compris le coût du présent jugement, la signification
d'icelui et ses suites ; Ordonne l'exécution provisoire du présent
jugement, nonobstant appel et sans qu'il soit besoin par le
demandeur de fournir caution, conformément à l'article quatorze
du premier Juin mil huit cent cinquante trois. Et vu les articles
437 du code de procèdure civile, 27 et 42 du décret du onze Juin
mil huit cent neuf, pour signifier au défendeur le présent jugement
commet Champion, l'un de ses huissiers audienciers. Ainsi jugé
les jour mois et an que dessus.
Lecucq secrétaire
Dudit jour six Juin —
Entre Monsieur Jules Omnès, ouvrier implanteur,
demeurant à Paris, rue Quicampoix, numéro quatre ;
Demandeur ; comparant ; D'une part ; Et Monsieur
Pagès, Maître coiffeur, demeurant et domicilié à Paris rue
de Rennes, numéro soixante cinq ; Défendeur ; Défaillant ;
D'autre part ; Point de fait = Par lettres du secrétaire du conseil
de Prud'hommes du Département de la seine pour l'industrie
des tissus en dates des Mercredi vingt deux et vendredi vingt
quatre Mai mil huit cent soixante dix huit Omnès fit citer
Pagès à comparaître par devant le dit conseil de Prud'hommes séant
en Bureaux Particuliers les Vendredi vingt quatre et Lundi vingt sept
Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait
sur la demande qu'il entendait former contre lui devant le dit
Conseil en paiement de la somme de six francs cinquante centimes
qu'il lui doit pour salaire. Pagès n'ayant pas comparu la
cause fut renvoyée devant le Bureau Général du conseil séant
le Jeudi six Juin mil huit cent soixante dix huit. Cité pour
le dit jour six Juin par lettre du secrétaire du conseil en
date du vingt neuf Mai mil huit cent soixante dix huit à la
requête de Omnès Pagès ne comparut pas. A l'appel de
la cause Omnès se présenta et conclut à ce qu'il plut au
Bureau Général du conseil donner défaut contre Pagès non
Comparant ni personne pour lui quoique dument appelé pour
le profit le condamner à lui payer avec intérêts suivant la loi
la somme de six francs cinquante centimes qu'il lui doit pour
salaire, plus, telle indemnité qu'il plaira au conseil fixer
pour perte de temps devant les Bureaux du conseil et les dépens.
Point de droit = Doit-on donner défaut contre Pagès non
comparant ni personne pour lui quoique dument appelé et pour
le profit adjuger au demandeur les conclusions pour lui
précèdemment prises ? Que doit-il être statué à l'égard des
dépens ? Après avoir entendu Omnès en ses demandes et
Conclusions et en avoir délibéré conformément à la loi ; Attendu
que la demande d'Omnès parait juste et fondée ; Que
d'ailleurs elle n'est pas contestée par Pagès non comparant ni
personne pour lui quoique dument appelé ; Attendu qu'en
ne comparaissant pas devant les Bureaux du Conseil Pagès a
causé à Omnès en préjudice de perte de temps, ce qu'il doit
réparer ; Par ces motifs = Le Bureau Général jugeant en
dernier ressort ; Vu l'article quarante du décret du onze
Juin mil huit cent neuf, portant règlement pour les conseils de
Prud'hommes ; Donne défaut contre Pagès non comparant 
ni personne pour lui quoique dument appelé ; Et adjugeant le
profit du dit défaut ; Condamne Pagès à payer avec intérêts suivant
la loi à Omnès la somme de six francs cinquante Centimes
qu'il lui doit pour salaire, plus une indemnité de six francs pour
temps perdu ; Le condamne en outre aux dépens taxés et liquidés
envers le demandeur à la somme de un franc, et à celle dûe
un Trésor Public, pour le papier timbré de la présente minute,
Conformément à la loi du sept août mil cent cinquante, ence
non compris le coût du présent jugement, la signification d'icelui
et ses suites. Et vu les articles quatre cent trente cinq du
code de procèdure civile, vingt sept et quarante deux du
décret du onze Juin mil huit cent neuf, portant réglement
vu d'allemagne
numéro cent quarante
pour les conseils pour signifier au défendeur le présent jugement,
Commet Champion, l'un de ses huissiers audienciers. Ainsi
jugé les jour mois et an que dessus
Lecucq secrétaire
— Dudit jour six Juin
Entre Monsieur Jean Baptiste Richard, demeurant à
rue de Flandre, 28 à Paris\tagissant au nom et
comme administrateur de la personne et des biens de son fils
mineur Victor, ouvrier en apprêts sur Etoffes ; Demandeur ; 
Comparant ; D'une part ; Et Monsieur Lecrique et
Compagnie, Maîtres appréteurs d'Etoffes, demeurant et
domiciliés à Paris, quarante trois rue Riquet et rue de
Tanger ; Défendeurs ; Défaillant ; D'autre part ;
Point de fait = Par lettres du secrétaire du conseil de Prud'hommes
du Département de la seine pour l'industrie des tissus en dates
des Mercredi vingt deux et Vendredi vingt quatre Mai mil
huit cent soixante dix huit Richard, ès noms et qualités qu'il agit,
fit citer Lecrique et Compagnie à comparaître par devant le dit
Conseil de Prud'hommes séant en Bureaux Particuliers les _
Vendredi vingt quatre et Lundi vingt sept Mai mil huit cent soixante
dix huit pour se concilier si faire se pouvait sur la demande qu'il 
entendait former contre eux devant le dit conseil en paiement de la
somme de sept francs qu'il lui doit pour prix de travaux de son
fils Victor. Lecrique et compagnie n'ayant pas comparu la
cause fut renvoyée devant le Bureau Général du conseil séant
le Jeudi six Juin mil huit cent soixante dix huit. Cités pour le 
dit jour six Juin par lettre du secrétaire du conseil en date
du vingt Neuf Mai mil huit cent soixante dix huit à la requête de
Richard Lecrique et compagnie ne comparurent pas. A l'appel 
de la cause Richard se présenta et conclut à ce qu'il plut
au Bureau Général du conseil donner défaut contre Lecrique et cie
non comparant ni personne pour eux quoique dument appelés et
pour le profit les condamner solidairement à lui payer avec
intérêts suivant la loi la somme de sept francs qu'ils lui
doivent pour salaire de son fils victor et les condamner
aux dépens. Point de droit = Doit-on donner défaut
contre Lecrique et compagnie non comparants ni personne pour
eux quoique dument appelés et pour le profit adjuger au 
demandeur les conclusions par lui précèdemment prises ?
Que doit-il être statué à l'égard des dépens ? Après avoir 
entendu Richard en ses demandes et conclusions et en avoir
délibéré conformément à la loi ; Attendu que la demande de
Richard parait juste et fondée ; Que d'ailleurs elle n'est pas
contestée par Lecrique et compagnie non comparant ni 
personne pour eux quoique dument appelés ; Attendu qu'en 
ne comparaissant pas devant les Bureaux du Conseil Lecrique
et Compagnie ont causé à Richard un préjudice de perte de
temps, ce qu'ils doivent réparer ; Par ces motifs = Le Bureau
Général jugeant en dernier ressort ; Vu l'article quarante un du
décret du onze Juin mil huit cent neuf, portant réglement pour les
conseil de Prud'hommes ; Donne défaut contre Lecrique et
Compagnie non comparants ni personne pour eux quoique
dument appelés ; Et adjugeant le profit du dit défaut ;
Condamne Lecrique et Compagnie à payer avec intérêts suiva
la loi à Richard la somme de sept francs qu'il lui doit pour
salaire de son fils victor, plus une indemnité de six francs pour
temps perdu ; Les condamne en outre aux dépens taxés et liquidés
envers le demandeur à la somme de un franc, et à celle dûe au
Trésor Public, pour le papier timbré de la présente minute,
conformément à la loi du sept août mil huit cent cinquante,
ence, non compris le coût du présent jugement, la signification 
d'icelui et ses suites ; Et vu les articles 435 du code de procédure
civile, 27 et 42 du décret du onze Juin 1809, pour signifier au 
défendeur le présent jugement, Commet Champion, l'un de
ses huissiers audienciers. Ainsi jugé les jour mois et an que 
dessus
Lecucq secrétaire
— Dudit jour six Juin —
Entre Monsieur Bréchot, ouvrier Galochier, demeurant 
à Paris, rue de Meaux, numéro trente sept, passage de la 
Brie ; Demandeur ; Comparant ; D'une part ; Et Monsieur
Vermérot, fabricant de Galoches, demeurant et domicilié à Paris
deux mots rayés nuls
Lecucq
— Dudit jour six Juin
Entre Monsieur — Emile Pilloy, ouvrier
Bourrelier, demeurant à Paris, rue de Cotte, numéro
seize ; Demandeur ; Comparant ; D'une part ; Et 
Monsieur Prevost père, Maître Bourrelier, demeurant
et domicilié à Paris, Montreuil sous Bois, près Paris,
rue du Pré, numéro trois ; Défendeur ; Défaillant ; 
D'autre part ; Point de fait = Par lettres du secretaire du
Conseil de Prud'hommes du Département de la seine pour 
l'Industrie des Tissus en dates des Mercredi vingt deux et
Vendredi vingt quatre Mai mil huit cent soixante dix huit
Pilloy fit citer Prevost à comparaître par devant le dit Conseil
de Prud'hommes séant en Bureaux Particuliers les Vendredi
Vingt quatre et Lundi vingt sept Mai mil huit cent soixante
dix huit pour se concilier si faire se pouvait sur la demande
qu'il entendait former contre lui devant le dit Conseil en
paiement de la somme de Cinquante sept francs se composant
de trente neuf francs pour prix de soixante dix huit
heures à cinquante centimes et de dix huit francs pour travaux
à la tache. Prevost n'ayant pas comparu la cause fut renvoyée
devant le Bureau Général du conseil séant le Jeudi six Juin mil
huit cent soixante dix huit. Cité pour le dit jour six Juin par
lettre du secrétaire du conseil en date du vingt neuf Mai mil huit 
Cent soixante dix huit à la requête de Pilloy Prevost ne comparut
pas. A l'appel de la cause Pilloy se présenta et conclut à ce qu'il 
plut au Bureau Général du conseil donner défaut contre Prevost
non comparant ni personne pour lui quoique dûment appelé et pour
le profit le condamner à lui payer avec intérêts suivant la loi la
somme de cinquante sept francs qu'il lui doit pour salaire, plus, telle
indemnité qu'il plaira au conseil fixer pour perte de temps devant les
Bureaux du conseil et les dépens. Point de droit = Doit-on donner
défaut contre Prevost non comparant ni personne pour lui quoique
dument appelé et pour le profit adjuger au demandeur les
conclusions par lui précèdemment prises ? Que doit-il être
statué à l'égard des dépens ? Après avoir entendu Pilloy en 
ses demandes et conclusions et en avoir délibéré conformément 
à la loi ; Attendu que les demandes de Pilloy paraissent justes
et fondées ; Que d'ailleurs elles ne sont pas contestées par Prévost
non comparant ni personne pour lui quoique dument appelé ;
Attendu qu'en ne comparaissant pas devant les Bureaux du 
Conseil Prevost a causé à Pilloy un préjudice de perte de
temps, ce qu'il doit réparer ; Sur ces motifs = Le Bureau
Général jugeant en dernier ressort ; vu l'article quarante un
du décret du onze Juin mil huit cent neuf, portant règlement
pour les conseils de Prud'hommes ; \tDonne défaut contre
Prévost non comparant ni personne pour lui quoique dument
appelé ; Et adjugeant le profit du dit défaut ; Condamne
Prévost à payer avec intérêts suivant la loi à Pilloy la somme
de cinquante sept francs qu'il lui doit pour salaire, plus une
indemnité de six francs pour temps perdu ; Le condamne en 
outre aux dépens taxés et liquidés envers le demandeur à la somme
de un franc, et à celle dûe au Trésor Public pour le prix
timbré de la présente minute, conformément à la loi du sept août mil
huit cent cinquante, ence, non compris le coût du présent jugement
la signification d'icelui et ses suites. Et vu les articles 435 du
Code de procèdure civile, 27 et 42 du décret du onze Juin 1809,
pour signifier au défendeur le présent jugement, Commet Champion,
l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an 
que dessus.
Lecucq secrétaire
— Dudit jour six Juin —
Entre Madame veuve Bernard, ouvrière demeurant
à Joinville le Pont, près Paris, rue du canal, numéro onze ;
Demanderesse ; Comparant ; D'une part ; Et Monsieur
Bardin, fabricant plumassier, demeurant et domicilié à 
Joinville le Pont, rue des Réservoirs ; Défendeur ; Comparant ; 
D'une part ; Point de fait = Par lettres du secrétaire du conseil
de Prud'hommes du Département de la seine pour l'industrie des
Tissus en dates des mardi vingt huit et Vendredi trente un Mai
mil huit cent soixante dix huit la veuve Bernard fit citer Bardin
comparaître par devant le dit conseil de Prud'hommes séant en 
Bureaux Particuliers les Vendredi trente un Mai et Lundi trois Juin
mil huit cent soixante dix huit pour se concilier si faire se pouvait
sur la demande qu'elle entendait former contre lui devant le dit
Conseil en paiement de la somme de vingt deux francs trente centimes
qu'il lui doit pour salaire. Bardin n'ayant pas comparut la cause
fut renvoyée devant le Bureau Général du conseil séant le Jeudi six
Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin
par lettre du secrétaire du conseil en date du trois Juin mil
huit cent soixante dix huit à la requête de la veuve Bernard
Bardin comparut. A l'appel de la cause la veuve Bernard se 
présenta et conclut à ce qu'il plut au Bureau Général du conseil
condamner à lui payer avec intérêts suivant la loi la somme de
vingt deux francs trente centimes qu'il lui doit pour salaire,
plus, telle indemnité qu'il plaira au conseil fixer pour perte de
temps devant les Bureaux du conseil et les dépens. Point de droits
De son coté Bardin se présenta et conclut à ce qu'il plut au Bureau
Général du conseil attendu que la veuve Bernard a quitté ses
ateliers sans faire la huitaine d'usage ; Que d'ailleurs le travail
dont elle reclame le prix ayant été par elle mal exécuté il ne lui
a doit pas la façon ; Par ces motifs = dire la veuve Bernard
non recevable en ses demandes, l'en débouter et la condamner
aux dépens. Point de droit = Doit-on condamner Bazin à payer
à la veuve Bernard la somme de vingt deux francs trente centimes
pour travaux de son état ? Ou bien doit on pour les raisons invoquées
par Bardin dire la veuve Bernard non recevable en sa demande,
l'en débouter ? Que doit-il être statué à l'égard des dépens ?
Après avoir entendu les parties en leurs demandes et conclusions
respectivement et en avoir délibéré conformément à la loi ; Attendu
qu'il est constant que Bardin doit à la veuve Bernard la
somme de vingt deux francs trente centimes pour travaux exécutés
à la tache ; Que Bardin qui allègue de mal façon de ces
travaux ne prouve pas et déclare ne plus pouvoir prouver ce
qu'il avance ; Attendu en ce qui concerne le départ subit de
la veuve Bernard des ateliers de Bardin que ce départ
remonte à quinze jours sans que Bardin ait, depuis, fait la
moindre démarche pour retenir ou faire rentrer son ouvrière,
ce qui est considéré comme un consentement au moins tacite
à son départ ; Attendu aussi que Bardin n'a pas comparu
devant les Bureaux Particuliers et a par cette non comparution
fait perdre du temps à la demanderesse qui en éprouve un préjudice
dont il lui doit réparation par la somme de quatre francs ; Par
ces motifs = Le Bureau Général jugeant en dernier ressort ;
condamne Bardin à payer à la veuve Bernard la somme 
de vingt deux francs trente centimes qu'il lui doit pour salaire,
plus une indemnité de quatre francs pour temps perdu ; Le
condamne en outre aux dépens taxés et liquidés envers la demanderesse
à la somme de un franc vingt centimes, et à celle dûe au Trésor
Public pour le papier timbré de la présente minute, conformément
à la loi du sept août mil huit cent cinquante, ence, non compris le
vingt neuf mots rayés
Comme nuls
Lecucq
coût du présent jugement, la signification d'icelui et ses suites.
Ainsi jugé les jour mois et an que dessus.
Lecucq secrétaire
— Dudit jour six Juin —
Entre Monsieur Jean Lardet, ouvrier tisseur, demeurant
à Gentilly près Paris, rue de la Comète, numéro cinq ;
Demandeur ; Comparant ; D'une part ; Et Monsieur Bardin
fabricant de tissus en plumes, demeurant et domicilié à Paris,
Boulevart Saint Jacques, numéro cinquante un ; Défendeur ; 
Comparant ; D'autre part ; Point de fait = Par lettres du secrétaire
du Conseil de Prud'hommes du Département de la seine pour
l'industrie des tissus en dates des Lundi treize et Mardi quatorze
Mai mil huit cent soixante dix huit Lardet fit citer Bardin à 
comparaître par devant le dit conseil de Prud'hommes séant en 
Bureaux Particuliers les Mardi quatorze et Vendredi dix sept Mai mil
huit cent soixante dix huit pour se concilier si faire se pouvait sur
la demande qu'il entendait former contre lui devant le dit conseil en
paiement de primo, cinquante francs pour le salaire de cinq journé
pour lui f dix journées par lui lui faites à raison de cinq francs par
jour ; Secondo, de vingt deux francs cinquante centimes pour
neuf journées à deux francs cinquante centimes du travail de la dame
Lardet, sa femme ; Tertio, de celle de sept cents francs restant dûs
sur le prix d'une machine par lui faite sur ses ordres. Bardin
n'ayant pas comparu la cause fut renvoyée devant le Bureau Général
du conseil séant le Jeudi six Juin mil huit cent soixante dix huit
Cité pour le dit jour six Juin par lettre du secrétaire du conseil en
vingt trois Mai mil huit cent soixante dix huit et ajournée au Jeudi
six Juin suivant mois. Suivant exploit de Champion, huissier
à Paris, en date du vingt sept Mai mil huit cent soixante dix huit,
visé pour timbre et Enregistré à Paris, le vingt huit Mai mil huit
Cent soixante dix huit, débet deux francs quinze centimes, signé de feuillez
Lardin fit citer Bardin à comparaître par devant le dit Conseil de
Prud'hommes séant en Bureau Général pour le Jeudi six Juin mil huit
cent soixante dix huit pour s'entendre condamner à lui payer la somme
de huit cent soixante douze francs cinquante centimes se composant
 
de primo, de cent cinquante francs pour indemnité d'un mois de 
congé, secondo vingt deux francs cinquate centimes pour solde de 
journies de la dame Tarset,et de set cents frocs pour prix 
Et d'une mécunque qui lui a été forrce par lu. A l’appel de la case 
partet se présenta et conclut à ce qu’il plut au Bureau Général 
du Conseil lui adjuger le bénifie des conclusions par lui préses dont 
la citalion exploit de chanmpion, lui près, enregistré et leondamner 
Bardin aux dépens. De son coté Bardin se présenta et conclut à ce 
qu’il plut au Bureau Général du Conseil attendu qu’il n’a pas enfagé 
Lerder comme ouvrier ; Que s’il et veuu chez lui le st pour y 
travailler pour son compte à lui Berdin, mois bien pour son profe compte 
t à une mécuviue qu’il lui a vendue à contition qu’elle fontinse 
farilement a jec l’arde d’une somme, le qui expligéé commet la dame 
tordes entre chez lui aménée par son mari qui prétendaint 
l’employer à cette machine, perte qu'elle duz abondonner le 
mecanique ne pouvant fonctiner ; attendu ependanit qu à 
offre payer les vingt deux francs cinquante centimes prodit des 
neuf journéen de la dine portis oure que celte lore n'oil 
uis prodtuit ; Par ces motifs - Dire lomn ore vrdit necerlle 
en ses demandes, l'en débouter et le condamner ux dépens. Pint de 
troit - Dait-on condamner trédin à payer à erdit la 
somme de huit cent soirante douze frons cinquate centimes par 
de la congé d'un mois de torrtet talaire de la dame verder 
de pretseue ongra parée d’a du et n le 
que lis ouidiet pur et le e l ue le dtie 
Lertit non recevableon ses demandes l'endébouter e que 
de sus te se dui l un deis pus purte le 
parties en leurs demandes et conclusions respectivement et en avoir 
et de d pre ille Mrede en in 
atis ant dene pr e du deu 
et peur en de e sand eur 
eu e e de du et en tortie 
er e e ie purs ser 
hu tit en e e e a e orer 
ote e oni e aten u elest 
prs sn dt lite d e e pusils 
aeden omi en ent les 
ei e e e pe e 
con es les u an e ur de se ete 
e te dement deprei teuip r 
et e deds pue an ten pete. 
pu de u e te de se pu lese  
qi 
 
e 
 
E 
dé 
 
a 
1835
des vngt deux francs cinquante centimes pour le sataire de 
la dame Pardr le Conseil n’a qu’à lui en donner aiteor 
de déclarer en compéteis pour le surplus ; Par ces motifs 
Le Bureau Général jugeant en premier ressort; donnea 
à Berstin de l’offre qu’il fait de payer à la jourre du 
conseil les vingt deux francs cinquante centimes qui lui da 
réclamés pour salaire de la dame Tardet. Le véutay 
on compétent pour coumaitre des entrer demandes de tavail 
qu'il reuvoie devant qui de troit et condamne lar dit u 
 dépens taxés et liquidés enven le Trésor Public, à la soe 
de deix francs quinze centimes pour le papier timbré 
l’enregistrement de la citation, conformément à la loi à 
sept aoux mil huit cent cinquante euce, non compris le cout 
du présent jugement, la signification d’icelui et ses suites. 
Ainsi jugé les jour mois et an que dessus. 
Dineu 
recucg létaire 
àQudit jour dix ving a 
Entre Mademoiselle Delasague, fille majeure, ouvrière 
demeurant à Paris, rue Sant Blonoré, numéro deux cens 
cinquante sept ; Demanderesse ; Comparant ; D’une par ; 
t Monsieur Chaput, Cutotiier, demeurant et domiée 
à Paris, rue de vanmes, numéro six ; Défendeur ; Défelle 
Dutre part ; Poit de fat - las tetres du peretire de linre 
de Prud’hommes du Département de la soine pour l’industrier 
des tissus en dates des vendredi trente un Mai et lundi treis 
Juin mil huit cent soixante dix huit la demoiselle Wéleve 
fit citer chaput à comparaître par devant le dit Conseil  
Prud’hommes sant en Bureont Particuliers les lundi trois et 
Mardi quatre juin mil huit, cest soixante dix fait pour se conctamner 
si fairre se pouvait sur la demande qu'elle se convais frrer
contre lui devant le dit Conseil en paiement de la somme de Eix 
deux frans cinquante centimes qu’il lui doit pour salaré ae 
chapit l'ayar pas comparala cause pt renvogyée les oise 
Bureau Général du Conseil séant le jeudi six Juin milhi 
comparante dix huit. Cité par l dit jour li, pui fil ene 
MA 
uts 
 
 
 
secrétaire du Conseil en date du quatre juin mil huit cent soixante 
dix huit à la réquite de la demoiselle Delevaique chapat ne 
comparut pas. A l'appel de la cause la demoiselle delevarque 
le présenta et conclut à ce qu'il plut au Bureau Général du Conseil 
donner défut contre chapert non comparant ni personne 
pour lui quoique dument appelé et pour le profit le condamner 
à lui payer avec intérêts suivant la loi la somme de trente deux 
francs cinquante centimes qu’il lui doit pour salaire plus telte 
indemnité qu’il plaira au Conseil fixer pour perte de temps 
devant les Bureux du Conseil et les dépens. Pont de droit — 
Dait-on donner défaut contre chapot non comparant en 
personne pour lui quoique dument appelé et pour le profit 
adjuger à la demanderelse les conclusion par elle précédemment 
prises ? Que doit il être statué à l’égard des dépens ? 
près avoir entendu la demoiselle Delevaique nss demandes 
et conclusions et en avoir délibéré conformément à la loi; 
Attendu que la demande de la demoiselle Dele vaique paroint 
juste et fondée ; Que d'ailleurs elle n'est pas contestée par 
Chapnt non comparant ni personne pour lu quoique dument 
appelé ; attendu qu’en ne comparuisant pas devait les
Bureaux du Conseil chapat a causé à la demoiselle de le vaique 
un préjudice de perte de temps, ce qu'il doit éprur ; Par 
de pentipe. le Brnveu prat l jugen en nere ps 
ou larticle quarante un du decres du onze Jui mil huit cent
el p re en eu te e réseu 
donne défaut contre Chapat non comparant ni personne 
pour lui quoique dument appelé ; Et adjugeant le profit du dit 
défaut ; condamne Chapat à payer avec intérêts suivant 
e  et uidte nonean pen t e sei 
aun en ant le e e t u ten du 
pu deit tr n e poir en iq u lu, le 
da deme t en de e ere 
du de te u e per de tue 
nir t u ie s pen 
p pere lele eseq ei q sar de onier 
uie u e e de paser 
e e e de onte et preur 
ante te u uig cin eit 
nde er ani e purs de pur 
eu ait lapre nt endes tis p l jur mit ie 
risill
an que dessus. 
H
demait-entéra 
A
 
 Audit jour dix Juin 
Entre Monsieur Parcors, ouvrier tailleur, demeurant à Cave 
rue courne fort , numéro quarante trois ; Demandeur ; Comparant 
D'’une part ; Et Monsieur Aingélà Maitre tailleur s hatit 
demeurant et domicilié à Paris, Boulevart saint Martie 
numéro quatre ; Défendeur ; Comparant ; D'autre paryr e 
Point de fait - Par lettres du secrétaire du Conseil de Prud’hommes 
du Département de la Seine pour l’industrie des tissus en dates deu 
Mardi quatorze et Vendredi dit sept Mai mil huit cent 
soixante dix huit siçon fit citer Engele à comparaître 
par devant le dit conseil de Prud’homme séant en Bureux Particu 
les vendredi dix sept et Vendredi vingt quatre eMai mil suit 
cent soixante dix huit pour se concilier si faire se pouvait pur 
la demande qu'il entendait former contre lui devant le dit Conseil 
en paiement de la somme de dix huit francs pour la façon d’iune 
vêtement. Sengéli n'ayant pas comparu la cause fut renvayée 
devant le Bureau Général du Conseil séant le jeudi six Juin mil 
huit cent soixante dix huit. Cité pour le dit jour six juin per 
lettre du secrétaire du Conseil en date du vingt cinq mai mil 
huit cent soixante dix huit à la réguate de Fiçon Angeti 
comparut. A l’appel de la cause, ficois se présenta et 
conclut à ce qu’il plut au Bureau Général du Conseil dun déde 
Angéti à lui payer avec intérêts suivant la loi la somme de 
dix hui francs qu’il lui doit pour salaire et le condamner ax 
dépens. De bon coté Angéli se présenta et conclut à ce qu'il 
plut au Bureau Général du conseil attendu que le vêtement 
du quel Lricois réclame le prix a été nil fait, ; quil etre 
d'ailleurs entre les moins pour le rataucher ; Par ces moff 
due Ricon non recevable en sa demande, le recevait 
reconventionnellement demandeur dire que lorsque le dit 
Bêtemenit sera rapporté rectifie le prix en ser payé  
condamner firois aux dépens. Pnt de droit — Doit on 
condamner cingélui à payer à Perrois la somme de doprns 
francs pour la façon d'une peerdessns. Ouben dutecn 
dire Ficon non recevable en sa demande, l'en débouter 
recevoir cnge li reconventionnellement demandent ; dire que 
préctableneunt au paiement du prix de dix huit francs deuit, 
sera tenu de rectifier le Baletot par de ssus qu'il a sitré  
mains ? Que doit-il être statué à l’épard des dépens 
Après avoir entendu les parties en leurs demandes et conclusions 
respectivement et en avoir délibéré conformément à la loi eu 
attendu qu'il résulte des explications farnies par lu 
oins 
a
 
ud  
i 
s 
ité
di 
l du 
 
 
r 
 
partiec que le par de pas qui fait l'objet du titnge a été trendu 
fait par Bicoin le vingt deux avril dernier ; que sil est ce jour 
entre les moins de Seiçon s’ess qu’angeta la loi à renvoyé 
àprir plus de trois semaines ; que la raison invaguée par 
Angelé qui aurait pu être humise à examen dans un de lai 
apportie ne pout plus l'être aujourd’hui ; Pir ces motifs 
Le Bureau Général jugeant en dernier retsort ; Condamne 
ingeli à le precdre pasression du par dessus et à en payer 
le prix defaçon par dix huit francs, reçoit angere dans 
la demande reconventionnelle, l'en déboute ; Le condamne 
aux dépens taxés et liquidés envers le demandeur à la somme de 
ufronc et à celle dui au Trésor Public, pour le papier 
timbré de la présente minute, coanformément à la loi du sept 
Août mil huit cent cinquante, ence, non compris le cout du 
présent jugement, la signification d’icelui et ses suites 
Ainsi jugé les jour mois et an que dessus. 
ecusq secrétaire 
E mou e 
Leonq 
dudit jour six cui 
Eintre apiy courdait Cinsiq aune epiet 
du ete eri ui ee lete mon ls 
Demandeur ; Comparant ; D’une part ; L Monpeur
iévred trdite. Mite hep e dement e 
quidte doui co lei de ete nte pasé 
ux, défard coupet de pasint 
a pe e e e e te or 
ae de t e ende eitese 
dur e renei de repue 
e pre a pus auet les 
e et ur e te eleure 
eu te e e poretr 
a e en e purs ite 
de u t e deen e 
po de p ren de e tes 
due e en emret 
en an qur e ci pen de 
daemar u i 
 
 
à 
 L
 
s  
 
 
LPr
 
huit. Cité pour le dit jour dix juin par lettre du secrétaire 
du Conseil en date du vingt cinq mai mil huit cent soixante 
dix huit à préqute de hattrot cr comparut.  
l’appel de la cause Talleir, se présenta et conclut à ce qudie 
plut au Bureau Général du Conseil condamner lrcord à laie 
payer avec intérêts suivant la loi la somme de vingt francon 
réparation du dommage qu’il lui cansé en nel soumpart pour 
le douze Mai dernier ann qu’il ene avait, pris l’engugement 
et le condamner aux dépens. De son coté Brard se présenter 
et conclut à ce qu'il plut au Bureau Général du Conseil elle 
qu’il est d’usage qu'un Carron adrepé par Bureau se 
Placement pour faire ertra le Demonche porte sa cort la 
vendredi avant le mout ; Que fralliot que prétendque la 
carte lui aété de livrée trop tors le vendredi pour qu’il 
lu partet le même jour anrait, pu tant au mans le porter 
le samedi matin le bonteur, ce qui lui en lévite de faire sa 
causse de chez lui au Bureau se placement ; Que n’était 
venu le samedi qu'a près nde et aboors qu'il en avoir demand 
un autre il sne fut l'ocuper ; Par ces motifs — Dire paltier 
non recevable en sa demande, l'en débouter comme mal foncéi 
elle et  condamner ax dépens. Pont de trait — Doit -on 
condamner Ernér à payer à Gaticot la somme de vingt frencs poar 
Eademité de non éxéccution de convention verbale de travail 
Ou bien doit-on dire pultiot non recevable en sa demande, l'un 
débouter ? Que doit il être statué à l’égard des dépens . Lprés d 
avoir entendu les parties en leurs demandes et conclusions respectivement 
et en avoir délibéré conformément à la loi ; attendu qu’il est 
d'usage constant que l'ouvrier envoyé par le Bureau de placement 
poir faire l’attre du Demonire porte la cost de dit Bureau 
de vendredi avant cinq heures ; attendu qu'il est constant que 
aaltcct ou dit -l n’a pu se présenter au domicile ? mois 
le vendredi le Bureau de Clacement lui ayant donné la tarte trope ; 
Pard s'esx présenté le samedi aper tors pour quatifier l’inqudte 
du défendeur et le précantion qu’il a prise de seproier n eemet 
garan ; Qu'on ce cas pellait n'it pas foire dans sa demande 
en indemnité ; Pur ces motifs - Le Bureau Général jugeant en dernir 
lessart ; Et galtrit non recevable en sa demandes l’en detré 
Le condamne ex dépens envers le Trésor Public pour le papier timbrée 
de la présente minute, conformément à la loi du segt du oir lu  
cent cinquante, once, non compris le cout du présent jugement ; qa 
sigification d’icelu et ses sutes. Cin vuit les jourmis euf laiceu 
Lecuiq secrétaire 
nmces 
M
 
Du dit jour dix Plinau 
Entré onsieur jandron fabricant chemisier demeurant à 
Paris, rue du quatte septomtre, numéro quinze Défendeur 
ou primipal ; Demandeur epposant ; Défaillant ; D’une 
part ; Le Monseur Wébot cupeur chemisier, demeurant 
à Pasis, ue Villedo, numéro cinq Demandeur  pronpal; 
Défendeur opposent ; comparant ; D'autre part ; cont 
de fait — Pur jugement Lendu par défaut par le conseil se 
Prud’hommes du Département de la Seine pour l’industrie des 
tissus le onze avril mil huit cent soixante dix huit, enregistré, 
Lunaron a été condamne à payer à Péot la somme de sept cent 
quatre vingt cinq trongs de pemitul et les accelure . Enqunte 
fit signifie à lundron par exploit de cnempron, lui prer a 
Ruris, en date du huit Mai mil huit cent soixante, dix tuit, 'agate 
à la réguéte de litetà Suivant exploit de Blnand comprar 
à Paris, en date du dix Mai mil huit cent paranti dix huit, 
vigé pour timbre et enregistré à Paris, le ouze Mai mil huit cent 
sorixmante dix huit. débet deux francs quinze centimes, signé 
de Pouiller, landion formé opposation à ce jugement et tta 
Citel à comparaitre par devant le dit Conseil de Prud’homes 
sige louveiliedt le ut su du e urecln d 
dix huit pour la voir recevoir opposent à c jugement, lvour 
dé charger des condamnetions contre lui prenuyé en peisnper 
et ruce frrires, s'entendre, le dit Pétel, délareer non recevablle 
uteant er e e lu es on eps 
p du it e tes dei lt e le e e demenra se 
dmenvare du ete e er it 
e e e det t e d e ine 
cdu pret et erdlit e lun e ue 
ru e er te pe ele t oesin 
auprse r e te l e u t eie 
ae ren ter e d es u erete 
enden e e e q ir 
e rei que eme de ense 
ae e e de ent lu it ue 
en p de e de tente quee 
e e e e e on e oaire 
du d prte  e es ue 
oui l  u domnaps pour 
puti ar de p sree et 
et e en a e le ue 
de s ui de de e preer 
dl
s 
 
MI
q
 
entendu Rotaf en ses demandes et conclusions et en avoir délibéré 
conformément à la loi ; Attendu que les demandes de setet 
paraissent justes et fondées ; Que d'ailleurs elles ne sont 
pas contestées par landon non comparant ni personne 
pour lui quoique demendeur opposat ; Attendu que cette 
non comparation dit été consilérée comme on atandans 
l’oppontion . Par es motifs - Le Bureau Général jugeant 
on fermerer répsort, on la forme mois Landon on de 
appolétion au jugement rendu contre lui le onze avril dernier 
senregistré, l'en déboute comme mal fonné en ccelle. Ordonne 
l’exécution pure et sumple du jugement auquel est oppolétior 
et condamne landon ax dépens taxés et liquidés en vers 
Le Trésor Public, à la somme de deux francs quinze centimes 
pour le papier timbré et l’enregistrement de l’appention, conformément 
à la loi du sept cox mil huit cent cinquante, ence, non compris la 
cous du présent jugement, la signification d'icelui et ses suites. 
Ansi jugé les jour mois et an que dessus. 
Derucq secrétaire 
Dmai  
t d
tn
a Mudit jour six viiq u 
Entre Monsieur Levonte, anvner lapeemntier, demeurant 
à Boles, au rondipement de Clermont leisa j 
Demandur, Comperant ; D'une part ; Et Monpeur 
Curtet Jeun Marie, ouvrier vailleur d’hobits, demeurant 
à Paris, passage Montes quien, numéro cinq ; Demander ; 
Comparant ; D’une part ; L Monsieur Lacembe,; tute 
dailleur d’habits, demeurant et domicilié à Paris rue lai 
sa numéro quinze ; Défendeur ; Comparant ; Dutelurt, 
Paint de fait - Par lettres du secrétaire du Conseil de srur ha 
du Département de la Seine pour l’industrie des tissus en 
dtates des Vendredi trente un Mai et lundi trois juin mil huit cent 
soixante dix huit, curtet fit citer Lacombe à Comparatre par devant 
ledit conseil de Prud’homme séant en Bureaux Particuliers les emble 
trois et Mardi quatre Cin mil huit cent soixante dix huit pour sin 
concilier à faire se pouvait sur la demande qu’il entendit forixen
contre lui devant le dit Conseil en paiement de la somme de 
Pi 
de
sit es arés nil 
Mn
ec
e 
 
Dre q 
u 
2
 
i 
ié 
 a 
 
q 
i 
 
 
i q 
 du 
soixante denx francs pour selaire. La coulée n'ayant pas comparu 
la cause fut renvoyée devant le Bureau Général du Conseil séant 
jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour 
six pis par lettre du secrétairé en date du quatre juin mil huit ent 
soixante dix huit à la réquité de Cortet Lecomle comparut; 
A l'appel de la cause Curet se présenta et conclut à ce qu'il plut  
au Bureau Général du Conseil condamner lacole à lui payer 
avec intérêts suivant la loi la somme de soinante deux francs qu'il lui 
doit pour prix de travaux de son état et le condonner aux dépens. 
De son coté Laconbe se présenta et conclut à ce q’il plut au 
Bureau Général du Conseil ettendu qu'il reconnit devoir la soume 
de soixante deux francs ; mois attendu qu'an dêtenant le fière qu'il 
 encère entre les moins turtet lui ceuse préjudice Luisque cette 
frèce n’ayant pa être livrée en temps la cestire pour comple 
Par ces motifs — Lire Cortet untucevable, en rademand e tavu 
débouter eile condamner aux dépens. foit de doit - Doit on 
condamner Lacoute à payer à cortets la somme de soixante 
de soixante deux froncs contre la remisé d’un ditent qu'il 
salire à Paton dit en du leis reant lut 
la demande ; l'en débouter ecevoir Lécombe reonvou tionnellement 
demandent ; Condamne Tortet a remitre le vêtement en 
psus eu disitre ou mis d sant 
au eu du du sent pu tie ueapsite 
d etito u ell t tent lerse lu 
de lete de e e e t l peiue 
en leurs demandes et conclusions repeetivement et encvord 
et e t te e te ite u 
ede ur tn du t esur 
purt la pre des ioise les our 
are e e e e t due 
a ler s r se te e 
dte t e poi i 
en e p e ten u 
reit e de u te enpe 
ente e pe de iq ount quilt 
eten de ses uee 
de de e titer 
duit en ue se 
andurs deuen der de 
aun u ant e e reie 
uar e e t enede 
tain e ti qu iq e te see 
inps e Mée 
minute, Cnformément à la loi du sept aût mil huit cent cinquante 
rue, non compris le cout du présent jugement, la signification d’iceluit 
et ses suites. Ainsi jugé les jour, mois et an que dessus. 
anaus  
Lecusq secrétaire 
te
u dit jour six Piig 
linre Monsieur Langer, ouvrier tordonner, demeurant à 
Paris, rue saint Honoré, numéro cent quarante neuf ; Demandeur 
Comparant ; D’une part ; A Madame veuve Denis, demeurant 
lais, rue le la Roguette, numéro trente six ; Défenderesse  
Défaillent ; D'utre part ; Point de fat – Poirant emplot dà 
Chau hurs, huipser à Paris, en date du vingt neuf Mai mil huit cent 
sorxante dix, huit, visé por tioibre et enregistré à Paris, le trente 
en Mai mil huit cent soixante dix huit débet quatre francs cinquante 
cinq centimes, signé à de Seuilles, Leufer a signifie et soifsé de prè 
à la veuve Dons, frimo dun jugement endu par le conseil de 
Prud'hommes du  Département de la Seine pour l’industrie des 
tissus le vingt un ferrier dernier enregistré; portant condamnetion 
contre le sieur Mllard, Cordonnier, demourant à Paris, a ceta 
Roquette, numéro trente six de la somme de cent trente trois francs 
de priniipal es intérêts et dépens liquidés ; Lecondo de la significtion 
à lui faite de ce jugemeit ; ot par exploit du ministère du dit 
uipier en date du seize Mad, enregistre ; Lé, D’'un autre jugement 
rendu sar le même Conseil le vint un Moit mil huit cent soixante 
dix huit, enregestre, d'éboutant le toir ltard de l’apposstion par ler 
formé du dit jugement du vingt un fevreer derier le condamner 
aux décens. CQ de la significtion à lui faite par exploit du tonne 
hupier en date du vingt cinq avril mil huit cent soixante dix tuit 
enregistré L? Du homme tant qui lui a été signifié par exploiy du 
même Loupier, en date du vingt sept avril dernier, enregistre. 
u ufin de la tontetire de saisse qui a été fate par exploit du ceit 
puipier endate du vingt quatre Mai courent, enregistrée eeuel 
lemême exploit donne siguation à la dite dame veuve Moui 
à comparaître le six Juin mil huit cent soixante dix huit jaut cinq 
de retevéé à l’adance et par devant Mappouns les meutres cn francs 
le compet de Prud’homme par l’industrie des tiss puis q u e 
six neis 
il 
les 
quel sai lene 
hil 
ecu
t
qu'il est creancier de la somme de cent trente trois francs pour prix 
de travoil et que le conseil par jugement du vingt un fevrer dernier 
condamne Celtard qu l’en couveait comne patron de l’étotlépement 
et qui ne serait, que ferant ; Attendu que ce dernier à jeune appention 
à e jugement et que le conseil l'a déboute de son appention et condame 
aux dépens par entre jugement de vingt un pars , que par suite de ce 
socent jugement fit gulement signifié, comme demant lui fit 
doané et que lorsqu'en voulit prétijuer la saisse des dpet matiliers 
et merchardon et qurnmpant la Bouttique le sieur ltard se trouvaut 
installé dans la dite Boulique s'y oppose étendant que les moilles 
et marchandiser étaeut, la proprité de Madame veuve Denis plntée 
comme cordonnnère à façon avence de la aquette, nmr trente six; 
qu'elle qétait imposée et que le loyer était également au nom de la dle 
dane ; Attendu que la dite dame Denis estpurente du sieur 
ellard, d'elle esnt en service et n’hibité en acue meme le 
fond de commet ce de chapurer exploité par le sieur, ellardes que 
d’ailleurs l’étabtifs enont dans répoudre du selaire des ouvriers etgltant 
proprrétaire de l'établifement elle duits être trnue au priemer de son 
la laire dant le dit établissement à profité ; Par ces motifs e taut 
entrer à de duvré en temps et lieu ententre la dite veuve Donis, 
déclarer commun à e avec ette es jugement rendut par le conseil 
de Prud’homme de la Seine pour l’industréie des tissus les vingt 
en e e e e e e a les leenie 
la demet e poer de e se fouir 
e pau t peur cites n e preésir 
te de el de pome de quen e e 
 e den pe u deu 
un deu is cete d ape 
nant e empre ecin eur 
de dte sen en an que te ere 
pu de n e u le qu eit 
te te e eu e u en tense 
e de p en e nt eue 
en eu e e e u e prer 
pu demen de es qur 
de t ete l praunt pur au 
e ait e n n entene 
rprete dea enan de fer 
ede t e pement 
e de p den e e de pet 
en e e e e e e et 
mrte enre t  e 
pageur en prenier le part ; Donne défaut contre la veuve Douis 
non comparant à personne pour elle quoique dument appelé. L 
adjugeant le profit du dit défaut ; Dit commu à la veuve 
Devis les jugements ren dus par le conseil les vingt un 
ferrier et ving un Mars mil huit cent soixante dix huit 
contre ellard, En conséquence ordonne l’exécution de sa 
jugement aussi bien contre la veuve Denis que contre lauder 
ensemble, prinmipet intérêts et frais Condamne la veuve Douée 
aux dépens taxés et liquidés envers le demandeur à la somme d 
sept francs soixante dix centimes et à celle de quatre francs 
cinquante cinq centimes envers le Trésor Public pour le papier 
timbré et l’enregistrement de la critation, conformément à la 
loi du sept aout mil huit cent cinquante, ence, non compris 
le cout du présent jugement, la signification d’icelui et ses 
suites. Et vu les articles 435 du code de procédure ciile 27 
42 du décret du onze juin 18099 pour signifier à ca défenderesse 
le présent jugement, commet Chanpion, l'un de ses huipsers 
audienciers. Ainsi jugé les jour mois et an que dessus. 
 
Durois l
Lecusq secrétaire 
 Quit jour six quiz 
Entre Monfieur andre Gugat, ouvrier passementier 
demeurant à Paris, rue Délettre, numéro douze et quclire 
Demandeur ; Comparant ; D’une part ; eE Monsieur Caquelt, 
fabricant papementier, demeurant et domicilié à Paris, bue 
des Cascusés, nunééro cinquante trois ; Défendeur ; Comprut 
D'autre part ; Point-de fait —  Par lettre du secrétaire qu londe 
de Prud’hommes du Département de la Seine pour l’industrie 
des tissus en date du Mardi vingt un Maii mil huit ce parant 
dix huit crayot fit citer la hur comparaître par devant le jré  
Conseil de Prud’hommes séant en Bureau Particulier le vendei 
vingt uatre Mai mil huit cent soixante dix huit pour setisien, 
nse faure se pavit sur la demendes lel rntieles pouer cesie 
deans le dit Conseil en faiement de la somme de quatre vin ffnc ue 
pour indemnité de perte de temps et de renvoi n tompé
A l'appel de la cause puret se présenta ea rxpusé le conil 
is e 
 
h
lille 
 
e nei 
pet  
n
 
de son coté taner se présenta et exposa u Conseil que quont 
il fit attendre Ganot il l’indemnise; que sil l'a conseré tout 
E 
à coup sius l’il lui fit un travail necuptette. Le Bnteun 
Particulier renvoya la cause devant un mimbre du Conseil à ce 
connaissant  Devant ce membre les partis ne purent être 
conciliées à la cause en cet dot fut renvoyée devant le Bureau 
Général du Conseil séant le jeudi six juin mil huit cent soixante 
dix huit. Sénnt, ce memle de coseil Cité pour le dit 
pour six juin par lettre du secrétaire du conseil en date du 
vingt sept Mai mil huit cent soixante dex huit à la renuute de huint 
la quet comparut. A l’appel de la cause ngit le pémmta 
le
et conclut à ce qu’il plut au Bureau Général de Conseil 
attendu qu'en lui fait ant attendre des matières laquet lui a 
causé un préjudice de perte de temps dont ià lui doit épusition 
qu'un le curetient instantemment, nonmbstant la prreusicque 
lui avoir fite Pahuet père de lai donner un curgement 
une fuit celui entrois terminé il lui cutse en odre préjéudice 
pour le uil réparition lui etéralement due Per ces motifis 
condamner laguet à lài payer avec intérêts suaint la loi la 
por de qute eit de ponr itle e n qe elesese 
t landeme aux des e rerle hui t pae  
aunt e de en ende e enit e e 
ouerent ce ant e uit doen lens 
te depr du le u demrantene 
sepen te u entente pauis et 
de ure prs et ie 
a epre pu t unt en 
enterdete, o ete du peraente t 
 
e den lu tee t ue te p e 
es; ciqui der te il qu tene 
que nte de en put 
aper du onte deu puremet 
mer e e e t 
qu eu de e de e 
adums en apre eneie 
e tur due ei qu de sir 
de e r de e er 
adean rand eux  er 
n ei ent uen et 
es e en en entie 
ante e n somie 
aem er e e t 
la 
P
pl
dir sept 
reuvai etdous mols 
rayes nulsrepprou 
Lecucs pe 
à
ayant fut in menvas travail n'y avaix aucun dait ; Qu'ed reitaud 
même que Caquet, père lui ont promis un chargement ; c qu'il 
était toujours lacuttotif onu fils de repuser puir qu’il est cesand 
patron ayant antorité pour diriger son traval, l’expérience fot 
de son ineapoité sofras à elle seute pour en pastifes T’neote 
Par ces motifs - Le Bureau Général jugeant, en dernier réport ; 
Dit Gayot non recevable en ses demandes , l'en déboute, ee la 
condamne aux dépens envers le Trésor Public, pour le papier timbré 
de la présente minute, conformément à la loi du sept aout, mil 
huit cent cinquante, ence, non compris le cout du présent jugement, 
la signification d’icelui et ses suites. Ainsi jugé les jour, mois et 
an que dessus. 
ere ue D 
l
Lecusq secrétaire 
Du dit jour dix Pinga
Entre Monsieur Pant laguet, ouvrier passementier, demeurant 
à Paris, rue Grat prolougé, numéro deux ; Demandeur 
Comparant ; D'une part ; L Madame Marguerite Cetime 
Caurant; épouse du sieur Louis Bertail, comme commeradement 
sous le nom de dame Bouguut, fabricante de paysementite 
demeurant et domiciliée à Paris, Boulevart de sepastapes, numéro 
quatre vnt dix huit ; Défenderesse ; Comparant ; D’autre part ; 
Ponit, de fait - Suivant xploit de champiin, suipcès à Pariseuns 
date du premier juin mil huit cent soixante dix huit, visé pour timbe  
enregistré à Paris, le trois juin mil huit cent sixute dix huit, cen dies 
ex paur pareite cinge tentioes, leigner duafeite sapit à 
mocilie et toiféé sapré à la dame Mésheurat, enne Rortel 
preme d’une jugement rendu par le conseil de Prud'homme du 
Département de la Seine pour l’industrie des tissus du vingtenc 
avril dernier, enregistré, portait condamnition à son profit 
cantre la dame veuve ongeautt, fabricante de lassemens ent 
Boulevart sétastept, numéro quatre vingt dix huit, le coti
francs de princifut pour salaire ed indemnité, plus ces intérêts den 
dépens. setondu de la signficstion de ce jugement par explioito 
dit chaznpion, luipsierà en date du nzt duvne dernier uagés 
tertie du commen dement de payer pais par etre enprisudice 
du ize dux de ent s tereit, duten de eu 
de larsse fuite à la maison Bougerants le vingt sept Mai 
dernier et dans laquelle Madame Bortal aà fat conmme qu’il 
n'y avair pas de Madame Bougeault et qu'elle édtas la propicétaire 
de la fabrique de passementerie ; Et par se même exploit à cité le 
dame Bertail sus nomme et le pour son mori pour la volidilé à 
comparaître par devant Mépieurs les Président y Dembres comparut 
le Bureau Général du Conseil de Prud’hommes lu l’industrie 
des tissus séant en Bureau Général le jeudi six juin mil huit cent 
soixante dix huit pour attendu qu'il travaille pour le compte des 
la maisou de fabrique dont elle est profiéitaire ; Que ne pouvant 
abtenir paiemint des salairer à lui dos il a du l'appelés e 
devant le conseil de Prud’hommes ; que ne conné saut par le noin qu'elle 
dit avoir de dame Mertail, il l’a fait citer au nom de dune 
Bougeault, nom sous lequel elle en génralement et commméuelement 
compme depuis lind temps tant avant que depuis le décis de 
Monsieur Bougeaut ; Attendu que sonte nomil a obétenu 
contre la dite dame run jugement la condamner à lui payer. 
las ux pour de ti eent est p e fous 
jugeant t l e du p prdu. aor 
d en des p pu de i u sier 
paiement de se part ; Que seulevant lorser en a voula xcuter 
pravant prer la de de nt pra ten e le ces 
de dure po lit cent er lur e ome e ie 
su e e te ten eui eun es puir 
et pur e en tonlaet tere de en eset 
du lis ae te e sasemet 
dan e e e t le sour 
dun preut e desthuit juemeint 
itre l due contit sre leee eense 
an eaent e s e t d reier 
de de sende demeant pour 
ut den e demante ce meies 
ede e e oent 
n gen e e e ee 
ei t eu eu e sur 
aer e e e on puret 
au e e u te puer 
ente e ei qu pene 
rdet u se e deier 
pesepor ce et de ete qu se e 
ete u t enc li utie 
a dermant poen ent que 
 
 
M 
q o
 
 
Drdr 
t 
somme au dit Caquet ui lui a bien son mis des ichantillens 
dans le lut d’avoir des commander, mois auquel elle n’avant 
commandé ; Par ces motifs - Dire laquet non recevable 
ses demandes, l'en débouter comme mal fondé en rcelle ste 
condamner aux dépens. tont de troit — Doit on dire 
applecitte à la dame Gertail le jugement rendu le vingt 
cinq avril dernier, enregistré, contre la veuve Boureau Gérnale 
conséquence condamner la dame Marguerite celine Lonvan 
comme Bertail à payer à Caquet les sommes portées Dat 
jugement ? Ou bien dait-on dire taquet non recevable 
en ses demandes, l'en débouter ; Que doit-il être statué à 
l’égard des frais et dépens ?Après avoir entendu les 
parties en leurs demandes et conclusions respectivement e 
un avoir délibéré conformément à la loi ; Attendu qu'ile 
eit constant que la dame Cgeent somme Bortailà 
accepté de cahuit un ichontitton par lui fait ; lui a donne 
ces matières, nufrancs pour confectionner des refrences de ce 
même acpontillon de chargements sur deux metiers aupris 
de cinquante francs des cent mètres ; que l’exéution de cet engugeant 
n’a pas été timplié par la same lo jaurent parce qu’elle troiuve 
plus avantageux de la faire exeiter par un autre ouvrier quine 
pris que quarante cinq francs des cent mètres, ce qu'il pouvait 
faire n'ayant eux aucune peure decréation mancun frans 
d'ichobittonnege ; Qu'et résulte de ce fait indeliés en lui même 
que caquet à éprouve un préjudice dant la dame lTousen 
lui doit réparation par la somme de soixante dix francs 
Attendu que la dame lavarent qui avait en des rapports de 
travail avoi Cahuet savois, bien que les lettres de conciliation 
et de citation la visaiens, encore qu'elles fassent adre présse 
la dame Bougeantt, nom auquet etle répous d’ailleurs comnuilti 
Qu'n celas elle estpassible de tout les prois qui ont été fots à 
duision du jugement. Pir ces motifs - Le Bureau Général 
jugeant en ternier lepart ; Dit applicible à la dame Marguer 
eline Coureit, somme Bortail, le jugement tendu las le Conseil 
le vingt cinq avril dernier contre la veuve Bauquuitt ; ordonne 
l’exécution de ce jugement contre la dite dame Bertaldeu 
Marguerite le ué Ceurent sisqu’à concusrenre de sois otes 
froncs de principal; condamne la dame Bertailt éellergt 
celine Ceurent aix frans et dépens de la première instant a 
plutat de l'oidlance desant la veuve Boureautt, commé à 'euf, 
de la présente instn tas et liquidés en vers le demanders e 
la somme de Cinq francs quatte hunigt quinze centims uit 
E
celle de deux dix frans soixante quinze centimes envers le Trésor 
Public pour le papier timbré et l'enregistrement de la citation ; 
Conformément à la loi du sept aout mil huit cent cinquante, 
ence, non compris le cout du présent jugement, la sinification 
d’icelui et ses suites . Aini jugé es jour mois et an que dessus. 
Lecucq secrétaire 
dmeau
Du dit jour dix Jing 
domant e 
Entre Mademoiselle Désirée Sanson, se disont ouvrière 
contrière demeurant à Paris, rue Fesnnier, numéro dix 
Demanderesle ; Comparant ; D’une dort ; t Monsieur et 
Madame Magnen, le sieur Magnen tant en son nom 
personnel que pur epaiter et autoriser la dame son épouse, 
Mantrelte contirre, demeurant et domiciliés, entembrle, le dits 
épout à Paris, avenie des taris, nméré qatre suit cinq 
Défendeurs ; Comparant ; D’autre part ; Poit de fit 
Par lettre du secrétaire du Conseil de Prud’homme du 
Dépastement de la Seine pour l'industrie des tissus en dates des 
Mercredi vingt neuf a vendredi trente  Mai mil huit cent 
huit li e t le te de se le e u 
dapers prde ts tel a sid en ue u 
Anseu té te eontritat d u umes 
e les epord s le er en p p t 
ente unde ete dais poir tetre qunt les 
e e pre s la o e e en per 
due mne u deant ens un 
epe e pe le deu le 
deur du ti ens ene n en pement 
dei e e der e paur 
edu due au de juemne 
ede t e e e e es lute 
ede e e que   
der eun, duedoint e e e 
e e e en e se 
e eit e 
aen e den e eser 
pr e enqu les 
Bourrnie de se sonfais ; que si elle lui à enteigne grctutenant le 
protique de la cuture s'eit qu'elle la veutoir endre expabla 
de complir l'umploi d’une smme de ciembre au lien de cettedeu 
borne d’enfans qu'elle avait occupée jusque la ; Que s'elle sa 
nourrie c’esrt qu'elle prit, l'engagement de lui payer sa nouriter 
par ena somme de se francs int ciq cntiomes pas jounr 
qu'en sai elle est sa débitrie et hou se trénière poureni et e 
ecinq frcès de l’appele devant Monsieur lejage de Pair la 
su arcondipement qui les ontorisé à détenir sa malle jusque 
jour ou elle de serapoiquittée envers eur ; Par ces motifs — Le 
déclarer en conpétut pour comaitre de la demande la demoiselle 
Sandon, l'en débouter et la condamner aux dépens. Deton 
coté la demoiselle San son se présenta et conclut à ce qu’il plaut 
au Bureau Général du conseil attendu que la dame Jourson 
l’a occuhze pendont cinq mois à des travaux de contire après 
quoi elle lui delivra un certfiret dant lequel elle est qualitée 
d’ouvrière conturière ; Par ces motifs — Dire les époux Mégaenu 
non recevables enteur demante ; les en débouter. Le déclarer 
compétent, retenir la cause et oedonner qu’il sera explique 
sur le fand et condamner les époux Magnon aux dépens. Point 
de troit - Doit-on se déclarer on complent pour comastre 
de la demande de la demoiselle sanson? Ou bien doit-on 
de déclarer compétent, retenir la cause et ordonner que les 
parties seront tenue de s'expliquer sur le fundi pour être 
statué ce qu'il appartiendeu?? Que doit-il être statué à l'égard 
es dépens ? ? Après avoir entendu les parties en leurs demantrs 
et conclussons sur le décliniture posé par les époux Mauven 
et en avoir délibéré conformément à la loi ; Attendu queles 
eeplications foaries par les parties se part que la dame Moaye 
n'oit pas maitrepa conturière ; que la demoiselle Sansalle 
nuné n'est pas ouvrière conterrière ; Que se les époux 
Magaen s'ont reçue chez euy et ti la dame Magnon lui a 
enjugné des étements de conture  la a danne un certificaton 
caprits a por deurt l lag loes le pe de u ait e p e 
somme de chembre qu'elle occupée en ce monent ; Qu'en recosté 
Conseil n’a pas à conmetre des faits cét de leurs sap parts ; u det 
ces motifs - Le Bureau Général jugeant en tremier ressort, l 
déclare n compétent pour commêtre de la demansle le lademoie 
saeison et la ouvrie devant que de troit ; Condamne le demable 
Sans on ux dépous envers le Trésor Public, pour le papier timbré  
prente ille, conformément à la loi du sgt ui muil luiét in 
ene non comprès le coiut, du présent jugemen q ui ue 
d’icelui et serpuiltés. 
Ainsi jugé les jour, mois et an que 
depas. 
ecusq serétaire 
Vergut 
du dit jour dix vi 
ntre Monsieur et Madame Potits, le sieur Petit huit 
en son nom personnel que pour eprriter etutoriser la dame 
son épouse ouvrière Conturière demeurant ensemble, les dits 
époux, à Paris, rue du Potian, numéro vingt treit 
Demandeurs ; Comparant ; D'une part ; Et Monsieur pus Foirser 
aptécien demeurantà Pains, rue Tris, numéro suite 
seize ; Défendeur ; Défaillas ; D'autre sart , Pont 
de fait à Suivent employ de champren lu pier à laris, en 
date du Vingt huit Mai mil huit let soixante dix huit, visé 
pour timbre et enregistré à Paris, le vingt nup Mai mil huit cent 
soixante dix huit déber trois frencs trente cinq centimes, signé  
saprit la per e pes e e silui 
 doite oen de en te seur 
du Département de la Sene pour l’industrie du tissus le vingt 
cinq avril mil huit cent soixante dix huit, enregistré porteu tindemnitions 
ate de per titene eui en den de setie 
conturière, de la somme de quarante six francs de pnpl te gu
eude ni aug eu t poux jugente e 
pu eom a e e e our 
enper e de en l prése e du ei es 
suite o an tin de n e 
apres ut en axé dt pe se 
asseren e eente  
qul domies dner 
pus u ue ctise 
hu etes un de e t nonet 
ue le sete e e n e te 
en de ent e d e t e et 
deratur ent ei en es eps 
ete soment de ur 
au de e pre  
Sar 
reuvri et quate mits 
aages nmoils apprenti 
quinze centimes plus à quatre franc d'indeunité pour temps perdu; 
attendu que la signification du jugement et le comma dement oit ile 
faits conformément à la loi ee que la centetire de saisse faute le 
vingt un Mai dernner Monsieur chailler à cit s’opposer à la same 
rétendant que tous les otéêts garnépant les dits tient lui ont été lu 
par Monpeur pales Fénier, aptrien à Pains rue de six, numéro 
sege, quoique sur la prte de l’appertement de trouve eeployi 
portant le non de sonnmeller ; attendu que le siour Jules qovrent 
n't qu’un prété nom et que même s'il était propriétaire de 
l’étabtifs éneux de conture enistant actuellement, passage la fersien 
numéro douze il doit être tru au paieme du salaire du pe 
ouvrières employé dans l’établifsement. Par ces motifs et tousunti 
à déclaire nltérieuremet, poir déclarer commun avei lui, sieuf 
juler Foirier le jugement rendu par le conseil le vingt cinq ane 
dernier, enregistré. L'entendre en conséquece condamner à leur payer 
la somme de quarante six francs quatre vingt quinze centimes ncoutret 
on la pitet des condamnations prinoncées, et s'entendre en outre cendmn 
entans les dépens et frois du jugement de premier instance, et ux francs 
d’exécution qui l'ont seive et, en outre, Fontandre condamner 
entans les frois du jugement commen . Al’appel de la canse jule 
De leur coté les époux Pobit se présentèrent 
Férier ne comporut pas. 
et conclurent à ce qu’il plut au Bureau Général du Conseil donner 
défaut, contre Jiter Vocrier non comparant ni personne pour lui 
quoique dument appelé et pour le profit leut adjuger le bénmipier d 
Cconclusions par eux prises dans la citation euploit de chaupsaux 
huipier, enregistré. Paint de tait — Pait-on donner défaut 
contre Jules Fvrier non comparant ni personne pour lui quoique due
appelé et pour le profit adjuger aux demandeurs les conclusions par ux
es précédemment prises ? Que doit-il être statué à l'égard des dépens ? 
près avoir entendu les époux Potit en leurs demadés et conclusions 
en avoir délibéré counformément à la loi ; Attendu que les demandersse 
époux Pabit proipanrt justis et fondées ; Que d'ailleurs elles nevont pas 
contestées par pales Février non comparant ni personne pour lui 
quoique dument appelé ; Par ce motifs L Bureau Général jugeant e 
en premier le part ; Dit commun à pules Février se jugement rendefat 
le Conseil le vingt cinq uvreil dernie, enregistré, contre les époux ar 
ou chnelles ; in conséquence ordonne l’execution de le jugement 
aiquants a pis lour contre te le lo vrer eu ete ur 
ou lckieller fait pour le principal des condamnetions pu prir e 
intérêts et frais de pour suites faites à l’arcusion de ce jugement; 
condomne polur Favier aux dépsouns de la préente eat ie oaie 
nlégu de anver les demandeures à la somme de cinq franq uai 
qi 
cinq centimes et à celle de trois frans trente cinq centimes envers le 
Trésor Public, pour le papier timbré et l'enregistrement de la 
citation, conformément à la loi du sept août mil huit cent 
cinquante, ence, non compris le cout du présent jugement, la signification 
d’icelui et ses suites. Ainsi jugé les jour, mois et an que desus. 
ecusq secrétaire à 
Emier
D
t 
Audience 
Du 
leudi treize Jng mil huit cent soixante 
dine huit 
Siégeuant : Messeurs 
Bagor, Prud houre Présidant lotoutience, 
en remplaiement de Mecheure Marienvel et snaud 
Présitent et Vère président du conseil de Prud’hommes 
du Département de la Seine pour l’industrie des tissus 
empechis, Deroyn, Menier Berthemer 
et Cusse, Prud'hommes epistée de Monsieur 
 e
Decucq, brétare du dit Consie 
Etre épens Poirit, viner papmentier 
demeurant à Paris, Toulevart de la pgare, mro cent cinquante àn 
dee ; Demadeur ; Comparant ; D’une part ;  Monsieur 
Dutr fabricant pasementier, demeurant et domclié à 
det, rue trtize, oummér prt ; Ptatie 
ples ’eseps ; E if onte e 
deuit se larde dt lu émin e e e ou 
Seine pour l’industrie des tissus en dates des Jeudi six et 
e de pue l e poe oeur 
cute da pat pus et le cicis demen 
enten de e e e e e e e te 
de eperte d it du ten e ui quet 
e sen t ts pur d t endeate 
nteme t pou d enes pus qutle ue 
eu e e e e er 
e e e e enr 
ape e desen ueti quige 
otr cit du cint qu7il  
du dite relere dumeiet 
ur de en te iquen 
e e e p t e se e 
pe n en anp les 
ene entisqnti 
a d e m t en ei se 
dman a prs eut due 
ou de pe ur 
ue des ai su euse 
é 
i o r 
os 
 
 
 
? A
Li
 
 quei 
Point de droit — Doit-on donner défaut contre Doit on 
comparant ni personne pour lui quoidue dument appelée ;t 
pour le profit adouger au enteindeur les conclusions puar lai 
précédemment prises ? Que doit-il être statué à l'égard 
des dépens ? Après avoir entendu Régat en ses demendre 
et conclusions et en avoir délibéré conformément à la loi 
Attendu que la demande de igar parait juste et fonéée  
Qque d’ailleurs ele nen pas contestée par Dety n on comparante 
ni personne pour lui quoique dument appelé ; Attendu qu’il 
ne comparuépanut pas devant les Bureaux du Conseil Diite à 
causé à Parat un préjudice de perte de temps, cequ'il 
doit répurer . Par es motifs - le Bureau Général jugte
en dernier repart ; Pu l'article quarante un du decret di 
conze juin mil huit cent neuf, portant renseuvent por leur 
conseil de Prud’hommes ; Donne défaut, contre Dieta non 
comparant ni personne pour lui quoique dument appelé ;Et 
adjugeant le profit du dit défaut ; condamne Deté à payer 
avec intérêts suivant la loi à Slgat, la somme de quatante 
deux francs qu'il lui doit pour salaire, plus une indemnité 
de six francs pour temps perdu ; Le condamne en outre 
eux dépns taxés et liquidés envers le demandeur à la somede 
un franc, et à celle due au Trésor Public, pour le papier 
timbré de la présente minute , conformément à la loi du sept 
août mil huit cent cinquante, ence, non compris le cout, du présente 
jugement la signification d’icelui t ses suites à et vu les 
entorles 435 du code de procédure civile, 27 et 42 du secret du 
onze juin 18099 pour sinoifier au défendeur le présent jugement Conmle 
Cl umpron jun de ses huissies audienciers. Ainsi jugé les jour 
mois et an que dessus. 
gagoffiet
Lecucq secrétaire 
 Qusi jour cingt siuiq  
Entre Monsieur Denis Mocke, demeurant à Paris,ue Brzfo  
numéro vingt deux agistant en nom et comme a demandete nt 
de la personet des tiens de sa fille mineure Couist, ouvrere 
cantiriée ; Demandeur ; Comparant ; D'une part ; 
Mademoiselle Celine Jainq Liéclain, l’atupe contire e
demeurant et domiciliée à Paris, rue de la champée ? Pontin ; 
numéro quinze ; Défenderesse ; Défaillant ; D’autre part ; 
Pains de fait - Par lettres du secrétaire du Conseil de Prud’hommes 
du Sépartement de la Seine pour l’industrie des tissus en dates des 
Mardi quatre et vendredi sept juin mil huit cent soixante dix cent 
pour se concilier si faire se pouvait sur la demande qu’il entendait 
former contrerlle devant le dit conseil en paiement de la somme de 
dix francs trente cinq centimes qu'elle rui doit pour prix de travaux 
de sa fille louise. La demoiselle Lievain n’ayant pas 
comparu la cause fut renvoyée devant le Bureau Général du
Conseil séant le jeudi treinze jun mil huit cent soixante dix huit 
Cités pour le dit jour snze juin par lettre du secrétaire du Conseil 
en date du onze Juin mil huit cent soixante six heaig à la requete 
de ock la demoiselle Gerain ne comparut pas. Le 
son coté ole se présenta et conclut, à ce qu’il plut au Bureau 
Général du Conseil donner défaut contre la demoiselle Térainn 
non comparante ni personne pour elle quoique dument appelée 
et pour le profit la condamner à lui payer avec intérêtts sivant 
la loi aa somme de dix francs trente luiq centimes qu'elle lui daoit 
pour solde du frix de travau de sa fille vnisi pas, cette 
indemnité qu'il plaira au Conseil fixer par perté de temps devant 
la peu e e se rnt eune Negcles  Cntre 
puen eaps tete la deme le tr oin en o prs 
puesen pur le pi de t n pifi ne 
ndes au demeide lrte er purse premnen 
pel pu de de pur 
uer du te  e e e e e enier 
rete e prde e e t emre le 
e resq t e des e desipu 
e u e deme peur 
qpur em e te e de sen t ps p ur 
per e ou es a u te resnie 
ande de e pr ent qut deux ceste 
den n tes surt 
nde e nde de e 
qurate u dte d u se de 
e i q e e desoe 
age n e e 
endem e es pour 
pur u e e rite purir 
de lite de e deon le 
apur e i cncis 
 
i
e 
cé C
 
cnt 
A
puste et fondée ; Que dailleur elle n'est pas contestée par la demoiel 
crevain non comparante ni personne pour elle quoique dament
appelé ; Attendu qu’en ne comparusant par devait le Bourer 
edu Conseil la demoiselle Lrévain a causé à Porte se 
préjudice de perte de temps, ce qu’il doit réparer ; Par 
motifs - Le Bureau Général jugeant en dernier repsort ; 
Dounne défaut contre la demoiselle Léévein on comparante 
ni personne pour elle quoique dument appelé ; Et adjugeat le 
profit du dit défaut ; condamne la demoiselle Lrévain 
payer avec intérêts suivant la loi à Maccy la somme de sex 
sans trente cinq centimes qu’il lui doit pour prix de travaux des 
sa fille couisne, plus une indemnité de quatre froncs pour temuse 
perdu ; Le condamne en outre aux dépens taxés et liquidés  
envers le demandeur à la somme de, ue fronc et à celle dui 
Trésor Public, pour le papier timbré de la présente minute, 
conformément à la loi du sept aot mil huit cent cinquante
ence non compriu le cout du présent jugement, la signification 
d’icelui et ses suites. tt vu les articles 435 du code de procder 
civile, 27 et 42 du decres du onze Juin 18099 pour signifier a 
la défenderese le présent jugement, commet champion, l'un ler 
ses huifsiers audienciers. Ainsi jugé les jour mois et an que 
dessus. 
Mrasgoffte 
Lecucq recrétaire 
e 
it 
ui 
 du dit jour Treize Puizan 
Entre Madame veuve Didier, demeurant à Paris, rue Louge 
numéro trente lix agissant en nom et comme satrèce n lutelle 
et légale de sa fille mineure ingele, ouvrière fluriste 
Demanderetse ; Comparant ; D'une part ; Madime 
veuve Minée Ségrot, fabricante de fleurs artificilles 
demeurant et domiciliée à Paris, Boulevart Montmrtr 
numéro quatorze ; Défenderse ; Défaillant ; D’andesr 
part ; cnq de fait ) Par lettres du secrétaire du conteil 2u  
Prud’hommes du Département de la Seine pour l’industrie de 
tésons en dates des undi trois e Mardi quatre juin mil huit nt 
soixante dix huit, la dame Didier fit citer la veuve Aune 
Soyrot à comparaître par devant le dit Conseil de Prud’home 
de
séunt en Bureaux Particuliers les mardi quatre et vendredi sept juin mit 
huit Cent soixante dix huit, pour se concilier si faire se pouvait sux 
la demande qu’elle entendit, former contre elle devat le dit Conseil en 
puement de la somme de quarante franc quelle li doit pour prix de 
Pa jeur Vnnée Segot n'ayant 
travaux de sa fille angèle ; 
pas comparu la cause fut renvoyée devant le Bureau Général du 
Conseil séant le jeudi liize juin mil huit cent soixente dix huit 
citée pour le dit jour treize juin par lettre du secrétaire du 
Conseil en date du heit juin mil huit cent soixante dix huit 
la réquête de la dame Didier la veuve sgné Trgrit ne 
comporut pas ni personne pour elle régalièremet. A l’appel 
de la cause la dame Didier se présenta et conclut à ce us plus 
a Bureau Général du Conseil donner défaut contre la veuve 
ine Pagrot, non comparante ni personne pour elle quoique 
dument appelée et pour le profit la consamnner à lui payer 
avec intérêts suivant la loi la somme de quarante francs qu'elle lui 
doit pour prix de travaux de sa fille congte, plu endenité 
qu'il plaira au Conseil fixer pour perite de temps devant le Bareaux 
du Conseil et les dépens. Pent de droit - Dit on donner défaut 
contre la veuve Mirnée Peret non comparante ni personne 
pour elle quoique dument appelée et pour e profit adjuger 
atele e e ple lu etens qu e pre e pr e 
Que doit-il être statué à l'égard des dépens ? Après avor 
entendu la dame Prdier en ses demander et conclusions e 
en an slé da prdent cle, doeunte 
ur d te eu dit purt te pat hre 
ete de de e eit dener 
po pe de en e e dene 
ere du jon eux e eur 
e e e le oue t e dere 
en e cine 
eade seun de en uetle 
eue e e pede 
ende e te ee u  
se dus e e e que q ene 
u de en e e e ent 
endt seoi e te des 
que e e conci inte 
de p d i ie ile 
den i de den tess  
pous e aunr e s qucities 
en e que e 
le 
à 
R
i 
e 
 
L BI5 
 
u 
d 
 
 
de la 
i 
u
i 
D
 t 
i ? A
dt 
en date du Mardi
vigthui, Mai mil
huit cent soixante dix 
huit
renvoi approuvé f 
Cite 
Lecc 
lagettre 
au Trésor Public pour le papier timbré de la présente minute, 
conformément à la loi du sent aon mil huit cent cinquante 
once non compris le cout du présent jugement, la signification 
t vu les articles 435 du code de 
d’icelui et ses suites. 
procédure civile, 27 et 42 du décret du onze juin 1809epou 
signifier à la défederese le présent jugement, commet coompar 
l’un de ses huissirs audienciers. Ainsi jugé les jour, mois et an 
que dessus. 
csoffe 
Lecucq serétaire 
r Audit jour treize Puin
ntre Monsieur Jeun craderse, ouvrier tailleur d’habit 
demeurant et domicilié à Paris, rue des lus, numéro vingt 
six ; Demandeur ; Comparant ; D'une part ; Et Monsieur 
restivens aitre tailleur dhabits, demeurant et 
domicilié à Paris, rue Poinq hronoré, numéro cent trente cent 
Défendeur ; Défaillante ; D'autre part ; Point de fait - Par 
lettres du secrétaire du Conseil de Prud’homme du Département de 
le seime par l'industrie des tissus Clavarie fit citer Thristions  
à comparaître par devant le dit conseil de Prud’homme s u
Bureaux Particuliers le endredi trente un Moai mil huit cent joiante 
dix huit pour se concilier si faire se pouvaits sur la demande 
qu’il entendait former contre lui devant le dit conseil en paiement 
de la somme de neuf francs soixante centimes qu’il lui doit
pour sitaire. A l’appel de la case Claverie se présenta 
et en posa comeudessus. Da santoté Thristinent le 
présenta, reconnt devoir la somme réclamée et ditedée 
refuserà la payer percé que le demandeur qui le tavait et 
nevent pas travailler le vingt sept moi, ce qui lui daute cux 
préjudice dont il lui dux réparation . Pur l’avis de Bureaux 
Particulier que Claverie ne tait pas tenur à le prévenir puitque 
dans cetteindustrie les parties se peuvant quitter non emrent, 
chaque jour mois encore à toute hence du jour Thistian 
plauis qu’il paierait le même jour au jour. Cette poé r 
ne fut pas cence et la cause fut renvoyée devant le oudeil qun le 
du conseil ting le jendi trige suin mil huit cent prse 
et 
aors 
huit. Cité pour le dit jour tenze juin par lettre du secrétaire 
du conseil en date du onze jun mil huit cent soixante dix huit 
à la réquate de Clavarie Boristios ne comparut pas. A 
l’appel de la cause Claverie se présenta et conclut à ce qu'il plut 
ai Bureau Général du Conseil donner défaut contre christianne 
non comparant ni personne pour lui quoique dument appelé et 
pour le profit le condamner à lui payer avec intérêts suisin 
la loi la somme de neuf francs soixante centimes qu’il lui doit pour 
prix de travaux de son état, plus, cette indnité qu'il plaira 
au Conseil fixer pour perte de temps devant les Bureaux du Conseil 
et les dépens. Point de droit - Doit-on donner défaut contre 
Cpristios non comparant ni personne pour lui quoique dument 
appelé et pour le profit adjuger au demonder les conclusions par 
lui précédemment prises ? Que doit-il être statué à l'égarddes 
dépens ? Après avoir entendu Clavèrie en ses demander et 
conclusions et en avor délibéré conformément à la loi ; Mttendu 
que la demande de Claverie en juste et pendée ; que d'ailleurs 
elle n'it plus contestée par Thiestions non comparant en
personne pour lui quoique dument appelé ; attendu que 
christions à fait perdre du temps à Claverce devant les 
Bureaux du Conseil, ce qui lui causà un préjudice dont il le 
dtoit réparation ; Par ces motifs – u Bureau Général 
jugeant en dernier ressert ; Donne défut contre Threstinent 
non comparant ni personne pour lui quoique dument appelé 
pudivant srapadte ie it conte e s se 
à payer avec intérêts suivant la loi à Aluveice la somme de 
du our per te ur i 
peame et oue pe u feimen 
e e te e e lu e e te ene le 
puir pitite de e de le u oneil 
de te u eu e e pour 
ete por di enie e ep 
paire u en e eue 
de  e u de s ds pomn 
e e re eur 
rei eus eu euppres s 
dedit en gu denmenenr es 
lngifle
Leng puilite 
De
uns 
 
Audi jour treize juin
Entre lesioer Moolphe Veedorn, ourier cappren 
demeurant à Paris, rue Saint Vinolas, numéro vingt ; Demandeur ; 
comparant ; D'une part ; Et Monsieur Besanvalleti 
Matre la papser demeurant et domicilié à Paris, rue Car 
lassal, numéro dir sept ; Défendeur ; Comparant ; audre 
part ; Pint de fit - Par lettres du secrétaire du Conseil de 
Prud’homme du Département de la Seme pour l’industrie du 
tissus en dates des Vendi six et vendredi sept juin mil huit cent 
soixante dix huit Tinesche fit citer Besenvelle à comparautre 
par devant le dit conseil de Prud’homme séant en Bureaux Particuliers 
les vendredi sept et Mardi onze juin mil huit cent soixante dix lui 
pour se concilier si faire se pouvait sur la demande qu’il entendat ferme 
contre lui devant le dit conseil en paiement, de la somme de vingt trei 
francs qu’il lui doit pour sétaire. Besonvelle n'ayant pas 
comparu la cause fut renvoyée devant le Bureau Général du Conseil 
séant le jeudi tronze juin mil huit cent soixante dix huit. Cité 
pour le dit jour tenze juin par lettre du secrétaire du Conseil en 
date du huit in mil huit cent soixante dix huit à la réguate de 
mésche Besavalle comparut . A l’appel de la cause 
oesch se présenta et conclut à ce qu’il plut au Bureau 
Général du Conseil condamner Besonvelle à lui payer ave 
intérêts suivant la loi la somme de vingt trois francs quil lui 
doit pour salaire et ce condamner aux dépens. Let ontile 
Tue Beranvelle se présenta et conclut à ce qu’il plut au 
Bureau Général du Conseil attendu que mesch sest présente 
chez lui d’offrant de travailler comme petit ouvrier au pris de 
cinq francs par jeur ; Que fin du premier jour ayant recoune 
qu’il ne savait riern du métier il le lui dix ; qu'il dornt 
lalors et attait de rester pour apprendre ; attendu que donte 
quatre deurs et demi qu’il est reste vrues chne lui a été d'auvie 
atilité il ne lui doit ion ; Par ces motifs — Dire vuce se o 
recevable en sademande, l'en débouter comme nal fonté et
uelle et le condamner aux dépens. Paint, de droit — Doiton condamner 
Besanvelle à payer à Vmesch la somme de vingt trois francs faur 
quatr auns le atin de travail ? Ou bien deuq en diue Muron 
nonrecevable en sa demande, l'en débouter ? Que doit-il tretide 
à l'égard des dépens ? Après avoir entendu les parties en leus demis s 
et conclusions respectivement et en avoir délibéré conformément 
la loi ; attendu qu'il et constent que Mausel a erei e 
chez Besonvelte pendunt quatre jours et deme ; qu'il etre 
trestais que la de pasier sur Besenelle li qi 
 
qu’il ne pouvait préteuve au prix de cinq francs qu’il avaice 
anmancé vouloir gagnet ; mois il ne pent s’ensuivre de 
cette déclaratien qu’il l’apoccupe pour rien ; qu'entenait 
compte de la faiblesse de l’ouvrier le Consil en d’avis qu’'il lui 
huit, paye deux francs pour sable ; Par ces motifs - Le 
Bureau Général jugeant en dernier restort ; consanné 
Besanvalle à payer à Arésch la somme de douze francs 
et déboute Nresche du surplus de sa demande, condmne 
Besonvalle aux dépens toxés et liquidés envers le demandeur à la 
somme de un franc, et à celle due au Trésor lablic, pour le 
papier timbré de la présente minute, conformément à la loi du 
sept aout mil huit cent cinquante, ence, non compris le cout du 
présent juement, la signification d’icelui et ses suites. Ainsi jugé 
les jour mois et an que dessus 
Magalli 
Lecucq secrétaire 
usi jour crige liuigz e
 
Entre Monsieur Auguste Flessis) ouvrier cordonner, demoirant 
à paris, rue Camada, numéro six ; Demandeur ; Comparant. 
mpat ; de te sour lat tort e te se 
demeurant et domicilié à Paris, rue du faubourg sant Denis, 
numéro cent quatre hnet dix ; Défendeur ; Comparant par la 
sieur Charles, son contre maitre, demeurant chez lu aux 
dur aupere te e p sensen e gue 
qurele pent e en der due deux 
pu i l époed det pr e ene e 
laqerant peredin e p semis 
ver qua e dit ede de ur 
e aun e t ue e ete 
mr de emen u e 
ente t de de poer 
de i e se e e e e 
a i e t ut ue 
a e e ceties 
eun esen ser 
an den prs ement 
la 
elles 
et 
se 
eoer
t
ls 
la 
cis 
D
MM  
 
it 
par lui entenslieu : Boré n’ayant pas comparu la cause fut 
envorée devant le Bureau Général du Conseil ésant le jeudi lui 
juin mil huit cent friquite die huit. Cité pour le dit jours 
seuil mots ragis nuil. 
hraze jun par tettre du sorétaure du conseil ui late des 
onze juin mil fuit cent soente dix huit à la réquite de Fessis 
Dauré compret. A l'appel de la cause Pessis de présme 
Me 
et concut à ce qu'il plut au Bureau Général du Conseil lev 
Recu
in 
Paire à lui payer avec intérêts suivant la loi la somme de 
quarant pours pour li tinie de la semaine de cougé qu'it re 
Gagelle 
lui a pas faipé faire comme d’usage et le condamner ex desote 
Dit son coté Pairé, par Charbes, son mandétaire, se travai 
et conclut à ce qu'il plut au Bureau Général du Conseil 
attendu qu'un règlement apposé dans ses atatien prévant 
les ouvriers qu'ils sne sont pas tenus de donner semaine de 
congé ni admis à le réclamer à l'haure de leur renvoir 
Que ce réclement, lorme entre lui et les ouvriers qui l’ait auqs 
un travaillant eue lui un contret verbal qui detouit l'ata 
du délai congé. Par ces motifs - Dire Flessis non 
recevable en sé demande, l'en débouter comme mal fonné 
celle et le condamner aux dépens. Pint de droit Doit-on 
condamner Pou à payer à Wlessis la somme de quarante 
francs pour lui tnir comtte lu de la semaine de congé qul 
de lui a pas toifré faire ? Ou bien dait-on dire lessis 
non recevable euse demande, l'en débouter à Que doit-il être 
statué à l’égard des dépens ? Après avoir entendu les parer 
en leurs demandes et conclusions respectivement et en avir 
délitéré conformément à la loi ; Attendu quil est constuatre 
 
qu'un réglement d’ételier appectié d'une mineure estensle 
dan Les ateliers de Paute rent les auvriers fibres de la etes 
filemant quoir bounr leursemble et résiprogquement le 
Patron, de les remercier à taite huire ; Que Flesst go 
déclare avoir en conneilsance, de ce recteames est mal fuit 
à demander semere de jongé que le dit réglement avait premes 
out de supfrmer ; Por ces motifs - Le Bureu Général 
jugeant en dernier repert ; Dit Ptessis non recevable 
on sa demande, l'on déboute et le condamne aux dépens 
euversr le Trésor Public, pour le papier timbré de laprése 
minute, cnformément à la loi du sept aout mil huit ent 
conquate, ene, non compre le cout le présente resn,  
cinifation d’ielui et sas sites. ies l u e et 
et anque dessus. 
Vagallie 
Leuicg serétaire 
E
 
Du dit jour Treize suin 
Entre Monsieur Bourlier Moitre, selfer, demeurant 
et domicilié à Paris, rue Harg, numéro soixente 
quatorze ; Demandeur ; Comparant ; D’une part ; Et 
Mlonsieur Bupetit, demeurant à Monierf seize 
et Martre, agissat au nom et comme admins trotur de la 
ersonne et des biens de son fil nneut constant apprenti 
Défendeur ; Comparant ; D’autre part ; Point de fait – 
Pur lettres du secrétaire du conseil de Prud’homme du 
Département de la Senme pour l’industrie des tissus en dates 
des Samedi premier et lundi trois juin mil huit cent 
soixante dix huit. onr lier fit citer comprtits à comparaître 
pas devant le dit conseil de Prud’hommes séant en Bureaux 
Particuliers les Lundi trois et vendredi sept juin mil huit 
Cent soixante dix huit pour se concilier si jaire se pouvait sur la 
demande qu’il entendait former contre lui devant le dit Conseil 
En éxécution de Conventions verbales d’apprentissage au 
paiement de la somme de deut cent francs à titre de deommage 
intérêts. Snpelit n'ayant pas comparu la cout fit rentayée 
devant le Bureau Général du Conseil séant le jour treize jun 
mil huit cent soixante dix huit. Cité pour le dit jour tinze 
Juin par lettre du secrétairée du Conseil en date du huit 
Juin mil huit cent soixante dix tit à la requite de Bouslier 
upept comparut . A l’appel de la cause Bourlier 
se présenta et conclut à caqt il plut au Bureau Général du 
Conseil dire et ordonner que dans le jour du jugement à 
intervenir epabit sera tru de faire rentrer chez lui son 
fils constant et de liy fépir pendant une anné qui lui 
reste à faire sur les trois qui ont été fixées pour la duteeten 
hui pe t p le u en p purt per du 
que cet apprentissage pro at demirrelie résolu ence ces 
condamner cou papit à lui payer avec intérêts suivant la loi 
la somme de deux Cent francs à titre de dommiges-intérêts et 
a en en es por le cande ete de pre 
M
et conclut à ce qu’'il plut au Bureau Général du Conseil attendu 
e de e pe es u es emiene 
a elie e tete de tent  que prse 
e e p e pet snteur 
pus ap an seit eme prl u 
e ene de te rgt u 
oe de e e e e der 
e le e e e ou simie 
 
 
 
’ept moits ragis uiles 
le
Lruch 
llée
Maq
qu 
Monter 
à 
it 
cit 
l'ablig peurant hite l’anée restant à faire à payee une 
somme de trois francs par semane par orter à la nourecter 
de son apprenti qui resta sun père parté ; mois attendu que 
Bourlier ne tit pas le nmouvel engagemez en privent, l’unpo 
des trois francs sur termoils il état en drait de comptes pour pai 
partie de sa nmurriture il se oit forcée de le reprendre ce quil 
fit le vingt un avril dernier non par se sonstraire à l’exécuti 
de t engagement, mois contrant et forcé du six de Bourlier 
Par ce motifs - Dire Bourlier nonrecevable en ses demandese 
l'en débouter ; Le recevoir reconventionnellement demandanm 
dire l'apprentissage de son fils purement etn timplement Vésolu d 
condamner Bourlier aux dépens. Paint de troit — Doit-on dire 
qu’il l ne papit sera tenu de faire rentrer son fils chez Bourtier 
pou cegtiner son apprenpsag par une année se présente, hui 
et fauté par lui de ce faire dire l'appentissage résolé, en cece 
condranner capelit à payer au tra Bourlier la somme de deux 
cents francs à titre de dommages-intérêts ? Ou bien dait in 
recesa toup . Dire Bourlier non recevabl en sa demone 
l'en débouter recevoir Papelit reconv entionnelleet demandes 
dire l’apprentissage doit s'agit purement et simplement résilé 
Que dott-il être statué à l’égard des dépens ? Ae cnneil 
très avoir entendu les parties en leurs demandes et conslutio 
respectivement et en osor délibéré conformément à la loi 
ttendu qu’il est constant que se mineur Ceapetit à 
été placé en apprentissage chez Bourlier pour trnq unne 
avec une rem unération foutature jusse du vingt ferrer  
dernier, mois décendé à jour ogatore pour une 
somme de trois francs par semaine ? que cet apprentissage 
doit recevoir son éxécution bindnet l'année restaut à 
faire sinon loupept payrà une indemnité que le coutre 
qui passate la demet moutères d’apprentisae 
u la somme de sivante Mrnde, cel emil le 
Bureau Général jugeant en premier ressort ; Dit et ordonée 
que dans les quatante huit heures à partir de ce jour 
bapelit sera tenu de faire rentrer sa fils constante 
dut hout lers pour à melle dnier huitquni 
pren a pet pal lui de se fice eui l s o ue 
 et cete de pess ; dit que lets apprentissage se ine 
demeurea résolu lu ce cas, ces anpretis à 
fréret du etit à payer avecintérêts sur n 
lui à Burlier la some de cinquante francs lin 
le domneries intbtle, à cort domne dus ou ln 
ces ux désous tixés et liquidés en vers le demandeur à la 
conme de uni frinc fet à celle die u Trésor Public, pour 
le papier ttimbré de la présente minute, onformement 
à la loi du sept aout mil huit cent cinquante, ence Fa 
compris le cout du présent jugement, la significatione 
d’icclu et ses suites. 
 Ainsi jugé les jour mois et an 
que defaut 
Eas
ptille
Lecusq secrétaire 
Du dit jour trenze Jiin
Entre Monpeur Louis Voubert soupeur d’habités, 
demeurant à Paris, rue de la bente tir numéro 
cinquante ; Demandeur ; Comparant ; D'une part ; 
Monsieur Purant ; confectionneur d’habits, demeurant 
et domicilié à Paris, rue croir des Patits chemps, numéro 
cinquante ; Défendeur ; Comparant ; D'utre part ; 
Dinq de fait - Par letres du secrétaire du Conseil de 
Prud'homes du  Département de la Seine pour l’industrie 
des tissus en dates des Mardi quatre et vendredi sept jaunvier
mil huit cent soixente dix huit Joulert fpt citer sinant à 
comparaître par devant le dit conseil de Prud’homme demil 
au Bureaux Particulier les Vendredi sept et Murdi onze vun 
mil huit censoixmmmte dix fait sur se concilier si faire 
se pouvait sur lu demende qu'it entendait former cente lui 
devant le dit conseil en delai conge domme dusage  
Denait- n’ayant pas comparu la caute fut renvayée devans le 
Bureau Général du Conseil séant le jeudi toize juin mil huit cent 
prbte de li di e le dt es hun sem pr 
lettre du secrétaire du Conseil en date du onte juin nif 
huit Cent sixante dix huit à la reate de outant denans 
aplels; Ail le det dte sudi épaut  
de eue sut an en et oncsue 
que det hui e e e e e qui 
edte pue t e e e fitr 
de de afur l contilous es re de lui poarseue 
udits pos l l se e er pl de 
onos 
 
qnr 
 
i
d  
 it 
fi 
t 
 M
t 
 
de dommages-intérêts et le condamner pans les deur ense 
dépens. De son coé dement se présenta et conclut à ci 
qu’il plut au Bureau Général du Conseil attendu qu’il 
embouche pouler, pour un cop de moins et qu'en sontaires 
le conseisa à cajournée de quatre francs cinquante centimes 
de non con mis sonners le prétend ; qu'il la anpe dé poue 
qu'il étant en capable de bien faire la latijue ; Devait 
motifs ; Dire Soulert non recevablien su semante 
l'en déboiter et le condanner ux dépens. Panit, de droit 
Dait-on dire que Finand sera tenu de recevoir etae 
lui peutert et de lui continuer pendant le reste du mill 
de juin du travail de son état de coupeur d’habits enten 
servant les appointement de cent vingt francs pour le dit 
mois, sinon et faute par lui de ce faire le condamner à payerau 
dit Toulert la somme de cent vingt francs à titre de dammaser 
intrêts ? Ou bien dait-on dire Toulert non recevable  
ses demandes, l'en débouter ? Que doit-il être statué à 
l’égard des dépens ? Après avoir entendu les parties en
lurs demandes et conclusions lespertivement et en avoir 
délibéré conformément à la loi ; Attendu que les ertiusion 
fournier par les parties et de l’examon des lisres de sinant 
ressort aue pubert est resté plus de trois chez simand e 
qualite de counpeur Fhibits ; que quoique payé par à 
comptes il était au mois ;  Qu'en ce cos il devait prévenut 
et être prévene en mois à l'avonce conformément à l’asa 
que reçit cette industrie ; Qrue Finand gur l'a causedie le 
premier juin pour l'avoir préven le dait recavoir chez lui et 
lui doit donner du travail ponstant tout ce qui deste à faire de 
mois de juin en lui payait le mois cen entier ; Par ces mots 
Le Bureau Général jugeant en dernier lepart ; Diit et deuie 
que sinant reuvea chez le toutert et lui donnére de 
travail de son état pendant tait le mois de juin présent vuren 
lui pupara six peurs par semaine pour se prourer du 
travail, sinon et faute par Dinand desa conforméere à Cetée 
décixon ; le condamne dès à présent à payer au dit Poulers le 
somme de cent vingt francs à titre de dommages-intérêts ; L 
condamne dant tous les catux dépens taxés et liquides euvriée 
demandeur à la somme de un franc et à cette due au Trésil 
Public, pour le papier timbré de la présente minute, conformément 
à la loi du sept aox mil huit cent cinquante, ence non comptrian at 
di pepren saqyant, las sopntils. d’calu elestile lure 
les jour mois et an que dessus 
Cle
as
Et
fancq etrétaie 
ce
Audience 
Jeudi vingt Jng mil huit ceut soixante
dix huit 
siégeant . Messieurs 
Dinaud, Péalies de la légion d'honneur, vice Prérident 
du Conseil de Prud’homme du Département de la Seine 
pour l’industrie des tissus, Tiendant l’olndame n 
remplacement de Monsieur Martenval Président de 
dit Conseil, supicté, Esssoy, Larcher Brentry, 
 edhriz ; Pud’hommes assistée de Monsieur 
secrétaire du dit Conseil. 
au ee2 
Entre Madame Veuve Méniar, ourière fauriste 
demeurant à Paris, rue de Belleiille, numéro cent sut ; 
Dumanderese ; Comparant ; D’une part ; L Monseur 
veuve lorne, demeurant et domiciliée à Paris, rue se feubeure 
Pain, Denis, numéro seize, agistant comme administrities induuante 
de la maison pe reprocation et de comme de la dame Tepetème 
se seur ; Défendeur ; Comparant ; D'autre part, Pont de fait – 
Der lettres du secrétaire du Conseil de Prud’hommes de Département de 
la Seine pour l'industrie des tissus en dates deus Vendredi trois et uunti six 
Mai mil huit cent sixunte dix huit la veuve Moron fa citer deuve 
Détorme à coumprraître par devant le dit conseil de Prud’hommes poun 
en Bureaux Particuliers les lundi tese Mardi sept Mai mil huit cent cinute 
aqfis pu t te e s fait puir ulaet cenitie 
entendait former contre lui devant le dit conseil en paiement de la soe 
de seize cent francs pour selde de provision à laison le cinq pour 
aet ur pes et onternspuen luitue 
n’ayant pas comparu la case fut renvoyée devant le Bureau 
prt se e it lu ’enti eil lui et li g tente 
ix lit e liy le e comme e enl enie 
et exposa au Conseil qu'elle entra leiy novembre mil huit cent 
pa t six du e e u elle deie 
u centre tur le su for p u en un u 
ente le te e end es ur 
eu er u prsente ei pu duie soant 
quea lur pons luit de dapres et coutde six 
depoit ce die teuite e it 
r is pas intatire emne ei 
M
jeuve Déferne, de soubité, se présenta et expost au Consil 
qu’il a payé à la veuve Moson chaque semaine le prisd 
les journées à sept francs, ausil a versées plus iaus 
sommes on à compte sur sa provision qu'il offrine et 
de cinq pour cent sur les bénifices et snon sur le prédeunr de 
rntert ; Qu'on fu de compte elle en la débitrice et ne 
sa créancière. ll demandu l’ajournement de la canse par 
prouver son apartion. Le Bureau Général ajournt a caunse 
et chargea l'un de ses memtres de l'examet de l’affaire taut 
au hoint devae de la convention qua ce lui des compte 
Par lettre du secrétaire du Conseil en date du vingt cinqlaie 
mil huit Cent soixante dix huit la veuve Mégron fit citer 
vouve Delarne à comparaître par devant le conseil de drudhom 
séant en Bureau Générall udi six Juin mil huit cent soixante 
dix hui pour t'entendre condamner à lui payer la somme 
de sene cent francs qu’il lui doit à raison la cinq pour leur 
sur le choffe net ses affaires et trentenr retondamner ax 
dépens.. A l’appel la veuve Menin se présenta exposa sa 
 demande et conclut à ce que veuve Vélorne fut condamne Pare 
à lui payer avec intérêts suivant la loi la somme de seize cent 
francs qu’il lu doit jur prousien à cinq ponr cent par les 
fairs et le condamner ux dépens. De son coté vouve 
Détorne se présenta et conclut à ce qu’il plut au Bureau 
Général du Conseil attendu que la veuve Mernan ent 
entrée et restée dans la maison cahitaine au pris de 
sept francs par jour et cinq pour fent sur les bénifices qus 
non boulement it a payé chaque semaine le produi des 
journées, mais à donne des à compte se chiffrans travant 
somme su parienme de brouvaut à celle qu'il erait de 
payer. Par e motifs – Pire la veuve Megrandon 
recevable en sa demande, l'en débouter et la condamner 
aux dépens. Le Bureau Général aourna au vendi tingt 
Juin mil huit cent soixante dix huit. LVeude vingt pui 
pasoue eMleseln seprétite et atirl les quel ls ri ue 
requierant l’adjudicetion. Vouve Desonne ne comparnt 
prs. Pungele soit ; Doit en en demn e l les 
aprsà la veuve Mayor la ome de sixe det faus 
p sitée u ie pair cent ui t d et e t e po 
e liun dit- il letre la seune Mésir mart e ue 
sa demente do rilet tu dit d’ te det fouier 
ou dien . depre euventel e le e e te e 
ce stesouns epadement ; compertienes re le nen 
 
nl 
 
 
d
 
it 
ue 
 
uns 
e 
 
i 
 
o
? De
 
 
unle 
 
 
charge de l’examon de la cause en son rappert verbal  
et en avoir délibéré conformément à la loi ; Attendu que de 
l'audition des parties à la barre du Conseil et de l'axamen 
le membre du Conseil doms à l’effet de voir la comptetible 
 
veuve Lébornée que la demande de la veuve Monrer 
Ee
est juste et fondée. Pur ces motifs - Le Bureau Général 
jugeant en premier repart  ; Condamne vouve Léforne 
à payer à la veuve Mégran la somme de seize cent 
francs qu’elle lui réclame comme soble de pronision 
sur les offaires de la maison capitime du cinq rovenb 
mil huit cent soixante treize au jour de sa sortie de condamne 
en outrer aux dépens taxés et liquidés en vers la demanderesse 
à la somme de deux francs cinq centimes, et à celle due 
du Trésor Public, pour le papier timbré de la présente 
minute, conformément à la loi du sept août mil huit 
cent cinquante, ence non compris le cout du présent jugement 
la signification d’icelui et ses suites. Ainsi jugé les 
jour, mois et an que dessus. 
 maie Gs 
decucq seurétaire 
ps  
Audience
du
Jeudi vingt Juin mil huit cet
soixantre dix huit
iégeannt, V’esseure, 
Marmenval, Thévalier de la férçon d’honnem, 
Présideit du Conseil de Prud’hommes du Département 
de la Seine pour l’industrie du tissus, Larcher, 
iroy, Monnier, Angitour Bonsher 
et, Brccc, Prud’hommes apsistés de Monsieur 
Lecucq secrétaire du dit conseit. 
Cite onjour e ladaune Dinil, 
le sieur Déneltant en son nom personnel que par 
apister et autoriser la dame son étouse ouvrière imentende 
en fleurs artificielles, demeurant ensemble, à Paris, 
rue Maudon, numéro dix huit ; Demandeurs ; 
Comparant ; D'une part ; L Monteur Monnser 
Cordonares, fatricant, en Marchand de fleurs artificielles 
demeurant et domicilié à Paris, rue Levonne, numéro 
vingt un ci devant d actuellement en la même Vile rue 
veuve Saint Eugustin, numéro trente ; Défendeur; 
cotilaut ; tute part ; Pouit dé fit - u cete 
du secrétaire du Conseil de Prud’homes du Département 
de la Seine pour l’industrie des tissus en date du Mardi 
ure dumeil ut retient de lat ingen eile 
prir ies doenver eprte pardaunt le duie 
Conseil de Prud’homes séant en Bureau Particulier le 
qudet ente taen lut du prante se li 
purt on de e pur l pri dn la ee 
ndtedet pour dt deu e e apour 
e te prete enppe es pour laremeitie 
atendur satrit.  e deseur se 
quit ene l prtint e p re u ee e 
un ere n eu de d t seent les 
es pere e la eugu es lomisie 
merdeie l te ent au 
qur du t i l ne e puser 
eun ere e e pre 
que e e quilese 
eu eu e ne qu us  
par doznnée, puis en reps pesjout ? Que le mardi quite 
juin Monnier ayant oulitement, toucédié lal dane 
Denael, alleu s'envu contrainte de lie faire appelé 
devant le Conseil auxfins de dommagesintérêts . De vo 
coté Monnier se présenta et exposa au Conseil qu’il 
est mevait qu'il ait tougédié la dame Denette e 
n’a rappelée qu’à l’éxécution de soncatitrnt on la ntine 
l’erdre de se trouver au mage sin le malie à neuf heureit 
au bien de dix qu'elle parai fait vou loir actopter. Les parties 
 n’ayant pu étée conciliées la canse fut renvoyée devant le 
Bueu Général du Conseil séant le jeudi vingt juin mil huit cent 
suixante dix huit. Cité pour le dit jour vingt juin par lettre du 
secrétaire du Conseil en date du quatorze Juin mil huit cent 
soiante dix huit à la riguite des époux Demalle Monnon 
se présenta et conclut à ce qu'il plut au Bureau Général du 
Conseil attendu qu'il s’auet, de l'interpretation don Tontree 
de travauil, d'une part ; D'autre purt e statuer sov me demantes 
de douze cent francs quand le conseil de Prud’hommes ape 
conmettre que des affaires dant la demande ent inférieure à a 
deux Caits francs ; Attendu querre qu’il a peaiti citre les époux 
senelle devant le tritaral de commerce sont compétent 
pour conmaitre d'une affaire drcette niture et de cette inparan 
Par ces motifs — Le délarant n compétent pour connaitre de la 
demande des époux Denelle les en débouter et les condamner 
aux dépense De leur coté les époux Denelle se présentèrent et 
 conclurent à ce qu’il plut au Bureau Générals du Conseil attendu 
que la dame Denelle nt entrée chez Monnier comme 
louvrière monteuse en fleurs et plarmes,; qu'il s'agit de l'astirfictions 
d'un contrat de travail et de sommages intérêts suins résuller 
de son inexécution ; Que dans l'espèce le choffie ’élové qu 
soit doit être sournis aux juge de prémier onstance ; lu 
d'autre part l'rtion de Monnier devant le tribanel de 
commetce était pas tervenme à celle qu'ils ont formée londe 
lui devant le Conseil ne pert avoir pour etet de chanter la 
rature de la cause qui ost me de retations d'autère 
cotun ; Par ces motifs –; Dre Monnier ontre on 
en sa demande,; l'in dé bouter ; La déclarer, competeur 
ritezer la cause etordonner qu’il sere passe entre du 
outecrtire, de Monner pa être apporé et sar 
le land condamner Mingie aux dépens. Pint de drit 
ait-en Le déclarer en compétait pour lomaitre de la demende 
des époux Denelle nenvayet ceux oi devant que de pit 
luillas 
pex M
M
eth
t 
Crtt
Meth
 
a
tip
 
iis 
parsiaint Memer 
apelus cesier 
Sréger Public 
reaja et un gois 
que eul alporie 
f 
ecu
t 
E
cets 
reuva pprouvé
et
 
 
 
A 
 
 
 
onlr 
it 
 
 
 
Ou bin doit-on se déclarer compêtent retenir la cause, r donmer 
que les parties l’expliquerant sur le faud pour être statué ce 
après avoir entendu les parties en leurs 
que de drait – 
demandes et conclusions respectivement et en avoir délibéré 
Conformément à la loi, sur le éctinctaire de Monnier, 
ttendu qu'il est constant que les demandes des époux 
Dinelle portent sur un contret de travail een conséquences 
que le conseil doit conmaitre de les demandes par être statué en 
premier ressot ; Par ces motifs — Dit Monnier non recevable 
en ses demandes, l'en déboute, retient la cause, or donne que 
les parties s'enpliquerant et que le conseil statuère en suite sar le 
mérité de leurs conclusions. L ce mineur Monnier lertire 
dec lavans faire défaut. de leur coté les époux Denelle 
contneut à ce qu'il plut au Bureau Général du Conseil donner 
défat contre Monnier non comparant ni personne pour lui 
quelque dument appelé et pour le proft le condamner à 
leur payer avec intérêts suivant la loi la somme de douze centes 
pris ur somrmilies de oveiles le suend es le 
condamner aux dépens. tant de dait- Doit-on donner 
défaut contre Monnier non comparait ni personne pour lui 
quoique dument appelé et pour le proft adjuger aux 
du ei tes l l le on lu i pr le e e ir 
purdint lap satié renselen pus à aer 
entendu les épous Denelle en leurs demandes et conclusions 
enier lité la soman l le e t cile 
uente de et e e u pe te per 
qr l le  e la te e pardemne e 
de pr e eme e ser 
conqurate e tie e t ten ene pls 
uint lentete eneit e la pa e psup  
permil suit de ou ge paprd ue 
esun tent de poueme pl 
adi encoemte ceu t e pee 
e ldi en en eu e de e 
en te e le t l so 
que du ene e de ets 
ou ete e t de e tretes 
are e de pr u 
ant pem l les quint e e 
aue des de ou de d cpte 
eu te eu teil se te te e e e e 
ui de cit e n i que es 
paur signifier au défendeur le présent jugement, coent chempu 
 Ainsi jugé les jour, mois et 
l'un de ses huisiers audienciers. 
an que dessus.
aviu àr 
Le cudf serétaire 
 Qu dit jour vingt sing 
Entre Monsieur Mathieu, ouvrie tailleur d’habits 
demeurant à Paris, rue Montranyu numéro neuf ; Demandeur; 
Comparant ; D'une part ; Et Monsieur Bordeau ; Maite 
dailleur d’habits, demeurant et domicilié à Paris, rue 
saint Fonsliqne, numéro deuze ; Défendeur ; Défaillant ; 
Pautre part ; Point de fait — Par lettres du secrétairn de 
conseil de Prud’hommes du Département de la Seine pour l'industrie 
des issus en dates des Mardi quatre et vendredi sept juin 
mil huit cent soixante dix luit Mathieu fit citer Bordeaux à 
comparaître par devant le dit Conseil de Prud’hommes séant en Bureux 
Particuliers les tendredi sept et Mardi onze juin mil huit cent 
soixante dix huis por se couilier si faire se pouvait sur la 
demande qu'il entendaiz former contre lui devant ledit Conseil 
au paiement de la somme de treite neuf francs qu’il lui doit 
pour salaire . Berdeant n'ayant pas comparu la cause fil 
uaagée devat le Burau férére du contile leur Chaid 
vingt juin mil huit cent soixante dix huit. Cité pour le dit jour 
Cti9 por par titre du serétaire du conteil en bité deu 
edte joun seil lus ae pirsate de lait à le ouitel oin 
Bordeair ne comparut pas. A l'appel de la cause Mathieui 
se présenta et conclut à ceu’il plut au Bureau Général du Contail 
donner défaut contre Bordeaus, non comparant ni personne 
pour lui quoique dument appelé et pour le profit le condamner 
à lui ayet anet ntelite suiveant la loi la some de oet 
frncs u’il reste lui devoir nayant donné le cinq juin vinq franc 
 sur lante seuf, Juailell duamité e ssaie e le our 
pis par pertie de temps deuns le Bnraux du contre u 
dépens. Pois de doit - Doit-on donner de fus letue 
coisdevur non comparant ni personne pour lui quoique dunen 
 disqpu epepede on dem t 
prlui pruédement pas à ui dit -il être ain 
 
 
 
A
 
n 
 
ue 
 itée 
s 
 
ils 
à 
us 
4 
t 
 
l’égard des dépens ? Après avoir entendu Mathieu en ser 
demandes et conclusions et en avor délibéré conformément 
à la oi ; attendu que la demande de Mathié parent 
juste et fondée ; Que d'ailleurs elle n'est pas contestée par 
Berdeaux non comparant ni personne pour lui quoique dument 
appelé ; attendu qu’en ne comparaisant pas levant les Bureaux 
du Conseil Berdeaux a causé à Mathie un préjudie de perte 
de temps, ce qu'il doit réparer ; Par ces motifs - Le Bureu Général 
jugeant en dernier rettert ; Du l’article quarante un qu decret de 
onze juin mil huit cnt neuf; partait rislement pour les contils de 
Prud'hommes ; Donne défaut contre Berdeux non comparant 
ni personne pour lui quoique dument appelé ; Et adjugent 
e profit du dit défaut ; condamne Bordeant à payer avec 
fard luvant lelé à Delheu la somede se euf francs 
qu’il lui doit pour sefaire, plus une indmnité de dix francs 
par taups perde qu lendemendet susles e 
liquidés envers le demondeur à la somme de un franc, et à cellee 
eue au Trésor Public, pour le papieer timbré de la présente 
minde, la sorément à l le de q ei que lus luse 
die t ris en ps de de inseunge 
e perte e e s l et le e ile sur 
ec peat t e t on ein e es 
e e e e e ex 
lui de ta t oer etie à largre e esoie 
Mavirn à
et an que des sus. 
Leuig recélaue 
duien en cingq ant 
e dete e e etie 
erenteu eede 
de den ete 
qa den dirt dementies 
de t in e one 
eni e e e te 
an e eri que m an du 
ete de de e eme dete et 
e e ne e e e 
Du dit jour vingt Jeuit 
Entre Monsieur Ematile Charsé 
t 
ouvrier sellier ; 
demeurant à Paris, pusage Bouchardy, numéro treis; 
Demandeur ; Comparant ; D’une part ; E Monsieur 
Balendier, autre prenoor de travaux de silernie, demeurant 
nt 
àParis, rue de tafoyette, numéro deut cent quatante trois ; 
Défendeur ; Comparant ; D’autre part ; ande Monsieur 
crousselle Maite Geltier, demeurant à Paris rue 
de lafayette, numéro cent trenteneuf ; Défendeur ; 
Comparant. Anssi d’autre part ; Point de fait 
ur lettres du secrétaire du Conseil de Prud’hommes du 
Département de la Seine pour l’industrie des tissus en 
dtates des samedi huit Juin mil huit cent soixnte dix 
huit, Charrier fit citer Balegdrer à comparaître par devant 
le dit conseil de Prud’hommes séant en Bureux Particulier se 
le Mardi onze juin mil huit ces soixante dix huit para 
se concilier si faire se pouvait sur la demande qu’il entendait suse 
former contre lui devant le dit conseil en paiement de la 
somme de trente six francs pour indemnité de temps 
perdu. A l’appel de la cause Cérrier se présenta et 
en posa comme à dessas. De son coté Balerdier se présente 
il retonnet qu’après avoir donné du travail pendant 
pate me lonaire à Blerer lu et pes pars  
donner enure il le pa venir Chaque jour de la durane 
mais qu’ayant lui même été abosé par prasselle sonitatron 
e e edemen en le e rdes. 
qu e par l e t s tendes dein et 
dandil prer t ondit u dt lui  
Mai
ente e pr t e lete lun dre dte 
a aon eu e te e les en eur 
pur en t e ne t poir 
rande eu ete deit 
re t e e e et enene 
ape ur e den ant le 
se eue a cem espse 
u deme pur deure 
des oue den drer 
ne e tente 
en e te 
ememre e quele 
ade dente is d 
e te de é doe en 
la 
I5x 
trois mots tayés oils
aux 
Lecuiq se 
 per 
De son coté trousselle se présenta et exposa au Conseil que 
non seulement il n'a pas embouche Charrier mois qu’il ne 
la seulement jamais vu chez lui. Le Bureau Particulier 
fut d'avis que Bulerdier qui reconnails aà avoir causé du 
perter de temps devait payer à Chévrier une indemnité 
qu’il fixa à vingt cinq francs et en qugea charrier à 
retirer sa demande envers trousselle qu’il ne jugea pas 
responsable. Aurrier ayant déclaré persister dans toutes 
ses demandes la cause fut renvoyée devant le Bureau Général 
du Conseil séant en Bureau Général lejoudi vingt Juin mil 
huit cent soixante dix huit. Cités pour le dit jour n dt jue 
par lettre du secrétaire du conseil en date du quatorze ju 
mil huit cent soixante dix huit à la réguite des Chabrel 
Malendier et troup elle comparurent. A lappel de la cause 
Chavrier se présenta et conclut à ce qu'il plut au Bureau
Général du conseil condamner solidairement Balessier 
est trousselle à lui payer avec intérêts suivant la loi la 
somme de trente fit francs pour indemnité de perte de 
temps et les condamner solidairement aux dépens. Poton, 
coté Balesdier se présenta et conclut à ce qu’il plus a Bureau 
Général du Conseil lui donner cite des les erves qu’il fait de 
résiiter à Troussille les sommes auxquelles le Conseil 
turair devoir le condamner envers le demandeur; 
De son coté Trousselle se présenta et conclut à ce qu'il plut  
au Bureau Général du Conseil le déclarer nin respinsibe 
erlemettre pors de causé duns dépens. sont de droit 
doit on condamner Baleydier et ousselle solidairement à 
payer à Charrier la somme de trente six francs à titre de 
réparction du préjudice pu'lils lui cause sienment ; Dot 
on donner cité à chartie de Baley dier des resorves qu’il 
fait de repiter à Troissielle les sommes auxquelles le 
pourris être condamné envers Chavror à Pait on dire 
truflle nou responsible et le mettre trort de couse 
Doit Que doit-il être statué à l’égard des dépens ? Apréès evorte. 
entreavu les parties en leurs demandes et concfusions respectivement 
et en avoir délibéré conformément à la loi ; Attendu qu'el 
qui coucerne troupelle qu’il est acquis aux débité ate
confié à Balegaier de s travaux pour être exécutes à la 
tatte et à des prix détermints ; que le fait davaint 
at refé au dit Belrysier des ouvriers, tefait fit à 
pevier se paut le end re uni tes eunver esprs ut 
deraites de son entrepreneur ; Eonce qui conderiet elgde d 
ls 
 
it 
eute 
 
attendu que charrier à déclaré à la barre du Conseil que 
Ballortier était son aperié de fait et non son patron ; que 
à le dit Balordier était en nom c'est que au seut pollediert 
un local et que s'il touctut une avant part sar les prix 
da façon d’était unsquement pour payer le prix de lalocations 
es pourvoir à de momes dépenses argentes ; Que ce fait 
acaires Charvier devant en déviduellement supporter le part des 
dommages qui rappent la sicé n’a rienc reclaner à 
sa rendier, son tollégène, éttent comme lui ; Par ces motifs  
Le Bureau Général jugeant en dernier réport ; Dit 
Trouselle non responsable, te met purs de cause ; Dit 
Charrier non recevable en sa demande envers Balesodier 
l'en déboute et le condamne ux dépens envers le Trésor 
Publie, pour le papier timbré de la présente minute,; 
conformément à la loi du sept tour mil huit cent cinquante 
ence, non compris le cout du présent jugement, la signification 
d’icelui et ses suites. Ainsi jugé les jour mois et an que dessus. 
Lecuig secrétaire 
eaeveir e  
 Quit jour vingt ing 
Entre Monseur Voiter Decroëq, ouvrier teltiers 
umér tote euf 
demeurant à Paris, rue 
Demandeur ; Comparant ; D’une part ; Et prima 
dendier tipe et pour eleur l 
setie emnt e de e e e ene 
eu qeat eu e e enes 
aeante euper cepits de tu ires 
dune du det inqe etie 
un tre e eteil n deu aeier 
au es de te de t ir 
de e e e e pus 
ser t e e esen 
e pu et em e te r 
aupe por e t es denses 
de e e ie 
hunt e ent u guse 
n e en pur de e is 
Moni
Conseil en paiement de la somme de trente six francs pour 
réparation du dommage qu’il lui a cause n la promettae 
du travail qu’il ne lui donna pas. A l’appel de la cause 
Lecrorqse présenta et exposa comme cn dessus. De son cote 
Baledier se présenta et exposa au Conseil qu'après avoir 
occupé Decroëz afsey régulièrement pouttant une se maner 
il le fit veuir chague pour de la semane suivante pousoint 
chaque jour paureur lui donner du travail qui lui était 
proms par tronpel son Patron mois cellaci ayant 
trouvé à faire faire à plus bas prir, ce qu’il lui destine 
il ne put on donner n'en ayant pas. Pa cemanent Devron, 
repuit la parale pour due que trousselle l'ayant embourset 
chez lui et l'ayant atrepe à Balerdier était responsable de 
pertes de temps qu'il avait éprouvées et demandu la remiselle 
la cause pour le pouvair aitionner. la cause e cet état 
fut renvoyée au vendredi quatorze Juin mil huit cet soixate 
dix huit. Cités pour le dit jour quatorze Juin par lettres du secrétaire 
du conseil à la reguate de Decroëq alesdier et troupelle 
comparurent. A l’appel de la cause Decroëq se présenta 
et affirma que tropselle l’avait embourtie et achrepé à 
Bagdier qui lui donna du travail sont sa rerommendation 
tropselle nia avoir imbouche secroëq et dit même ne 
l'avoir jamais vucher lui. Le Bureau Particulier fut 
d’avis que Baleririer qui resonnat avoir fait perdre à 
Decroëqune sormaine lui promettait du travail devait lui 
payer une indemnité qu’il fixa à la somme de vingt cinq 
francs à il tengagea Decroëg à retirer sa demande en 
ce qui concerne trousselle qu’il ne crot pas respontible 
Decroëy ayant déclaré persister dans ses demandes le 
cause fut renvoyée devant le Bureau Général du Conseil 
séant le jeudi vingt Juin mil huit cent loixnte dix huit 
Cités pour le dit jour vingt Juin par lettres du secrétaire 
du Conseil en date du quatorze Juin mil huit cent faisine 
dixhuit à la requete de secrous Balerdier et pronssill 
comparurent. A l’appel de la cause Decrot à se 
présenta et conclut à ce qu’il plut a Bureau Général du Conseil 
condamner solidairement Balerdier et Trousselle à lui pay 
avec intérêts suivant la loi la somme de trente six francs pa 
réparation du dommage qu’il a éprouve par le chemaie 
de toute une semaine et les condamner solidairement eaer 
dépens. De son coté Bolgdier se présente et conclut égul 
plut au Bureau Général du Conseil lui donne vacte des 
i
 V
ligé
 
 
 
l 
u
resorsesqu’il fait de repiter à tronsselle ces sommes ourouilles age 
le Conseil croirait devor le condamner. De son coté tapelle 
se présenta et conclut à ce qu’il plut au Bureau Général 
dmuaaner 
du Conseil le dire non responsable et le mettre puis de 
causé suns dépens. Point de droit — Doit-on condamner 
Baloudier et troupelle solidairement à payer à Decrocq 
la somme de trente six francs pour indemnité de chamage 
Dait on donner aite à Balegdier des reservis qu’il fait 
eel 
de répete à tronselle les sommes axmelles il aurait été 
condamne ? Dait-on dire trousselle non recevable et le 
mettre pors de cause ? Que doit-il être statué à l’égard et 
des dépens ? Après avoir entendu les parties en leurs demandes e 
et conclusions respectivement et en avoir délibéré conformément 
e 
à la loi ; Attendu en ce qui conterne cropelle qu'il est acqust 
aux déboté quel a confié à Balerdier de s travaux pour être à 
exécités à la taihe et à des prix de termins ; que le 
fait par oupselle devoir adrehé des ouvriers au di 
Wolerdier, refait fut il acquit, ne peut prendre responsable 
en ces ouvriers du fait de son entreprenent ; Cresé qui conterne 
Baleydier; attendu que Decroya déclaré à la berredu 
Conseil que Baley dier était son aprcée et non son datronds 
que e Bales dier était sulen nom ses que sul il pas sidant 
anlocal ; Que si le dit Balesdier aprélevé sur les prix 
de façon une avant part avait portige d’était anquement 
par payer les tavoes et res le paes ue miner 
fournitures ; que ce fuit acquis Decroëq devant in dividuellement 
sapporter la part des dommages qui etteiuent la socete 
vislé rien à réclamer à Baler dier, Sourellegue, attent some 
cinq pet cesmuitte le sire hant puant 
donietrs, Cit ouete pe i quel le demie 
pus lasile, et douden en tent 
demande envers Balendier l'en déboute et le condamne eux 
dus minte deu cite pu e e te 
sa preeane omen desqou 
entererant juige es trer 
uain les p e te sesin ei 
purs lesoul eu ses eu n esi e 
 aerui et
reuig serétai 
L
E
Du dit jour vingtuin 
Entre Monsieur Toales Mégeont, ouvrier Poller demeurant 
à Paris, rue des paissonniers, numéro seize ; Demandeur ; 
Comparant ; D'une part ; E prima elonsieur Bales u 
entrepreneur de travaux de telterie, demeurant à larise 
rue de Lafayette, numéro deux cent quarante trois ; Demandeur ; 
comparant ; D'une part ; Ctecand, elaupeur Trouselie 
Maitre sellier, demeurant et domicilié à Paris, rue de 
Lafayette, numéro cent trente neuf ; Défendeur ; 
Comparant ; causoi d'autre part ; Oout de fait - Par lettre 
du secrétaire du Conseil de Prud’homme du Département 
de la Seine pour l’industrie des tissus en date du samedi 
huit Juin mil huit cent soixante dix Mignoul fit citer Bales,jour 
à comparaître par devant le dit Conseil de Prud’hommes séant en 
Bureau Particulier le Mardi onze juin mil huit cent soixante 
dix huit pour se concilier si faire se pouvait sur la demande 
qu'il entendu former contre lui devant le dit Conseil en 
paiement de la somme de trente six francs pour réparation 
du dommage qu’il lui a cause fir des pertes de temps 
à l’appel de la cause Mogeaul se présenta et exposa 
comme ndessus. De son coté Baleydier se présenta 
et reconnut avoir employé Migeaul opt et régulièrement 
pendant toute une semaine et l'avair fait venir chaque 
jour de la deuxrème ponsant pouvour lui donner dé 
travail son Patron, trousselle, lui en ayant promis; 
mais celui ayant trouvé à faire travailler à des 
prix de duits ne lui on donna pas. Mle moment 
Mégeaul repris la parate pour exposer que troisselle 
l'ayant embouihe et a dripe à Balesdier devait être 
responsable de son chamage .Il demando àe 
l’actionner en responsabilité. La cause en ret état 
fut renvoyée au Bureau Particulier du vendredi 
quatorze du même mois. Cités pour le dit jour 
Balesdier et trousselle comparurent. A llappel 
de la cause Migeail se présenta enposa comme tle 
huit Jain et offerma que trousselle l'avair enlere 
et envoyéet Balesdier avec l’ordre de le recevait, lu 
tronsselle ia avoir embauché le demandeur et décara 
ne l’avoir même jamais ve cher lui . le Bureau 
pParticulier fut d'avis que Balerdier reconnaisaut avor 
fit chener Migesl pendant toute une somane 
le faisant, venir chez lui chaque jour il lui devait suf 
une indemnité qu’il fixa à vingt cinq francs. Wendeg 
Migneul à retirer sa demande envers tronsselle qu’il 
de jugea pas être responsable ; Migeaul ayant 
déclaré persister dans ses demandes la cause fut renvayée 
devant le Bureau Général du Conseil séant le jeudi vingt 
Juin mil huit cent soixante dix huit. Cités pour le dit jour vingt 
Juin par lettres du secrétaire du Conseil en date du quatorze 
Juin mil huit Cent soixante dix huit à la réquate de Migeant 
Balegihier et tousselle comparurent. A l’appel de la 
causé Migeoul se présenta et conclut à ce qu'il plut au 
Bureau Général du Conseil condamner solidairement Balendier 
et Traisselle à lui payer avec intérêts suivant la loi la somme 
de trente six francs à titre de réparation du dommage qu’il 
àéprouvé par du chomage de leur fait et les condamner 
ux dépens. De son coté Balerdier se présenta et conclut à 
ce qu’il plut au Bureau Général du Conseil lui donner cité 
des reserves qu’il fait de répiter à tronsselle les sommes 
ausquelles il croirait devoir le condamner. De son coté 
dmame 
tronpselle se présenta et conclut à ce qu’il plut au Bureau
Général du Conseil le dire non responsable et le mettre 
chers de causé, sans dépens. Point de droit — Doit -on 
condamner Baleydier et trausselte solidairement à payer 
à Mégeanl la somme de trente six francs pour indemnité 
de chemage ; Dait-on donner acte à Balesdier des 
résorves qu’il fit de répéter à Cromptelle les sommes ax 
quelles et arait été condlamné à Dait on dire tropelle 
nun respousible et le mettre port de causé ? Qu ditit 
M
être statué à l’égard des dépens ? Après avoir entendu 
les parties en leurs demandes et conclusions respectivement 
dem anr silé la soméent lelis Meneau 
qu tacane compele e du peour eis el adu 
du travail à Balerdier pour être exenités à la toite eta des 
poar deni, que les devie parsemes  
dit Balesvier, afit fat i prouvé, ne puit le rendre desponsible 
erde emie er dete ntomn cons ant 
ou e e de e le u étire 
de eis e e ite deant e quentu 
pere e e e en es our 
e l e e e e eie 
oefaute m p t de doment paur 
pr le er e et our u parse 
ate nenr deus, ou prme eu 
ene 
t 
it, 
Ai 
devant in deréduillement sapporter la part ses dommages 
qui fraphent la souile n'arien à reclamer à Balais  
son talleque attend comme lui ; Par ces motifs 
Le Bureau Général jugeant en dernier report ; Dits 
Trouft elle non responsable, le met sers de conte 
Dit Mégean nonrecevable en sa demande envers 
Balesdier, son callegue, attent comme, l'en déboute 
ei te condamne ux dépens anvers le Trésor Public pur 
le papier timbré de la présente minute, conformément 
à la loi du sept aot mil huit cent cinquante rence 
non compris le cout du présent jugement, la signuigées 
d'icelui et ses suites. Ainsi jugé les jour mai et en 
que dessus. 
Decraveu  
Leincg secrétaire 
rant 
Du dit jour vingt rnz
entre Monsieur Benort Lébart, ouvrier sellier; 
demeurant à Paris, rue des tctuset saint Martin, numéroi 
vingt trois ; Demandeur ; Comparant ; D’une part ; 
primo ; Monsieur Balerdier, entretreneur de travaiux 
de tellerie demeurant à Paris, rue de Lafayette 
Guméro deux cent quarante trois. Aafendeur comparant ; 
D’autre part ; L serando , Monsieur Trousselle, 
Maitre sellier, demeurant et domicilié à Paris, rue 
de rapayette, numéro cent trente neuf ; Défenreur ; 
comparant ; Quulis d’autre part ; Point de fait — Fri 
lettre du secrétaire du conseil de Prud’hommes du Département 
de la Seine pour l’industrie des tissus en date du farmes 
huit juin mil huit cent soixante dix huit Lebont fit et tere 
Balerdier à comparaître part devant le dit Conseil de Pondi 
hommes séant en Bureau Particulier le mardi onze juin 
mil huit cent soixante dix huit pour se concilier si faire si 
pouvait sur la demande qu’il entendait former cont 
lui devant le dit conseil en paiement de la somme de 
Trentesix francs pour ruparation du dommagef 
lui a causé en le faisoit chamer taute une Lemander 
là l’appel de la cause le bert se présenta et exposa teu 
cidissus. De son coté Balesoier se présenta 
M
exposa au conseil qu’après avor ocupe de bert asser 
régulièrement pendant une se maine quoiqu’il l’ant 
ni occuper de vantage, et que penvant la deuxième 
il l’e fat revinir chaque jour lui promettait de travaul 
comptaut sur la promepe qui lui était chaque jour fuite 
par trousselle, son Patron, et qu’il ne tunt pas ayant 
travé à faire travailler à des près enférieurs à libre 
mement Lebert repis, la pirate pour dire que trousselle 
l'ayant mtauché et adrepé à Batesdier il demandant 
à le citer comme responsable. La cause en ce étt 
fut renvoyée devant le Bureau Particulier du Conseil séant 
le vendredi quatorze Juin mil huit cent soixante dix eup
Cités pour le dit jour quatorze juin par lettre du secréaaire 
du Conseil à la requete de retert Ba lerdior es trousselle 
comparurent.. A l’appel de la cause le bert se présenta 
rappelu la demande envers Balerdier et demandà la 
responsabilité de Troufs elle comme l'ayant embourte et 
adrefié au dit Balégdier , tront selle de son coté de 
présenta et exposa au conseil qu’il n’a mellement 
embaurtie déberr, que de plus il ne l’a jamais vai 
chez lui. Le Bureau Particulier fut d’avis que Balesrer 
reconnaisant avon fait perdre du temps à Le bart lui 
devais une indemnité de vingt cinq francs, et ne trouvait 
pas motif arendre topselle respousable il enqadea 
Le pert à retirer lu demande envers celui à Lebers 
ayant déclaré parsister dans ses derindes la cause fut 
renvoyée devant le Bureau Général du Conseil séant 
de jeudi vingt Juin mil huit cent soixante dix tuit 
Cités pour le dit jour vingt juin par tettres du secrétaire 
du Conseil en date du quatorze juin mil huit cent soixante 
dix huit à la réquite de lebeit Balegdier et trousselle 
Comparurent. A l'appel de la cause le bort se présenta 
et conclut à ce qu'il plut au Bureau Général du Conseil 
condamner solidairement Baley dier e troup elle à 
lui payer avec intérêts suivant la loi la sonme dde
 diu ue e e ue etese t lemit 
prde prur se poer cait on pome es 
en de e en er lsent s senbile 
pl e de du tin le t saou 
Burea Graral du Conseil lui donneraite des réserves 
e l e e pat l domen e le 
deu u demen p te 
s
E 
un mit jusée noff
Ak
Lécule 
Da
le
 
 
 
 
 
it 
it 
 
il 
de son coté, se présenta et conclut à ce qu’il plut au Bureau
Général du Conseil le déclrer non lesponsable et le metir 
rois de cause dons dépiens. Pnt de droit  Doit-on 
condamner Bolesdier et trousselle à payer à Le bers le 
somme de trente six francs pour léparation du tort qu'ils 
lui ont causé par pertes de temps ? Dcit on donevaulle 
Balerdier des reserves qu’il fait de repeter à trons sette 
les sommes aux quelles il pourroy être condamne par la 
Conseil ? Dait-on dire trous selle non responsabelie 
le mettre pors de causé ? Que doit-il être statué à 
l’égard des dépens ? Après avoir entendu les parties en 
leurs demandes et conclusions les pertivement et en 
avoir délibéré conformément à la loi ; aAttendu qu’il est 
acquis eux débots que troufs elle à confié à Baleyier 
de s travaux pour être exeuités à la toche et à des 
prix déteminés ; que le fait par Trouls elle d’avair 
la drepé des ouvriers au dit Balerdier, cefut fit il 
acguis, ne pourrait le rendre resonsable vis à vis de 
les ouvriers du fait de son estrepreneur à leur égard ur 
en ce qui concerte Balesdier ; attendu que Lebert à 
déclaré à la baire du Conseil que Balesdier était son
apraiée et non son patron ; que s'il était seul en norte 
s'est que seul il pepsédat un locat ; que s’il prétéveir 
une avant part sur les prix de façans d'était uniquement 
pour payer les loyers du local et pourvoir au paiement de 
memes dépenses communés ; Que cepuis arquis Lbars 
sta rous à réclamer à Balesidier son allègue ettente 
comme lui par les dommanes qus fraphent l’apruition 
et dont chaque membre dos saphort et la part 
en déveduellement ; Par ces motifs - Le Bureau Général 
jugeant en dernier retsort ; Dit Troulelle non responsiable 
emet lurs de causé, dit de bert non recevable centime 
demande envers Séont Balesdier, l'en déboute et le 
Condamne aux dépens envers le Trésor Public, pour le papiei 
timbré de la présente minte, conformément à la loi du sept 
Aui mil huit cent cinquante, ence, non compris le cout drus 
présent jugement, la signification d’iceli et ses suites. 
Ainsi jugé les jour mois et un que dessus. 
ecrevin e
Lecucg secrétaire 
t
Du dit jour vingt suin 
Entre Mosieur Porcaille à hustive, auvnier tellier, 
demeurant à Paris, rue du Levroge, numéro vingt 
cinq lis ; Demandeur ; Comparant ; D’une part ; Et 
primo Monsieur Bélegoier, ttre preneur de travaux 
de teltere demeurant à Paris, rue de lafigette 
numéro dex cent quatante trois ; Défendeur ; Comparat ; 
Dtautre part ; Et Monfieur Trousselle, Mautre 
ellies demeurant à Paris, rue se lafagete, numéro 
cent trente neuf ; Défendeur ; Comparant tupsi 
d’autre part ; Point de fait - Par lettre du secrétairé 
du Conseil de Prud’hommes du Département de la Seine 
pour l’industrie des tissus en date du Samedi huit 
Juin mil huit cent soixante dix huit Couaille fit citer 
Balesdier à comparaître pardevant le dit conseil de 
Prud’hommes séant en Bureau Particulier le Mardi inz 
juin mil hui cent soixante dix hui pour se concilier 
si faire se paivait sur la demande qu’il entendit former 
contre lui devant le dit conseil en paiemet de la somme 
de trente six francs pour réparation du tort quil lui a 
causé en sefabsant chiner toute une semaine 
A l’appel de la cause ouaille se présenta et exposé cume 
e dessus. De son coté Balesdier se présenta et expos a 
du Conseil qu'il employa Camille pendant toute une 
semaine cpez renulaement quoiqu’il oit pupent étre 
l’oucher plus se le travail avair été plus atondant ; 
Que peudant la deuxième semaine il le ft, veur chague 
jour experait aup chaque pour lui donner du tavail 
quatr tes pour poali que onsele suilie 
ayant trouvé à fare faire à meilleur morché ne lui 
donna rien quoique l'ayant ajourné de jour en 
jour e de le pomd suilte sur la perde 
pour dire que Trousselle l'avant intouthé et ed’russe 
à Véberne dot responsable du fix de ce dernier 
et demande la remise de la cause pour l'actirnner 
dugen teleue en téle dit duites 
appeler Balerdor et truselle devait le Bureau 
de eite e ten ue jugent mis. Cits 
de la cause Suailfe se présenta, repit, sa demandes 
pre d ure der teuit e de de 
ae le e p e des pour 
 de t de pu s uratesan 
se présenta et exposa au Conseil qu’il n’a pi mi 
emboucher souaille un l’adresserà Balendier 
puisqu’il ne l’a samais vu n chez lui ni ailloure 
Le Bureau Particulier pt davis nue puisque Ba les dure 
reconné par avax fait chemer ille il lui devait payer 
une indemnité qu’il fixa à la somme de vingt cinq frntc 
quand à trousselle il qe le sout pas responsable de 
engagea ouaille à abousonner sa demande à seu 
EardeCouaille ayant déclaré persister dans sa demoinde 
la cause fut renvoyée devant le Bureau Général du Conseil 
deant le jeudi vingt Juin mil huit cent soixante dix huit 
Cités pour le dit jour nit juin par lettres du secrétaire du 
Conseil en date du quatorze juin mil huit cent soixante e 
trait à la réguate de souaille Balegotier et Troussitte 
comparurent. A l'appel de la cause coneille se présenta 
et conclut à ce qu’il plut au Bureau Général du Conseil 
condamner solidairemen, Balestier et Coupelle à lui 
payer avec intérêts suivant la loi la somme de trente six 
francs pour répiration du dommaze qu’il a’etrouse par 
le chomage de taute une semiaine et les condamner 
solidairement aux dépens. De son coté Balerdier se présent 
et conclut à ce quilt plut au Bureau Général du Conseil lui 
donner aîte des résorver qu’il fait de résiiter à Troup ete 
les sommes auxocellet le conseil croirair devor le condamner 
proupselle, de son coté, conclut à ce qu’il plut au Bureau 
Général du conseil le dire non respontable et le matte lorr 
de cause sons dépens. Rais de troit — Doit-on condamner 
Baley dier etcroupelle à payer à Ponaille la sommed 
trente six francs pour indemnité de perte detemps ? Dait 
on donner aité à Balesdier des reésorves qu'il fait de régiten 
à trousselle les sommes auxquelles il aurait été condamne 
par le conseil ; Doit on dite trousselle non responsible 
le mettre trors de cause ; que doit-il être statué à 
l’égard des dépens ? Après avoir entendu les parties en 
leurs et conclusions respectivement et en avoir délibéré 
Conformément à la loi ; Attendu que ce qui loncerne Trantelle 
qu’il refert des débats q’’il a confié à Béerdier des travix 
par être faits à la toitre eu des pris determinés ; qqu’dl 
fait pur trousselle d'avoir atreté à Bales dois dit 
ouvriers, le fit fit il acquis, ne pourrait le rendre envers e 
ouvriers responsable du fit de son athetrenent, Cnre 
condame Balepier; atendu que vuille Pittre dou 
e 
i e 
 
d
un u 
 
di 
M
Barre du Conseil que Baleydier était son apaice et nonton 
Patron ? Que si Bales dier était seul en nomc est que seul il 
pasés aun local il que s'il a prélevé sur le prix de façon 
une avant part Wéled anrquement pour payer les loyers du sdif 
local et aussi pourvoir au paiement de meues fournitirer. 
Qu’en ce fait acquis douaille devant en déviduellement lapporter 
de part, des dommages qui atteigent la loceté n’aren a 
réclamer à Balesdier, son colléque, attent comne lui ; Par ces 
motifs - Le Bureau Général jugeant en dernier réport ; n 
ce qui concerne Trouptelle le z non responsable et le methirs 
La cause ; Dit sonclle non recevable en la demande envers 
Baleydier, l'en déboute et le condamne aux dépens envers le 
Trésor Public, pour le papier timbré de la présente minute, 
conformément à la loi du sept aout mil huit cent cinquante, 
ence, non compris le cout du présent jugement, la signification 
d’icelui et ses suites. Ainsi jugé les jour, mois et an que dessus.
Lecucq secrétaire 
avee 
.Ausi jour vint ciniq 
Entre Monsieur Boulain Auelphe, ouvrier letties, demerant 
à Pais, rue beremtf, numéro soixante dix 
Demandeur ; Comparant ; D’une part ;Mosieur 
Bélerdier, entretreneur de travaux dépelarie, de mourant 
à Pain, rue de lafayette, numéro deux cent quarante 
trois ; Défendeur ; Comparant ; D’autre part ; Lt 
se de de sur Consele, rte d e pome 
à Paris, rue de Calayette, numéro cant trente neuf 
Déléjant Consant; Crs ntiepus lit 
ptis u loitete a el e se la e doie 
d entie et euig en de e dous 
elle de onr les su et lu pent  
pes duti lesir le ur de sart some 
prs te e e dueiten e ei 
de la det n tes ponde q ues ute 
a de t e e e aur 
cte eu es e per et en 
urs pus e entre de quele 
causé en le faisaunt chaner tute rne semaine. A l’appel 
de la cause Poulin se présenta et exposa comme et 
dessus. De son coté Balejuer se présenta et exposa 
au conseil qu’il a ccupé Poulain after régulièrement 
pendant une semaine ; qu’en suite I le fis nenir tont 
les jours pousait pouvait lui on donner josqué ce la loi 
était promis par Trousselle, son Patron ; mais que le 
dit Crousselle ayant truvé à faire executer se travoux 
à des pris de tuits le fit, vamement attendre. Pare 
moment Poulain repris ca parale pour dire que 
pousselle l'ayant embauché et adressé à Bas ue
devait être responsable qu’il a subl et demandasue 
la cause fut ajournée, pour le faire appelés E 
respon sabilité. La cause en et état fut ajournée a 
Juudi à vendredi quatorze Juin même mois. Cités 
pour le dit jour quatorze Juin par lettres du secrétaire 
du Conseil les détendeus comparurent. A l'appel 
de la cause Voulain rappela sa demande en vers Bales te 
tterma que Trousselle l'avait embouché et adressir 
li Boleydier son entrepreneur et demandu qu’a 
aison de ces faits il fut tenu comme responsable de 
l'indemnité demande à son entrepreneur. Lé 
Bureau Particulier fut davis nue Balesdier reonnaipa 
avoir fut chomer Poulai pendant tuné semaine 
il lui devait une indemnité qu’il fixa à vingt cinq 
francs ; Quanda tous selle il ne le crut pas repous du 
4 en garea Poulain à aton donner à l’égard de 
Coupelle Poulain ayat déclaré persister dans sa 
demande la cause fut renvoyée devant le Bureau Généra 
du Conseil séant le Jeudi vingt Juin mil huit cent soixante 
dix huit. Cités pour le dit jour vingt juin par lettres du 
secrétaire du Conseil en dates du quatorze Juin mil huit 
cent soixante dix hui à la requite de Poulain Balesjon 
et trousselle Comprrurent. A l’appel de la case 
Poulain se présenta et conclut à ce qu’il plut au Bureau 
Général du Conseil condamner lolidairement 
Boleydor et trousselle à lui payer avec intérêts 
suivant la loi la somme de trente six francs à titre 
de répuration du dommase qu'il a éprouvé par 
Comage de toute cne semaine et les condamne 
solidairement ux dépens. De son coté Baley dox 
se présenta et conclut à ce qu’il plut au Bureau Général 
ufl 
inq mi 
une ; 
l
de
s 
2 
nsil 
 
que e e eu 
A
aeuig de 
i
  
 auls 
? A
 
 
du Conseil lui donner aite des resarves qu’il fait de 
répiter à Trousselle les sommes auxquelles le 
Conseil eurai devoir le condamner envers coulain ; 
trousselle, de son coté, se présenta et conclut à ce qu’il plur 
au Bureau Général du Conseil le déclrer noreomble 
ar responsable envers Poulain et le mettre pers de cause 
dans dépens ? Paint de troit — Doit-on condamner 
Balendier et troups elle solidairement à payer à 
Poulain la somme de trente six francs pour l’indemnitér 
de temps de chamage; Doit on donner cité à 
Bolesdier des réserves qu’il fait de étarter à troupselle 
les sommes euxquelles il pourrait être condamne 
Daitaou dire rousselle non responsable et le mettre 
pors de causé ? Que dit-il être tatué à l’égard 
des dépens ? Après avoir entendu les parties en leurs 
demandes et conclusions respertivement et en csoir 
délibéré conformément à la loi ; Attendu qu’il eit 
acvis ux débetsque troupelle à confié à Bébersi 
des travaux pour être executés à la toitre et à des peis 
de termines ; Qu le fis par trouselle d'avoir accetté 
de onvents à l’atrder, apis les it poup la larer 
le rendre responsable envers ces ouvriers des aitert 
de tete fremte à la dauese enaer tere le der 
ud ue su les tilté le uire su titire 
qe silu e les eu drenl litne tie 
e e e u ei e t fasier 
en lete et eu s l rél se pus on purs seiles e 
prix de façon avant portire ee ne fit qu'inquement 
dur ui lite l e mn pareten cinse 
e lus seli de e en e e le ier 
nu el u te en lu se le em e quie 
ce de l r e t e d n en le eue 
aure eu re sa commnt le 
hur t e e t e de es le 
ent prser e e e 
rde men te ua due 
areden e te es u ant soir 
et prs e e te se poeur 
edt e e l ten u e e aur 
rspu e e eu d er 
edereveure 
demerait u e t u atese  
Leuigerétine 
r Dudi jour vingt Ping fu 
Entre Maisieur Fentane Consegdier, ontretreneu de 
travaux de elterie, demeurant à Paris, rue de Lafay édu 
uméro tent deix cit quarante troit ; Demandeur; 
Comparant ; D'une part ; Et Monsieur Trousselle 
e
Maitre sollier demeurant et domicilié à Paris rue 
de lapaette, numéro cont trente neuf ; Défendeur ; 
Comparant ; D’autre part ; Point de fait - Par lettre 
du secrétaire du Conseil de Prud’hommes du Déurtenent 
de la seme pour l’industrie des tissus en date du moisi 
ouze juin mil huit cent soixante dix huit Baregdier fit 
citte trousselle à comparaître par devant le dit conseil 
de Prud’hommes séant en Bureau Particulier le vendredi 
quatorze Juin mil huit cent soixante dix huit pour se concilier 
si faire se pouvait sur la demande qu'il entendait fermen 
contre lui devant le dit conseil en paiement de la somme 
de cent ving francs pour indemnité de chomage et en quratre 
des indemnités qu'il pourrait être condamné à payer à 
treire, ouvriers qu’il a intrainés dans le même case 
A l’appel de la cause Borerdor se présenta et exposa uu 
Conseil que suivant convention verbale en date du vingt leai 
dernier troutselle s'eit engagé à lui donner les travaux à des 
Conditions de terminées nun seulemeant pour lui mois encore 
pour autant douvriers qu'il en pourrait trouvert ; Que le dit 
Trousselle qui l’accutra pendant une première semaine et 
trure ouvrièers emiboustrés à cet offet trouvant à fuire parer 
ces travaux à des prir reduits de les lui dama tros quoique 
le reit à chaque les demain, ce qui fait qu’il a non veulemre 
perdu son temps ; mois en cole fait perdre clui à d’avriers 
qui lui réclament des dommages-intérêts. De Venntate 
Tronsselle se présenta et exposa au Conseil qu’il ne i 
et Pis 
engage à donner du travail à Bales dier qelan sur etq 
mésure de ses besoins et non a toubon plaisir et par 
quantités, fres ; que s’il n’on donna que peuà la soin det
aue n’était pas propté pour les livraissons à lui avait conseil 
d'ajourner eu qu'encore les apprêts étirent en retird, enta 
cis il n’était tenu à reuni envers Balesdier. Le teueau Particlier 
n’ayant pa concilier les parties la cause fut renvoyée devant 
le Bureau Général du Conseil séant le huddi vingt ui 
mil huit cent soixnte dix huit. Cité pour le dit jour mi 
jui par lettre du secrétaire du Conseil en date du mattrné 
juin mil huit Cent soixante dix huit à la réquitte de iteger 
5 
i 
Il 
u 
 
papelle Comparut . A l’appel de la cause Raleglier se 
présenta et roctut à ce qu'il plut au Bureau Général du 
Conseil attendu que troupel qui devait lui donner du 
ravail pour lu et ses ouvriers l’atupe conte une semane 
lans lui en donner et, chose plus auve l’afit veur chaque 
jour pour le remettre au tendemin ; qu'il ut constant que 
troupelle lui a causé un dommage dont il lui doit réparution 
Par ces motifs – Condaane Tousselle à lui payer avec intérêts 
suivant la loi la somme de cent vingt francs de dommages intérêts 
et le condamner aux dépens. De son oté Ausselle se 
présenta et conclut à ce qu’il plut au Bureau Général du 
Conseil attendu qu'en acceptaut de Balerdier ses frir de façon 
il ne pitvis à vis de lui d’outre eugugement que de la domse 
des travaux à exécuter au fur et a mésire de ses besons et par
quatités qu'il jugerait ; Qu'avit eu le soin de certains erti leuil
les fit coupersappreter et les lui donna le dout et enpaye 
le prix ; Par ces motifs — Dire Ba legdor nonréccvalte en 
sa demande, l'en débouter et le condamner aux dépens. tont 
de droit — Doit-on condamner Troupelle à payer à Walensier 
la somme de cent vingt francs pour l’induniser du temps de couage 
Conquel il l’a otistrent par son fait à Paben dit -ooun dire 
du de e e dt endemnit ourl e e noer 
pet tre s’atué à rare du it àl peur deveir 
de parté à las ler de eu ese prisant ren 
avoir délibéré conformément à la loi ; attendu qu'il est constant 
que Baleydier s'engageant le vingt mai dernier à traviller pour 
da pete de a dis et e dede eseutie de 
di perdéster e ugit du jun cnee 
e pare en n es et foui p en i e 
n lu e t t e deiar sape 
quete u sa teusen de lure leq i 
au e te e des sutie uete sus 
sesg et eu et on e rdeer 
pasden de en de eit 
et an premen a s liur 
eue e e t de tinet  
ent e t pouir 
eni re es quendene 
eu e e per 
 de ietr eu en e e 
Duriern e 
reurq usétire 
1 
Mudience 
jeudi vingt set soug mil huit cent 
sixante dix huit 
siégeant. Dessieurs 
Brand Théalier de la Ségon d’homnler 
dure Président du Conseil de Prud’homme du 
Département de la Seine pour l’industrie des 
E
Tissus, président l’audence en remplacement 
de Monsieur Marienvat, Trerideut 
du dit Conseil imperte Carrony, Léforge
Blanchiz et Margier Prul’hommes 
asistes le Monsieur Lecusq, secrétaire 
du dit Conseil. 
Entre Monseur Ponnet demetant à Paris, 
rue du Boulo, numéro dix sept, agissant à nom 
et comme administreteur de la personne et des biens pe 
la ville mineure Margueite, ouvrière ; Demandeur 
Comparant ; D’une part ; L Monsieur Varner, 
Maitre ailleur d’hotit, demeurant et domicilié à 
Paris, rue Montmartre, numéro cent surante, six; 
Défendeur ; D’éfailleus ; D'autre part ; Poir de fit 
Par lettres du secrétaire du conseil de Prud’homme du
Département de la Seine pour l’industrie des tissus 
en dates des Vendredi sept e mardi onze juin mil 
huit cent soixante dix huit Ponet fit citer Vagrer 
a de sute purie t de le er der 
eu den eil e t e lated n enilier 
Apaus peuise le part quage cenis 
s lit lu e e e e pour 
le atis u t u e it en de por  
aente t u te 
de er ure ue de 
e ten q e suti ee 
de en e e u rceton 
et enie e e prs leus 
deu e de e e de 
ende e e i eu 
de it e n pur 
due cengetie ui nt 
amn 
  
 
 
 
 c 
 
 
 
onor 
is D
o
Paguer fit citer Vlagyer à comparaise pardevant le dit conseil 
de Prud’hommes séait en Bureau Général lendi vingt sixx 
juin mil huit cent sixante dix huit pouir s'entendre condamne 
lui payer avec intérêts suivant la loi la somme de quatre vingt 
deux francs cinquante centimes qu’il lui doit corpris de 
travaux de sa fille Margueute, plus, tette indemnité qu’i 
plasra au Conseil fixer pour perte de temts devant les 
Bureaux du Conseil et reslégiens. A l’appel de la canse 
Mlagner ne comparut pas. De son coté Poinner le 
présenta et conclut à ce qu’il plut au Bureau Général 
du Conseil donner défaut contre Bacirer non comparu 
ni personne pour lui quoique dument appelé et pour ce 
profit reui adjuger le bénifice des conclusions par lui 
préses dant la citation exploit de champrou, tempsede 
enregistré. Point de droit — fait-on donner défaut 
contre Vagner non comparant ni personne pour lui quoique 
dument appelé et pour le profit adjuger au demandeur 
conclusions par lui précédeumment prises ? Que doit il enre 
statué à l'égard les dépens ? Après avoir entendu Poiger 
en ses demandes et conclusions et en avoir délibéré conforméent 
à la loi Mndu que les demandes de oiguet pasursa 
justes et fondées ; Que d'ailleurs lles ne sont pas confestée 
par Vaprer non comparant ni personne pour lui quoique dudent 
l'appelé ; Attendu qu'en ne comparupant pas devant les 
Bureux du Conseil lagner a cause à Vorgney u présender 
de perte de temps, ce qu’il doit réparer ; Par ces mols 
e Buraun enret saint condemner atsort ; Done dépaur 
contre agner non comparant ni personne pour l 
quoique dument appelé ; Et adjugeant le proft du dit dé faute; 
condamne Clagner à payer avec intérêts suivant la loi à 
Peigner la somme de quatre vingt deux francs cinquante cent 
qusil lui dit pour lire de trtix de sa file de ron ins 
plus une indemnisté de neuf francs pour temps perdu d 
condamne en outre aux dépens trrois et ligudés envers le 
demender e lle somme de deuve forens t t e det our 
quante ca timoes avrirs le Treur Publi pour le ppie dut 
tinttrée et l'enreustrement de la citation la sormement 
li de sept du mil hui det oinquante en ces pon tompré  
cted preut oue a cine e te erie e e le 
a de lu dil es pre de lesadeante en te te e nt 
inque liu pe ent lent por prmiene 
pe épourx lunde et hapes. C eer 
jugé les jour mois et an que dessus. 
Lecug secrétaire 
dmanie 
e Du dit jour vingt det sinz
Entre Monsieur et Madame ol cseur Cil 
tant en son nom personnel que pour apsister et autoriser la 
dame son épouse, ouvrière qu'eltière, demeurant ensemble 
les dits éhoux, à Paris, rue sainte Marthe, numéro trente 
quatre ; Demandeur ; Comparant ; D'une part ; Et 
Monsieur et Madame Combier, le sour combraytant
en son nom personnel que pa aister et autoriser la dame 
son épouse Goletrère demeurant ensemble les tits éhout; 
à Pans, rue saint Meur, numéro deux cent dix ; Défndeurs ; 
Défaillants ; D’autre part ; Point de fait - Par lettres du 
secrétaire du Conseil de Prud’homme du Département de la 
erme pour l'indstrie des tissus en dates des lundi dix 
apretende det luis doveil le es purte qui 
du pour Cihl par seur le vaire contie la parte 
par devant le dit conseil de Prud’homme s an Bureaux 
atintes le vrit ei ui t unt t en e ps nil 
huit cent soixante dix huit pour se conclu si faire se pouvait 
son le derentete Cinseur puin ce tersein 
le dit conseil en paiement de la somme de enze francs pour 
purt luit cende l de oere 
e pet e prig ent u ciles 
e de e e eni quent é p 
agfe e e ce t enie are ue 
e et e le ue d  
anter t du e pus 
aen o l ar tente 
ea de te e que 
pu eni e pdseus 
an tere ei u e 
quei e u ne 
apen de unt t eil 
unt e e te 
 
é
 
Bel, plus tette indmnité qu'il plaira au Conseil fixer pour 
perte de temps devant les Bureaux du Conseil et les dépens ? 
Pouix de droit — Doit-on donner défaut contre es époux Conmlarion 
non comparants ni personne por eux quoique dument apelés  
pour le profit adjuger aux demandeurs les conclusions par eux 
précédemment prises ; Que doit-il être statué à l'égardde 
dépens ? Après avoir entendu les époux Bel en leurs demandes et 
conclusions et en avoir délibéré conformément à la loi. Mltende 
que la demander des époux Wel posus juste et fendée; que 
d’ailleurs elle nt'est lus contestée par les époux Comproy non comparut 
ni personne pour eux quoique dument appelés ; attendu qu’en e 
comparusant pas devant les Bureaux du Conseil les époux Compal 
ont causé aux épeus ief un préjudice de perte de temps, le 
qu'ils doivent réparer ; Par ces motifs u Bureau Général jugeant 
en dernier repart ; au tarticle quarante un du décret du oize quigt 
mil huit cent neuf, portant réslement pour les conseils de Prud’homme 
Donne depau contre les époux Cambror non comparant ni personne 
pour ux quoique dument appelés ; Et adjugeant le profit du dit 
défaut ; condamne les époux Cambray à payer avec intérêts 
suivant la loi ax époux Rel la somme de ente francs u’il 
leur doivent pour prix de travaux de la dame Gel, prusane 
indemnité de quatre francs pour temps perdu ; Les condamne r 
outre aux dépens taxés et liquidés envers les demandeurs à la soe 
de un franc, et à celle due au Trésor Public, pour le papier timbré 
de la présente minute, conformément, à la loi du sept aout mil 
hui cent cinquante ence, non compris le cout du présent jugement, la 
signifiecction d’icelui et ses suites. Et vu les articles 435 du Codede 
procédure civile, 27 et 42 du decret du onze juin 18e9, pour 
signifier aux défendeurs le présent jugement, conmet. Chompran, 
l'uin de ses huisiers audienciers. Ansi jugé les jour mois et un 
que dessus.
Jeurg iétre  Dineanx 
Ausit jour vingt cent Sing a 
ntre etenser ertordaure Polarée e sour Colrs le e tr 
omparent u por apales la deme due part mire par 
dom e de e es u e si ete i e 
vingt ; Demandeurs ; Comparant ; D’une part ; E Machenr 
Madame Gorguard, le sour Torgardtant en son nom 
personnel que pour apister la dame son épouse fabricnte de fleurs 
artificielles, demeurant et domiciliés à Paris, rue des deux torter 
sant Jonveur, numéro vingt huit ; Défendeurs ; Défaillant ; 
D’untre part ; Point de fait - Par lettre du secrétaire du Conseil 
de Prud’hommes du Département de la Seine pour l’industrie des 
Tissus en date du Lundi dix sept Juin mil huit Cent soixante dix 
huit les époux Citer pres citer les époux soiguard à comparaitre 
par devant le dit Conseil de Prud’hommes hui en Bureau Particulier 
le mardi dix huit juin mil huit cent soixante dix huit pour se concilier 
si jaire se pouvait sus la demande qu’ils entendaiens former contre 
eux devant le dit Conseil en paiemet de cent quarante trois francs 
qu'ils leur doivent pour prix de travaux de la dame Citer. A
l’appel de la cause les époux Piter se présentèrent et exposstant comme 
dessus. De on coté orgon se présenta et reconnait devant 
la somme déclamée et fis, l’engagement de la payer par quinze 
francs le vingt deux Juin et ingt francs tant let quinze jurs, le 
saqmedi à le vingt deux juin les époux Gergmard ne poyeant prasut 
la causé en cet étot fut renvoyée devant le Bureau Général du Conseil 
séant le Judi vingt sept i mil huit cent soixante dix huit. Cité 
pour le dit jour vingt sept juin par lettre du Ginétaire du Conseil 
en date du vingt cinq juin mil huit cent soixante, dex luis 
pur tarout de l qurt coitit e gore demin 
ne comprmes pour e e l er le eoue 
Citer se présentèrent et conclurent à ce qu’il plut au Bureau 
prte se tarat sui u ent le genent 
qu ante pre e onden s 
se per e rf te la deme e lem ent ene 
e pe t e pu ile 
ou e e te e our 
eren e e eux 
aun e et en etite 
eende re de u tegen de 
au per t pu en en pesr 
audepeur a e coux 
e e e es 
e dem e de en uete 
etant a toemnité 
apee es de demnet 
que den esente 
eupre en eneique 
n 
Ees 
 
E
D
 
i 
ui 
is 
in 
e 
M
t 
ni personne pour eui quoique dument appelés ; Attenda que 
e
les époux Forshgard ont fait perdre du temps aux chaue 
iter devant les Bureaux du Conseil, ce qui leur cantei 
préjudice dont ils leur doivent réparation ; Par ces motis 
Le Bureau Général jugeant en dernier report, ou bartule 
quarante un du décres du onze juin mil huit cent neufer 
portant regument pour les conseil de Prud’hommes ; Done 
défaut contre les époux Gorguard non comparants ni 
personne pour eui quoique dument, appelés ; t adjugeant le 
profit dedit défaut condamne les époux Lorguiarda 
payer avec intérêts suivant la loi ax époux Céter la some 
de cent quarante trois francs quits leur doivent pour prix 
de trevux de la dame Poter, plus une indmnité de deux 
trancs pour temps perdu  Les condamne en outre aux dépens 
taxés et liquidés envers les demandeurs à la somede suivait 
cinq centimes, et à celle due au Trésor Public pour le papier 
timbré de la présente minute, conformément à la loi du sept coute 
mil huit cent cinquante, ence non compris le cout du présent 
jugement, la signifiation d’icelui et ses suites. Et vules 
articles 435 du code de procédure civile 27 et 42 du secret 
du onze juin 1809, pour signifier aux défendeurs le présent jugement 
Comet Chompron, l'un de ses tempsiérs audienciers a 
jugé les jour mois et an que dessus.
Lecucq térnétaireammeuoil 
 Du dit jour vingt sejt Puinu 
Entre Monsieur Charles Fétemett, ouvrier tordonner peurs 
à Paris, rue Paint souveur, numéro vingt huit ; Demanders 
comparant ; D'une part ; Et Monsieur Gavail, Mntre 
cordonnier, demeurant et domicilié à Paris, rue Vayevir 
numéro quatre ; Défendeur ; Défaillant ;D’autre hart ; ores 
de fait – Par lettres du secrétaire du Conseil de Prud'hommes du 
Département de la Seine pour l’industrie des tissus en dates des 
Lundi trois et Mardi quatre juin mil huit cent sixante dix huit 
étemill fit citer Lavail à comparaître par devant le dit conseil 
de Prud'hommes séant en Bureaux Particuliers les mordi quel 
et vendredi sept juin mil huit, cent soixante dix huit pour se 
concilier si faire se pouvait sur la demande qusil entendu 
former contre lui devant le dit conseil en paiement de la somme de 
cinquante deux francs soixante quinze centimes qu’il lui doit 
pour lalaire . Lavail n'ayant pas comprru la cause ft renvayée 
devant le Bureau Général du conseil séant le jeudi vingt et 
ajournée au putte Vingt sept juin mil huit cent soixante six temp 
Cité pour le dit suivant exploif de champron, luipier à Paris, en 
date du vingt un juin mil huit cent soixante dix huit, visé pour 
timbre et enregistré à Paris, le vingt deux Juin mil huit cent soirante 
dix huit, etmitte fit citer ravail à comparaître par devant le dit 
Conseil de Prud’hommes séant en Bureau Général le jeudi vingt sept 
juin mil huit cent soixante dix lui pour s'entendre condamner u 
lui payer avec intérêts suivant la loi la somme de cinquante deux 
francs soixante quinze centimes qu’il lui doit pour salaire plus, 
telte indemnité qu'il plaira au Conseil frer pour perte de 
temps devant les Bureaux du Conseil et les dépens. A l'’appel 
de la cause Lavail ne comparut pas. De son coté schunt se 
présenta et conclutz à ce qu’il plut au Bureau Général du Conseil 
donner défau contre Lavail non comparant ni personn pour lui 
quoique dumet appelé et pour le profit lui adjuger le 
bénifice des conclusions par lui prises dans le citation en plois 
de Champour, lui par enregistré. Poin de troit — Doit-on 
donner défaut contre lavail non comparant ni personne pour lui 
quoique dument appelé et pour le profit adjuger au demandeur 
les conclusions par lui précédemment prises ? Que doit-il etre 
statué à l’égard des dépens ? Après avoir entendu létimelte 
en ses demandes et conclusions et en avor délibéré conformément
à la loi ; Mttendu que la demande sihuett purai juste t 
fondée ; que d'ailleurs elle n'est pas contestée par lavail non 
comparant ni personne pour lui quoique dument appelé ; 
attendu qu'en ne comparipant jes devant les Bureux du Conseil 
Lavail a causé à Btmill un préjudice de perte de temps, ce 
qu’il doit réparer. Par ces motif - Le Bureau Général jugeant 
ente e n comn da d it ente tue 
en lre t par me teu pé de pur e 
pprecit et i el u en es 
pr le oe uet cinre es soir 
de e e senr 
indemnité de nuf francs pour temps pardu ; le condamne 
unr e de tis e d en t lerie se 
épeur la premie ue e er 
i 
uil 
it 
é 
 
franc quire centimes envens le Trésor Public, pour le papier. 
timbré et l’enregistrement de la ctation, conformément à la loi  
sept aoux mil huit cent cinquante, ence, non compris le cout du 
présent jugement, la signification d'icelui et ses suites. t 
vu les articles 435 du code de procédure civile, 27 et 42 du 
decres, du onze juin 18099 pour signifier au défendeur le prémier 
jugement , Comme Rompi, l’un de ses huipars audienir 
Aiusi jugé les jour moois et an que dessus. 
Deneur  
Lecuuq secrétaire 
Audit jour vingt set sinig
Entre Mademoiselle Maria Lesqune, ouvrière brodeute 
demeurant à Paris, rue Mongé, numéro cent huit ; Demandeur ; 
comparant ; D'une part ; Et Monsieur es Madame Coureille 
le sieur Courelle tant en son nom personnelque pour apristedt 
autoriser la dame son épouse brodeuse, demeurant et domecili 
onsemble, les dits époux, à Paris, rue du faubourg Montmestre 
numéro vingt huit ; Défendeur ; Comparant ; D’autre 
part , cn de fait - Par lettres du secrétaire du Conseil de 
Prud'hommes du Département de la Seine pour l’industrieu 
Tissus en date des Jendi vingt trois et vendredi vingt quit 
Mai mil huit cent soixante dix huit la demoiselle Lequine 
fit citer les époux Courcelle à comparaître par devant lesdit 
Conseil de Prud’homme séant en Bureaux Particuliers les Vendredi 
ving quatre et lundi vingt sept Mai mil huit cent saixante 
dil huit pour se concilier si faire se pouvait sur la demande 
qu’elle entendai former contre eux devant le dit Conseil en priéement 
de la somme de vingt cinq francs pour prix de travanx le trocdie 
A l'appel de la cause le vingt sept Mai les époux Conreil 
n'ayant pas comparu le vingt quatre la demoiselle Lguran, 
se présenta et exposa cou eu deseus. De leur Cite 
les époux Conrielles se présentèrent, drens que le prix du Gravail 
bien la trit de six francs contigusiéte mal fit et rendrer 
lardivement entendaus ne rien payer. La demanderees 
épara que son travail était nreplachablement faist 
LBureau Particulier veulent être fixé sur le mérite du Traral 
et sur sa valeur Vénble renvoya la cause à V’eroameil 
 
t 
 
 
qu  
Di  
i
membre du conseil à ce comaitsant. Les parties n'avant pu 
ctre conciliée, la cause fut renvoyée devant le Bureau Général 
du Conseil séanz le jeudi vingt sept juin mil huit cent soixante 
dix huit. Ctés pour le dit jour vingt sept Juin par lettre du 
secrétaire du Conseil en date du vingt un juin mil huit 
cent loixante dix huit à la réquete de la demoiselle Lequine 
les époux Courcelles comparurent. A l’appel de la cause le demoiselle 
Semine se présenta et conclut à ce qu’il plut au Bureau Général 
du Conseil condamner les époux Courcelles à lui payer avec intérêts 
suivant la loi la somme de vingt cinq francs pour travaux de 
broderies et les condanner ux dépens. De leur coté les époux 
Courcelles se présentèrant et conclurent à ceguil plut à Bureau 
Général du Conseil attendu que la semoiselle Legune lui arendu 
un travail mal fait et en acceptable par la loie qu'elle a employée 
Qu'on tant cas, ce travail fut il acreptable le prix n’envaidrauit 
que six francs et non vingt cinq ur ces motifs — Dire la 
demoiselle Leonne non recevable en sa demande, l'en débouter 
comme mal fondée en celle et la condamner aux dépens. Paint 
de drot — Doit-on condamner les époux Courcelles à payer 
dà la demoiselle Léquine la somme de vingt cinq francs pour 
travaux de brochrre ? Oubion doit-on dire la demoiselle Lequine ; 
onrécemble en ses demandes, l'en déboutes ? Que doit-il être 
statué à l'égard des dépens ? Après avoir entendu les parties 
en leurs demandes et conclusions respectivement et en avou 
délibéré conformément à la loi ; attendu qu’il est constant 
que la soie employée e travail de brodrie qui fait l'objit 
du titige est nne soie mathinte ; Mais attendu que cet 
en couvenments pouvait être cité par ls épous Courcelles en 
chaisissant et fournipsant et mimes la toie du bien don 
taser le soin à l’ouvrière qui ne puit certeimeent par 
avoir la même, connailsence qu’eut et oui ne doit pas enconser 
une semblabli responsabilité ; attendu eu ce qui donverne 
le prix du travait qui le Conseil qui à legilement quce lavrier 
d’apprenition bestime à due francs , Eor ces motifs – Le 
dpen r te jour an deuer t s lu sante n 
lanre l eu ile u tete e a lire 
alrende i7 pui qu pae t sux dte ies 
d dit den it e mne tende a pent 
dou qeme lrsae t de e e ter deu deniont 
a deme tep le mrdt pure u la tie 
u e e d pumnt 
ape et esprqute enet 
non compris le cout du présent jugement, la signification d'icelui 
Ainsi jugé les jour, mois et an que dessus. 
et ses suites. 
derurg seuctaire  Dnaurlte 
du dit jour vingt sept sing 
Entre Monsieur Dauté, fabricans de tissus, demeurant 
Qu 
et domicilié à Paris, rue saint paire, numéro vingt 
Défendeur au tirmipal ; Demandeur appotant ; Comparaite 
D'une part ; Et Madame Délatiace, ouvrière, demantant 
à antenit le Maudouin oise ; Demanderesse au princépar 
Défenderesse ; opprosent ; Comparant ; D'autre part ; Paint le 
aux termes d'un 
fait- Par jugement rendu par défaut par le conseil de Prud’home 
pouvoir sons devante 
du Département de la Seine pour l'industrie des tissuste neouf 
pasé en date du 
anthuil le 
houdun, du Ma mil huit cent soixante dix huit, enregistré ; Dendé à étter 
premier Mi mil condamné à payer à la dame Défatrace la somme de trois cent 
huit cent soixante soixente quinze frons de princépat et les accilsire: d jugement 
dix huit, enregttrée fut Simifié à Fandit par exploit de Ginaad, uipier ; en dit 
reuve epreuvdu dix neuf Juin mil huit cent soixante dix huit, enregistré à la 
ruguite de Dauilé. suivant en péloit de cham prin huipare 
à lain en date du vingt Juin mil huit cent soixante dix traid 
udisé pour titre elenregistré à Paris, le vingt un juin mil 
Devai
huit cent soixante dix huit, détes deux francs quinze centimes spur 
de Siuilles ; Daudé forme apposition à A jugement et Citre 
les époux Délahacé à comparaître par devant le dit Conseil de 
Prud’homme dént en Bureau Général le jeudi vingt sept juin mil 
huit cent soixante dix huit pour le voir de cevoir opposant à le gugeme 
le voir, de charger des condamnations contre dux prosoncées en 
principal et accepaires, s'entendre lus dix époux Délalice 
déclarer non recevable eu leurs demandeu, les en débouter; Cone 
mal fondés en celles, et s'entendre condamner ux dépens de la 
première comme de la présente inistance. A l’appel de la cause 
Dande se présenta et conclut à ce qu’il plut au Bureau Général 
du Conseil attendu que la dame Délahayé n’et pas auveiit 
dans l'acaption du mot ; qu'elle entreprenense de travaux de 
fet execiter par dtentres ouvrières à Monteuil le hondamne 
ses ouirons ; Qu'en outre elle lui achate la soie qu'elle faut 
 
M 
or 
o
I 
 
t 
 
 pour 
n 
et
 
avi les façons et a forfait ; que le conseil n’était pas compateut 
pour lomuitre des différent entre les fabricaute et les entrehreneus 
de travaux à lerhuit, d'ut à tort que les époux Delcheye aun 
près contre lui le neuf Mai dernier en jugement par défaut 
Par ces motifs — Dire le jugent nul et de mil uffes ; 
Ptatuant à mouveu Le déclrer n compétent pour tommêtre 
de la demande des époux Délataye, les renvoyer devant jui 
de droit et les condamner aux dépent de la première comme 
de la présente inistance. De son coté la dame Détatoge 
se présenta et conclut à ce qu’il plut au Bureau Général 
du Conseil attendu qu’elle n’est ni marchon de ni fabricant; 
qu’elle n’a fait avec Dandé auun oite de commerte 
Que si elle ne ceute pas seule les trovuy qui lui sont confiés 
per cmperté à Dau de puisqu'elle en suite responsable 
Bois avis de lui de l'ixeution de cestranailr ; Par ces motifs 
Dire Doudé, nonrecevable en vondéblinatoire, se déclarer 
competent, rétenir la cause et ordonner qu’il sera procéde à 
l’examen de sa demande pour être statué sur le fautà Pants 
de droit - Doit on se déclarer en compétent pour conaitre 
de la demande des éour Délatoye il renvayer la cause 
devaut qui de drait ? Ou bien doit-on se déclarer confétent 
retenir la cause et ordonner que les parties s'entliqueran pour 
être statué au fand. Que doit-il être statué à l'égarddes 
dépens ? Après avoir entendu les parties en leurs demandes ete 
conclusions respectivement et en avoir délibéré conformément 
à la loi ; Attendu que des explications fournies par les partiese 
que la dame Delahoya quoiqu’oupput des ouvrirres 
pour l’éxcution de travaux qui lui roit fournis par dans é 
n’est pas monis vis t sis de cela a une ouvrière prtinee 
du conseil de Prud’hommes puisqu’elle ne furait avei 
le travail que la soce que est é la brodeuse ce qu'et le fle 
au contirier au à la conturière ; que cette fourniture 
et e e r p le con der 
een de préte us e pent  
edue preut dmit lur puastur 
qui de pr e e pit 
edt peu e t e re desenite 
on et du n t ee eis 
de en e de en tur 
ete em e se e seut 
endere e un que lut 
e du l te e nten ingq en 
eit journée par être reprise le jendi quatre puillet mil huit 
cnt seinente dix huit. Ainsi jugé les jour mois et an que 
depis 
Leniig ecrétare L Dunaiof 

```


### 5.3 Contenu textuel après nettoyage de base et ajout des indices de structure logique"

```py
pattern_cesure = re.compile(r"[¬]+[ \n]+")
source_as_text = " ".join(re.sub(pattern_cesure, "", txt_content).split("\n"))
source_as_text = re.sub(r" {2,}", r" ", source_as_text)

source_w_clues = add_courthearing_clues(source_as_text)
source_w_clues = add_case_clues(source_w_clues)

clean_text = force_clean_manusrit_div(remove_extra_divs(source_w_clues))
print(clean_text)
```
Output
```txt
<div type=\"courtHearing\">Audience </div><div type=\"case\">du Jeudi six Juin mil huit cent soixante dix huit. Siégeaient : Messieurs Pinaud, Chevalier de la Légion d'honneur, vice Président du conseil de Prud'homme du Département de la Seine pour l'Industrie des tissus, Lerroy, Larcher, Platiau, Broutin, Godfrin et Meslien, Prud'hommes assistés de Monsieur Lecucq, secrétaire dudit conseil Entre Monsieur et Madame Lablanche, le sieur Lablanche tant en son nom personnel que pour assister et autoriser la dame son épouse ouvrière passementière, demeurant ensemble, les dits époux, à Paris, rue Besfroid, numéro dix ; Demandeur ; Comparant ; D'une part ; et Monsieur Nisson, fabricant passementier, demeurant et domicilié à Paris, rue Rambuteau, numéro trente huit ; Défendeur ; Défaillant ; D'autre part ; Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la Seine pour l'industrie des tissus en dates des Lundi vingt et Mardi vingt un Mai mil huit cent soixante dix huit les époux Lablanche firent citer nisson à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particulier les Mardi Vingt un et vendredi vingt trois Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'ils entendaient former contre lui devant le dit conseil en paiement de la somme de douze francs pour prix de travaux de la dame Lablanche. A l'appel de la cause le vingt trois Mai mil, nisson n'ayant pas comparu le vingt un, la dame Lablanche se présenta et exposa comme cidessous. De son coté Nisson se présenta et exposa au Conseil que la dame Lablanche qui avait entrepris un travail qu'elle devait coudre le vendredi l'obligea à l'aller chercher chez elle le Lendemain samedi ; pour ce fait il entendit, Quoique le travail fut bien exécuté, ne payer que moitié prix de façon . Les parties n'ayant pu être conciliées la cause fut renvoyée devant le Bureau Général du Conseil séant le Jeudi six Juin mil huit Cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt cinq Mai mil huit cent soixante dix huit à la requête des époux Lablanche quatre mots rayés nuls Lecucq Nisson ne comparut pas. A l'appel de la cause les époux Lablanche se présentèrent et conclurent à ce qu'il plut au Bureau Général du Conseil donner défaut contre Nisson non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à leur payer avec intérêts suivant la loi la somme de douze francs qu'il leur doit pour prix de travaux de la dame Lablanche, plus, telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du Conseil et les dépens Point de droit = Doit-on donner défaut contre Nisson non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui adjuger aux demandeurs les conclusions par eux précèdemment prises ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu les époux Lablanche en leurs demande et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande des époux Lablanche parait juste et fondée ; Que d'ailleurs elle n'est plus contestée par Nisson non comparant ni personne pour lui quoique dument appelé ; Attendu que Nisson a fait perdre du temps aux époux Lablanche devant les Bureaux du conseil, ce qui leur cause un préjudice dont il leur doit réparation Par ces motifs = Le Bureau Général jugeant en dernier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf portant règlement pour les Conseils de Prud'hommes, Donne défaut contre Nisson non comparant ni personne pour lui quoique dument appelé ; Et adjugeant profit du dit défaut ; Condamne Nisson à payer avec intérêts suivant la loi aux époux Lablanche la somme de douze francs qu'il leur doit pour prix de travaux de la dame Lablanche, plus une indemnité de deux francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers les demandeurs à la somme de un franc, et à celle due au Trésor Public, pour le papier timbré de la présente minute, Conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement, la signification d'icelui et ses suites. Et vu les articles 437 du code de procédure civile, 27 et 42 du décret du onze Juin 1809, pour signifier au défendeur le présent jugement, Commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et que dessus Lecucq secrétaire </div><div type=\"case\">Du dit jour six Juin — Entre Monsieur Gutz Willer, ouvrier cordonnier, demeurant à Paris, rue de Jouy, impasse Guepin, numéro quatre ; Demandeur ; Comparant ; D'une part ; Et Monsieur Caillier Maître cordonnier, demeurant et domicilié à Paris, rue Notre dame de Nazareth, numéro soixante dix ; Défendeur ; Défaillant ; D'autre part ; Point de fait = Par lettre du secrétaire du Conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en date du Mardi vingt un Mai mil huit cent soixante dix huit Gutz willer fit citer Caillier à comparaître par devant le dit Conseil de Prud'hommes séant en Bureau Particulier le Vendredi vingt quatre Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit conseil en remboursement de cinquante centimes pour pareille somme qu'il a payée au Bureau de placement qui l'adressa chez lui au moyen d'une carte et en celle de cinq francs pour perte de temps. A l'appel de la cause Gutz Willer se présenta et exposa au conseil que s'étant présenté au Bureau de placement pour y demander en carte fut envoyé à Caillier qui ne le reçut pas prétendant qu'il arrivait alors que la place était prises ; or, il résulte d'une clause qui régit les Bureaux de placement que le cas échéant le Patron rembourse le prix de la carte qui est de cinquante centimes et l'ouvrier n'a alors qu'à se retirer ; Mais Caillier s'est refusé à rembourser et fut cause qu'il perdait sa journée. De son coté Caillier se présenta et exposa au Conseil qu'il a en effet demandé au Bureau de Placement des ouvriers cordonniers un ouvrier, mais ce Bureau le lui envoyant près cinq semaines rien détonnant qu'il n'en ait plus besoin au jour de l'envoi. C'est pourquoi il s'opposa au remboursement du coût de la carte. Le Bureau Particulier fut d'avis que Caillier qui reconnut n'avoir pas contremandé la demande au Bureau d'un ouvrier aurait dû rembourser le coût de la caste, que s'étant refusé à le faire il devait les cinquante centimes et une indemnité de temps perdu qu'il fixa à quatre francs. Sur le refus de Caillier de se rendre à l'avis du Bureau Particulier la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt quatre Mai mil huit Cent soixante dix huit à la requête de Gutzwiller Caillier ne comparut pas. A l'appel de la cause Gutzwiller se présenta et Conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Caillier non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de quatre francs cinquante Centimes se composant de celle de cinquante centimes pour le prix d'une carte émanant du Bureau de Paiement et de celle de quatr francs pour indemnité du temps qu'il a fait perdre devant les Bureaux du conseil et les dépens. Point de droit = Doit-on donner défaut contre Caillier non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu Gutzwiller en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande de Gutzwiller parait juste et fondée ; Que d'ailleurs elle n'est plus contestée par Caillier non comparant ni personne pour lui quoique dument appelé ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant règlement po les Conseils de Prud'hommes ; Donne défaut conter Caillier non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Caillier à payer avec intérêts suivant la loi à Gutzwiller la somme de quatre francs cinquante centimes pour remboursement d'un coût d'un carte de Bureau de Placement et indemnité de temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de soixante cinq centimes, et à celle dûe au Trésor Public pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante non compris le coût du présent jugement, la signification d'icelui et ses suites. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire </div><div type=\"case\">Du dit jour six Juin Entre Monsieur et Madame Poitier, le sieur Poitier tant en son nom personnel que pour assister et autoriser la dame son épouse ouvrière brodeuse, demeurant ensemble, les dits époux, à Paris, rue Beauregard, numéro neuf Demandeurs ; Comparant ; D'une part ; Et Monsieur Dieu fabricant de broderies, demeurant et domicilié à Paris, rue Turbigo, numéro cinquante trois ; Défendeur ; Défaillant ; D'autre part ; Point de fait = Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l'industrie des Tissus en dates des Mardi vingt un et Vendredi Vingt quatre Mai mil huit cent soixante dix huit les époux Poitier firent citer Dieu à comparaître par devant ledit conseil de Prud'hommes séant en Bureaux Particuliers les vendredi vingt quatre et Mardi vingt huit Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'ils entendaient former contre lui devant le dit conseil en paiement de la somme de vingt quatre francs qu'il leur doit pour prix de travaux de la dame Poitier. Dieu n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt neuf Mais mil huit cent soixante dix huit à la requête des époux Poitier Dieu ne comparut pas. A l'appel de la cause les époux Poitier se présentèrent et conclurent à ce qu'il plut a Bureau Général du conseil donner défaut contre Dieu non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à leur payer avec intérêts suivant la loi la somme de vingt quatre francs qu'il leur doit pour prix de travaux de la dame Poirier, plus telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du conseil et les dépens. Point de droit = Doit-on donner défaut contre Dieu non comparant ni personne pour lui quoique ce dument appelé et pour le profit adjuger aux demandeurs les conclusions pour eux précèdemment prises ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu les époux Poitier en leurs demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande des époux Poitier parait juste et fondée ; Que d'ailleurs elle n'est pas contestée par Dieu non comparant ni personne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du conseil Dieu a causé aux époux Poitier un préjudice de perte de temps, ce qu'il doit réparer ; Pour ces motifs = Le Bureau Général jugeant en dernier ressort ; Donne défaut contre Dieu non comparant ni personne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du conseil Dieu a causé aux époux Poitiers un préjudice de perte de temps, ce qu'il doit réparer ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; Donne défaut contre Dieu non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Dieu à payer avec intérêts suivant la loi aux époux Poirier la somme de vingt quatre francs qu'il leur doit pour prix de travaux de la dame Poirier, plus une indemnité de quatre francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers les demandeurs à la somme de un franc, et à celle dûe au Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement, la signification d'icelui et ses suites. Et vu les articles 437 du code de procèdure civile, 27 et 42 du décret du onze Juin 1804, pour signifier au défendeur le présent jugement, commet champion, l'un de ses huissiers- audienciers. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire </div><div type=\"case\">Dudit jour six Juin — Entre Monsieur Afchain, (Edouard) ouvrier passementier, demeurant à Paris, rue des cendriers, numéro trente six ; Demandeur ; Comparant ; D'une part ; Et Monsieur Bailleux, fabricant passementier, demeurant et domicilié à Paris, rue d'angoulême, numéro cinquante ; Défendeur ; Défaillant ; D'autre part ; point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la Seine pour l'industrie des tissus en dates des Mercredi vingt deux et vendredi vingt quatre Mai mil huit cent soixante dix huit Afchain fit citer Bailleux à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi vingt quatre et Lundi vingt sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit conseil en paiement de la somme de cinquante huit francs cinquante centimes qu'il lui doit pour salaire et en celle de vingt sept francs cinquante centimes pour perte de temps par la faute Bailleux n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du trente un Mais mil huit cent soixante dix huit à la requête d'Afchain Bailleux ne comparant pas. A l'appel de la cause Afchain se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé et pour le profit le condamne à lui payer avec intérêts suivant la loi la somme de cinquante huit francs cinquante centimes qu'il lui doit pour salaire et celle de vingt sept francs cinquante centimes pour temps perdu et le condamner aux dépens. Point de droit = Doit-on donner défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions pour lui précedemment prises ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu Afchain en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande d'Afchain parait juste et fondée ; Que d'ailleurs elle n'est pas contestée par Bailleux non comparant ni personne pour lu Quoique dument appelé ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant règlement pour les conseils de Prud'hommes ; Donne défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit dépens ; condamne Bailleux à payer avec intérêts suivant la loi à Afchain la somme de cinquante huit francs cinquante centimes qu'il lui doit pour salaire, puis celle de vingt sept francs cinquante centimes pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe au Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement, la signification d'icelui et ses suites. Et vu les articles 437 du code de procèdure civile, 27 et 42 du décret du onze Juin 1809, pour signifier au défendeur le présent jugement, commet champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire </div><div type=\"case\">Dudit jour six Juin Entre Monsieur Zephir Dignoire, ouvrier passementier, demeurant à Paris, Cité Popincourt, numéro six ; Demandeur ; Comparant ; D'une part ; Et Monsieur Bailleux, fabricant passementier, demeurant et domicilié à Paris, rue d'Angoulême, numéro cinquante ; Défendeur ; Défaillant ; D'autre part ; Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en dates des Mercredi vingt deux et vendredi vingt quatre Mai mil huit cent soixante dx huit Dignoire fit citer Bailleux à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi vingt quatre et Lundi sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit conseil en paiement de la somme de cinq cent soixante treize francs cinquante centimes qu'il lui doit pour salaire. Bailleux n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt neuf Mai mil huit cent soixante dix huit à la requête de Dignoire Bailleux ne comparut pas. A l'appel de la cause Dignoire se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de cinq cent soixante treize francs cinquante centimes qu'il lui doit pour salaire, plus, telle indemnité qu'il plaira au Conseil fixer pour perte de temps devant les Bureaux du Conseil et les dépens, ordonner l'exécution provisoire du payement à intervenir, nonobstant appel et sans qu'il soit besoin par lui de fournir caution, conformément à l'article quatorze de la loi du premier Juin mil huit cent cinquante trois. Point de droit = Doit-on donner défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu Dignoire en ses demandes et Conclusions et en avoir délibéré conformément à la loi ; Attendu que les demandes de Dignoire paraissent justes et fondées ; Que d'ailleurs elles ne sont pas contestée par Bailleux non comparant ni \trsonne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du conseil Bailleux a causé à Dignoire un préjudice de perte de temps, ce qu'il doit réparer ; Par ces motifs = Le Bureau Général jugeant en premier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant règlement pour les Conseils de Prud'hommes ; Donne défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Bailleux à payer avec intérêts suivant la loi à Dignoire la somme de Cinq cent soixante treize francs cinquante centimes qu'il lui doit pour salaire, plus une indemnité de six francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe au Trésor Public, pour la papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante ; ence, non compris le coût du présent jugement, la signification d'icelui et ses suites ; Ordonne l'exécution provisoire du présent jugement, nonobstant appel et sans qu'il soit besoin par le demandeur de fournir caution, conformément à l'article quatorze du premier Juin mil huit cent cinquante trois. Et vu les articles 437 du code de procèdure civile, 27 et 42 du décret du onze Juin mil huit cent neuf, pour signifier au défendeur le présent jugement commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire </div><div type=\"case\">Dudit jour six Juin — Entre Monsieur Jules Omnès, ouvrier implanteur, demeurant à Paris, rue Quicampoix, numéro quatre ; Demandeur ; comparant ; D'une part ; Et Monsieur Pagès, Maître coiffeur, demeurant et domicilié à Paris rue de Rennes, numéro soixante cinq ; Défendeur ; Défaillant ; D'autre part ; Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en dates des Mercredi vingt deux et vendredi vingt quatre Mai mil huit cent soixante dix huit Omnès fit citer Pagès à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi vingt quatre et Lundi vingt sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit Conseil en paiement de la somme de six francs cinquante centimes qu'il lui doit pour salaire. Pagès n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt neuf Mai mil huit cent soixante dix huit à la requête de Omnès Pagès ne comparut pas. A l'appel de la cause Omnès se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Pagès non Comparant ni personne pour lui quoique dument appelé pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de six francs cinquante centimes qu'il lui doit pour salaire, plus, telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du conseil et les dépens. Point de droit = Doit-on donner défaut contre Pagès non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions pour lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu Omnès en ses demandes et Conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande d'Omnès parait juste et fondée ; Que d'ailleurs elle n'est pas contestée par Pagès non comparant ni personne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du Conseil Pagès a causé à Omnès en préjudice de perte de temps, ce qu'il doit réparer ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; Vu l'article quarante du décret du onze Juin mil huit cent neuf, portant règlement pour les conseils de Prud'hommes ; Donne défaut contre Pagès non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Pagès à payer avec intérêts suivant la loi à Omnès la somme de six francs cinquante Centimes qu'il lui doit pour salaire, plus une indemnité de six francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe un Trésor Public, pour le papier timbré de la présente minute, Conformément à la loi du sept août mil cent cinquante, ence non compris le coût du présent jugement, la signification d'icelui et ses suites. Et vu les articles quatre cent trente cinq du code de procèdure civile, vingt sept et quarante deux du décret du onze Juin mil huit cent neuf, portant réglement vu d'allemagne numéro cent quarante pour les conseils pour signifier au défendeur le présent jugement, Commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus Lecucq secrétaire — </div><div type=\"case\">Dudit jour six Juin Entre Monsieur Jean Baptiste Richard, demeurant à rue de Flandre, 28 à Paris\tagissant au nom et comme administrateur de la personne et des biens de son fils mineur Victor, ouvrier en apprêts sur Etoffes ; Demandeur ; Comparant ; D'une part ; Et Monsieur Lecrique et Compagnie, Maîtres appréteurs d'Etoffes, demeurant et domiciliés à Paris, quarante trois rue Riquet et rue de Tanger ; Défendeurs ; Défaillant ; D'autre part ; Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en dates des Mercredi vingt deux et Vendredi vingt quatre Mai mil huit cent soixante dix huit Richard, ès noms et qualités qu'il agit, fit citer Lecrique et Compagnie à comparaître par devant le dit Conseil de Prud'hommes séant en Bureaux Particuliers les _ Vendredi vingt quatre et Lundi vingt sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre eux devant le dit conseil en paiement de la somme de sept francs qu'il lui doit pour prix de travaux de son fils Victor. Lecrique et compagnie n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cités pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt Neuf Mai mil huit cent soixante dix huit à la requête de Richard Lecrique et compagnie ne comparurent pas. A l'appel de la cause Richard se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Lecrique et cie non comparant ni personne pour eux quoique dument appelés et pour le profit les condamner solidairement à lui payer avec intérêts suivant la loi la somme de sept francs qu'ils lui doivent pour salaire de son fils victor et les condamner aux dépens. Point de droit = Doit-on donner défaut contre Lecrique et compagnie non comparants ni personne pour eux quoique dument appelés et pour le profit adjuger au demandeur les conclusions par lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu Richard en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande de Richard parait juste et fondée ; Que d'ailleurs elle n'est pas contestée par Lecrique et compagnie non comparant ni personne pour eux quoique dument appelés ; Attendu qu'en ne comparaissant pas devant les Bureaux du Conseil Lecrique et Compagnie ont causé à Richard un préjudice de perte de temps, ce qu'ils doivent réparer ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; Vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant réglement pour les conseil de Prud'hommes ; Donne défaut contre Lecrique et Compagnie non comparants ni personne pour eux quoique dument appelés ; Et adjugeant le profit du dit défaut ; Condamne Lecrique et Compagnie à payer avec intérêts suiva la loi à Richard la somme de sept francs qu'il lui doit pour salaire de son fils victor, plus une indemnité de six francs pour temps perdu ; Les condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe au Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement, la signification d'icelui et ses suites ; Et vu les articles 435 du code de procédure civile, 27 et 42 du décret du onze Juin 1809, pour signifier au défendeur le présent jugement, Commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus Lecucq secrétaire — </div><div type=\"case\">Dudit jour six Juin — Entre Monsieur Bréchot, ouvrier Galochier, demeurant à Paris, rue de Meaux, numéro trente sept, passage de la Brie ; Demandeur ; Comparant ; D'une part ; Et Monsieur Vermérot, fabricant de Galoches, demeurant et domicilié à Paris deux mots rayés nuls Lecucq — </div><div type=\"case\">Dudit jour six Juin Entre Monsieur — Emile Pilloy, ouvrier Bourrelier, demeurant à Paris, rue de Cotte, numéro seize ; Demandeur ; Comparant ; D'une part ; Et Monsieur Prevost père, Maître Bourrelier, demeurant et domicilié à Paris, Montreuil sous Bois, près Paris, rue du Pré, numéro trois ; Défendeur ; Défaillant ; D'autre part ; Point de fait = Par lettres du secretaire du Conseil de Prud'hommes du Département de la seine pour l'Industrie des Tissus en dates des Mercredi vingt deux et Vendredi vingt quatre Mai mil huit cent soixante dix huit Pilloy fit citer Prevost à comparaître par devant le dit Conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi Vingt quatre et Lundi vingt sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit Conseil en paiement de la somme de Cinquante sept francs se composant de trente neuf francs pour prix de soixante dix huit heures à cinquante centimes et de dix huit francs pour travaux à la tache. Prevost n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt neuf Mai mil huit Cent soixante dix huit à la requête de Pilloy Prevost ne comparut pas. A l'appel de la cause Pilloy se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Prevost non comparant ni personne pour lui quoique dûment appelé et pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de cinquante sept francs qu'il lui doit pour salaire, plus, telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du conseil et les dépens. Point de droit = Doit-on donner défaut contre Prevost non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu Pilloy en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que les demandes de Pilloy paraissent justes et fondées ; Que d'ailleurs elles ne sont pas contestées par Prévost non comparant ni personne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du Conseil Prevost a causé à Pilloy un préjudice de perte de temps, ce qu'il doit réparer ; Sur ces motifs = Le Bureau Général jugeant en dernier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant règlement pour les conseils de Prud'hommes ; \tDonne défaut contre Prévost non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Prévost à payer avec intérêts suivant la loi à Pilloy la somme de cinquante sept francs qu'il lui doit pour salaire, plus une indemnité de six francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe au Trésor Public pour le prix timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement la signification d'icelui et ses suites. Et vu les articles 435 du Code de procèdure civile, 27 et 42 du décret du onze Juin 1809, pour signifier au défendeur le présent jugement, Commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire — </div><div type=\"case\">Dudit jour six Juin — Entre Madame veuve Bernard, ouvrière demeurant à Joinville le Pont, près Paris, rue du canal, numéro onze ; Demanderesse ; Comparant ; D'une part ; Et Monsieur Bardin, fabricant plumassier, demeurant et domicilié à Joinville le Pont, rue des Réservoirs ; Défendeur ; Comparant ; D'une part ; Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la seine pour l'industrie des Tissus en dates des mardi vingt huit et Vendredi trente un Mai mil huit cent soixante dix huit la veuve Bernard fit citer Bardin comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi trente un Mai et Lundi trois Juin mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'elle entendait former contre lui devant le dit Conseil en paiement de la somme de vingt deux francs trente centimes qu'il lui doit pour salaire. Bardin n'ayant pas comparut la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du trois Juin mil huit cent soixante dix huit à la requête de la veuve Bernard Bardin comparut. A l'appel de la cause la veuve Bernard se présenta et conclut à ce qu'il plut au Bureau Général du conseil condamner à lui payer avec intérêts suivant la loi la somme de vingt deux francs trente centimes qu'il lui doit pour salaire, plus, telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du conseil et les dépens. Point de droits De son coté Bardin se présenta et conclut à ce qu'il plut au Bureau Général du conseil attendu que la veuve Bernard a quitté ses ateliers sans faire la huitaine d'usage ; Que d'ailleurs le travail dont elle reclame le prix ayant été par elle mal exécuté il ne lui a doit pas la façon ; Par ces motifs = dire la veuve Bernard non recevable en ses demandes, l'en débouter et la condamner aux dépens. Point de droit = Doit-on condamner Bazin à payer à la veuve Bernard la somme de vingt deux francs trente centimes pour travaux de son état ? Ou bien doit on pour les raisons invoquées par Bardin dire la veuve Bernard non recevable en sa demande, l'en débouter ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu les parties en leurs demandes et conclusions respectivement et en avoir délibéré conformément à la loi ; Attendu qu'il est constant que Bardin doit à la veuve Bernard la somme de vingt deux francs trente centimes pour travaux exécutés à la tache ; Que Bardin qui allègue de mal façon de ces travaux ne prouve pas et déclare ne plus pouvoir prouver ce qu'il avance ; Attendu en ce qui concerne le départ subit de la veuve Bernard des ateliers de Bardin que ce départ remonte à quinze jours sans que Bardin ait, depuis, fait la moindre démarche pour retenir ou faire rentrer son ouvrière, ce qui est considéré comme un consentement au moins tacite à son départ ; Attendu aussi que Bardin n'a pas comparu devant les Bureaux Particuliers et a par cette non comparution fait perdre du temps à la demanderesse qui en éprouve un préjudice dont il lui doit réparation par la somme de quatre francs ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; condamne Bardin à payer à la veuve Bernard la somme de vingt deux francs trente centimes qu'il lui doit pour salaire, plus une indemnité de quatre francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers la demanderesse à la somme de un franc vingt centimes, et à celle dûe au Trésor Public pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le vingt neuf mots rayés Comme nuls Lecucq coût du présent jugement, la signification d'icelui et ses suites. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire — </div><div type=\"case\">Dudit jour six Juin — Entre Monsieur Jean Lardet, ouvrier tisseur, demeurant à Gentilly près Paris, rue de la Comète, numéro cinq ; Demandeur ; Comparant ; D'une part ; Et Monsieur Bardin fabricant de tissus en plumes, demeurant et domicilié à Paris, Boulevart Saint Jacques, numéro cinquante un ; Défendeur ; Comparant ; D'autre part ; Point de fait = Par lettres du secrétaire du Conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en dates des Lundi treize et Mardi quatorze Mai mil huit cent soixante dix huit Lardet fit citer Bardin à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Mardi quatorze et Vendredi dix sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit conseil en paiement de primo, cinquante francs pour le salaire de cinq journé pour lui f dix journées par lui lui faites à raison de cinq francs par jour ; Secondo, de vingt deux francs cinquante centimes pour neuf journées à deux francs cinquante centimes du travail de la dame Lardet, sa femme ; Tertio, de celle de sept cents francs restant dûs sur le prix d'une machine par lui faite sur ses ordres. Bardin n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit Cité pour le dit jour six Juin par lettre du secrétaire du conseil en vingt trois Mai mil huit cent soixante dix huit et ajournée au Jeudi six Juin suivant mois. Suivant exploit de Champion, huissier à Paris, en date du vingt sept Mai mil huit cent soixante dix huit, visé pour timbre et Enregistré à Paris, le vingt huit Mai mil huit Cent soixante dix huit, débet deux francs quinze centimes, signé de feuillez Lardin fit citer Bardin à comparaître par devant le dit Conseil de Prud'hommes séant en Bureau Général pour le Jeudi six Juin mil huit cent soixante dix huit pour s'entendre condamner à lui payer la somme de huit cent soixante douze francs cinquante centimes se composant de primo, de cent cinquante francs pour indemnité d'un mois de congé, secondo vingt deux francs cinquate centimes pour solde de journies de la dame Tarset,et de set cents frocs pour prix Et d'une mécunque qui lui a été forrce par lu. A l’appel de la case partet se présenta et conclut à ce qu’il plut au Bureau Général du Conseil lui adjuger le bénifie des conclusions par lui préses dont la citalion exploit de chanmpion, lui près, enregistré et leondamner Bardin aux dépens. De son coté Bardin se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu qu’il n’a pas enfagé Lerder comme ouvrier ; Que s’il et veuu chez lui le st pour y travailler pour son compte à lui Berdin, mois bien pour son profe compte t à une mécuviue qu’il lui a vendue à contition qu’elle fontinse farilement a jec l’arde d’une somme, le qui expligéé commet la dame tordes entre chez lui aménée par son mari qui prétendaint l’employer à cette machine, perte qu'elle duz abondonner le mecanique ne pouvant fonctiner ; attendu ependanit qu à offre payer les vingt deux francs cinquante centimes prodit des neuf journéen de la dine portis oure que celte lore n'oil uis prodtuit ; Par ces motifs - Dire lomn ore vrdit necerlle en ses demandes, l'en débouter et le condamner ux dépens. Pint de troit - Dait-on condamner trédin à payer à erdit la somme de huit cent soirante douze frons cinquate centimes par de la congé d'un mois de torrtet talaire de la dame verder de pretseue ongra parée d’a du et n le que lis ouidiet pur et le e l ue le dtie Lertit non recevableon ses demandes l'endébouter e que de sus te se dui l un deis pus purte le parties en leurs demandes et conclusions respectivement et en avoir et de d pre ille Mrede en in atis ant dene pr e du deu et peur en de e sand eur eu e e de du et en tortie er e e ie purs ser hu tit en e e e a e orer ote e oni e aten u elest prs sn dt lite d e e pusils aeden omi en ent les ei e e e pe e con es les u an e ur de se ete e te dement deprei teuip r et e deds pue an ten pete. pu de u e te de se pu lese qi e E dé a 1835 des vngt deux francs cinquante centimes pour le sataire de la dame Pardr le Conseil n’a qu’à lui en donner aiteor de déclarer en compéteis pour le surplus ; Par ces motifs Le Bureau Général jugeant en premier ressort; donnea à Berstin de l’offre qu’il fait de payer à la jourre du conseil les vingt deux francs cinquante centimes qui lui da réclamés pour salaire de la dame Tardet. Le véutay on compétent pour coumaitre des entrer demandes de tavail qu'il reuvoie devant qui de troit et condamne lar dit u dépens taxés et liquidés enven le Trésor Public, à la soe de deix francs quinze centimes pour le papier timbré l’enregistrement de la citation, conformément à la loi à sept aoux mil huit cent cinquante euce, non compris le cout du présent jugement, la signification d’icelui et ses suites. Ainsi jugé les jour mois et an que dessus. Dineu recucg létaire àQudit jour dix ving a Entre Mademoiselle Delasague, fille majeure, ouvrière demeurant à Paris, rue Sant Blonoré, numéro deux cens cinquante sept ; Demanderesse ; Comparant ; D’une par ; t Monsieur Chaput, Cutotiier, demeurant et domiée à Paris, rue de vanmes, numéro six ; Défendeur ; Défelle Dutre part ; Poit de fat - las tetres du peretire de linre de Prud’hommes du Département de la soine pour l’industrier des tissus en dates des vendredi trente un Mai et lundi treis Juin mil huit cent soixante dix huit la demoiselle Wéleve fit citer chaput à comparaître par devant le dit Conseil Prud’hommes sant en Bureont Particuliers les lundi trois et Mardi quatre juin mil huit, cest soixante dix fait pour se conctamner si fairre se pouvait sur la demande qu'elle se convais frrer contre lui devant le dit Conseil en paiement de la somme de Eix deux frans cinquante centimes qu’il lui doit pour salaré ae chapit l'ayar pas comparala cause pt renvogyée les oise Bureau Général du Conseil séant le jeudi six Juin milhi comparante dix huit. Cité par l dit jour li, pui fil ene MA uts secrétaire du Conseil en date du quatre juin mil huit cent soixante dix huit à la réquite de la demoiselle Delevaique chapat ne comparut pas. A l'appel de la cause la demoiselle delevarque le présenta et conclut à ce qu'il plut au Bureau Général du Conseil donner défut contre chapert non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de trente deux francs cinquante centimes qu’il lui doit pour salaire plus telte indemnité qu’il plaira au Conseil fixer pour perte de temps devant les Bureux du Conseil et les dépens. Pont de droit — Dait-on donner défaut contre chapot non comparant en personne pour lui quoique dument appelé et pour le profit adjuger à la demanderelse les conclusion par elle précédemment prises ? Que doit il être statué à l’égard des dépens ? près avoir entendu la demoiselle Delevaique nss demandes et conclusions et en avoir délibéré conformément à la loi; Attendu que la demande de la demoiselle Dele vaique paroint juste et fondée ; Que d'ailleurs elle n'est pas contestée par Chapnt non comparant ni personne pour lu quoique dument appelé ; attendu qu’en ne comparuisant pas devait les Bureaux du Conseil chapat a causé à la demoiselle de le vaique un préjudice de perte de temps, ce qu'il doit éprur ; Par de pentipe. le Brnveu prat l jugen en nere ps ou larticle quarante un du decres du onze Jui mil huit cent el p re en eu te e réseu donne défaut contre Chapat non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; condamne Chapat à payer avec intérêts suivant e et uidte nonean pen t e sei aun en ant le e e t u ten du pu deit tr n e poir en iq u lu, le da deme t en de e ere du de te u e per de tue nir t u ie s pen p pere lele eseq ei q sar de onier uie u e e de paser e e e de onte et preur ante te u uig cin eit nde er ani e purs de pur eu ait lapre nt endes tis p l jur mit ie risill an que dessus. H demait-entéra A Audit jour dix Juin Entre Monsieur Parcors, ouvrier tailleur, demeurant à Cave rue courne fort , numéro quarante trois ; Demandeur ; Comparant D'’une part ; Et Monsieur Aingélà Maitre tailleur s hatit demeurant et domicilié à Paris, Boulevart saint Martie numéro quatre ; Défendeur ; Comparant ; D'autre paryr e Point de fait - Par lettres du secrétaire du Conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus en dates deu Mardi quatorze et Vendredi dit sept Mai mil huit cent soixante dix huit siçon fit citer Engele à comparaître par devant le dit conseil de Prud’homme séant en Bureux Particu les vendredi dix sept et Vendredi vingt quatre eMai mil suit cent soixante dix huit pour se concilier si faire se pouvait pur la demande qu'il entendait former contre lui devant le dit Conseil en paiement de la somme de dix huit francs pour la façon d’iune vêtement. Sengéli n'ayant pas comparu la cause fut renvayée devant le Bureau Général du Conseil séant le jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six juin per lettre du secrétaire du Conseil en date du vingt cinq mai mil huit cent soixante dix huit à la réguate de Fiçon Angeti comparut. A l’appel de la cause, ficois se présenta et conclut à ce qu’il plut au Bureau Général du Conseil dun déde Angéti à lui payer avec intérêts suivant la loi la somme de dix hui francs qu’il lui doit pour salaire et le condamner ax dépens. De bon coté Angéli se présenta et conclut à ce qu'il plut au Bureau Général du conseil attendu que le vêtement du quel Lricois réclame le prix a été nil fait, ; quil etre d'ailleurs entre les moins pour le rataucher ; Par ces moff due Ricon non recevable en sa demande, le recevait reconventionnellement demandeur dire que lorsque le dit Bêtemenit sera rapporté rectifie le prix en ser payé condamner firois aux dépens. Pnt de droit — Doit on condamner cingélui à payer à Perrois la somme de doprns francs pour la façon d'une peerdessns. Ouben dutecn dire Ficon non recevable en sa demande, l'en débouter recevoir cnge li reconventionnellement demandent ; dire que préctableneunt au paiement du prix de dix huit francs deuit, sera tenu de rectifier le Baletot par de ssus qu'il a sitré mains ? Que doit-il être statué à l’épard des dépens Après avoir entendu les parties en leurs demandes et conclusions respectivement et en avoir délibéré conformément à la loi eu attendu qu'il résulte des explications farnies par lu oins a ud i s ité di l du r partiec que le par de pas qui fait l'objet du titnge a été trendu fait par Bicoin le vingt deux avril dernier ; que sil est ce jour entre les moins de Seiçon s’ess qu’angeta la loi à renvoyé àprir plus de trois semaines ; que la raison invaguée par Angelé qui aurait pu être humise à examen dans un de lai apportie ne pout plus l'être aujourd’hui ; Pir ces motifs Le Bureau Général jugeant en dernier retsort ; Condamne ingeli à le precdre pasression du par dessus et à en payer le prix defaçon par dix huit francs, reçoit angere dans la demande reconventionnelle, l'en déboute ; Le condamne aux dépens taxés et liquidés envers le demandeur à la somme de ufronc et à celle dui au Trésor Public, pour le papier timbré de la présente minute, coanformément à la loi du sept Août mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites Ainsi jugé les jour mois et an que dessus. ecusq secrétaire E mou e Leonq dudit jour six cui Eintre apiy courdait Cinsiq aune epiet du ete eri ui ee lete mon ls Demandeur ; Comparant ; D’une part ; L Monpeur iévred trdite. Mite hep e dement e quidte doui co lei de ete nte pasé ux, défard coupet de pasint a pe e e e e te or ae de t e ende eitese dur e renei de repue e pre a pus auet les e et ur e te eleure eu te e e poretr a e en e purs ite de u t e deen e po de p ren de e tes due e en emret en an qur e ci pen de daemar u i à L s LPr huit. Cité pour le dit jour dix juin par lettre du secrétaire du Conseil en date du vingt cinq mai mil huit cent soixante dix huit à préqute de hattrot cr comparut. l’appel de la cause Talleir, se présenta et conclut à ce qudie plut au Bureau Général du Conseil condamner lrcord à laie payer avec intérêts suivant la loi la somme de vingt francon réparation du dommage qu’il lui cansé en nel soumpart pour le douze Mai dernier ann qu’il ene avait, pris l’engugement et le condamner aux dépens. De son coté Brard se présenter et conclut à ce qu'il plut au Bureau Général du Conseil elle qu’il est d’usage qu'un Carron adrepé par Bureau se Placement pour faire ertra le Demonche porte sa cort la vendredi avant le mout ; Que fralliot que prétendque la carte lui aété de livrée trop tors le vendredi pour qu’il lu partet le même jour anrait, pu tant au mans le porter le samedi matin le bonteur, ce qui lui en lévite de faire sa causse de chez lui au Bureau se placement ; Que n’était venu le samedi qu'a près nde et aboors qu'il en avoir demand un autre il sne fut l'ocuper ; Par ces motifs — Dire paltier non recevable en sa demande, l'en débouter comme mal foncéi elle et condamner ax dépens. Pont de trait — Doit -on condamner Ernér à payer à Gaticot la somme de vingt frencs poar Eademité de non éxéccution de convention verbale de travail Ou bien doit-on dire pultiot non recevable en sa demande, l'un débouter ? Que doit il être statué à l’égard des dépens . Lprés d avoir entendu les parties en leurs demandes et conclusions respectivement et en avoir délibéré conformément à la loi ; attendu qu’il est d'usage constant que l'ouvrier envoyé par le Bureau de placement poir faire l’attre du Demonire porte la cost de dit Bureau de vendredi avant cinq heures ; attendu qu'il est constant que aaltcct ou dit -l n’a pu se présenter au domicile ? mois le vendredi le Bureau de Clacement lui ayant donné la tarte trope ; Pard s'esx présenté le samedi aper tors pour quatifier l’inqudte du défendeur et le précantion qu’il a prise de seproier n eemet garan ; Qu'on ce cas pellait n'it pas foire dans sa demande en indemnité ; Pur ces motifs - Le Bureau Général jugeant en dernir lessart ; Et galtrit non recevable en sa demandes l’en detré Le condamne ex dépens envers le Trésor Public pour le papier timbrée de la présente minute, conformément à la loi du segt du oir lu cent cinquante, once, non compris le cout du présent jugement ; qa sigification d’icelu et ses sutes. Cin vuit les jourmis euf laiceu Lecuiq secrétaire nmces M </div><div type=\"case\">Du dit jour dix Plinau Entré onsieur jandron fabricant chemisier demeurant à Paris, rue du quatte septomtre, numéro quinze Défendeur ou primipal ; Demandeur epposant ; Défaillant ; D’une part ; Le Monseur Wébot cupeur chemisier, demeurant à Pasis, ue Villedo, numéro cinq Demandeur pronpal; Défendeur opposent ; comparant ; D'autre part ; cont de fait — Pur jugement Lendu par défaut par le conseil se Prud’hommes du Département de la Seine pour l’industrie des tissus le onze avril mil huit cent soixante dix huit, enregistré, Lunaron a été condamne à payer à Péot la somme de sept cent quatre vingt cinq trongs de pemitul et les accelure . Enqunte fit signifie à lundron par exploit de cnempron, lui prer a Ruris, en date du huit Mai mil huit cent soixante, dix tuit, 'agate à la réguéte de litetà Suivant exploit de Blnand comprar à Paris, en date du dix Mai mil huit cent paranti dix huit, vigé pour timbre et enregistré à Paris, le ouze Mai mil huit cent sorixmante dix huit. débet deux francs quinze centimes, signé de Pouiller, landion formé opposation à ce jugement et tta Citel à comparaitre par devant le dit Conseil de Prud’homes sige louveiliedt le ut su du e urecln d dix huit pour la voir recevoir opposent à c jugement, lvour dé charger des condamnetions contre lui prenuyé en peisnper et ruce frrires, s'entendre, le dit Pétel, délareer non recevablle uteant er e e lu es on eps p du it e tes dei lt e le e e demenra se dmenvare du ete e er it e e e det t e d e ine cdu pret et erdlit e lun e ue ru e er te pe ele t oesin auprse r e te l e u t eie ae ren ter e d es u erete enden e e e q ir e rei que eme de ense ae e e de ent lu it ue en p de e de tente quee e e e e e on e oaire du d prte e es ue oui l u domnaps pour puti ar de p sree et et e en a e le ue de s ui de de e preer dl s MI q entendu Rotaf en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que les demandes de setet paraissent justes et fondées ; Que d'ailleurs elles ne sont pas contestées par landon non comparant ni personne pour lui quoique demendeur opposat ; Attendu que cette non comparation dit été consilérée comme on atandans l’oppontion . Par es motifs - Le Bureau Général jugeant on fermerer répsort, on la forme mois Landon on de appolétion au jugement rendu contre lui le onze avril dernier senregistré, l'en déboute comme mal fonné en ccelle. Ordonne l’exécution pure et sumple du jugement auquel est oppolétior et condamne landon ax dépens taxés et liquidés en vers Le Trésor Public, à la somme de deux francs quinze centimes pour le papier timbré et l’enregistrement de l’appention, conformément à la loi du sept cox mil huit cent cinquante, ence, non compris la cous du présent jugement, la signification d'icelui et ses suites. Ansi jugé les jour mois et an que dessus. Derucq secrétaire Dmai t d tn a Mudit jour six viiq u Entre Monsieur Levonte, anvner lapeemntier, demeurant à Boles, au rondipement de Clermont leisa j Demandur, Comperant ; D'une part ; Et Monpeur Curtet Jeun Marie, ouvrier vailleur d’hobits, demeurant à Paris, passage Montes quien, numéro cinq ; Demander ; Comparant ; D’une part ; L Monsieur Lacembe,; tute dailleur d’habits, demeurant et domicilié à Paris rue lai sa numéro quinze ; Défendeur ; Comparant ; Dutelurt, Paint de fait - Par lettres du secrétaire du Conseil de srur ha du Département de la Seine pour l’industrie des tissus en dtates des Vendredi trente un Mai et lundi trois juin mil huit cent soixante dix huit, curtet fit citer Lacombe à Comparatre par devant ledit conseil de Prud’homme séant en Bureaux Particuliers les emble trois et Mardi quatre Cin mil huit cent soixante dix huit pour sin concilier à faire se pouvait sur la demande qu’il entendit forixen contre lui devant le dit Conseil en paiement de la somme de Pi de sit es arés nil Mn ec e Dre q u 2 i ié a q i i q du soixante denx francs pour selaire. La coulée n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six pis par lettre du secrétairé en date du quatre juin mil huit ent soixante dix huit à la réquité de Cortet Lecomle comparut; A l'appel de la cause Curet se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner lacole à lui payer avec intérêts suivant la loi la somme de soinante deux francs qu'il lui doit pour prix de travaux de son état et le condonner aux dépens. De son coté Laconbe se présenta et conclut à ce q’il plut au Bureau Général du Conseil ettendu qu'il reconnit devoir la soume de soixante deux francs ; mois attendu qu'an dêtenant le fière qu'il encère entre les moins turtet lui ceuse préjudice Luisque cette frèce n’ayant pa être livrée en temps la cestire pour comple Par ces motifs — Lire Cortet untucevable, en rademand e tavu débouter eile condamner aux dépens. foit de doit - Doit on condamner Lacoute à payer à cortets la somme de soixante de soixante deux froncs contre la remisé d’un ditent qu'il salire à Paton dit en du leis reant lut la demande ; l'en débouter ecevoir Lécombe reonvou tionnellement demandent ; Condamne Tortet a remitre le vêtement en psus eu disitre ou mis d sant au eu du du sent pu tie ueapsite d etito u ell t tent lerse lu de lete de e e e t l peiue en leurs demandes et conclusions repeetivement et encvord et e t te e te ite u ede ur tn du t esur purt la pre des ioise les our are e e e e t due a ler s r se te e dte t e poi i en e p e ten u reit e de u te enpe ente e pe de iq ount quilt eten de ses uee de de e titer duit en ue se andurs deuen der de aun u ant e e reie uar e e t enede tain e ti qu iq e te see inps e Mée minute, Cnformément à la loi du sept aût mil huit cent cinquante rue, non compris le cout du présent jugement, la signification d’iceluit et ses suites. Ainsi jugé les jour, mois et an que dessus. anaus Lecusq secrétaire te u dit jour six Piig linre Monsieur Langer, ouvrier tordonner, demeurant à Paris, rue saint Honoré, numéro cent quarante neuf ; Demandeur Comparant ; D’une part ; A Madame veuve Denis, demeurant lais, rue le la Roguette, numéro trente six ; Défenderesse Défaillent ; D'utre part ; Point de fat – Poirant emplot dà Chau hurs, huipser à Paris, en date du vingt neuf Mai mil huit cent sorxante dix, huit, visé por tioibre et enregistré à Paris, le trente en Mai mil huit cent soixante dix huit débet quatre francs cinquante cinq centimes, signé à de Seuilles, Leufer a signifie et soifsé de prè à la veuve Dons, frimo dun jugement endu par le conseil de Prud'hommes du Département de la Seine pour l’industrie des tissus le vingt un ferrier dernier enregistré; portant condamnetion contre le sieur Mllard, Cordonnier, demourant à Paris, a ceta Roquette, numéro trente six de la somme de cent trente trois francs de priniipal es intérêts et dépens liquidés ; Lecondo de la significtion à lui faite de ce jugemeit ; ot par exploit du ministère du dit uipier en date du seize Mad, enregistre ; Lé, D’'un autre jugement rendu sar le même Conseil le vint un Moit mil huit cent soixante dix huit, enregestre, d'éboutant le toir ltard de l’apposstion par ler formé du dit jugement du vingt un fevreer derier le condamner aux décens. CQ de la significtion à lui faite par exploit du tonne hupier en date du vingt cinq avril mil huit cent soixante dix tuit enregistré L? Du homme tant qui lui a été signifié par exploiy du même Loupier, en date du vingt sept avril dernier, enregistre. u ufin de la tontetire de saisse qui a été fate par exploit du ceit puipier endate du vingt quatre Mai courent, enregistrée eeuel lemême exploit donne siguation à la dite dame veuve Moui à comparaître le six Juin mil huit cent soixante dix huit jaut cinq de retevéé à l’adance et par devant Mappouns les meutres cn francs le compet de Prud’homme par l’industrie des tiss puis q u e six neis il les quel sai lene hil ecu t qu'il est creancier de la somme de cent trente trois francs pour prix de travoil et que le conseil par jugement du vingt un fevrer dernier condamne Celtard qu l’en couveait comne patron de l’étotlépement et qui ne serait, que ferant ; Attendu que ce dernier à jeune appention à e jugement et que le conseil l'a déboute de son appention et condame aux dépens par entre jugement de vingt un pars , que par suite de ce socent jugement fit gulement signifié, comme demant lui fit doané et que lorsqu'en voulit prétijuer la saisse des dpet matiliers et merchardon et qurnmpant la Bouttique le sieur ltard se trouvaut installé dans la dite Boulique s'y oppose étendant que les moilles et marchandiser étaeut, la proprité de Madame veuve Denis plntée comme cordonnnère à façon avence de la aquette, nmr trente six; qu'elle qétait imposée et que le loyer était également au nom de la dle dane ; Attendu que la dite dame Denis estpurente du sieur ellard, d'elle esnt en service et n’hibité en acue meme le fond de commet ce de chapurer exploité par le sieur, ellardes que d’ailleurs l’étabtifs enont dans répoudre du selaire des ouvriers etgltant proprrétaire de l'établifement elle duits être trnue au priemer de son la laire dant le dit établissement à profité ; Par ces motifs e taut entrer à de duvré en temps et lieu ententre la dite veuve Donis, déclarer commun à e avec ette es jugement rendut par le conseil de Prud’homme de la Seine pour l’industréie des tissus les vingt en e e e e e e a les leenie la demet e poer de e se fouir e pau t peur cites n e preésir te de el de pome de quen e e e den pe u deu un deu is cete d ape nant e empre ecin eur de dte sen en an que te ere pu de n e u le qu eit te te e eu e u en tense e de p en e nt eue en eu e e e u e prer pu demen de es qur de t ete l praunt pur au e ait e n n entene rprete dea enan de fer ede t e pement e de p den e e de pet en e e e e e e et mrte enre t e pageur en prenier le part ; Donne défaut contre la veuve Douis non comparant à personne pour elle quoique dument appelé. L adjugeant le profit du dit défaut ; Dit commu à la veuve Devis les jugements ren dus par le conseil les vingt un ferrier et ving un Mars mil huit cent soixante dix huit contre ellard, En conséquence ordonne l’exécution de sa jugement aussi bien contre la veuve Denis que contre lauder ensemble, prinmipet intérêts et frais Condamne la veuve Douée aux dépens taxés et liquidés envers le demandeur à la somme d sept francs soixante dix centimes et à celle de quatre francs cinquante cinq centimes envers le Trésor Public pour le papier timbré et l’enregistrement de la critation, conformément à la loi du sept aout mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites. Et vu les articles 435 du code de procédure ciile 27 42 du décret du onze juin 18099 pour signifier à ca défenderesse le présent jugement, commet Chanpion, l'un de ses huipsers audienciers. Ainsi jugé les jour mois et an que dessus. Durois l Lecusq secrétaire Quit jour six quiz Entre Monfieur andre Gugat, ouvrier passementier demeurant à Paris, rue Délettre, numéro douze et quclire Demandeur ; Comparant ; D’une part ; eE Monsieur Caquelt, fabricant papementier, demeurant et domicilié à Paris, bue des Cascusés, nunééro cinquante trois ; Défendeur ; Comprut D'autre part ; Point-de fait — Par lettre du secrétaire qu londe de Prud’hommes du Département de la Seine pour l’industrie des tissus en date du Mardi vingt un Maii mil huit ce parant dix huit crayot fit citer la hur comparaître par devant le jré Conseil de Prud’hommes séant en Bureau Particulier le vendei vingt uatre Mai mil huit cent soixante dix huit pour setisien, nse faure se pavit sur la demendes lel rntieles pouer cesie deans le dit Conseil en faiement de la somme de quatre vin ffnc ue pour indemnité de perte de temps et de renvoi n tompé A l'appel de la cause puret se présenta ea rxpusé le conil is e h lille e nei pet n de son coté taner se présenta et exposa u Conseil que quont il fit attendre Ganot il l’indemnise; que sil l'a conseré tout E à coup sius l’il lui fit un travail necuptette. Le Bnteun Particulier renvoya la cause devant un mimbre du Conseil à ce connaissant Devant ce membre les partis ne purent être conciliées à la cause en cet dot fut renvoyée devant le Bureau Général du Conseil séant le jeudi six juin mil huit cent soixante dix huit. Sénnt, ce memle de coseil Cité pour le dit pour six juin par lettre du secrétaire du conseil en date du vingt sept Mai mil huit cent soixante dex huit à la renuute de huint la quet comparut. A l’appel de la cause ngit le pémmta le et conclut à ce qu’il plut au Bureau Général de Conseil attendu qu'en lui fait ant attendre des matières laquet lui a causé un préjudice de perte de temps dont ià lui doit épusition qu'un le curetient instantemment, nonmbstant la prreusicque lui avoir fite Pahuet père de lai donner un curgement une fuit celui entrois terminé il lui cutse en odre préjéudice pour le uil réparition lui etéralement due Per ces motifis condamner laguet à lài payer avec intérêts suaint la loi la por de qute eit de ponr itle e n qe elesese t landeme aux des e rerle hui t pae aunt e de en ende e enit e e ouerent ce ant e uit doen lens te depr du le u demrantene sepen te u entente pauis et de ure prs et ie a epre pu t unt en enterdete, o ete du peraente t e den lu tee t ue te p e es; ciqui der te il qu tene que nte de en put aper du onte deu puremet mer e e e t qu eu de e de e adums en apre eneie e tur due ei qu de sir de e r de e er adean rand eux er n ei ent uen et es e en en entie ante e n somie aem er e e t la P pl dir sept reuvai etdous mols rayes nulsrepprou Lecucs pe à ayant fut in menvas travail n'y avaix aucun dait ; Qu'ed reitaud même que Caquet, père lui ont promis un chargement ; c qu'il était toujours lacuttotif onu fils de repuser puir qu’il est cesand patron ayant antorité pour diriger son traval, l’expérience fot de son ineapoité sofras à elle seute pour en pastifes T’neote Par ces motifs - Le Bureau Général jugeant, en dernier réport ; Dit Gayot non recevable en ses demandes , l'en déboute, ee la condamne aux dépens envers le Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept aout, mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites. Ainsi jugé les jour, mois et an que dessus. ere ue D l Lecusq secrétaire </div><div type=\"case\">Du dit jour dix Pinga Entre Monsieur Pant laguet, ouvrier passementier, demeurant à Paris, rue Grat prolougé, numéro deux ; Demandeur Comparant ; D'une part ; L Madame Marguerite Cetime Caurant; épouse du sieur Louis Bertail, comme commeradement sous le nom de dame Bouguut, fabricante de paysementite demeurant et domiciliée à Paris, Boulevart de sepastapes, numéro quatre vnt dix huit ; Défenderesse ; Comparant ; D’autre part ; Ponit, de fait - Suivant xploit de champiin, suipcès à Pariseuns date du premier juin mil huit cent soixante dix huit, visé pour timbe enregistré à Paris, le trois juin mil huit cent sixute dix huit, cen dies ex paur pareite cinge tentioes, leigner duafeite sapit à mocilie et toiféé sapré à la dame Mésheurat, enne Rortel preme d’une jugement rendu par le conseil de Prud'homme du Département de la Seine pour l’industrie des tissus du vingtenc avril dernier, enregistré, portait condamnition à son profit cantre la dame veuve ongeautt, fabricante de lassemens ent Boulevart sétastept, numéro quatre vingt dix huit, le coti francs de princifut pour salaire ed indemnité, plus ces intérêts den dépens. setondu de la signficstion de ce jugement par explioito dit chaznpion, luipsierà en date du nzt duvne dernier uagés tertie du commen dement de payer pais par etre enprisudice du ize dux de ent s tereit, duten de eu de larsse fuite à la maison Bougerants le vingt sept Mai dernier et dans laquelle Madame Bortal aà fat conmme qu’il n'y avair pas de Madame Bougeault et qu'elle édtas la propicétaire de la fabrique de passementerie ; Et par se même exploit à cité le dame Bertail sus nomme et le pour son mori pour la volidilé à comparaître par devant Mépieurs les Président y Dembres comparut le Bureau Général du Conseil de Prud’hommes lu l’industrie des tissus séant en Bureau Général le jeudi six juin mil huit cent soixante dix huit pour attendu qu'il travaille pour le compte des la maisou de fabrique dont elle est profiéitaire ; Que ne pouvant abtenir paiemint des salairer à lui dos il a du l'appelés e devant le conseil de Prud’hommes ; que ne conné saut par le noin qu'elle dit avoir de dame Mertail, il l’a fait citer au nom de dune Bougeault, nom sous lequel elle en génralement et commméuelement compme depuis lind temps tant avant que depuis le décis de Monsieur Bougeaut ; Attendu que sonte nomil a obétenu contre la dite dame run jugement la condamner à lui payer. las ux pour de ti eent est p e fous jugeant t l e du p prdu. aor d en des p pu de i u sier paiement de se part ; Que seulevant lorser en a voula xcuter pravant prer la de de nt pra ten e le ces de dure po lit cent er lur e ome e ie su e e te ten eui eun es puir et pur e en tonlaet tere de en eset du lis ae te e sasemet dan e e e t le sour dun preut e desthuit juemeint itre l due contit sre leee eense an eaent e s e t d reier de de sende demeant pour ut den e demante ce meies ede e e oent n gen e e e ee ei t eu eu e sur aer e e e on puret au e e u te puer ente e ei qu pene rdet u se e deier pesepor ce et de ete qu se e ete u t enc li utie a dermant poen ent que M q o Drdr t somme au dit Caquet ui lui a bien son mis des ichantillens dans le lut d’avoir des commander, mois auquel elle n’avant commandé ; Par ces motifs - Dire laquet non recevable ses demandes, l'en débouter comme mal fondé en rcelle ste condamner aux dépens. tont de troit — Doit on dire applecitte à la dame Gertail le jugement rendu le vingt cinq avril dernier, enregistré, contre la veuve Boureau Gérnale conséquence condamner la dame Marguerite celine Lonvan comme Bertail à payer à Caquet les sommes portées Dat jugement ? Ou bien dait-on dire taquet non recevable en ses demandes, l'en débouter ; Que doit-il être statué à l’égard des frais et dépens ?Après avoir entendu les parties en leurs demandes et conclusions respectivement e un avoir délibéré conformément à la loi ; Attendu qu'ile eit constant que la dame Cgeent somme Bortailà accepté de cahuit un ichontitton par lui fait ; lui a donne ces matières, nufrancs pour confectionner des refrences de ce même acpontillon de chargements sur deux metiers aupris de cinquante francs des cent mètres ; que l’exéution de cet engugeant n’a pas été timplié par la same lo jaurent parce qu’elle troiuve plus avantageux de la faire exeiter par un autre ouvrier quine pris que quarante cinq francs des cent mètres, ce qu'il pouvait faire n'ayant eux aucune peure decréation mancun frans d'ichobittonnege ; Qu'et résulte de ce fait indeliés en lui même que caquet à éprouve un préjudice dant la dame lTousen lui doit réparation par la somme de soixante dix francs Attendu que la dame lavarent qui avait en des rapports de travail avoi Cahuet savois, bien que les lettres de conciliation et de citation la visaiens, encore qu'elles fassent adre présse la dame Bougeantt, nom auquet etle répous d’ailleurs comnuilti Qu'n celas elle estpassible de tout les prois qui ont été fots à duision du jugement. Pir ces motifs - Le Bureau Général jugeant en ternier lepart ; Dit applicible à la dame Marguer eline Coureit, somme Bortail, le jugement tendu las le Conseil le vingt cinq avril dernier contre la veuve Bauquuitt ; ordonne l’exécution de ce jugement contre la dite dame Bertaldeu Marguerite le ué Ceurent sisqu’à concusrenre de sois otes froncs de principal; condamne la dame Bertailt éellergt celine Ceurent aix frans et dépens de la première instant a plutat de l'oidlance desant la veuve Boureautt, commé à 'euf, de la présente instn tas et liquidés en vers le demanders e la somme de Cinq francs quatte hunigt quinze centims uit E celle de deux dix frans soixante quinze centimes envers le Trésor Public pour le papier timbré et l'enregistrement de la citation ; Conformément à la loi du sept aout mil huit cent cinquante, ence, non compris le cout du présent jugement, la sinification d’icelui et ses suites . Aini jugé es jour mois et an que dessus. Lecucq secrétaire dmeau </div><div type=\"case\">Du dit jour dix Jing domant e Entre Mademoiselle Désirée Sanson, se disont ouvrière contrière demeurant à Paris, rue Fesnnier, numéro dix Demanderesle ; Comparant ; D’une dort ; t Monsieur et Madame Magnen, le sieur Magnen tant en son nom personnel que pur epaiter et autoriser la dame son épouse, Mantrelte contirre, demeurant et domiciliés, entembrle, le dits épout à Paris, avenie des taris, nméré qatre suit cinq Défendeurs ; Comparant ; D’autre part ; Poit de fit Par lettre du secrétaire du Conseil de Prud’homme du Dépastement de la Seine pour l'industrie des tissus en dates des Mercredi vingt neuf a vendredi trente Mai mil huit cent huit li e t le te de se le e u dapers prde ts tel a sid en ue u Anseu té te eontritat d u umes e les epord s le er en p p t ente unde ete dais poir tetre qunt les e e pre s la o e e en per due mne u deant ens un epe e pe le deu le deur du ti ens ene n en pement dei e e der e paur edu due au de juemne ede t e e e e es lute ede e e que der eun, duedoint e e e e e e en e se e eit e aen e den e eser pr e enqu les Bourrnie de se sonfais ; que si elle lui à enteigne grctutenant le protique de la cuture s'eit qu'elle la veutoir endre expabla de complir l'umploi d’une smme de ciembre au lien de cettedeu borne d’enfans qu'elle avait occupée jusque la ; Que s'elle sa nourrie c’esrt qu'elle prit, l'engagement de lui payer sa nouriter par ena somme de se francs int ciq cntiomes pas jounr qu'en sai elle est sa débitrie et hou se trénière poureni et e ecinq frcès de l’appele devant Monsieur lejage de Pair la su arcondipement qui les ontorisé à détenir sa malle jusque jour ou elle de serapoiquittée envers eur ; Par ces motifs — Le déclarer en conpétut pour comaitre de la demande la demoiselle Sandon, l'en débouter et la condamner aux dépens. Deton coté la demoiselle San son se présenta et conclut à ce qu’il plaut au Bureau Général du conseil attendu que la dame Jourson l’a occuhze pendont cinq mois à des travaux de contire après quoi elle lui delivra un certfiret dant lequel elle est qualitée d’ouvrière conturière ; Par ces motifs — Dire les époux Mégaenu non recevables enteur demante ; les en débouter. Le déclarer compétent, retenir la cause et oedonner qu’il sera explique sur le fand et condamner les époux Magnon aux dépens. Point de troit - Doit-on se déclarer on complent pour comastre de la demande de la demoiselle sanson? Ou bien doit-on de déclarer compétent, retenir la cause et ordonner que les parties seront tenue de s'expliquer sur le fundi pour être statué ce qu'il appartiendeu?? Que doit-il être statué à l'égard es dépens ? ? Après avoir entendu les parties en leurs demantrs et conclussons sur le décliniture posé par les époux Mauven et en avoir délibéré conformément à la loi ; Attendu queles eeplications foaries par les parties se part que la dame Moaye n'oit pas maitrepa conturière ; que la demoiselle Sansalle nuné n'est pas ouvrière conterrière ; Que se les époux Magaen s'ont reçue chez euy et ti la dame Magnon lui a enjugné des étements de conture la a danne un certificaton caprits a por deurt l lag loes le pe de u ait e p e somme de chembre qu'elle occupée en ce monent ; Qu'en recosté Conseil n’a pas à conmetre des faits cét de leurs sap parts ; u det ces motifs - Le Bureau Général jugeant en tremier ressort, l déclare n compétent pour commêtre de la demansle le lademoie saeison et la ouvrie devant que de troit ; Condamne le demable Sans on ux dépous envers le Trésor Public, pour le papier timbré prente ille, conformément à la loi du sgt ui muil luiét in ene non comprès le coiut, du présent jugemen q ui ue d’icelui et serpuiltés. Ainsi jugé les jour, mois et an que depas. ecusq serétaire Vergut du dit jour dix vi ntre Monsieur et Madame Potits, le sieur Petit huit en son nom personnel que pour eprriter etutoriser la dame son épouse ouvrière Conturière demeurant ensemble, les dits époux, à Paris, rue du Potian, numéro vingt treit Demandeurs ; Comparant ; D'une part ; Et Monsieur pus Foirser aptécien demeurantà Pains, rue Tris, numéro suite seize ; Défendeur ; Défaillas ; D'autre sart , Pont de fait à Suivent employ de champren lu pier à laris, en date du Vingt huit Mai mil huit let soixante dix huit, visé pour timbre et enregistré à Paris, le vingt nup Mai mil huit cent soixante dix huit déber trois frencs trente cinq centimes, signé saprit la per e pes e e silui doite oen de en te seur du Département de la Sene pour l’industrie du tissus le vingt cinq avril mil huit cent soixante dix huit, enregistré porteu tindemnitions ate de per titene eui en den de setie conturière, de la somme de quarante six francs de pnpl te gu eude ni aug eu t poux jugente e pu eom a e e e our enper e de en l prése e du ei es suite o an tin de n e apres ut en axé dt pe se asseren e eente qul domies dner pus u ue ctise hu etes un de e t nonet ue le sete e e n e te en de ent e d e t e et deratur ent ei en es eps ete soment de ur au de e pre Sar reuvri et quate mits aages nmoils apprenti quinze centimes plus à quatre franc d'indeunité pour temps perdu; attendu que la signification du jugement et le comma dement oit ile faits conformément à la loi ee que la centetire de saisse faute le vingt un Mai dernner Monsieur chailler à cit s’opposer à la same rétendant que tous les otéêts garnépant les dits tient lui ont été lu par Monpeur pales Fénier, aptrien à Pains rue de six, numéro sege, quoique sur la prte de l’appertement de trouve eeployi portant le non de sonnmeller ; attendu que le siour Jules qovrent n't qu’un prété nom et que même s'il était propriétaire de l’étabtifs éneux de conture enistant actuellement, passage la fersien numéro douze il doit être tru au paieme du salaire du pe ouvrières employé dans l’établifsement. Par ces motifs et tousunti à déclaire nltérieuremet, poir déclarer commun avei lui, sieuf juler Foirier le jugement rendu par le conseil le vingt cinq ane dernier, enregistré. L'entendre en conséquece condamner à leur payer la somme de quarante six francs quatre vingt quinze centimes ncoutret on la pitet des condamnations prinoncées, et s'entendre en outre cendmn entans les dépens et frois du jugement de premier instance, et ux francs d’exécution qui l'ont seive et, en outre, Fontandre condamner entans les frois du jugement commen . Al’appel de la canse jule De leur coté les époux Pobit se présentèrent Férier ne comporut pas. et conclurent à ce qu’il plut au Bureau Général du Conseil donner défaut, contre Jiter Vocrier non comparant ni personne pour lui quoique dument appelé et pour le profit leut adjuger le bénmipier d Cconclusions par eux prises dans la citation euploit de chaupsaux huipier, enregistré. Paint de tait — Pait-on donner défaut contre Jules Fvrier non comparant ni personne pour lui quoique due appelé et pour le profit adjuger aux demandeurs les conclusions par ux es précédemment prises ? Que doit-il être statué à l'égard des dépens ? près avoir entendu les époux Potit en leurs demadés et conclusions en avoir délibéré counformément à la loi ; Attendu que les demandersse époux Pabit proipanrt justis et fondées ; Que d'ailleurs elles nevont pas contestées par pales Février non comparant ni personne pour lui quoique dument appelé ; Par ce motifs L Bureau Général jugeant e en premier le part ; Dit commun à pules Février se jugement rendefat le Conseil le vingt cinq uvreil dernie, enregistré, contre les époux ar ou chnelles ; in conséquence ordonne l’execution de le jugement aiquants a pis lour contre te le lo vrer eu ete ur ou lckieller fait pour le principal des condamnetions pu prir e intérêts et frais de pour suites faites à l’arcusion de ce jugement; condomne polur Favier aux dépsouns de la préente eat ie oaie nlégu de anver les demandeures à la somme de cinq franq uai qi cinq centimes et à celle de trois frans trente cinq centimes envers le Trésor Public, pour le papier timbré et l'enregistrement de la citation, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites. Ainsi jugé les jour, mois et an que desus. ecusq secrétaire à Emier D t </div><div type=\"courtHearing\">Audience Du leudi treize Jng mil huit cent soixante dine huit Siégeuant : Messeurs Bagor, Prud houre Présidant lotoutience, en remplaiement de Mecheure Marienvel et snaud Présitent et Vère président du conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus empechis, Deroyn, Menier Berthemer et Cusse, Prud'hommes epistée de Monsieur e Decucq, brétare du dit Consie Etre épens Poirit, viner papmentier demeurant à Paris, Toulevart de la pgare, mro cent cinquante àn dee ; Demadeur ; Comparant ; D’une part ; Monsieur Dutr fabricant pasementier, demeurant et domclié à det, rue trtize, oummér prt ; Ptatie ples ’eseps ; E if onte e deuit se larde dt lu émin e e e ou Seine pour l’industrie des tissus en dates des Jeudi six et e de pue l e poe oeur cute da pat pus et le cicis demen enten de e e e e e e e te de eperte d it du ten e ui quet e sen t ts pur d t endeate nteme t pou d enes pus qutle ue eu e e e e er e e e e enr ape e desen ueti quige otr cit du cint qu7il du dite relere dumeiet ur de en te iquen e e e p t e se e pe n en anp les ene entisqnti a d e m t en ei se dman a prs eut due ou de pe ur ue des ai su euse é i o r os ? A Li quei Point de droit — Doit-on donner défaut contre Doit on comparant ni personne pour lui quoidue dument appelée ;t pour le profit adouger au enteindeur les conclusions puar lai précédemment prises ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu Régat en ses demendre et conclusions et en avoir délibéré conformément à la loi Attendu que la demande de igar parait juste et fonéée Qque d’ailleurs ele nen pas contestée par Dety n on comparante ni personne pour lui quoique dument appelé ; Attendu qu’il ne comparuépanut pas devant les Bureaux du Conseil Diite à causé à Parat un préjudice de perte de temps, cequ'il doit répurer . Par es motifs - le Bureau Général jugte en dernier repart ; Pu l'article quarante un du decret di conze juin mil huit cent neuf, portant renseuvent por leur conseil de Prud’hommes ; Donne défaut, contre Dieta non comparant ni personne pour lui quoique dument appelé ;Et adjugeant le profit du dit défaut ; condamne Deté à payer avec intérêts suivant la loi à Slgat, la somme de quatante deux francs qu'il lui doit pour salaire, plus une indemnité de six francs pour temps perdu ; Le condamne en outre eux dépns taxés et liquidés envers le demandeur à la somede un franc, et à celle due au Trésor Public, pour le papier timbré de la présente minute , conformément à la loi du sept août mil huit cent cinquante, ence, non compris le cout, du présente jugement la signification d’icelui t ses suites à et vu les entorles 435 du code de procédure civile, 27 et 42 du secret du onze juin 18099 pour sinoifier au défendeur le présent jugement Conmle Cl umpron jun de ses huissies audienciers. Ainsi jugé les jour mois et an que dessus. gagoffiet Lecucq secrétaire Qusi jour cingt siuiq Entre Monsieur Denis Mocke, demeurant à Paris,ue Brzfo numéro vingt deux agistant en nom et comme a demandete nt de la personet des tiens de sa fille mineure Couist, ouvrere cantiriée ; Demandeur ; Comparant ; D'une part ; Mademoiselle Celine Jainq Liéclain, l’atupe contire e demeurant et domiciliée à Paris, rue de la champée ? Pontin ; numéro quinze ; Défenderesse ; Défaillant ; D’autre part ; Pains de fait - Par lettres du secrétaire du Conseil de Prud’hommes du Sépartement de la Seine pour l’industrie des tissus en dates des Mardi quatre et vendredi sept juin mil huit cent soixante dix cent pour se concilier si faire se pouvait sur la demande qu’il entendait former contrerlle devant le dit conseil en paiement de la somme de dix francs trente cinq centimes qu'elle rui doit pour prix de travaux de sa fille louise. La demoiselle Lievain n’ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi treinze jun mil huit cent soixante dix huit Cités pour le dit jour snze juin par lettre du secrétaire du Conseil en date du onze Juin mil huit cent soixante six heaig à la requete de ock la demoiselle Gerain ne comparut pas. Le son coté ole se présenta et conclut, à ce qu’il plut au Bureau Général du Conseil donner défaut contre la demoiselle Térainn non comparante ni personne pour elle quoique dument appelée et pour le profit la condamner à lui payer avec intérêtts sivant la loi aa somme de dix francs trente luiq centimes qu'elle lui daoit pour solde du frix de travau de sa fille vnisi pas, cette indemnité qu'il plaira au Conseil fixer par perté de temps devant la peu e e se rnt eune Negcles Cntre puen eaps tete la deme le tr oin en o prs puesen pur le pi de t n pifi ne ndes au demeide lrte er purse premnen pel pu de de pur uer du te e e e e e enier rete e prde e e t emre le e resq t e des e desipu e u e deme peur qpur em e te e de sen t ps p ur per e ou es a u te resnie ande de e pr ent qut deux ceste den n tes surt nde e nde de e qurate u dte d u se de e i q e e desoe age n e e endem e es pour pur u e e rite purir de lite de e deon le apur e i cncis i e cé C cnt A puste et fondée ; Que dailleur elle n'est pas contestée par la demoiel crevain non comparante ni personne pour elle quoique dament appelé ; Attendu qu’en ne comparusant par devait le Bourer edu Conseil la demoiselle Lrévain a causé à Porte se préjudice de perte de temps, ce qu’il doit réparer ; Par motifs - Le Bureau Général jugeant en dernier repsort ; Dounne défaut contre la demoiselle Léévein on comparante ni personne pour elle quoique dument appelé ; Et adjugeat le profit du dit défaut ; condamne la demoiselle Lrévain payer avec intérêts suivant la loi à Maccy la somme de sex sans trente cinq centimes qu’il lui doit pour prix de travaux des sa fille couisne, plus une indemnité de quatre froncs pour temuse perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de, ue fronc et à celle dui Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept aot mil huit cent cinquante ence non compriu le cout du présent jugement, la signification d’icelui et ses suites. tt vu les articles 435 du code de procder civile, 27 et 42 du decres du onze Juin 18099 pour signifier a la défenderese le présent jugement, commet champion, l'un ler ses huifsiers audienciers. Ainsi jugé les jour mois et an que dessus. Mrasgoffte Lecucq recrétaire e it ui du dit jour Treize Puizan Entre Madame veuve Didier, demeurant à Paris, rue Louge numéro trente lix agissant en nom et comme satrèce n lutelle et légale de sa fille mineure ingele, ouvrière fluriste Demanderetse ; Comparant ; D'une part ; Madime veuve Minée Ségrot, fabricante de fleurs artificilles demeurant et domiciliée à Paris, Boulevart Montmrtr numéro quatorze ; Défenderse ; Défaillant ; D’andesr part ; cnq de fait ) Par lettres du secrétaire du conteil 2u Prud’hommes du Département de la Seine pour l’industrie de tésons en dates des undi trois e Mardi quatre juin mil huit nt soixante dix huit, la dame Didier fit citer la veuve Aune Soyrot à comparaître par devant le dit Conseil de Prud’home de séunt en Bureaux Particuliers les mardi quatre et vendredi sept juin mit huit Cent soixante dix huit, pour se concilier si faire se pouvait sux la demande qu’elle entendit, former contre elle devat le dit Conseil en puement de la somme de quarante franc quelle li doit pour prix de Pa jeur Vnnée Segot n'ayant travaux de sa fille angèle ; pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi liize juin mil huit cent soixente dix huit citée pour le dit jour treize juin par lettre du secrétaire du Conseil en date du heit juin mil huit cent soixante dix huit la réquête de la dame Didier la veuve sgné Trgrit ne comporut pas ni personne pour elle régalièremet. A l’appel de la cause la dame Didier se présenta et conclut à ce us plus a Bureau Général du Conseil donner défaut contre la veuve ine Pagrot, non comparante ni personne pour elle quoique dument appelée et pour le profit la consamnner à lui payer avec intérêts suivant la loi la somme de quarante francs qu'elle lui doit pour prix de travaux de sa fille congte, plu endenité qu'il plaira au Conseil fixer pour perite de temps devant le Bareaux du Conseil et les dépens. Pent de droit - Dit on donner défaut contre la veuve Mirnée Peret non comparante ni personne pour elle quoique dument appelée et pour e profit adjuger atele e e ple lu etens qu e pre e pr e Que doit-il être statué à l'égard des dépens ? Après avor entendu la dame Prdier en ses demander et conclusions e en an slé da prdent cle, doeunte ur d te eu dit purt te pat hre ete de de e eit dener po pe de en e e dene ere du jon eux e eur e e e le oue t e dere en e cine eade seun de en uetle eue e e pede ende e te ee u se dus e e e que q ene u de en e e e ent endt seoi e te des que e e conci inte de p d i ie ile den i de den tess pous e aunr e s qucities en e que e le à R i e L BI5 u d de la i u i D t i ? A dt en date du Mardi vigthui, Mai mil huit cent soixante dix huit renvoi approuvé f Cite Lecc lagettre au Trésor Public pour le papier timbré de la présente minute, conformément à la loi du sent aon mil huit cent cinquante once non compris le cout du présent jugement, la signification t vu les articles 435 du code de d’icelui et ses suites. procédure civile, 27 et 42 du décret du onze juin 1809epou signifier à la défederese le présent jugement, commet coompar l’un de ses huissirs audienciers. Ainsi jugé les jour, mois et an que dessus. csoffe Lecucq serétaire r Audit jour treize Puin ntre Monsieur Jeun craderse, ouvrier tailleur d’habit demeurant et domicilié à Paris, rue des lus, numéro vingt six ; Demandeur ; Comparant ; D'une part ; Et Monsieur restivens aitre tailleur dhabits, demeurant et domicilié à Paris, rue Poinq hronoré, numéro cent trente cent Défendeur ; Défaillante ; D'autre part ; Point de fait - Par lettres du secrétaire du Conseil de Prud’homme du Département de le seime par l'industrie des tissus Clavarie fit citer Thristions à comparaître par devant le dit conseil de Prud’homme s u Bureaux Particuliers le endredi trente un Moai mil huit cent joiante dix huit pour se concilier si faire se pouvaits sur la demande qu’il entendait former contre lui devant le dit conseil en paiement de la somme de neuf francs soixante centimes qu’il lui doit pour sitaire. A l’appel de la case Claverie se présenta et en posa comeudessus. Da santoté Thristinent le présenta, reconnt devoir la somme réclamée et ditedée refuserà la payer percé que le demandeur qui le tavait et nevent pas travailler le vingt sept moi, ce qui lui daute cux préjudice dont il lui dux réparation . Pur l’avis de Bureaux Particulier que Claverie ne tait pas tenur à le prévenir puitque dans cetteindustrie les parties se peuvant quitter non emrent, chaque jour mois encore à toute hence du jour Thistian plauis qu’il paierait le même jour au jour. Cette poé r ne fut pas cence et la cause fut renvoyée devant le oudeil qun le du conseil ting le jendi trige suin mil huit cent prse et aors huit. Cité pour le dit jour tenze juin par lettre du secrétaire du conseil en date du onze jun mil huit cent soixante dix huit à la réquate de Clavarie Boristios ne comparut pas. A l’appel de la cause Claverie se présenta et conclut à ce qu'il plut ai Bureau Général du Conseil donner défaut contre christianne non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui payer avec intérêts suisin la loi la somme de neuf francs soixante centimes qu’il lui doit pour prix de travaux de son état, plus, cette indnité qu'il plaira au Conseil fixer pour perte de temps devant les Bureaux du Conseil et les dépens. Point de droit - Doit-on donner défaut contre Cpristios non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demonder les conclusions par lui précédemment prises ? Que doit-il être statué à l'égarddes dépens ? Après avoir entendu Clavèrie en ses demander et conclusions et en avor délibéré conformément à la loi ; Mttendu que la demande de Claverie en juste et pendée ; que d'ailleurs elle n'it plus contestée par Thiestions non comparant en personne pour lui quoique dument appelé ; attendu que christions à fait perdre du temps à Claverce devant les Bureaux du Conseil, ce qui lui causà un préjudice dont il le dtoit réparation ; Par ces motifs – u Bureau Général jugeant en dernier ressert ; Donne défut contre Threstinent non comparant ni personne pour lui quoique dument appelé pudivant srapadte ie it conte e s se à payer avec intérêts suivant la loi à Aluveice la somme de du our per te ur i peame et oue pe u feimen e e te e e lu e e te ene le puir pitite de e de le u oneil de te u eu e e pour ete por di enie e ep paire u en e eue de e u de s ds pomn e e re eur rei eus eu euppres s dedit en gu denmenenr es lngifle Leng puilite De uns Audi jour treize juin Entre lesioer Moolphe Veedorn, ourier cappren demeurant à Paris, rue Saint Vinolas, numéro vingt ; Demandeur ; comparant ; D'une part ; Et Monsieur Besanvalleti Matre la papser demeurant et domicilié à Paris, rue Car lassal, numéro dir sept ; Défendeur ; Comparant ; audre part ; Pint de fit - Par lettres du secrétaire du Conseil de Prud’homme du Département de la Seme pour l’industrie du tissus en dates des Vendi six et vendredi sept juin mil huit cent soixante dix huit Tinesche fit citer Besenvelle à comparautre par devant le dit conseil de Prud’homme séant en Bureaux Particuliers les vendredi sept et Mardi onze juin mil huit cent soixante dix lui pour se concilier si faire se pouvait sur la demande qu’il entendat ferme contre lui devant le dit conseil en paiement, de la somme de vingt trei francs qu’il lui doit pour sétaire. Besonvelle n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi tronze juin mil huit cent soixante dix huit. Cité pour le dit jour tenze juin par lettre du secrétaire du Conseil en date du huit in mil huit cent soixante dix huit à la réguate de mésche Besavalle comparut . A l’appel de la cause oesch se présenta et conclut à ce qu’il plut au Bureau Général du Conseil condamner Besonvelle à lui payer ave intérêts suivant la loi la somme de vingt trois francs quil lui doit pour salaire et ce condamner aux dépens. Let ontile Tue Beranvelle se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu que mesch sest présente chez lui d’offrant de travailler comme petit ouvrier au pris de cinq francs par jeur ; Que fin du premier jour ayant recoune qu’il ne savait riern du métier il le lui dix ; qu'il dornt lalors et attait de rester pour apprendre ; attendu que donte quatre deurs et demi qu’il est reste vrues chne lui a été d'auvie atilité il ne lui doit ion ; Par ces motifs — Dire vuce se o recevable en sademande, l'en débouter comme nal fonté et uelle et le condamner aux dépens. Paint, de droit — Doiton condamner Besanvelle à payer à Vmesch la somme de vingt trois francs faur quatr auns le atin de travail ? Ou bien deuq en diue Muron nonrecevable en sa demande, l'en débouter ? Que doit-il tretide à l'égard des dépens ? Après avoir entendu les parties en leus demis s et conclusions respectivement et en avoir délibéré conformément la loi ; attendu qu'il et constent que Mausel a erei e chez Besonvelte pendunt quatre jours et deme ; qu'il etre trestais que la de pasier sur Besenelle li qi qu’il ne pouvait préteuve au prix de cinq francs qu’il avaice anmancé vouloir gagnet ; mois il ne pent s’ensuivre de cette déclaratien qu’il l’apoccupe pour rien ; qu'entenait compte de la faiblesse de l’ouvrier le Consil en d’avis qu’'il lui huit, paye deux francs pour sable ; Par ces motifs - Le Bureau Général jugeant en dernier restort ; consanné Besanvalle à payer à Arésch la somme de douze francs et déboute Nresche du surplus de sa demande, condmne Besonvalle aux dépens toxés et liquidés envers le demandeur à la somme de un franc, et à celle due au Trésor lablic, pour le papier timbré de la présente minute, conformément à la loi du sept aout mil huit cent cinquante, ence, non compris le cout du présent juement, la signification d’icelui et ses suites. Ainsi jugé les jour mois et an que dessus Magalli Lecucq secrétaire usi jour crige liuigz e Entre Monsieur Auguste Flessis) ouvrier cordonner, demoirant à paris, rue Camada, numéro six ; Demandeur ; Comparant. mpat ; de te sour lat tort e te se demeurant et domicilié à Paris, rue du faubourg sant Denis, numéro cent quatre hnet dix ; Défendeur ; Comparant par la sieur Charles, son contre maitre, demeurant chez lu aux dur aupere te e p sensen e gue qurele pent e en der due deux pu i l époed det pr e ene e laqerant peredin e p semis ver qua e dit ede de ur e aun e t ue e ete mr de emen u e ente t de de poer de i e se e e e e a i e t ut ue a e e ceties eun esen ser an den prs ement la elles et se eoer t ls la cis D MM it par lui entenslieu : Boré n’ayant pas comparu la cause fut envorée devant le Bureau Général du Conseil ésant le jeudi lui juin mil huit cent friquite die huit. Cité pour le dit jours seuil mots ragis nuil. hraze jun par tettre du sorétaure du conseil ui late des onze juin mil fuit cent soente dix huit à la réquite de Fessis Dauré compret. A l'appel de la cause Pessis de présme Me et concut à ce qu'il plut au Bureau Général du Conseil lev Recu in Paire à lui payer avec intérêts suivant la loi la somme de quarant pours pour li tinie de la semaine de cougé qu'it re Gagelle lui a pas faipé faire comme d’usage et le condamner ex desote Dit son coté Pairé, par Charbes, son mandétaire, se travai et conclut à ce qu'il plut au Bureau Général du Conseil attendu qu'un règlement apposé dans ses atatien prévant les ouvriers qu'ils sne sont pas tenus de donner semaine de congé ni admis à le réclamer à l'haure de leur renvoir Que ce réclement, lorme entre lui et les ouvriers qui l’ait auqs un travaillant eue lui un contret verbal qui detouit l'ata du délai congé. Par ces motifs - Dire Flessis non recevable en sé demande, l'en débouter comme mal fonné celle et le condamner aux dépens. Pint de droit Doit-on condamner Pou à payer à Wlessis la somme de quarante francs pour lui tnir comtte lu de la semaine de congé qul de lui a pas toifré faire ? Ou bien dait-on dire lessis non recevable euse demande, l'en débouter à Que doit-il être statué à l’égard des dépens ? Après avoir entendu les parer en leurs demandes et conclusions respectivement et en avir délitéré conformément à la loi ; Attendu quil est constuatre qu'un réglement d’ételier appectié d'une mineure estensle dan Les ateliers de Paute rent les auvriers fibres de la etes filemant quoir bounr leursemble et résiprogquement le Patron, de les remercier à taite huire ; Que Flesst go déclare avoir en conneilsance, de ce recteames est mal fuit à demander semere de jongé que le dit réglement avait premes out de supfrmer ; Por ces motifs - Le Bureu Général jugeant en dernier repert ; Dit Ptessis non recevable on sa demande, l'on déboute et le condamne aux dépens euversr le Trésor Public, pour le papier timbré de laprése minute, cnformément à la loi du sept aout mil huit ent conquate, ene, non compre le cout le présente resn, cinifation d’ielui et sas sites. ies l u e et et anque dessus. Vagallie Leuicg serétaire E </div><div type=\"case\">Du dit jour Treize suin Entre Monsieur Bourlier Moitre, selfer, demeurant et domicilié à Paris, rue Harg, numéro soixente quatorze ; Demandeur ; Comparant ; D’une part ; Et Mlonsieur Bupetit, demeurant à Monierf seize et Martre, agissat au nom et comme admins trotur de la ersonne et des biens de son fil nneut constant apprenti Défendeur ; Comparant ; D’autre part ; Point de fait – Pur lettres du secrétaire du conseil de Prud’homme du Département de la Senme pour l’industrie des tissus en dates des Samedi premier et lundi trois juin mil huit cent soixante dix huit. onr lier fit citer comprtits à comparaître pas devant le dit conseil de Prud’hommes séant en Bureaux Particuliers les Lundi trois et vendredi sept juin mil huit Cent soixante dix huit pour se concilier si jaire se pouvait sur la demande qu’il entendait former contre lui devant le dit Conseil En éxécution de Conventions verbales d’apprentissage au paiement de la somme de deut cent francs à titre de deommage intérêts. Snpelit n'ayant pas comparu la cout fit rentayée devant le Bureau Général du Conseil séant le jour treize jun mil huit cent soixante dix huit. Cité pour le dit jour tinze Juin par lettre du secrétairée du Conseil en date du huit Juin mil huit cent soixante dix tit à la requite de Bouslier upept comparut . A l’appel de la cause Bourlier se présenta et conclut à caqt il plut au Bureau Général du Conseil dire et ordonner que dans le jour du jugement à intervenir epabit sera tru de faire rentrer chez lui son fils constant et de liy fépir pendant une anné qui lui reste à faire sur les trois qui ont été fixées pour la duteeten hui pe t p le u en p purt per du que cet apprentissage pro at demirrelie résolu ence ces condamner cou papit à lui payer avec intérêts suivant la loi la somme de deux Cent francs à titre de dommiges-intérêts et a en en es por le cande ete de pre M et conclut à ce qu’'il plut au Bureau Général du Conseil attendu e de e pe es u es emiene a elie e tete de tent que prse e e p e pet snteur pus ap an seit eme prl u e ene de te rgt u oe de e e e e der e le e e e ou simie ’ept moits ragis uiles le Lruch llée Maq qu Monter à it cit l'ablig peurant hite l’anée restant à faire à payee une somme de trois francs par semane par orter à la nourecter de son apprenti qui resta sun père parté ; mois attendu que Bourlier ne tit pas le nmouvel engagemez en privent, l’unpo des trois francs sur termoils il état en drait de comptes pour pai partie de sa nmurriture il se oit forcée de le reprendre ce quil fit le vingt un avril dernier non par se sonstraire à l’exécuti de t engagement, mois contrant et forcé du six de Bourlier Par ce motifs - Dire Bourlier nonrecevable en ses demandese l'en débouter ; Le recevoir reconventionnellement demandanm dire l'apprentissage de son fils purement etn timplement Vésolu d condamner Bourlier aux dépens. Paint de troit — Doit-on dire qu’il l ne papit sera tenu de faire rentrer son fils chez Bourtier pou cegtiner son apprenpsag par une année se présente, hui et fauté par lui de ce faire dire l'appentissage résolé, en cece condranner capelit à payer au tra Bourlier la somme de deux cents francs à titre de dommages-intérêts ? Ou bien dait in recesa toup . Dire Bourlier non recevabl en sa demone l'en débouter recevoir Papelit reconv entionnelleet demandes dire l’apprentissage doit s'agit purement et simplement résilé Que dott-il être statué à l’égard des dépens ? Ae cnneil très avoir entendu les parties en leurs demandes et conslutio respectivement et en osor délibéré conformément à la loi ttendu qu’il est constant que se mineur Ceapetit à été placé en apprentissage chez Bourlier pour trnq unne avec une rem unération foutature jusse du vingt ferrer dernier, mois décendé à jour ogatore pour une somme de trois francs par semaine ? que cet apprentissage doit recevoir son éxécution bindnet l'année restaut à faire sinon loupept payrà une indemnité que le coutre qui passate la demet moutères d’apprentisae u la somme de sivante Mrnde, cel emil le Bureau Général jugeant en premier ressort ; Dit et ordonée que dans les quatante huit heures à partir de ce jour bapelit sera tenu de faire rentrer sa fils constante dut hout lers pour à melle dnier huitquni pren a pet pal lui de se fice eui l s o ue et cete de pess ; dit que lets apprentissage se ine demeurea résolu lu ce cas, ces anpretis à fréret du etit à payer avecintérêts sur n lui à Burlier la some de cinquante francs lin le domneries intbtle, à cort domne dus ou ln ces ux désous tixés et liquidés en vers le demandeur à la conme de uni frinc fet à celle die u Trésor Public, pour le papier ttimbré de la présente minute, onformement à la loi du sept aout mil huit cent cinquante, ence Fa compris le cout du présent jugement, la significatione d’icclu et ses suites. Ainsi jugé les jour mois et an que defaut Eas ptille Lecusq secrétaire </div><div type=\"case\">Du dit jour trenze Jiin Entre Monpeur Louis Voubert soupeur d’habités, demeurant à Paris, rue de la bente tir numéro cinquante ; Demandeur ; Comparant ; D'une part ; Monsieur Purant ; confectionneur d’habits, demeurant et domicilié à Paris, rue croir des Patits chemps, numéro cinquante ; Défendeur ; Comparant ; D'utre part ; Dinq de fait - Par letres du secrétaire du Conseil de Prud'homes du Département de la Seine pour l’industrie des tissus en dates des Mardi quatre et vendredi sept jaunvier mil huit cent soixente dix huit Joulert fpt citer sinant à comparaître par devant le dit conseil de Prud’homme demil au Bureaux Particulier les Vendredi sept et Murdi onze vun mil huit censoixmmmte dix fait sur se concilier si faire se pouvait sur lu demende qu'it entendait former cente lui devant le dit conseil en delai conge domme dusage Denait- n’ayant pas comparu la caute fut renvayée devans le Bureau Général du Conseil séant le jeudi toize juin mil huit cent prbte de li di e le dt es hun sem pr lettre du secrétaire du Conseil en date du onte juin nif huit Cent sixante dix huit à la reate de outant denans aplels; Ail le det dte sudi épaut de eue sut an en et oncsue que det hui e e e e e qui edte pue t e e e fitr de de afur l contilous es re de lui poarseue udits pos l l se e er pl de onos qnr i d it fi t M t de dommages-intérêts et le condamner pans les deur ense dépens. De son coé dement se présenta et conclut à ci qu’il plut au Bureau Général du Conseil attendu qu’il embouche pouler, pour un cop de moins et qu'en sontaires le conseisa à cajournée de quatre francs cinquante centimes de non con mis sonners le prétend ; qu'il la anpe dé poue qu'il étant en capable de bien faire la latijue ; Devait motifs ; Dire Soulert non recevablien su semante l'en déboiter et le condanner ux dépens. Panit, de droit Dait-on dire que Finand sera tenu de recevoir etae lui peutert et de lui continuer pendant le reste du mill de juin du travail de son état de coupeur d’habits enten servant les appointement de cent vingt francs pour le dit mois, sinon et faute par lui de ce faire le condamner à payerau dit Toulert la somme de cent vingt francs à titre de dammaser intrêts ? Ou bien dait-on dire Toulert non recevable ses demandes, l'en débouter ? Que doit-il être statué à l’égard des dépens ? Après avoir entendu les parties en lurs demandes et conclusions lespertivement et en avoir délibéré conformément à la loi ; Attendu que les ertiusion fournier par les parties et de l’examon des lisres de sinant ressort aue pubert est resté plus de trois chez simand e qualite de counpeur Fhibits ; que quoique payé par à comptes il était au mois ; Qu'en ce cos il devait prévenut et être prévene en mois à l'avonce conformément à l’asa que reçit cette industrie ; Qrue Finand gur l'a causedie le premier juin pour l'avoir préven le dait recavoir chez lui et lui doit donner du travail ponstant tout ce qui deste à faire de mois de juin en lui payait le mois cen entier ; Par ces mots Le Bureau Général jugeant en dernier lepart ; Diit et deuie que sinant reuvea chez le toutert et lui donnére de travail de son état pendant tait le mois de juin présent vuren lui pupara six peurs par semaine pour se prourer du travail, sinon et faute par Dinand desa conforméere à Cetée décixon ; le condamne dès à présent à payer au dit Poulers le somme de cent vingt francs à titre de dommages-intérêts ; L condamne dant tous les catux dépens taxés et liquides euvriée demandeur à la somme de un franc et à cette due au Trésil Public, pour le papier timbré de la présente minute, conformément à la loi du sept aox mil huit cent cinquante, ence non comptrian at di pepren saqyant, las sopntils. d’calu elestile lure les jour mois et an que dessus Cle as Et fancq etrétaie ce </div><div type=\"courtHearing\">Audience Jeudi vingt Jng mil huit ceut soixante dix huit siégeant . Messieurs Dinaud, Péalies de la légion d'honneur, vice Prérident du Conseil de Prud’homme du Département de la Seine pour l’industrie des tissus, Tiendant l’olndame n remplacement de Monsieur Martenval Président de dit Conseil, supicté, Esssoy, Larcher Brentry, edhriz ; Pud’hommes assistée de Monsieur secrétaire du dit Conseil. au ee2 Entre Madame Veuve Méniar, ourière fauriste demeurant à Paris, rue de Belleiille, numéro cent sut ; Dumanderese ; Comparant ; D’une part ; L Monseur veuve lorne, demeurant et domiciliée à Paris, rue se feubeure Pain, Denis, numéro seize, agistant comme administrities induuante de la maison pe reprocation et de comme de la dame Tepetème se seur ; Défendeur ; Comparant ; D'autre part, Pont de fait – Der lettres du secrétaire du Conseil de Prud’hommes de Département de la Seine pour l'industrie des tissus en dates deus Vendredi trois et uunti six Mai mil huit cent sixunte dix huit la veuve Moron fa citer deuve Détorme à coumprraître par devant le dit conseil de Prud’hommes poun en Bureaux Particuliers les lundi tese Mardi sept Mai mil huit cent cinute aqfis pu t te e s fait puir ulaet cenitie entendait former contre lui devant le dit conseil en paiement de la soe de seize cent francs pour selde de provision à laison le cinq pour aet ur pes et onternspuen luitue n’ayant pas comparu la case fut renvoyée devant le Bureau prt se e it lu ’enti eil lui et li g tente ix lit e liy le e comme e enl enie et exposa au Conseil qu'elle entra leiy novembre mil huit cent pa t six du e e u elle deie u centre tur le su for p u en un u ente le te e end es ur eu er u prsente ei pu duie soant quea lur pons luit de dapres et coutde six depoit ce die teuite e it r is pas intatire emne ei M jeuve Déferne, de soubité, se présenta et expost au Consil qu’il a payé à la veuve Moson chaque semaine le prisd les journées à sept francs, ausil a versées plus iaus sommes on à compte sur sa provision qu'il offrine et de cinq pour cent sur les bénifices et snon sur le prédeunr de rntert ; Qu'on fu de compte elle en la débitrice et ne sa créancière. ll demandu l’ajournement de la canse par prouver son apartion. Le Bureau Général ajournt a caunse et chargea l'un de ses memtres de l'examet de l’affaire taut au hoint devae de la convention qua ce lui des compte Par lettre du secrétaire du Conseil en date du vingt cinqlaie mil huit Cent soixante dix huit la veuve Mégron fit citer vouve Delarne à comparaître par devant le conseil de drudhom séant en Bureau Générall udi six Juin mil huit cent soixante dix hui pour t'entendre condamner à lui payer la somme de sene cent francs qu’il lui doit à raison la cinq pour leur sur le choffe net ses affaires et trentenr retondamner ax dépens.. A l’appel la veuve Menin se présenta exposa sa demande et conclut à ce que veuve Vélorne fut condamne Pare à lui payer avec intérêts suivant la loi la somme de seize cent francs qu’il lu doit jur prousien à cinq ponr cent par les fairs et le condamner ux dépens. De son coté vouve Détorne se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu que la veuve Mernan ent entrée et restée dans la maison cahitaine au pris de sept francs par jour et cinq pour fent sur les bénifices qus non boulement it a payé chaque semaine le produi des journées, mais à donne des à compte se chiffrans travant somme su parienme de brouvaut à celle qu'il erait de payer. Par e motifs – Pire la veuve Megrandon recevable en sa demande, l'en débouter et la condamner aux dépens. Le Bureau Général aourna au vendi tingt Juin mil huit cent soixante dix huit. LVeude vingt pui pasoue eMleseln seprétite et atirl les quel ls ri ue requierant l’adjudicetion. Vouve Desonne ne comparnt prs. Pungele soit ; Doit en en demn e l les aprsà la veuve Mayor la ome de sixe det faus p sitée u ie pair cent ui t d et e t e po e liun dit- il letre la seune Mésir mart e ue sa demente do rilet tu dit d’ te det fouier ou dien . depre euventel e le e e te e ce stesouns epadement ; compertienes re le nen nl d it ue uns e i o ? De unle charge de l’examon de la cause en son rappert verbal et en avoir délibéré conformément à la loi ; Attendu que de l'audition des parties à la barre du Conseil et de l'axamen le membre du Conseil doms à l’effet de voir la comptetible veuve Lébornée que la demande de la veuve Monrer Ee est juste et fondée. Pur ces motifs - Le Bureau Général jugeant en premier repart  ; Condamne vouve Léforne à payer à la veuve Mégran la somme de seize cent francs qu’elle lui réclame comme soble de pronision sur les offaires de la maison capitime du cinq rovenb mil huit cent soixante treize au jour de sa sortie de condamne en outrer aux dépens taxés et liquidés en vers la demanderesse à la somme de deux francs cinq centimes, et à celle due du Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence non compris le cout du présent jugement la signification d’icelui et ses suites. Ainsi jugé les jour, mois et an que dessus. maie Gs decucq seurétaire ps </div><div type=\"courtHearing\">Audience du Jeudi vingt Juin mil huit cet soixantre dix huit iégeannt, V’esseure, Marmenval, Thévalier de la férçon d’honnem, Présideit du Conseil de Prud’hommes du Département de la Seine pour l’industrie du tissus, Larcher, iroy, Monnier, Angitour Bonsher et, Brccc, Prud’hommes apsistés de Monsieur Lecucq secrétaire du dit conseit. Cite onjour e ladaune Dinil, le sieur Déneltant en son nom personnel que par apister et autoriser la dame son étouse ouvrière imentende en fleurs artificielles, demeurant ensemble, à Paris, rue Maudon, numéro dix huit ; Demandeurs ; Comparant ; D'une part ; L Monteur Monnser Cordonares, fatricant, en Marchand de fleurs artificielles demeurant et domicilié à Paris, rue Levonne, numéro vingt un ci devant d actuellement en la même Vile rue veuve Saint Eugustin, numéro trente ; Défendeur; cotilaut ; tute part ; Pouit dé fit - u cete du secrétaire du Conseil de Prud’homes du Département de la Seine pour l’industrie des tissus en date du Mardi ure dumeil ut retient de lat ingen eile prir ies doenver eprte pardaunt le duie Conseil de Prud’homes séant en Bureau Particulier le qudet ente taen lut du prante se li purt on de e pur l pri dn la ee ndtedet pour dt deu e e apour e te prete enppe es pour laremeitie atendur satrit. e deseur se quit ene l prtint e p re u ee e un ere n eu de d t seent les es pere e la eugu es lomisie merdeie l te ent au qur du t i l ne e puser eun ere e e pre que e e quilese eu eu e ne qu us par doznnée, puis en reps pesjout ? Que le mardi quite juin Monnier ayant oulitement, toucédié lal dane Denael, alleu s'envu contrainte de lie faire appelé devant le Conseil auxfins de dommagesintérêts . De vo coté Monnier se présenta et exposa au Conseil qu’il est mevait qu'il ait tougédié la dame Denette e n’a rappelée qu’à l’éxécution de soncatitrnt on la ntine l’erdre de se trouver au mage sin le malie à neuf heureit au bien de dix qu'elle parai fait vou loir actopter. Les parties n’ayant pu étée conciliées la canse fut renvoyée devant le Bueu Général du Conseil séant le jeudi vingt juin mil huit cent suixante dix huit. Cité pour le dit jour vingt juin par lettre du secrétaire du Conseil en date du quatorze Juin mil huit cent soiante dix huit à la riguite des époux Demalle Monnon se présenta et conclut à ce qu'il plut au Bureau Général du Conseil attendu qu'il s’auet, de l'interpretation don Tontree de travauil, d'une part ; D'autre purt e statuer sov me demantes de douze cent francs quand le conseil de Prud’hommes ape conmettre que des affaires dant la demande ent inférieure à a deux Caits francs ; Attendu querre qu’il a peaiti citre les époux senelle devant le tritaral de commerce sont compétent pour conmaitre d'une affaire drcette niture et de cette inparan Par ces motifs — Le délarant n compétent pour connaitre de la demande des époux Denelle les en débouter et les condamner aux dépense De leur coté les époux Denelle se présentèrent et conclurent à ce qu’il plut au Bureau Générals du Conseil attendu que la dame Denelle nt entrée chez Monnier comme louvrière monteuse en fleurs et plarmes,; qu'il s'agit de l'astirfictions d'un contrat de travail et de sommages intérêts suins résuller de son inexécution ; Que dans l'espèce le choffie ’élové qu soit doit être sournis aux juge de prémier onstance ; lu d'autre part l'rtion de Monnier devant le tribanel de commetce était pas tervenme à celle qu'ils ont formée londe lui devant le Conseil ne pert avoir pour etet de chanter la rature de la cause qui ost me de retations d'autère cotun ; Par ces motifs –; Dre Monnier ontre on en sa demande,; l'in dé bouter ; La déclarer, competeur ritezer la cause etordonner qu’il sere passe entre du outecrtire, de Monner pa être apporé et sar le land condamner Mingie aux dépens. Pint de drit ait-en Le déclarer en compétait pour lomaitre de la demende des époux Denelle nenvayet ceux oi devant que de pit luillas pex M M eth t Crtt Meth a tip iis parsiaint Memer apelus cesier Sréger Public reaja et un gois que eul alporie f ecu t E cets reuva pprouvé et A onlr it Ou bin doit-on se déclarer compêtent retenir la cause, r donmer que les parties l’expliquerant sur le faud pour être statué ce après avoir entendu les parties en leurs que de drait – demandes et conclusions respectivement et en avoir délibéré Conformément à la loi, sur le éctinctaire de Monnier, ttendu qu'il est constant que les demandes des époux Dinelle portent sur un contret de travail een conséquences que le conseil doit conmaitre de les demandes par être statué en premier ressot ; Par ces motifs — Dit Monnier non recevable en ses demandes, l'en déboute, retient la cause, or donne que les parties s'enpliquerant et que le conseil statuère en suite sar le mérité de leurs conclusions. L ce mineur Monnier lertire dec lavans faire défaut. de leur coté les époux Denelle contneut à ce qu'il plut au Bureau Général du Conseil donner défat contre Monnier non comparant ni personne pour lui quelque dument appelé et pour le proft le condamner à leur payer avec intérêts suivant la loi la somme de douze centes pris ur somrmilies de oveiles le suend es le condamner aux dépens. tant de dait- Doit-on donner défaut contre Monnier non comparait ni personne pour lui quoique dument appelé et pour le proft adjuger aux du ei tes l l le on lu i pr le e e ir purdint lap satié renselen pus à aer entendu les épous Denelle en leurs demandes et conclusions enier lité la soman l le e t cile uente de et e e u pe te per qr l le e la te e pardemne e de pr e eme e ser conqurate e tie e t ten ene pls uint lentete eneit e la pa e psup permil suit de ou ge paprd ue esun tent de poueme pl adi encoemte ceu t e pee e ldi en en eu e de e en te e le t l so que du ene e de ets ou ete e t de e tretes are e de pr u ant pem l les quint e e aue des de ou de d cpte eu te eu teil se te te e e e e ui de cit e n i que es paur signifier au défendeur le présent jugement, coent chempu Ainsi jugé les jour, mois et l'un de ses huisiers audienciers. an que dessus. aviu àr Le cudf serétaire Qu dit jour vingt sing Entre Monsieur Mathieu, ouvrie tailleur d’habits demeurant à Paris, rue Montranyu numéro neuf ; Demandeur; Comparant ; D'une part ; Et Monsieur Bordeau ; Maite dailleur d’habits, demeurant et domicilié à Paris, rue saint Fonsliqne, numéro deuze ; Défendeur ; Défaillant ; Pautre part ; Point de fait — Par lettres du secrétairn de conseil de Prud’hommes du Département de la Seine pour l'industrie des issus en dates des Mardi quatre et vendredi sept juin mil huit cent soixante dix luit Mathieu fit citer Bordeaux à comparaître par devant le dit Conseil de Prud’hommes séant en Bureux Particuliers les tendredi sept et Mardi onze juin mil huit cent soixante dix huis por se couilier si faire se pouvait sur la demande qu'il entendaiz former contre lui devant ledit Conseil au paiement de la somme de treite neuf francs qu’il lui doit pour salaire . Berdeant n'ayant pas comparu la cause fil uaagée devat le Burau férére du contile leur Chaid vingt juin mil huit cent soixante dix huit. Cité pour le dit jour Cti9 por par titre du serétaire du conteil en bité deu edte joun seil lus ae pirsate de lait à le ouitel oin Bordeair ne comparut pas. A l'appel de la cause Mathieui se présenta et conclut à ceu’il plut au Bureau Général du Contail donner défaut contre Bordeaus, non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui ayet anet ntelite suiveant la loi la some de oet frncs u’il reste lui devoir nayant donné le cinq juin vinq franc sur lante seuf, Juailell duamité e ssaie e le our pis par pertie de temps deuns le Bnraux du contre u dépens. Pois de doit - Doit-on donner de fus letue coisdevur non comparant ni personne pour lui quoique dunen disqpu epepede on dem t prlui pruédement pas à ui dit -il être ain A n ue itée s ils à us 4 t l’égard des dépens ? Après avoir entendu Mathieu en ser demandes et conclusions et en avor délibéré conformément à la oi ; attendu que la demande de Mathié parent juste et fondée ; Que d'ailleurs elle n'est pas contestée par Berdeaux non comparant ni personne pour lui quoique dument appelé ; attendu qu’en ne comparaisant pas levant les Bureaux du Conseil Berdeaux a causé à Mathie un préjudie de perte de temps, ce qu'il doit réparer ; Par ces motifs - Le Bureu Général jugeant en dernier rettert ; Du l’article quarante un qu decret de onze juin mil huit cnt neuf; partait rislement pour les contils de Prud'hommes ; Donne défaut contre Berdeux non comparant ni personne pour lui quoique dument appelé ; Et adjugent e profit du dit défaut ; condamne Bordeant à payer avec fard luvant lelé à Delheu la somede se euf francs qu’il lui doit pour sefaire, plus une indmnité de dix francs par taups perde qu lendemendet susles e liquidés envers le demondeur à la somme de un franc, et à cellee eue au Trésor Public, pour le papieer timbré de la présente minde, la sorément à l le de q ei que lus luse die t ris en ps de de inseunge e perte e e s l et le e ile sur ec peat t e t on ein e es e e e e e ex lui de ta t oer etie à largre e esoie Mavirn à et an que des sus. Leuig recélaue duien en cingq ant e dete e e etie erenteu eede de den ete qa den dirt dementies de t in e one eni e e e te an e eri que m an du ete de de e eme dete et e e ne e e e </div><div type=\"case\">Du dit jour vingt Jeuit Entre Monsieur Ematile Charsé t ouvrier sellier ; demeurant à Paris, pusage Bouchardy, numéro treis; Demandeur ; Comparant ; D’une part ; E Monsieur Balendier, autre prenoor de travaux de silernie, demeurant nt àParis, rue de tafoyette, numéro deut cent quatante trois ; Défendeur ; Comparant ; D’autre part ; ande Monsieur crousselle Maite Geltier, demeurant à Paris rue de lafayette, numéro cent trenteneuf ; Défendeur ; Comparant. Anssi d’autre part ; Point de fait ur lettres du secrétaire du Conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus en dtates des samedi huit Juin mil huit cent soixnte dix huit, Charrier fit citer Balegdrer à comparaître par devant le dit conseil de Prud’hommes séant en Bureux Particulier se le Mardi onze juin mil huit ces soixante dix huit para se concilier si faire se pouvait sur la demande qu’il entendait suse former contre lui devant le dit conseil en paiement de la somme de trente six francs pour indemnité de temps perdu. A l’appel de la cause Cérrier se présenta et en posa comme à dessas. De son coté Balerdier se présente il retonnet qu’après avoir donné du travail pendant pate me lonaire à Blerer lu et pes pars donner enure il le pa venir Chaque jour de la durane mais qu’ayant lui même été abosé par prasselle sonitatron e e edemen en le e rdes. qu e par l e t s tendes dein et dandil prer t ondit u dt lui Mai ente e pr t e lete lun dre dte a aon eu e te e les en eur pur en t e ne t poir rande eu ete deit re t e e e et enene ape ur e den ant le se eue a cem espse u deme pur deure des oue den drer ne e tente en e te ememre e quele ade dente is d e te de é doe en la I5x trois mots tayés oils aux Lecuiq se per De son coté trousselle se présenta et exposa au Conseil que non seulement il n'a pas embouche Charrier mois qu’il ne la seulement jamais vu chez lui. Le Bureau Particulier fut d'avis que Bulerdier qui reconnails aà avoir causé du perter de temps devait payer à Chévrier une indemnité qu’il fixa à vingt cinq francs et en qugea charrier à retirer sa demande envers trousselle qu’il ne jugea pas responsable. Aurrier ayant déclaré persister dans toutes ses demandes la cause fut renvoyée devant le Bureau Général du Conseil séant en Bureau Général lejoudi vingt Juin mil huit cent soixante dix huit. Cités pour le dit jour n dt jue par lettre du secrétaire du conseil en date du quatorze ju mil huit cent soixante dix huit à la réguite des Chabrel Malendier et troup elle comparurent. A lappel de la cause Chavrier se présenta et conclut à ce qu'il plut au Bureau Général du conseil condamner solidairement Balessier est trousselle à lui payer avec intérêts suivant la loi la somme de trente fit francs pour indemnité de perte de temps et les condamner solidairement aux dépens. Poton, coté Balesdier se présenta et conclut à ce qu’il plus a Bureau Général du Conseil lui donner cite des les erves qu’il fait de résiiter à Troussille les sommes auxquelles le Conseil turair devoir le condamner envers le demandeur; De son coté Trousselle se présenta et conclut à ce qu'il plut au Bureau Général du Conseil le déclarer nin respinsibe erlemettre pors de causé duns dépens. sont de droit doit on condamner Baleydier et ousselle solidairement à payer à Charrier la somme de trente six francs à titre de réparction du préjudice pu'lils lui cause sienment ; Dot on donner cité à chartie de Baley dier des resorves qu’il fait de repiter à Troissielle les sommes auxquelles le pourris être condamné envers Chavror à Pait on dire truflle nou responsible et le mettre trort de couse Doit Que doit-il être statué à l’égard des dépens ? Apréès evorte. entreavu les parties en leurs demandes et concfusions respectivement et en avoir délibéré conformément à la loi ; Attendu qu'el qui coucerne troupelle qu’il est acquis aux débité ate confié à Balegaier de s travaux pour être exécutes à la tatte et à des prix détermints ; que le fait davaint at refé au dit Belrysier des ouvriers, tefait fit à pevier se paut le end re uni tes eunver esprs ut deraites de son entrepreneur ; Eonce qui conderiet elgde d ls it eute attendu que charrier à déclaré à la barre du Conseil que Ballortier était son aperié de fait et non son patron ; que à le dit Balordier était en nom c'est que au seut pollediert un local et que s'il touctut une avant part sar les prix da façon d’était unsquement pour payer le prix de lalocations es pourvoir à de momes dépenses argentes ; Que ce fait acaires Charvier devant en déviduellement supporter le part des dommages qui rappent la sicé n’a rienc reclaner à sa rendier, son tollégène, éttent comme lui ; Par ces motifs Le Bureau Général jugeant en dernier réport ; Dit Trouselle non responsable, te met purs de cause ; Dit Charrier non recevable en sa demande envers Balesodier l'en déboute et le condamne ux dépens envers le Trésor Publie, pour le papier timbré de la présente minute,; conformément à la loi du sept tour mil huit cent cinquante ence, non compris le cout du présent jugement, la signification d’icelui et ses suites. Ainsi jugé les jour mois et an que dessus. Lecuig secrétaire eaeveir e Quit jour vingt ing Entre Monseur Voiter Decroëq, ouvrier teltiers umér tote euf demeurant à Paris, rue Demandeur ; Comparant ; D’une part ; Et prima dendier tipe et pour eleur l setie emnt e de e e e ene eu qeat eu e e enes aeante euper cepits de tu ires dune du det inqe etie un tre e eteil n deu aeier au es de te de t ir de e e e e pus ser t e e esen e pu et em e te r aupe por e t es denses de e e ie hunt e ent u guse n e en pur de e is Moni Conseil en paiement de la somme de trente six francs pour réparation du dommage qu’il lui a cause n la promettae du travail qu’il ne lui donna pas. A l’appel de la cause Lecrorqse présenta et exposa comme cn dessus. De son cote Baledier se présenta et exposa au Conseil qu'après avoir occupé Decroëz afsey régulièrement pouttant une se maner il le fit veuir chague pour de la semane suivante pousoint chaque jour paureur lui donner du travail qui lui était proms par tronpel son Patron mois cellaci ayant trouvé à faire faire à plus bas prir, ce qu’il lui destine il ne put on donner n'en ayant pas. Pa cemanent Devron, repuit la parale pour due que trousselle l'ayant embourset chez lui et l'ayant atrepe à Balerdier était responsable de pertes de temps qu'il avait éprouvées et demandu la remiselle la cause pour le pouvair aitionner. la cause e cet état fut renvoyée au vendredi quatorze Juin mil huit cet soixate dix huit. Cités pour le dit jour quatorze Juin par lettres du secrétaire du conseil à la reguate de Decroëq alesdier et troupelle comparurent. A l’appel de la cause Decroëq se présenta et affirma que tropselle l’avait embourtie et achrepé à Bagdier qui lui donna du travail sont sa rerommendation tropselle nia avoir imbouche secroëq et dit même ne l'avoir jamais vucher lui. Le Bureau Particulier fut d’avis que Baleririer qui resonnat avoir fait perdre à Decroëqune sormaine lui promettait du travail devait lui payer une indemnité qu’il fixa à la somme de vingt cinq francs à il tengagea Decroëg à retirer sa demande en ce qui concerne trousselle qu’il ne crot pas respontible Decroëy ayant déclaré persister dans ses demandes le cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi vingt Juin mil huit cent loixnte dix huit Cités pour le dit jour vingt Juin par lettres du secrétaire du Conseil en date du quatorze Juin mil huit cent faisine dixhuit à la requete de secrous Balerdier et pronssill comparurent. A l’appel de la cause Decrot à se présenta et conclut à ce qu’il plut a Bureau Général du Conseil condamner solidairement Balerdier et Trousselle à lui pay avec intérêts suivant la loi la somme de trente six francs pa réparation du dommage qu’il a éprouve par le chemaie de toute une semaine et les condamner solidairement eaer dépens. De son coté Bolgdier se présente et conclut égul plut au Bureau Général du Conseil lui donne vacte des i V ligé l u resorsesqu’il fait de repiter à tronsselle ces sommes ourouilles age le Conseil croirait devor le condamner. De son coté tapelle se présenta et conclut à ce qu’il plut au Bureau Général dmuaaner du Conseil le dire non responsable et le mettre puis de causé suns dépens. Point de droit — Doit-on condamner Baloudier et troupelle solidairement à payer à Decrocq la somme de trente six francs pour indemnité de chamage Dait on donner aite à Balegdier des reservis qu’il fait eel de répete à tronselle les sommes axmelles il aurait été condamne ? Dait-on dire trousselle non recevable et le mettre pors de cause ? Que doit-il être statué à l’égard et des dépens ? Après avoir entendu les parties en leurs demandes e et conclusions respectivement et en avoir délibéré conformément e à la loi ; Attendu en ce qui conterne cropelle qu'il est acqust aux déboté quel a confié à Balerdier de s travaux pour être à exécités à la taihe et à des prix de termins ; que le fait par oupselle devoir adrehé des ouvriers au di Wolerdier, refait fut il acquit, ne peut prendre responsable en ces ouvriers du fait de son entreprenent ; Cresé qui conterne Baleydier; attendu que Decroya déclaré à la berredu Conseil que Baley dier était son aprcée et non son datronds que e Bales dier était sulen nom ses que sul il pas sidant anlocal ; Que si le dit Balesdier aprélevé sur les prix de façon une avant part avait portige d’était anquement par payer les tavoes et res le paes ue miner fournitures ; que ce fuit acquis Decroëq devant in dividuellement sapporter la part des dommages qui etteiuent la socete vislé rien à réclamer à Baler dier, Sourellegue, attent some cinq pet cesmuitte le sire hant puant donietrs, Cit ouete pe i quel le demie pus lasile, et douden en tent demande envers Balendier l'en déboute et le condamne eux dus minte deu cite pu e e te sa preeane omen desqou entererant juige es trer uain les p e te sesin ei purs lesoul eu ses eu n esi e aerui et reuig serétai L E </div><div type=\"case\">Du dit jour vingtuin Entre Monsieur Toales Mégeont, ouvrier Poller demeurant à Paris, rue des paissonniers, numéro seize ; Demandeur ; Comparant ; D'une part ; E prima elonsieur Bales u entrepreneur de travaux de telterie, demeurant à larise rue de Lafayette, numéro deux cent quarante trois ; Demandeur ; comparant ; D'une part ; Ctecand, elaupeur Trouselie Maitre sellier, demeurant et domicilié à Paris, rue de Lafayette, numéro cent trente neuf ; Défendeur ; Comparant ; causoi d'autre part ; Oout de fait - Par lettre du secrétaire du Conseil de Prud’homme du Département de la Seine pour l’industrie des tissus en date du samedi huit Juin mil huit cent soixante dix Mignoul fit citer Bales,jour à comparaître par devant le dit Conseil de Prud’hommes séant en Bureau Particulier le Mardi onze juin mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendu former contre lui devant le dit Conseil en paiement de la somme de trente six francs pour réparation du dommage qu’il lui a cause fir des pertes de temps à l’appel de la cause Mogeaul se présenta et exposa comme ndessus. De son coté Baleydier se présenta et reconnut avoir employé Migeaul opt et régulièrement pendant toute une semaine et l'avair fait venir chaque jour de la deuxrème ponsant pouvour lui donner dé travail son Patron, trousselle, lui en ayant promis; mais celui ayant trouvé à faire travailler à des prix de duits ne lui on donna pas. Mle moment Mégeaul repris la parate pour exposer que troisselle l'ayant embouihe et a dripe à Balesdier devait être responsable de son chamage .Il demando àe l’actionner en responsabilité. La cause en ret état fut renvoyée au Bureau Particulier du vendredi quatorze du même mois. Cités pour le dit jour Balesdier et trousselle comparurent. A llappel de la cause Migeail se présenta enposa comme tle huit Jain et offerma que trousselle l'avair enlere et envoyéet Balesdier avec l’ordre de le recevait, lu tronsselle ia avoir embauché le demandeur et décara ne l’avoir même jamais ve cher lui . le Bureau pParticulier fut d'avis que Balerdier reconnaisaut avor fit chener Migesl pendant toute une somane le faisant, venir chez lui chaque jour il lui devait suf une indemnité qu’il fixa à vingt cinq francs. Wendeg Migneul à retirer sa demande envers tronsselle qu’il de jugea pas être responsable ; Migeaul ayant déclaré persister dans ses demandes la cause fut renvayée devant le Bureau Général du Conseil séant le jeudi vingt Juin mil huit cent soixante dix huit. Cités pour le dit jour vingt Juin par lettres du secrétaire du Conseil en date du quatorze Juin mil huit Cent soixante dix huit à la réquate de Migeant Balegihier et tousselle comparurent. A l’appel de la causé Migeoul se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner solidairement Balendier et Traisselle à lui payer avec intérêts suivant la loi la somme de trente six francs à titre de réparation du dommage qu’il àéprouvé par du chomage de leur fait et les condamner ux dépens. De son coté Balerdier se présenta et conclut à ce qu’il plut au Bureau Général du Conseil lui donner cité des reserves qu’il fait de répiter à tronsselle les sommes ausquelles il croirait devoir le condamner. De son coté dmame tronpselle se présenta et conclut à ce qu’il plut au Bureau Général du Conseil le dire non responsable et le mettre chers de causé, sans dépens. Point de droit — Doit -on condamner Baleydier et trausselte solidairement à payer à Mégeanl la somme de trente six francs pour indemnité de chemage ; Dait-on donner acte à Balesdier des résorves qu’il fit de répéter à Cromptelle les sommes ax quelles et arait été condlamné à Dait on dire tropelle nun respousible et le mettre port de causé ? Qu ditit M être statué à l’égard des dépens ? Après avoir entendu les parties en leurs demandes et conclusions respectivement dem anr silé la soméent lelis Meneau qu tacane compele e du peour eis el adu du travail à Balerdier pour être exenités à la toite eta des poar deni, que les devie parsemes dit Balesvier, afit fat i prouvé, ne puit le rendre desponsible erde emie er dete ntomn cons ant ou e e de e le u étire de eis e e ite deant e quentu pere e e e en es our e l e e e e eie oefaute m p t de doment paur pr le er e et our u parse ate nenr deus, ou prme eu ene t it, Ai devant in deréduillement sapporter la part ses dommages qui fraphent la souile n'arien à reclamer à Balais son talleque attend comme lui ; Par ces motifs Le Bureau Général jugeant en dernier report ; Dits Trouft elle non responsable, le met sers de conte Dit Mégean nonrecevable en sa demande envers Balesdier, son callegue, attent comme, l'en déboute ei te condamne ux dépens anvers le Trésor Public pur le papier timbré de la présente minute, conformément à la loi du sept aot mil huit cent cinquante rence non compris le cout du présent jugement, la signuigées d'icelui et ses suites. Ainsi jugé les jour mai et en que dessus. Decraveu Leincg secrétaire rant </div><div type=\"case\">Du dit jour vingt rnz entre Monsieur Benort Lébart, ouvrier sellier; demeurant à Paris, rue des tctuset saint Martin, numéroi vingt trois ; Demandeur ; Comparant ; D’une part ; primo ; Monsieur Balerdier, entretreneur de travaiux de tellerie demeurant à Paris, rue de Lafayette Guméro deux cent quarante trois. Aafendeur comparant ; D’autre part ; L serando , Monsieur Trousselle, Maitre sellier, demeurant et domicilié à Paris, rue de rapayette, numéro cent trente neuf ; Défenreur ; comparant ; Quulis d’autre part ; Point de fait — Fri lettre du secrétaire du conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus en date du farmes huit juin mil huit cent soixante dix huit Lebont fit et tere Balerdier à comparaître part devant le dit Conseil de Pondi hommes séant en Bureau Particulier le mardi onze juin mil huit cent soixante dix huit pour se concilier si faire si pouvait sur la demande qu’il entendait former cont lui devant le dit conseil en paiement de la somme de Trentesix francs pour ruparation du dommagef lui a causé en le faisoit chamer taute une Lemander là l’appel de la cause le bert se présenta et exposa teu cidissus. De son coté Balesoier se présenta M exposa au conseil qu’après avor ocupe de bert asser régulièrement pendant une se maine quoiqu’il l’ant ni occuper de vantage, et que penvant la deuxième il l’e fat revinir chaque jour lui promettait de travaul comptaut sur la promepe qui lui était chaque jour fuite par trousselle, son Patron, et qu’il ne tunt pas ayant travé à faire travailler à des près enférieurs à libre mement Lebert repis, la pirate pour dire que trousselle l'ayant mtauché et adrepé à Batesdier il demandant à le citer comme responsable. La cause en ce étt fut renvoyée devant le Bureau Particulier du Conseil séant le vendredi quatorze Juin mil huit cent soixante dix eup Cités pour le dit jour quatorze juin par lettre du secréaaire du Conseil à la requete de retert Ba lerdior es trousselle comparurent.. A l’appel de la cause le bert se présenta rappelu la demande envers Balerdier et demandà la responsabilité de Troufs elle comme l'ayant embourte et adrefié au dit Balégdier , tront selle de son coté de présenta et exposa au conseil qu’il n’a mellement embaurtie déberr, que de plus il ne l’a jamais vai chez lui. Le Bureau Particulier fut d’avis que Balesrer reconnaisant avon fait perdre du temps à Le bart lui devais une indemnité de vingt cinq francs, et ne trouvait pas motif arendre topselle respousable il enqadea Le pert à retirer lu demande envers celui à Lebers ayant déclaré parsister dans ses derindes la cause fut renvoyée devant le Bureau Général du Conseil séant de jeudi vingt Juin mil huit cent soixante dix tuit Cités pour le dit jour vingt juin par tettres du secrétaire du Conseil en date du quatorze juin mil huit cent soixante dix huit à la réquite de lebeit Balegdier et trousselle Comparurent. A l'appel de la cause le bort se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner solidairement Baley dier e troup elle à lui payer avec intérêts suivant la loi la sonme dde diu ue e e ue etese t lemit prde prur se poer cait on pome es en de e en er lsent s senbile pl e de du tin le t saou Burea Graral du Conseil lui donneraite des réserves e l e e pat l domen e le deu u demen p te s E un mit jusée noff Ak Lécule Da le it it il de son coté, se présenta et conclut à ce qu’il plut au Bureau Général du Conseil le déclrer non lesponsable et le metir rois de cause dons dépiens. Pnt de droit Doit-on condamner Bolesdier et trousselle à payer à Le bers le somme de trente six francs pour léparation du tort qu'ils lui ont causé par pertes de temps ? Dcit on donevaulle Balerdier des reserves qu’il fait de repeter à trons sette les sommes aux quelles il pourroy être condamne par la Conseil ? Dait-on dire trous selle non responsabelie le mettre pors de causé ? Que doit-il être statué à l’égard des dépens ? Après avoir entendu les parties en leurs demandes et conclusions les pertivement et en avoir délibéré conformément à la loi ; aAttendu qu’il est acquis eux débots que troufs elle à confié à Baleyier de s travaux pour être exeuités à la toche et à des prix déteminés ; que le fait par Trouls elle d’avair la drepé des ouvriers au dit Balerdier, cefut fit il acguis, ne pourrait le rendre resonsable vis à vis de les ouvriers du fait de son estrepreneur à leur égard ur en ce qui concerte Balesdier ; attendu que Lebert à déclaré à la baire du Conseil que Balesdier était son apraiée et non son patron ; que s'il était seul en norte s'est que seul il pepsédat un locat ; que s’il prétéveir une avant part sur les prix de façans d'était uniquement pour payer les loyers du local et pourvoir au paiement de memes dépenses communés ; Que cepuis arquis Lbars sta rous à réclamer à Balesidier son allègue ettente comme lui par les dommanes qus fraphent l’apruition et dont chaque membre dos saphort et la part en déveduellement ; Par ces motifs - Le Bureau Général jugeant en dernier retsort ; Dit Troulelle non responsiable emet lurs de causé, dit de bert non recevable centime demande envers Séont Balesdier, l'en déboute et le Condamne aux dépens envers le Trésor Public, pour le papiei timbré de la présente minte, conformément à la loi du sept Aui mil huit cent cinquante, ence, non compris le cout drus présent jugement, la signification d’iceli et ses suites. Ainsi jugé les jour mois et un que dessus. ecrevin e Lecucg secrétaire t </div><div type=\"case\">Du dit jour vingt suin Entre Mosieur Porcaille à hustive, auvnier tellier, demeurant à Paris, rue du Levroge, numéro vingt cinq lis ; Demandeur ; Comparant ; D’une part ; Et primo Monsieur Bélegoier, ttre preneur de travaux de teltere demeurant à Paris, rue de lafigette numéro dex cent quatante trois ; Défendeur ; Comparat ; Dtautre part ; Et Monfieur Trousselle, Mautre ellies demeurant à Paris, rue se lafagete, numéro cent trente neuf ; Défendeur ; Comparant tupsi d’autre part ; Point de fait - Par lettre du secrétairé du Conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus en date du Samedi huit Juin mil huit cent soixante dix huit Couaille fit citer Balesdier à comparaître pardevant le dit conseil de Prud’hommes séant en Bureau Particulier le Mardi inz juin mil hui cent soixante dix hui pour se concilier si faire se paivait sur la demande qu’il entendit former contre lui devant le dit conseil en paiemet de la somme de trente six francs pour réparation du tort quil lui a causé en sefabsant chiner toute une semaine A l’appel de la cause ouaille se présenta et exposé cume e dessus. De son coté Balesdier se présenta et expos a du Conseil qu'il employa Camille pendant toute une semaine cpez renulaement quoiqu’il oit pupent étre l’oucher plus se le travail avair été plus atondant ; Que peudant la deuxième semaine il le ft, veur chague jour experait aup chaque pour lui donner du tavail quatr tes pour poali que onsele suilie ayant trouvé à fare faire à meilleur morché ne lui donna rien quoique l'ayant ajourné de jour en jour e de le pomd suilte sur la perde pour dire que Trousselle l'avant intouthé et ed’russe à Véberne dot responsable du fix de ce dernier et demande la remise de la cause pour l'actirnner dugen teleue en téle dit duites appeler Balerdor et truselle devait le Bureau de eite e ten ue jugent mis. Cits de la cause Suailfe se présenta, repit, sa demandes pre d ure der teuit e de de ae le e p e des pour de t de pu s uratesan se présenta et exposa au Conseil qu’il n’a pi mi emboucher souaille un l’adresserà Balendier puisqu’il ne l’a samais vu n chez lui ni ailloure Le Bureau Particulier pt davis nue puisque Ba les dure reconné par avax fait chemer ille il lui devait payer une indemnité qu’il fixa à la somme de vingt cinq frntc quand à trousselle il qe le sout pas responsable de engagea ouaille à abousonner sa demande à seu EardeCouaille ayant déclaré persister dans sa demoinde la cause fut renvoyée devant le Bureau Général du Conseil deant le jeudi vingt Juin mil huit cent soixante dix huit Cités pour le dit jour nit juin par lettres du secrétaire du Conseil en date du quatorze juin mil huit cent soixante e trait à la réguate de souaille Balegotier et Troussitte comparurent. A l'appel de la cause coneille se présenta et conclut à ce qu’il plut au Bureau Général du Conseil condamner solidairemen, Balestier et Coupelle à lui payer avec intérêts suivant la loi la somme de trente six francs pour répiration du dommaze qu’il a’etrouse par le chomage de taute une semiaine et les condamner solidairement aux dépens. De son coté Balerdier se présent et conclut à ce quilt plut au Bureau Général du Conseil lui donner aîte des résorver qu’il fait de résiiter à Troup ete les sommes auxocellet le conseil croirair devor le condamner proupselle, de son coté, conclut à ce qu’il plut au Bureau Général du conseil le dire non respontable et le matte lorr de cause sons dépens. Rais de troit — Doit-on condamner Baley dier etcroupelle à payer à Ponaille la sommed trente six francs pour indemnité de perte detemps ? Dait on donner aité à Balesdier des reésorves qu'il fait de régiten à trousselle les sommes auxquelles il aurait été condamne par le conseil ; Doit on dite trousselle non responsible le mettre trors de cause ; que doit-il être statué à l’égard des dépens ? Après avoir entendu les parties en leurs et conclusions respectivement et en avoir délibéré Conformément à la loi ; Attendu que ce qui loncerne Trantelle qu’il refert des débats q’’il a confié à Béerdier des travix par être faits à la toitre eu des pris determinés ; qqu’dl fait pur trousselle d'avoir atreté à Bales dois dit ouvriers, le fit fit il acquis, ne pourrait le rendre envers e ouvriers responsable du fit de son athetrenent, Cnre condame Balepier; atendu que vuille Pittre dou e i e d un u di M Barre du Conseil que Baleydier était son apaice et nonton Patron ? Que si Bales dier était seul en nomc est que seul il pasés aun local il que s'il a prélevé sur le prix de façon une avant part Wéled anrquement pour payer les loyers du sdif local et aussi pourvoir au paiement de meues fournitirer. Qu’en ce fait acquis douaille devant en déviduellement lapporter de part, des dommages qui atteigent la loceté n’aren a réclamer à Balesdier, son colléque, attent comne lui ; Par ces motifs - Le Bureau Général jugeant en dernier réport ; n ce qui concerne Trouptelle le z non responsable et le methirs La cause ; Dit sonclle non recevable en la demande envers Baleydier, l'en déboute et le condamne aux dépens envers le Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept aout mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites. Ainsi jugé les jour, mois et an que dessus. Lecucq secrétaire avee .Ausi jour vint ciniq Entre Monsieur Boulain Auelphe, ouvrier letties, demerant à Pais, rue beremtf, numéro soixante dix Demandeur ; Comparant ; D’une part ;Mosieur Bélerdier, entretreneur de travaux dépelarie, de mourant à Pain, rue de lafayette, numéro deux cent quarante trois ; Défendeur ; Comparant ; D’autre part ; Lt se de de sur Consele, rte d e pome à Paris, rue de Calayette, numéro cant trente neuf Déléjant Consant; Crs ntiepus lit ptis u loitete a el e se la e doie d entie et euig en de e dous elle de onr les su et lu pent pes duti lesir le ur de sart some prs te e e dueiten e ei de la det n tes ponde q ues ute a de t e e e aur cte eu es e per et en urs pus e entre de quele causé en le faisaunt chaner tute rne semaine. A l’appel de la cause Poulin se présenta et exposa comme et dessus. De son coté Balejuer se présenta et exposa au conseil qu’il a ccupé Poulain after régulièrement pendant une semaine ; qu’en suite I le fis nenir tont les jours pousait pouvait lui on donner josqué ce la loi était promis par Trousselle, son Patron ; mais que le dit Crousselle ayant truvé à faire executer se travoux à des pris de tuits le fit, vamement attendre. Pare moment Poulain repris ca parale pour dire que pousselle l'ayant embauché et adressé à Bas ue devait être responsable qu’il a subl et demandasue la cause fut ajournée, pour le faire appelés E respon sabilité. La cause en et état fut ajournée a Juudi à vendredi quatorze Juin même mois. Cités pour le dit jour quatorze Juin par lettres du secrétaire du Conseil les détendeus comparurent. A l'appel de la cause Voulain rappela sa demande en vers Bales te tterma que Trousselle l'avait embouché et adressir li Boleydier son entrepreneur et demandu qu’a aison de ces faits il fut tenu comme responsable de l'indemnité demande à son entrepreneur. Lé Bureau Particulier fut davis nue Balesdier reonnaipa avoir fut chomer Poulai pendant tuné semaine il lui devait une indemnité qu’il fixa à vingt cinq francs ; Quanda tous selle il ne le crut pas repous du 4 en garea Poulain à aton donner à l’égard de Coupelle Poulain ayat déclaré persister dans sa demande la cause fut renvoyée devant le Bureau Généra du Conseil séant le Jeudi vingt Juin mil huit cent soixante dix huit. Cités pour le dit jour vingt juin par lettres du secrétaire du Conseil en dates du quatorze Juin mil huit cent soixante dix hui à la requite de Poulain Balesjon et trousselle Comprrurent. A l’appel de la case Poulain se présenta et conclut à ce qu’il plut au Bureau Général du Conseil condamner lolidairement Boleydor et trousselle à lui payer avec intérêts suivant la loi la somme de trente six francs à titre de répuration du dommase qu'il a éprouvé par Comage de toute cne semaine et les condamne solidairement ux dépens. De son coté Baley dox se présenta et conclut à ce qu’il plut au Bureau Général ufl inq mi une ; l de s 2 nsil que e e eu A aeuig de i auls ? A du Conseil lui donner aite des resarves qu’il fait de répiter à Trousselle les sommes auxquelles le Conseil eurai devoir le condamner envers coulain ; trousselle, de son coté, se présenta et conclut à ce qu’il plur au Bureau Général du Conseil le déclrer noreomble ar responsable envers Poulain et le mettre pers de cause dans dépens ? Paint de troit — Doit-on condamner Balendier et troups elle solidairement à payer à Poulain la somme de trente six francs pour l’indemnitér de temps de chamage; Doit on donner cité à Bolesdier des réserves qu’il fait de étarter à troupselle les sommes euxquelles il pourrait être condamne Daitaou dire rousselle non responsable et le mettre pors de causé ? Que dit-il être tatué à l’égard des dépens ? Après avoir entendu les parties en leurs demandes et conclusions respertivement et en csoir délibéré conformément à la loi ; Attendu qu’il eit acvis ux débetsque troupelle à confié à Bébersi des travaux pour être executés à la toitre et à des peis de termines ; Qu le fis par trouselle d'avoir accetté de onvents à l’atrder, apis les it poup la larer le rendre responsable envers ces ouvriers des aitert de tete fremte à la dauese enaer tere le der ud ue su les tilté le uire su titire qe silu e les eu drenl litne tie e e e u ei e t fasier en lete et eu s l rél se pus on purs seiles e prix de façon avant portire ee ne fit qu'inquement dur ui lite l e mn pareten cinse e lus seli de e en e e le ier nu el u te en lu se le em e quie ce de l r e t e d n en le eue aure eu re sa commnt le hur t e e t e de es le ent prser e e e rde men te ua due areden e te es u ant soir et prs e e te se poeur edt e e l ten u e e aur rspu e e eu d er edereveure demerait u e t u atese Leuigerétine r Dudi jour vingt Ping fu Entre Maisieur Fentane Consegdier, ontretreneu de travaux de elterie, demeurant à Paris, rue de Lafay édu uméro tent deix cit quarante troit ; Demandeur; Comparant ; D'une part ; Et Monsieur Trousselle e Maitre sollier demeurant et domicilié à Paris rue de lapaette, numéro cont trente neuf ; Défendeur ; Comparant ; D’autre part ; Point de fait - Par lettre du secrétaire du Conseil de Prud’hommes du Déurtenent de la seme pour l’industrie des tissus en date du moisi ouze juin mil huit cent soixante dix huit Baregdier fit citte trousselle à comparaître par devant le dit conseil de Prud’hommes séant en Bureau Particulier le vendredi quatorze Juin mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait fermen contre lui devant le dit conseil en paiement de la somme de cent ving francs pour indemnité de chomage et en quratre des indemnités qu'il pourrait être condamné à payer à treire, ouvriers qu’il a intrainés dans le même case A l’appel de la cause Borerdor se présenta et exposa uu Conseil que suivant convention verbale en date du vingt leai dernier troutselle s'eit engagé à lui donner les travaux à des Conditions de terminées nun seulemeant pour lui mois encore pour autant douvriers qu'il en pourrait trouvert ; Que le dit Trousselle qui l’accutra pendant une première semaine et trure ouvrièers emiboustrés à cet offet trouvant à fuire parer ces travaux à des prir reduits de les lui dama tros quoique le reit à chaque les demain, ce qui fait qu’il a non veulemre perdu son temps ; mois en cole fait perdre clui à d’avriers qui lui réclament des dommages-intérêts. De Venntate Tronsselle se présenta et exposa au Conseil qu’il ne i et Pis engage à donner du travail à Bales dier qelan sur etq mésure de ses besoins et non a toubon plaisir et par quantités, fres ; que s’il n’on donna que peuà la soin det aue n’était pas propté pour les livraissons à lui avait conseil d'ajourner eu qu'encore les apprêts étirent en retird, enta cis il n’était tenu à reuni envers Balesdier. Le teueau Particlier n’ayant pa concilier les parties la cause fut renvoyée devant le Bureau Général du Conseil séant le huddi vingt ui mil huit cent soixnte dix huit. Cité pour le dit jour mi jui par lettre du secrétaire du Conseil en date du mattrné juin mil huit Cent soixante dix huit à la réquitte de iteger 5 i Il u papelle Comparut . A l’appel de la cause Raleglier se présenta et roctut à ce qu'il plut au Bureau Général du Conseil attendu que troupel qui devait lui donner du ravail pour lu et ses ouvriers l’atupe conte une semane lans lui en donner et, chose plus auve l’afit veur chaque jour pour le remettre au tendemin ; qu'il ut constant que troupelle lui a causé un dommage dont il lui doit réparution Par ces motifs – Condaane Tousselle à lui payer avec intérêts suivant la loi la somme de cent vingt francs de dommages intérêts et le condamner aux dépens. De son oté Ausselle se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu qu'en acceptaut de Balerdier ses frir de façon il ne pitvis à vis de lui d’outre eugugement que de la domse des travaux à exécuter au fur et a mésire de ses besons et par quatités qu'il jugerait ; Qu'avit eu le soin de certains erti leuil les fit coupersappreter et les lui donna le dout et enpaye le prix ; Par ces motifs — Dire Ba legdor nonréccvalte en sa demande, l'en débouter et le condamner aux dépens. tont de droit — Doit-on condamner Troupelle à payer à Walensier la somme de cent vingt francs pour l’induniser du temps de couage Conquel il l’a otistrent par son fait à Paben dit -ooun dire du de e e dt endemnit ourl e e noer pet tre s’atué à rare du it àl peur deveir de parté à las ler de eu ese prisant ren avoir délibéré conformément à la loi ; attendu qu'il est constant que Baleydier s'engageant le vingt mai dernier à traviller pour da pete de a dis et e dede eseutie de di perdéster e ugit du jun cnee e pare en n es et foui p en i e n lu e t t e deiar sape quete u sa teusen de lure leq i au e te e des sutie uete sus sesg et eu et on e rdeer pasden de en de eit et an premen a s liur eue e e t de tinet ent e t pouir eni re es quendene eu e e per de ietr eu en e e Duriern e reurq usétire 1 Mudience jeudi vingt set soug mil huit cent sixante dix huit siégeant. Dessieurs Brand Théalier de la Ségon d’homnler dure Président du Conseil de Prud’homme du Département de la Seine pour l’industrie des E Tissus, président l’audence en remplacement de Monsieur Marienvat, Trerideut du dit Conseil imperte Carrony, Léforge Blanchiz et Margier Prul’hommes asistes le Monsieur Lecusq, secrétaire du dit Conseil. Entre Monseur Ponnet demetant à Paris, rue du Boulo, numéro dix sept, agissant à nom et comme administreteur de la personne et des biens pe la ville mineure Margueite, ouvrière ; Demandeur Comparant ; D’une part ; L Monsieur Varner, Maitre ailleur d’hotit, demeurant et domicilié à Paris, rue Montmartre, numéro cent surante, six; Défendeur ; D’éfailleus ; D'autre part ; Poir de fit Par lettres du secrétaire du conseil de Prud’homme du Département de la Seine pour l’industrie des tissus en dates des Vendredi sept e mardi onze juin mil huit cent soixante dix huit Ponet fit citer Vagrer a de sute purie t de le er der eu den eil e t e lated n enilier Apaus peuise le part quage cenis s lit lu e e e e pour le atis u t u e it en de por aente t u te de er ure ue de e ten q e suti ee de en e e u rceton et enie e e prs leus deu e de e e de ende e e i eu de it e n pur due cengetie ui nt amn c onor is D o Paguer fit citer Vlagyer à comparaise pardevant le dit conseil de Prud’hommes séait en Bureau Général lendi vingt sixx juin mil huit cent sixante dix huit pouir s'entendre condamne lui payer avec intérêts suivant la loi la somme de quatre vingt deux francs cinquante centimes qu’il lui doit corpris de travaux de sa fille Margueute, plus, tette indemnité qu’i plasra au Conseil fixer pour perte de temts devant les Bureaux du Conseil et reslégiens. A l’appel de la canse Mlagner ne comparut pas. De son coté Poinner le présenta et conclut à ce qu’il plut au Bureau Général du Conseil donner défaut contre Bacirer non comparu ni personne pour lui quoique dument appelé et pour ce profit reui adjuger le bénifice des conclusions par lui préses dant la citation exploit de champrou, tempsede enregistré. Point de droit — fait-on donner défaut contre Vagner non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur conclusions par lui précédeumment prises ? Que doit il enre statué à l'égard les dépens ? Après avoir entendu Poiger en ses demandes et conclusions et en avoir délibéré conforméent à la loi Mndu que les demandes de oiguet pasursa justes et fondées ; Que d'ailleurs lles ne sont pas confestée par Vaprer non comparant ni personne pour lui quoique dudent l'appelé ; Attendu qu'en ne comparupant pas devant les Bureux du Conseil lagner a cause à Vorgney u présender de perte de temps, ce qu’il doit réparer ; Par ces mols e Buraun enret saint condemner atsort ; Done dépaur contre agner non comparant ni personne pour l quoique dument appelé ; Et adjugeant le proft du dit dé faute; condamne Clagner à payer avec intérêts suivant la loi à Peigner la somme de quatre vingt deux francs cinquante cent qusil lui dit pour lire de trtix de sa file de ron ins plus une indemnisté de neuf francs pour temps perdu d condamne en outre aux dépens trrois et ligudés envers le demender e lle somme de deuve forens t t e det our quante ca timoes avrirs le Treur Publi pour le ppie dut tinttrée et l'enreustrement de la citation la sormement li de sept du mil hui det oinquante en ces pon tompré cted preut oue a cine e te erie e e le a de lu dil es pre de lesadeante en te te e nt inque liu pe ent lent por prmiene pe épourx lunde et hapes. C eer jugé les jour mois et an que dessus. Lecug secrétaire dmanie e </div><div type=\"case\">Du dit jour vingt det sinz Entre Monsieur et Madame ol cseur Cil tant en son nom personnel que pour apsister et autoriser la dame son épouse, ouvrière qu'eltière, demeurant ensemble les dits éhoux, à Paris, rue sainte Marthe, numéro trente quatre ; Demandeur ; Comparant ; D'une part ; Et Monsieur et Madame Combier, le sour combraytant en son nom personnel que pa aister et autoriser la dame son épouse Goletrère demeurant ensemble les tits éhout; à Pans, rue saint Meur, numéro deux cent dix ; Défndeurs ; Défaillants ; D’autre part ; Point de fait - Par lettres du secrétaire du Conseil de Prud’homme du Département de la erme pour l'indstrie des tissus en dates des lundi dix apretende det luis doveil le es purte qui du pour Cihl par seur le vaire contie la parte par devant le dit conseil de Prud’homme s an Bureaux atintes le vrit ei ui t unt t en e ps nil huit cent soixante dix huit pour se conclu si faire se pouvait son le derentete Cinseur puin ce tersein le dit conseil en paiement de la somme de enze francs pour purt luit cende l de oere e pet e prig ent u ciles e de e e eni quent é p agfe e e ce t enie are ue e et e le ue d anter t du e pus aen o l ar tente ea de te e que pu eni e pdseus an tere ei u e quei e u ne apen de unt t eil unt e e te é Bel, plus tette indmnité qu'il plaira au Conseil fixer pour perte de temps devant les Bureaux du Conseil et les dépens ? Pouix de droit — Doit-on donner défaut contre es époux Conmlarion non comparants ni personne por eux quoique dument apelés pour le profit adjuger aux demandeurs les conclusions par eux précédemment prises ; Que doit-il être statué à l'égardde dépens ? Après avoir entendu les époux Bel en leurs demandes et conclusions et en avoir délibéré conformément à la loi. Mltende que la demander des époux Wel posus juste et fendée; que d’ailleurs elle nt'est lus contestée par les époux Comproy non comparut ni personne pour eux quoique dument appelés ; attendu qu’en e comparusant pas devant les Bureaux du Conseil les époux Compal ont causé aux épeus ief un préjudice de perte de temps, le qu'ils doivent réparer ; Par ces motifs u Bureau Général jugeant en dernier repart ; au tarticle quarante un du décret du oize quigt mil huit cent neuf, portant réslement pour les conseils de Prud’homme Donne depau contre les époux Cambror non comparant ni personne pour ux quoique dument appelés ; Et adjugeant le profit du dit défaut ; condamne les époux Cambray à payer avec intérêts suivant la loi ax époux Rel la somme de ente francs u’il leur doivent pour prix de travaux de la dame Gel, prusane indemnité de quatre francs pour temps perdu ; Les condamne r outre aux dépens taxés et liquidés envers les demandeurs à la soe de un franc, et à celle due au Trésor Public, pour le papier timbré de la présente minute, conformément, à la loi du sept aout mil hui cent cinquante ence, non compris le cout du présent jugement, la signifiecction d’icelui et ses suites. Et vu les articles 435 du Codede procédure civile, 27 et 42 du decret du onze juin 18e9, pour signifier aux défendeurs le présent jugement, conmet. Chompran, l'uin de ses huisiers audienciers. Ansi jugé les jour mois et un que dessus. Jeurg iétre  Dineanx Ausit jour vingt cent Sing a ntre etenser ertordaure Polarée e sour Colrs le e tr omparent u por apales la deme due part mire par dom e de e es u e si ete i e vingt ; Demandeurs ; Comparant ; D’une part ; E Machenr Madame Gorguard, le sour Torgardtant en son nom personnel que pour apister la dame son épouse fabricnte de fleurs artificielles, demeurant et domiciliés à Paris, rue des deux torter sant Jonveur, numéro vingt huit ; Défendeurs ; Défaillant ; D’untre part ; Point de fait - Par lettre du secrétaire du Conseil de Prud’hommes du Département de la Seine pour l’industrie des Tissus en date du Lundi dix sept Juin mil huit Cent soixante dix huit les époux Citer pres citer les époux soiguard à comparaitre par devant le dit Conseil de Prud’hommes hui en Bureau Particulier le mardi dix huit juin mil huit cent soixante dix huit pour se concilier si jaire se pouvait sus la demande qu’ils entendaiens former contre eux devant le dit Conseil en paiemet de cent quarante trois francs qu'ils leur doivent pour prix de travaux de la dame Citer. A l’appel de la cause les époux Piter se présentèrent et exposstant comme dessus. De on coté orgon se présenta et reconnait devant la somme déclamée et fis, l’engagement de la payer par quinze francs le vingt deux Juin et ingt francs tant let quinze jurs, le saqmedi à le vingt deux juin les époux Gergmard ne poyeant prasut la causé en cet étot fut renvoyée devant le Bureau Général du Conseil séant le Judi vingt sept i mil huit cent soixante dix huit. Cité pour le dit jour vingt sept juin par lettre du Ginétaire du Conseil en date du vingt cinq juin mil huit cent soixante, dex luis pur tarout de l qurt coitit e gore demin ne comprmes pour e e l er le eoue Citer se présentèrent et conclurent à ce qu’il plut au Bureau prte se tarat sui u ent le genent qu ante pre e onden s se per e rf te la deme e lem ent ene e pe t e pu ile ou e e te e our eren e e eux aun e et en etite eende re de u tegen de au per t pu en en pesr audepeur a e coux e e e es e dem e de en uete etant a toemnité apee es de demnet que den esente eupre en eneique n Ees E D i ui is in e M t ni personne pour eui quoique dument appelés ; Attenda que e les époux Forshgard ont fait perdre du temps aux chaue iter devant les Bureaux du Conseil, ce qui leur cantei préjudice dont ils leur doivent réparation ; Par ces motis Le Bureau Général jugeant en dernier report, ou bartule quarante un du décres du onze juin mil huit cent neufer portant regument pour les conseil de Prud’hommes ; Done défaut contre les époux Gorguard non comparants ni personne pour eui quoique dument, appelés ; t adjugeant le profit dedit défaut condamne les époux Lorguiarda payer avec intérêts suivant la loi ax époux Céter la some de cent quarante trois francs quits leur doivent pour prix de trevux de la dame Poter, plus une indmnité de deux trancs pour temps perdu Les condamne en outre aux dépens taxés et liquidés envers les demandeurs à la somede suivait cinq centimes, et à celle due au Trésor Public pour le papier timbré de la présente minute, conformément à la loi du sept coute mil huit cent cinquante, ence non compris le cout du présent jugement, la signifiation d’icelui et ses suites. Et vules articles 435 du code de procédure civile 27 et 42 du secret du onze juin 1809, pour signifier aux défendeurs le présent jugement Comet Chompron, l'un de ses tempsiérs audienciers a jugé les jour mois et an que dessus. Lecucq térnétaireammeuoil </div><div type=\"case\">Du dit jour vingt sejt Puinu Entre Monsieur Charles Fétemett, ouvrier tordonner peurs à Paris, rue Paint souveur, numéro vingt huit ; Demanders comparant ; D'une part ; Et Monsieur Gavail, Mntre cordonnier, demeurant et domicilié à Paris, rue Vayevir numéro quatre ; Défendeur ; Défaillant ;D’autre hart ; ores de fait – Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l’industrie des tissus en dates des Lundi trois et Mardi quatre juin mil huit cent sixante dix huit étemill fit citer Lavail à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les mordi quel et vendredi sept juin mil huit, cent soixante dix huit pour se concilier si faire se pouvait sur la demande qusil entendu former contre lui devant le dit conseil en paiement de la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour lalaire . Lavail n'ayant pas comprru la cause ft renvayée devant le Bureau Général du conseil séant le jeudi vingt et ajournée au putte Vingt sept juin mil huit cent soixante six temp Cité pour le dit suivant exploif de champron, luipier à Paris, en date du vingt un juin mil huit cent soixante dix huit, visé pour timbre et enregistré à Paris, le vingt deux Juin mil huit cent soirante dix huit, etmitte fit citer ravail à comparaître par devant le dit Conseil de Prud’hommes séant en Bureau Général le jeudi vingt sept juin mil huit cent soixante dix lui pour s'entendre condamner u lui payer avec intérêts suivant la loi la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour salaire plus, telte indemnité qu'il plaira au Conseil frer pour perte de temps devant les Bureaux du Conseil et les dépens. A l'’appel de la cause Lavail ne comparut pas. De son coté schunt se présenta et conclutz à ce qu’il plut au Bureau Général du Conseil donner défau contre Lavail non comparant ni personn pour lui quoique dumet appelé et pour le profit lui adjuger le bénifice des conclusions par lui prises dans le citation en plois de Champour, lui par enregistré. Poin de troit — Doit-on donner défaut contre lavail non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précédemment prises ? Que doit-il etre statué à l’égard des dépens ? Après avoir entendu létimelte en ses demandes et conclusions et en avor délibéré conformément à la loi ; Mttendu que la demande sihuett purai juste t fondée ; que d'ailleurs elle n'est pas contestée par lavail non comparant ni personne pour lui quoique dument appelé ; attendu qu'en ne comparipant jes devant les Bureux du Conseil Lavail a causé à Btmill un préjudice de perte de temps, ce qu’il doit réparer. Par ces motif - Le Bureau Général jugeant ente e n comn da d it ente tue en lre t par me teu pé de pur e pprecit et i el u en es pr le oe uet cinre es soir de e e senr indemnité de nuf francs pour temps pardu ; le condamne unr e de tis e d en t lerie se épeur la premie ue e er i uil it é franc quire centimes envens le Trésor Public, pour le papier. timbré et l’enregistrement de la ctation, conformément à la loi sept aoux mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d'icelui et ses suites. t vu les articles 435 du code de procédure civile, 27 et 42 du decres, du onze juin 18099 pour signifier au défendeur le prémier jugement , Comme Rompi, l’un de ses huipars audienir Aiusi jugé les jour moois et an que dessus. Deneur Lecuuq secrétaire Audit jour vingt set sinig Entre Mademoiselle Maria Lesqune, ouvrière brodeute demeurant à Paris, rue Mongé, numéro cent huit ; Demandeur ; comparant ; D'une part ; Et Monsieur es Madame Coureille le sieur Courelle tant en son nom personnelque pour apristedt autoriser la dame son épouse brodeuse, demeurant et domecili onsemble, les dits époux, à Paris, rue du faubourg Montmestre numéro vingt huit ; Défendeur ; Comparant ; D’autre part , cn de fait - Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l’industrieu Tissus en date des Jendi vingt trois et vendredi vingt quit Mai mil huit cent soixante dix huit la demoiselle Lequine fit citer les époux Courcelle à comparaître par devant lesdit Conseil de Prud’homme séant en Bureaux Particuliers les Vendredi ving quatre et lundi vingt sept Mai mil huit cent saixante dil huit pour se concilier si faire se pouvait sur la demande qu’elle entendai former contre eux devant le dit Conseil en priéement de la somme de vingt cinq francs pour prix de travanx le trocdie A l'appel de la cause le vingt sept Mai les époux Conreil n'ayant pas comparu le vingt quatre la demoiselle Lguran, se présenta et exposa cou eu deseus. De leur Cite les époux Conrielles se présentèrent, drens que le prix du Gravail bien la trit de six francs contigusiéte mal fit et rendrer lardivement entendaus ne rien payer. La demanderees épara que son travail était nreplachablement faist LBureau Particulier veulent être fixé sur le mérite du Traral et sur sa valeur Vénble renvoya la cause à V’eroameil t qu Di i membre du conseil à ce comaitsant. Les parties n'avant pu ctre conciliée, la cause fut renvoyée devant le Bureau Général du Conseil séanz le jeudi vingt sept juin mil huit cent soixante dix huit. Ctés pour le dit jour vingt sept Juin par lettre du secrétaire du Conseil en date du vingt un juin mil huit cent loixante dix huit à la réquete de la demoiselle Lequine les époux Courcelles comparurent. A l’appel de la cause le demoiselle Semine se présenta et conclut à ce qu’il plut au Bureau Général du Conseil condamner les époux Courcelles à lui payer avec intérêts suivant la loi la somme de vingt cinq francs pour travaux de broderies et les condanner ux dépens. De leur coté les époux Courcelles se présentèrant et conclurent à ceguil plut à Bureau Général du Conseil attendu que la semoiselle Legune lui arendu un travail mal fait et en acceptable par la loie qu'elle a employée Qu'on tant cas, ce travail fut il acreptable le prix n’envaidrauit que six francs et non vingt cinq ur ces motifs — Dire la demoiselle Leonne non recevable en sa demande, l'en débouter comme mal fondée en celle et la condamner aux dépens. Paint de drot — Doit-on condamner les époux Courcelles à payer dà la demoiselle Léquine la somme de vingt cinq francs pour travaux de brochrre ? Oubion doit-on dire la demoiselle Lequine ; onrécemble en ses demandes, l'en déboutes ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu les parties en leurs demandes et conclusions respectivement et en avou délibéré conformément à la loi ; attendu qu’il est constant que la soie employée e travail de brodrie qui fait l'objit du titige est nne soie mathinte ; Mais attendu que cet en couvenments pouvait être cité par ls épous Courcelles en chaisissant et fournipsant et mimes la toie du bien don taser le soin à l’ouvrière qui ne puit certeimeent par avoir la même, connailsence qu’eut et oui ne doit pas enconser une semblabli responsabilité ; attendu eu ce qui donverne le prix du travait qui le Conseil qui à legilement quce lavrier d’apprenition bestime à due francs , Eor ces motifs – Le dpen r te jour an deuer t s lu sante n lanre l eu ile u tete e a lire alrende i7 pui qu pae t sux dte ies d dit den it e mne tende a pent dou qeme lrsae t de e e ter deu deniont a deme tep le mrdt pure u la tie u e e d pumnt ape et esprqute enet non compris le cout du présent jugement, la signification d'icelui Ainsi jugé les jour, mois et an que dessus. et ses suites. derurg seuctaire Dnaurlte du dit jour vingt sept sing Entre Monsieur Dauté, fabricans de tissus, demeurant Qu et domicilié à Paris, rue saint paire, numéro vingt Défendeur au tirmipal ; Demandeur appotant ; Comparaite D'une part ; Et Madame Délatiace, ouvrière, demantant à antenit le Maudouin oise ; Demanderesse au princépar Défenderesse ; opprosent ; Comparant ; D'autre part ; Paint le aux termes d'un fait- Par jugement rendu par défaut par le conseil de Prud’home pouvoir sons devante du Département de la Seine pour l'industrie des tissuste neouf pasé en date du anthuil le houdun, du Ma mil huit cent soixante dix huit, enregistré ; Dendé à étter premier Mi mil condamné à payer à la dame Défatrace la somme de trois cent huit cent soixante soixente quinze frons de princépat et les accilsire: d jugement dix huit, enregttrée fut Simifié à Fandit par exploit de Ginaad, uipier ; en dit reuve epreuvdu dix neuf Juin mil huit cent soixante dix huit, enregistré à la ruguite de Dauilé. suivant en péloit de cham prin huipare à lain en date du vingt Juin mil huit cent soixante dix traid udisé pour titre elenregistré à Paris, le vingt un juin mil Devai huit cent soixante dix huit, détes deux francs quinze centimes spur de Siuilles ; Daudé forme apposition à A jugement et Citre les époux Délahacé à comparaître par devant le dit Conseil de Prud’homme dént en Bureau Général le jeudi vingt sept juin mil huit cent soixante dix huit pour le voir de cevoir opposant à le gugeme le voir, de charger des condamnations contre dux prosoncées en principal et accepaires, s'entendre lus dix époux Délalice déclarer non recevable eu leurs demandeu, les en débouter; Cone mal fondés en celles, et s'entendre condamner ux dépens de la première comme de la présente inistance. A l’appel de la cause Dande se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu que la dame Délahayé n’et pas auveiit dans l'acaption du mot ; qu'elle entreprenense de travaux de fet execiter par dtentres ouvrières à Monteuil le hondamne ses ouirons ; Qu'en outre elle lui achate la soie qu'elle faut M or o I t pour n et avi les façons et a forfait ; que le conseil n’était pas compateut pour lomuitre des différent entre les fabricaute et les entrehreneus de travaux à lerhuit, d'ut à tort que les époux Delcheye aun près contre lui le neuf Mai dernier en jugement par défaut Par ces motifs — Dire le jugent nul et de mil uffes ; Ptatuant à mouveu Le déclrer n compétent pour tommêtre de la demande des époux Délataye, les renvoyer devant jui de droit et les condamner aux dépent de la première comme de la présente inistance. De son coté la dame Détatoge se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu qu’elle n’est ni marchon de ni fabricant; qu’elle n’a fait avec Dandé auun oite de commerte Que si elle ne ceute pas seule les trovuy qui lui sont confiés per cmperté à Dau de puisqu'elle en suite responsable Bois avis de lui de l'ixeution de cestranailr ; Par ces motifs Dire Doudé, nonrecevable en vondéblinatoire, se déclarer competent, rétenir la cause et ordonner qu’il sera procéde à l’examen de sa demande pour être statué sur le fautà Pants de droit - Doit on se déclarer en compétent pour conaitre de la demande des éour Délatoye il renvayer la cause devaut qui de drait ? Ou bien doit-on se déclarer confétent retenir la cause et ordonner que les parties s'entliqueran pour être statué au fand. Que doit-il être statué à l'égarddes dépens ? Après avoir entendu les parties en leurs demandes ete conclusions respectivement et en avoir délibéré conformément à la loi ; Attendu que des explications fournies par les partiese que la dame Delahoya quoiqu’oupput des ouvrirres pour l’éxcution de travaux qui lui roit fournis par dans é n’est pas monis vis t sis de cela a une ouvrière prtinee du conseil de Prud’hommes puisqu’elle ne furait avei le travail que la soce que est é la brodeuse ce qu'et le fle au contirier au à la conturière ; que cette fourniture et e e r p le con der een de préte us e pent edue preut dmit lur puastur qui de pr e e pit edt peu e t e re desenite on et du n t ee eis de en e de en tur ete em e se e seut endere e un que lut e du l te e nten ingq en eit journée par être reprise le jendi quatre puillet mil huit cnt seinente dix huit. Ainsi jugé les jour mois et an que depis Leniig ecrétare L Dunaiof
```

### 5.4 Contenu du fichier XML après ajout des éléments de structure"

```py
tree = build_tree(clean_text)
tree = add_n_counters_to_div(tree, type="courtHearing")
tree = add_n_counters_to_div(tree, type="case")
tree = finely_structure_cases(tree)
tree = identification_parties_et_date_audience(tree)

print(tree.prettify())
```
Output
```txt
[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Bréchot, ouvrier Galochier, demeurant à Paris, rue de Meaux, numéro trente sept, passage de la Brie ; Demandeur ; Comparant ; D'une part ; </span>Et Monsieur Vermérot, fabricant de Galoches, demeurant et domicilié à Paris deux mots rayés nuls Lecucq — "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Madame veuve Bernard, ouvrière demeurant à Joinville le Pont, près Paris, rue du canal, numéro onze ; Demanderesse ; Comparant ; D'une part ; </span><span type="part1">Et Monsieur Bardin, fabricant plumassier, demeurant et domicilié à Joinville le Pont, rue des Réservoirs ; Défendeur ; Comparant ; D'une part ; </span>"

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Levonte, anvner lapeemntier, demeurant à Boles, au rondipement de Clermont leisa j Demandur, Comperant ; D'une part ; </span><span type="part1">Et Monpeur Curtet Jeun Marie, ouvrier vailleur d’hobits, demeurant à Paris, passage Montes quien, numéro cinq ; Demander ; Comparant ; D’une part ; </span>L Monsieur Lacembe,; tute dailleur d’habits, demeurant et domicilié à Paris rue lai sa numéro quinze ; Défendeur ; Comparant ; Dutelurt, Paint de fait - Par lettres du secrétaire du Conseil de srur ha du Département de la Seine pour l’industrie des tissus en dtates des Vendredi trente un Mai et lundi trois juin mil huit cent soixante dix huit, curtet fit citer Lacombe à Comparatre par devant ledit conseil de Prud’homme séant en Bureaux Particuliers les emble trois et Mardi quatre Cin mil huit cent soixante dix huit pour sin concilier à faire se pouvait sur la demande qu’il entendit forixen contre lui devant le dit Conseil en paiement de la somme de Pi de sit es arés nil Mn ec e Dre q u 2 i ié a q i i q du soixante denx francs pour selaire. La coulée n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six pis par lettre du secrétairé en date du quatre juin mil huit ent soixante dix huit à la réquité de Cortet Lecomle comparut; A l'appel de la cause Curet se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner lacole à lui payer avec intérêts suivant la loi la somme de soinante deux francs qu'il lui doit pour prix de travaux de son état et le condonner aux dépens. De son coté Laconbe se présenta et conclut à ce q’il plut au Bureau Général du Conseil ettendu qu'il reconnit devoir la soume de soixante deux francs ; mois attendu qu'an dêtenant le fière qu'il encère entre les moins turtet lui ceuse préjudice Luisque cette frèce n’ayant pa être livrée en temps la cestire pour comple Par ces motifs — Lire Cortet untucevable, en rademand e tavu débouter eile condamner aux dépens. foit de doit - Doit on condamner Lacoute à payer à cortets la somme de soixante de soixante deux froncs contre la remisé d’un ditent qu'il salire à Paton dit en du leis reant lut la demande ; l'en débouter ecevoir Lécombe reonvou tionnellement demandent ; Condamne Tortet a remitre le vêtement en psus eu disitre ou mis d sant au eu du du sent pu tie ueapsite d etito u ell t tent lerse lu de lete de e e e t l peiue en leurs demandes et conclusions repeetivement et encvord et e t te e te ite u ede ur tn du t esur purt la pre des ioise les our are e e e e t due a ler s r se te e dte t e poi i en e p e ten u reit e de u te enpe ente e pe de iq ount quilt eten de ses uee de de e titer duit en ue se andurs deuen der de aun u ant e e reie uar e e t enede tain e ti qu iq e te see inps e Mée minute, Cnformément à la loi du sept aût mil huit cent cinquante rue, non compris le cout du présent jugement, la signification d’iceluit et ses suites. "

[0] Unable to find something like "D'une part ;" in:
 "Entre Mademoiselle Désirée Sanson, se disont ouvrière contrière demeurant à Paris, rue Fesnnier, numéro dix Demanderesle ; Comparant ; D’une dort ; t Monsieur et Madame Magnen, le sieur Magnen tant en son nom personnel que pur epaiter et autoriser la dame son épouse, Mantrelte contirre, demeurant et domiciliés, entembrle, le dits épout à Paris, avenie des taris, nméré qatre suit cinq Défendeurs ; Comparant ; D’autre part ; Poit de fit Par lettre du secrétaire du Conseil de Prud’homme du Dépastement de la Seine pour l'industrie des tissus en dates des Mercredi vingt neuf a vendredi trente Mai mil huit cent huit li e t le te de se le e u dapers prde ts tel a sid en ue u Anseu té te eontritat d u umes e les epord s le er en p p t ente unde ete dais poir tetre qunt les e e pre s la o e e en per due mne u deant ens un epe e pe le deu le deur du ti ens ene n en pement dei e e der e paur edu due au de juemne ede t e e e e es lute ede e e que der eun, duedoint e e e e e e en e se e eit e aen e den e eser pr e enqu les Bourrnie de se sonfais ; que si elle lui à enteigne grctutenant le protique de la cuture s'eit qu'elle la veutoir endre expabla de complir l'umploi d’une smme de ciembre au lien de cettedeu borne d’enfans qu'elle avait occupée jusque la ; Que s'elle sa nourrie c’esrt qu'elle prit, l'engagement de lui payer sa nouriter par ena somme de se francs int ciq cntiomes pas jounr qu'en sai elle est sa débitrie et hou se trénière poureni et e ecinq frcès de l’appele devant Monsieur lejage de Pair la su arcondipement qui les ontorisé à détenir sa malle jusque jour ou elle de serapoiquittée envers eur ; Par ces motifs — Le déclarer en conpétut pour comaitre de la demande la demoiselle Sandon, l'en débouter et la condamner aux dépens. Deton coté la demoiselle San son se présenta et conclut à ce qu’il plaut au Bureau Général du conseil attendu que la dame Jourson l’a occuhze pendont cinq mois à des travaux de contire après quoi elle lui delivra un certfiret dant lequel elle est qualitée d’ouvrière conturière ; Par ces motifs — Dire les époux Mégaenu non recevables enteur demante ; les en débouter. Le déclarer compétent, retenir la cause et oedonner qu’il sera explique sur le fand et condamner les époux Magnon aux dépens. Point de troit - Doit-on se déclarer on complent pour comastre de la demande de la demoiselle sanson? Ou bien doit-on de déclarer compétent, retenir la cause et ordonner que les parties seront tenue de s'expliquer sur le fundi pour être statué ce qu'il appartiendeu?? Que doit-il être statué à l'égard es dépens ? ? "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monpeur Louis Voubert soupeur d’habités, demeurant à Paris, rue de la bente tir numéro cinquante ; Demandeur ; Comparant ; D'une part ; </span>Monsieur Purant ; confectionneur d’habits, demeurant et domicilié à Paris, rue croir des Patits chemps, numéro cinquante ; Défendeur ; Comparant ; D'utre part ; Dinq de fait - Par letres du secrétaire du Conseil de Prud'homes du Département de la Seine pour l’industrie des tissus en dates des Mardi quatre et vendredi sept jaunvier mil huit cent soixente dix huit Joulert fpt citer sinant à comparaître par devant le dit conseil de Prud’homme demil au Bureaux Particulier les Vendredi sept et Murdi onze vun mil huit censoixmmmte dix fait sur se concilier si faire se pouvait sur lu demende qu'it entendait former cente lui devant le dit conseil en delai conge domme dusage Denait- n’ayant pas comparu la caute fut renvayée devans le Bureau Général du Conseil séant le jeudi toize juin mil huit cent prbte de li di e le dt es hun sem pr lettre du secrétaire du Conseil en date du onte juin nif huit Cent sixante dix huit à la reate de outant denans aplels; Ail le det dte sudi épaut de eue sut an en et oncsue que det hui e e e e e qui edte pue t e e e fitr de de afur l contilous es re de lui poarseue udits pos l l se e er pl de onos qnr i d it fi t M t de dommages-intérêts et le condamner pans les deur ense dépens. De son coé dement se présenta et conclut à ci qu’il plut au Bureau Général du Conseil attendu qu’il embouche pouler, pour un cop de moins et qu'en sontaires le conseisa à cajournée de quatre francs cinquante centimes de non con mis sonners le prétend ; qu'il la anpe dé poue qu'il étant en capable de bien faire la latijue ; Devait motifs ; Dire Soulert non recevablien su semante l'en déboiter et le condanner ux dépens. Panit, de droit Dait-on dire que Finand sera tenu de recevoir etae lui peutert et de lui continuer pendant le reste du mill de juin du travail de son état de coupeur d’habits enten servant les appointement de cent vingt francs pour le dit mois, sinon et faute par lui de ce faire le condamner à payerau dit Toulert la somme de cent vingt francs à titre de dammaser intrêts ? Ou bien dait-on dire Toulert non recevable ses demandes, l'en débouter ? Que doit-il être statué à l’égard des dépens ? "

[0] Unable to find something like "D'autre part ;" in:
 "<span type="part1">Entre Monsieur Charles Fétemett, ouvrier tordonner peurs à Paris, rue Paint souveur, numéro vingt huit ; Demanders comparant ; D'une part ; </span>Et Monsieur Gavail, Mntre cordonnier, demeurant et domicilié à Paris, rue Vayevir numéro quatre ; Défendeur ; Défaillant ;D’autre hart ; ores de fait – Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l’industrie des tissus en dates des Lundi trois et Mardi quatre juin mil huit cent sixante dix huit étemill fit citer Lavail à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les mordi quel et vendredi sept juin mil huit, cent soixante dix huit pour se concilier si faire se pouvait sur la demande qusil entendu former contre lui devant le dit conseil en paiement de la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour lalaire . Lavail n'ayant pas comprru la cause ft renvayée devant le Bureau Général du conseil séant le jeudi vingt et ajournée au putte Vingt sept juin mil huit cent soixante six temp Cité pour le dit suivant exploif de champron, luipier à Paris, en date du vingt un juin mil huit cent soixante dix huit, visé pour timbre et enregistré à Paris, le vingt deux Juin mil huit cent soirante dix huit, etmitte fit citer ravail à comparaître par devant le dit Conseil de Prud’hommes séant en Bureau Général le jeudi vingt sept juin mil huit cent soixante dix lui pour s'entendre condamner u lui payer avec intérêts suivant la loi la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour salaire plus, telte indemnité qu'il plaira au Conseil frer pour perte de temps devant les Bureaux du Conseil et les dépens. A l'’appel de la cause Lavail ne comparut pas. De son coté schunt se présenta et conclutz à ce qu’il plut au Bureau Général du Conseil donner défau contre Lavail non comparant ni personn pour lui quoique dumet appelé et pour le profit lui adjuger le bénifice des conclusions par lui prises dans le citation en plois de Champour, lui par enregistré. Poin de troit — Doit-on donner défaut contre lavail non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précédemment prises ? Que doit-il etre statué à l’égard des dépens ? "

<?xml version="1.0" encoding="utf-8"?>
<body>
 <div type="manuscrit">
  <div n="0" type="courtHearing">
   Audience
  </div>
  <div n="0" type="case">
   <opener>
    du Jeudi six Juin mil huit cent soixante dix huit. Siégeaient : Messieurs Pinaud, Chevalier de la Légion d'honneur, vice Président du conseil de Prud'homme du Département de la Seine pour l'Industrie des tissus, Lerroy, Larcher, Platiau, Broutin, Godfrin et Meslien, Prud'hommes assistés de Monsieur Lecucq, secrétaire dudit conseil
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur et Madame Lablanche, le sieur Lablanche tant en son nom personnel que pour assister et autoriser la dame son épouse ouvrière passementière, demeurant ensemble, les dits époux, à Paris, rue Besfroid, numéro dix ; Demandeur ; Comparant ; D'une part ;
     </span>
     <span type="part2">
      et Monsieur Nisson, fabricant passementier, demeurant et domicilié à Paris, rue Rambuteau, numéro trente huit ; Défendeur ; Défaillant ; D'autre part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la Seine pour l'industrie des tissus en dates des Lundi vingt et Mardi vingt un Mai mil huit cent soixante dix huit les époux Lablanche firent citer nisson à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particulier les Mardi Vingt un et vendredi vingt trois Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'ils entendaient former contre lui devant le dit conseil en paiement de la somme de douze francs pour prix de travaux de la dame Lablanche. A l'appel de la cause le vingt trois Mai mil, nisson n'ayant pas comparu le vingt un, la dame Lablanche se présenta et exposa comme cidessous. De son coté Nisson se présenta et exposa au Conseil que la dame Lablanche qui avait entrepris un travail qu'elle devait coudre le vendredi l'obligea à l'aller chercher chez elle le Lendemain samedi ; pour ce fait il entendit, Quoique le travail fut bien exécuté, ne payer que moitié prix de façon . Les parties n'ayant pu être conciliées la cause fut renvoyée devant le Bureau Général du Conseil séant le Jeudi six Juin mil huit Cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt cinq Mai mil huit cent soixante dix huit à la requête des époux Lablanche quatre mots rayés nuls Lecucq Nisson ne comparut pas. A l'appel de la cause les époux Lablanche se présentèrent et conclurent à ce qu'il plut au Bureau Général du Conseil donner défaut contre Nisson non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à leur payer avec intérêts suivant la loi la somme de douze francs qu'il leur doit pour prix de travaux de la dame Lablanche, plus, telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du Conseil et les dépens
   </div>
   <div type="pointDeDroit">
    Point de droit = Doit-on donner défaut contre Nisson non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui adjuger aux demandeurs les conclusions par eux précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
   </div>
   <div type="judgement">
    Après avoir entendu les époux Lablanche en leurs demande et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande des époux Lablanche parait juste et fondée ; Que d'ailleurs elle n'est plus contestée par Nisson non comparant ni personne pour lui quoique dument appelé ; Attendu que Nisson a fait perdre du temps aux époux Lablanche devant les Bureaux du conseil, ce qui leur cause un préjudice dont il leur doit réparation Par ces motifs = Le Bureau Général jugeant en dernier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf portant règlement pour les Conseils de Prud'hommes, Donne défaut contre Nisson non comparant ni personne pour lui quoique dument appelé ; Et adjugeant profit du dit défaut ; Condamne Nisson à payer avec intérêts suivant la loi aux époux Lablanche la somme de douze francs qu'il leur doit pour prix de travaux de la dame Lablanche, plus une indemnité de deux francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers les demandeurs à la somme de un franc, et à celle due au Trésor Public, pour le papier timbré de la présente minute, Conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement, la signification d'icelui et ses suites. Et vu les articles 437 du code de procédure civile, 27 et 42 du décret du onze Juin 1809, pour signifier au défendeur le présent jugement, Commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et que dessus Lecucq secrétaire
   </div>
  </div>
  <div n="1" type="case">
   <opener>
    Du dit jour six Juin —
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur Gutz Willer, ouvrier cordonnier, demeurant à Paris, rue de Jouy, impasse Guepin, numéro quatre ; Demandeur ; Comparant ; D'une part ;
     </span>
     <span type="part2">
      Et Monsieur Caillier Maître cordonnier, demeurant et domicilié à Paris, rue Notre dame de Nazareth, numéro soixante dix ; Défendeur ; Défaillant ; D'autre part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait = Par lettre du secrétaire du Conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en date du Mardi vingt un Mai mil huit cent soixante dix huit Gutz willer fit citer Caillier à comparaître par devant le dit Conseil de Prud'hommes séant en Bureau Particulier le Vendredi vingt quatre Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit conseil en remboursement de cinquante centimes pour pareille somme qu'il a payée au Bureau de placement qui l'adressa chez lui au moyen d'une carte et en celle de cinq francs pour perte de temps. A l'appel de la cause Gutz Willer se présenta et exposa au conseil que s'étant présenté au Bureau de placement pour y demander en carte fut envoyé à Caillier qui ne le reçut pas prétendant qu'il arrivait alors que la place était prises ; or, il résulte d'une clause qui régit les Bureaux de placement que le cas échéant le Patron rembourse le prix de la carte qui est de cinquante centimes et l'ouvrier n'a alors qu'à se retirer ; Mais Caillier s'est refusé à rembourser et fut cause qu'il perdait sa journée. De son coté Caillier se présenta et exposa au Conseil qu'il a en effet demandé au Bureau de Placement des ouvriers cordonniers un ouvrier, mais ce Bureau le lui envoyant près cinq semaines rien détonnant qu'il n'en ait plus besoin au jour de l'envoi. C'est pourquoi il s'opposa au remboursement du coût de la carte. Le Bureau Particulier fut d'avis que Caillier qui reconnut n'avoir pas contremandé la demande au Bureau d'un ouvrier aurait dû rembourser le coût de la caste, que s'étant refusé à le faire il devait les cinquante centimes et une indemnité de temps perdu qu'il fixa à quatre francs. Sur le refus de Caillier de se rendre à l'avis du Bureau Particulier la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt quatre Mai mil huit Cent soixante dix huit à la requête de Gutzwiller Caillier ne comparut pas. A l'appel de la cause Gutzwiller se présenta et Conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Caillier non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de quatre francs cinquante Centimes se composant de celle de cinquante centimes pour le prix d'une carte émanant du Bureau de Paiement et de celle de quatr francs pour indemnité du temps qu'il a fait perdre devant les Bureaux du conseil et les dépens.
   </div>
   <div type="pointDeDroit">
    Point de droit = Doit-on donner défaut contre Caillier non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
   </div>
   <div type="judgement">
    Après avoir entendu Gutzwiller en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande de Gutzwiller parait juste et fondée ; Que d'ailleurs elle n'est plus contestée par Caillier non comparant ni personne pour lui quoique dument appelé ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant règlement po les Conseils de Prud'hommes ; Donne défaut conter Caillier non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Caillier à payer avec intérêts suivant la loi à Gutzwiller la somme de quatre francs cinquante centimes pour remboursement d'un coût d'un carte de Bureau de Placement et indemnité de temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de soixante cinq centimes, et à celle dûe au Trésor Public pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante non compris le coût du présent jugement, la signification d'icelui et ses suites. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire
   </div>
  </div>
  <div n="2" type="case">
   <opener>
    Du dit jour six Juin
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur et Madame Poitier, le sieur Poitier tant en son nom personnel que pour assister et autoriser la dame son épouse ouvrière brodeuse, demeurant ensemble, les dits époux, à Paris, rue Beauregard, numéro neuf Demandeurs ; Comparant ; D'une part ;
     </span>
     <span type="part2">
      Et Monsieur Dieu fabricant de broderies, demeurant et domicilié à Paris, rue Turbigo, numéro cinquante trois ; Défendeur ; Défaillant ; D'autre part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait = Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l'industrie des Tissus en dates des Mardi vingt un et Vendredi Vingt quatre Mai mil huit cent soixante dix huit les époux Poitier firent citer Dieu à comparaître par devant ledit conseil de Prud'hommes séant en Bureaux Particuliers les vendredi vingt quatre et Mardi vingt huit Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'ils entendaient former contre lui devant le dit conseil en paiement de la somme de vingt quatre francs qu'il leur doit pour prix de travaux de la dame Poitier. Dieu n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt neuf Mais mil huit cent soixante dix huit à la requête des époux Poitier Dieu ne comparut pas. A l'appel de la cause les époux Poitier se présentèrent et conclurent à ce qu'il plut a Bureau Général du conseil donner défaut contre Dieu non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à leur payer avec intérêts suivant la loi la somme de vingt quatre francs qu'il leur doit pour prix de travaux de la dame Poirier, plus telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du conseil et les dépens.
   </div>
   <div type="pointDeDroit">
    Point de droit = Doit-on donner défaut contre Dieu non comparant ni personne pour lui quoique ce dument appelé et pour le profit adjuger aux demandeurs les conclusions pour eux précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
   </div>
   <div type="judgement">
    Après avoir entendu les époux Poitier en leurs demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande des époux Poitier parait juste et fondée ; Que d'ailleurs elle n'est pas contestée par Dieu non comparant ni personne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du conseil Dieu a causé aux époux Poitier un préjudice de perte de temps, ce qu'il doit réparer ; Pour ces motifs = Le Bureau Général jugeant en dernier ressort ; Donne défaut contre Dieu non comparant ni personne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du conseil Dieu a causé aux époux Poitiers un préjudice de perte de temps, ce qu'il doit réparer ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; Donne défaut contre Dieu non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Dieu à payer avec intérêts suivant la loi aux époux Poirier la somme de vingt quatre francs qu'il leur doit pour prix de travaux de la dame Poirier, plus une indemnité de quatre francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers les demandeurs à la somme de un franc, et à celle dûe au Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement, la signification d'icelui et ses suites. Et vu les articles 437 du code de procèdure civile, 27 et 42 du décret du onze Juin 1804, pour signifier au défendeur le présent jugement, commet champion, l'un de ses huissiers- audienciers. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire
   </div>
  </div>
  <div n="3" type="case">
   <opener>
    Dudit jour six Juin —
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur Afchain, (Edouard) ouvrier passementier, demeurant à Paris, rue des cendriers, numéro trente six ; Demandeur ; Comparant ; D'une part ;
     </span>
     <span type="part2">
      Et Monsieur Bailleux, fabricant passementier, demeurant et domicilié à Paris, rue d'angoulême, numéro cinquante ; Défendeur ; Défaillant ; D'autre part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la Seine pour l'industrie des tissus en dates des Mercredi vingt deux et vendredi vingt quatre Mai mil huit cent soixante dix huit Afchain fit citer Bailleux à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi vingt quatre et Lundi vingt sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit conseil en paiement de la somme de cinquante huit francs cinquante centimes qu'il lui doit pour salaire et en celle de vingt sept francs cinquante centimes pour perte de temps par la faute Bailleux n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du trente un Mais mil huit cent soixante dix huit à la requête d'Afchain Bailleux ne comparant pas. A l'appel de la cause Afchain se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé et pour le profit le condamne à lui payer avec intérêts suivant la loi la somme de cinquante huit francs cinquante centimes qu'il lui doit pour salaire et celle de vingt sept francs cinquante centimes pour temps perdu et le condamner aux dépens.
   </div>
   <div type="pointDeDroit">
    Point de droit = Doit-on donner défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions pour lui précedemment prises ? Que doit-il être statué à l'égard des dépens ?
   </div>
   <div type="judgement">
    Après avoir entendu Afchain en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande d'Afchain parait juste et fondée ; Que d'ailleurs elle n'est pas contestée par Bailleux non comparant ni personne pour lu Quoique dument appelé ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant règlement pour les conseils de Prud'hommes ; Donne défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit dépens ; condamne Bailleux à payer avec intérêts suivant la loi à Afchain la somme de cinquante huit francs cinquante centimes qu'il lui doit pour salaire, puis celle de vingt sept francs cinquante centimes pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe au Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement, la signification d'icelui et ses suites. Et vu les articles 437 du code de procèdure civile, 27 et 42 du décret du onze Juin 1809, pour signifier au défendeur le présent jugement, commet champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire
   </div>
  </div>
  <div n="4" type="case">
   <opener>
    Dudit jour six Juin
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur Zephir Dignoire, ouvrier passementier, demeurant à Paris, Cité Popincourt, numéro six ; Demandeur ; Comparant ; D'une part ;
     </span>
     <span type="part2">
      Et Monsieur Bailleux, fabricant passementier, demeurant et domicilié à Paris, rue d'Angoulême, numéro cinquante ; Défendeur ; Défaillant ; D'autre part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en dates des Mercredi vingt deux et vendredi vingt quatre Mai mil huit cent soixante dx huit Dignoire fit citer Bailleux à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi vingt quatre et Lundi sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit conseil en paiement de la somme de cinq cent soixante treize francs cinquante centimes qu'il lui doit pour salaire. Bailleux n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt neuf Mai mil huit cent soixante dix huit à la requête de Dignoire Bailleux ne comparut pas. A l'appel de la cause Dignoire se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de cinq cent soixante treize francs cinquante centimes qu'il lui doit pour salaire, plus, telle indemnité qu'il plaira au Conseil fixer pour perte de temps devant les Bureaux du Conseil et les dépens, ordonner l'exécution provisoire du payement à intervenir, nonobstant appel et sans qu'il soit besoin par lui de fournir caution, conformément à l'article quatorze de la loi du premier Juin mil huit cent cinquante trois.
   </div>
   <div type="pointDeDroit">
    Point de droit = Doit-on donner défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
   </div>
   <div type="judgement">
    Après avoir entendu Dignoire en ses demandes et Conclusions et en avoir délibéré conformément à la loi ; Attendu que les demandes de Dignoire paraissent justes et fondées ; Que d'ailleurs elles ne sont pas contestée par Bailleux non comparant ni \trsonne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du conseil Bailleux a causé à Dignoire un préjudice de perte de temps, ce qu'il doit réparer ; Par ces motifs = Le Bureau Général jugeant en premier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant règlement pour les Conseils de Prud'hommes ; Donne défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Bailleux à payer avec intérêts suivant la loi à Dignoire la somme de Cinq cent soixante treize francs cinquante centimes qu'il lui doit pour salaire, plus une indemnité de six francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe au Trésor Public, pour la papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante ; ence, non compris le coût du présent jugement, la signification d'icelui et ses suites ; Ordonne l'exécution provisoire du présent jugement, nonobstant appel et sans qu'il soit besoin par le demandeur de fournir caution, conformément à l'article quatorze du premier Juin mil huit cent cinquante trois. Et vu les articles 437 du code de procèdure civile, 27 et 42 du décret du onze Juin mil huit cent neuf, pour signifier au défendeur le présent jugement commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire
   </div>
  </div>
  <div n="5" type="case">
   <opener>
    Dudit jour six Juin —
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur Jules Omnès, ouvrier implanteur, demeurant à Paris, rue Quicampoix, numéro quatre ; Demandeur ; comparant ; D'une part ;
     </span>
     <span type="part2">
      Et Monsieur Pagès, Maître coiffeur, demeurant et domicilié à Paris rue de Rennes, numéro soixante cinq ; Défendeur ; Défaillant ; D'autre part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en dates des Mercredi vingt deux et vendredi vingt quatre Mai mil huit cent soixante dix huit Omnès fit citer Pagès à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi vingt quatre et Lundi vingt sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit Conseil en paiement de la somme de six francs cinquante centimes qu'il lui doit pour salaire. Pagès n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt neuf Mai mil huit cent soixante dix huit à la requête de Omnès Pagès ne comparut pas. A l'appel de la cause Omnès se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Pagès non Comparant ni personne pour lui quoique dument appelé pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de six francs cinquante centimes qu'il lui doit pour salaire, plus, telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du conseil et les dépens.
   </div>
   <div type="pointDeDroit">
    Point de droit = Doit-on donner défaut contre Pagès non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions pour lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
   </div>
   <div type="judgement">
    Après avoir entendu Omnès en ses demandes et Conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande d'Omnès parait juste et fondée ; Que d'ailleurs elle n'est pas contestée par Pagès non comparant ni personne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du Conseil Pagès a causé à Omnès en préjudice de perte de temps, ce qu'il doit réparer ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; Vu l'article quarante du décret du onze Juin mil huit cent neuf, portant règlement pour les conseils de Prud'hommes ; Donne défaut contre Pagès non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Pagès à payer avec intérêts suivant la loi à Omnès la somme de six francs cinquante Centimes qu'il lui doit pour salaire, plus une indemnité de six francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe un Trésor Public, pour le papier timbré de la présente minute, Conformément à la loi du sept août mil cent cinquante, ence non compris le coût du présent jugement, la signification d'icelui et ses suites. Et vu les articles quatre cent trente cinq du code de procèdure civile, vingt sept et quarante deux du décret du onze Juin mil huit cent neuf, portant réglement vu d'allemagne numéro cent quarante pour les conseils pour signifier au défendeur le présent jugement, Commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus Lecucq secrétaire —
   </div>
  </div>
  <div n="6" type="case">
   <opener>
    Dudit jour six Juin
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur Jean Baptiste Richard, demeurant à rue de Flandre, 28 à Paris\tagissant au nom et comme administrateur de la personne et des biens de son fils mineur Victor, ouvrier en apprêts sur Etoffes ; Demandeur ; Comparant ; D'une part ;
     </span>
     <span type="part2">
      Et Monsieur Lecrique et Compagnie, Maîtres appréteurs d'Etoffes, demeurant et domiciliés à Paris, quarante trois rue Riquet et rue de Tanger ; Défendeurs ; Défaillant ; D'autre part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en dates des Mercredi vingt deux et Vendredi vingt quatre Mai mil huit cent soixante dix huit Richard, ès noms et qualités qu'il agit, fit citer Lecrique et Compagnie à comparaître par devant le dit Conseil de Prud'hommes séant en Bureaux Particuliers les _ Vendredi vingt quatre et Lundi vingt sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre eux devant le dit conseil en paiement de la somme de sept francs qu'il lui doit pour prix de travaux de son fils Victor. Lecrique et compagnie n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cités pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt Neuf Mai mil huit cent soixante dix huit à la requête de Richard Lecrique et compagnie ne comparurent pas. A l'appel de la cause Richard se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Lecrique et cie non comparant ni personne pour eux quoique dument appelés et pour le profit les condamner solidairement à lui payer avec intérêts suivant la loi la somme de sept francs qu'ils lui doivent pour salaire de son fils victor et les condamner aux dépens.
   </div>
   <div type="pointDeDroit">
    Point de droit = Doit-on donner défaut contre Lecrique et compagnie non comparants ni personne pour eux quoique dument appelés et pour le profit adjuger au demandeur les conclusions par lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
   </div>
   <div type="judgement">
    Après avoir entendu Richard en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande de Richard parait juste et fondée ; Que d'ailleurs elle n'est pas contestée par Lecrique et compagnie non comparant ni personne pour eux quoique dument appelés ; Attendu qu'en ne comparaissant pas devant les Bureaux du Conseil Lecrique et Compagnie ont causé à Richard un préjudice de perte de temps, ce qu'ils doivent réparer ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; Vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant réglement pour les conseil de Prud'hommes ; Donne défaut contre Lecrique et Compagnie non comparants ni personne pour eux quoique dument appelés ; Et adjugeant le profit du dit défaut ; Condamne Lecrique et Compagnie à payer avec intérêts suiva la loi à Richard la somme de sept francs qu'il lui doit pour salaire de son fils victor, plus une indemnité de six francs pour temps perdu ; Les condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe au Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement, la signification d'icelui et ses suites ; Et vu les articles 435 du code de procédure civile, 27 et 42 du décret du onze Juin 1809, pour signifier au défendeur le présent jugement, Commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus Lecucq secrétaire —
   </div>
  </div>
  <div n="7" type="case">
   <opener>
    Dudit jour six Juin —
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur Bréchot, ouvrier Galochier, demeurant à Paris, rue de Meaux, numéro trente sept, passage de la Brie ; Demandeur ; Comparant ; D'une part ;
     </span>
     Et Monsieur Vermérot, fabricant de Galoches, demeurant et domicilié à Paris deux mots rayés nuls Lecucq —
    </rs>
   </div>
  </div>
  <div n="8" type="case">
   <opener>
    Dudit jour six Juin
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur — Emile Pilloy, ouvrier Bourrelier, demeurant à Paris, rue de Cotte, numéro seize ; Demandeur ; Comparant ; D'une part ;
     </span>
     <span type="part2">
      Et Monsieur Prevost père, Maître Bourrelier, demeurant et domicilié à Paris, Montreuil sous Bois, près Paris, rue du Pré, numéro trois ; Défendeur ; Défaillant ; D'autre part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait = Par lettres du secretaire du Conseil de Prud'hommes du Département de la seine pour l'Industrie des Tissus en dates des Mercredi vingt deux et Vendredi vingt quatre Mai mil huit cent soixante dix huit Pilloy fit citer Prevost à comparaître par devant le dit Conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi Vingt quatre et Lundi vingt sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit Conseil en paiement de la somme de Cinquante sept francs se composant de trente neuf francs pour prix de soixante dix huit heures à cinquante centimes et de dix huit francs pour travaux à la tache. Prevost n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt neuf Mai mil huit Cent soixante dix huit à la requête de Pilloy Prevost ne comparut pas. A l'appel de la cause Pilloy se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Prevost non comparant ni personne pour lui quoique dûment appelé et pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de cinquante sept francs qu'il lui doit pour salaire, plus, telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du conseil et les dépens.
   </div>
   <div type="pointDeDroit">
    Point de droit = Doit-on donner défaut contre Prevost non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
   </div>
   <div type="judgement">
    Après avoir entendu Pilloy en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que les demandes de Pilloy paraissent justes et fondées ; Que d'ailleurs elles ne sont pas contestées par Prévost non comparant ni personne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du Conseil Prevost a causé à Pilloy un préjudice de perte de temps, ce qu'il doit réparer ; Sur ces motifs = Le Bureau Général jugeant en dernier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant règlement pour les conseils de Prud'hommes ; \tDonne défaut contre Prévost non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Prévost à payer avec intérêts suivant la loi à Pilloy la somme de cinquante sept francs qu'il lui doit pour salaire, plus une indemnité de six francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe au Trésor Public pour le prix timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement la signification d'icelui et ses suites. Et vu les articles 435 du Code de procèdure civile, 27 et 42 du décret du onze Juin 1809, pour signifier au défendeur le présent jugement, Commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire —
   </div>
  </div>
  <div n="9" type="case">
   <opener>
    Dudit jour six Juin —
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Madame veuve Bernard, ouvrière demeurant à Joinville le Pont, près Paris, rue du canal, numéro onze ; Demanderesse ; Comparant ; D'une part ;
     </span>
     <span type="part1">
      Et Monsieur Bardin, fabricant plumassier, demeurant et domicilié à Joinville le Pont, rue des Réservoirs ; Défendeur ; Comparant ; D'une part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la seine pour l'industrie des Tissus en dates des mardi vingt huit et Vendredi trente un Mai mil huit cent soixante dix huit la veuve Bernard fit citer Bardin comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi trente un Mai et Lundi trois Juin mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'elle entendait former contre lui devant le dit Conseil en paiement de la somme de vingt deux francs trente centimes qu'il lui doit pour salaire. Bardin n'ayant pas comparut la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du trois Juin mil huit cent soixante dix huit à la requête de la veuve Bernard Bardin comparut. A l'appel de la cause la veuve Bernard se présenta et conclut à ce qu'il plut au Bureau Général du conseil condamner à lui payer avec intérêts suivant la loi la somme de vingt deux francs trente centimes qu'il lui doit pour salaire, plus, telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du conseil et les dépens.
   </div>
   <div type="pointDeDroit">
    Point de droits De son coté Bardin se présenta et conclut à ce qu'il plut au Bureau Général du conseil attendu que la veuve Bernard a quitté ses ateliers sans faire la huitaine d'usage ; Que d'ailleurs le travail dont elle reclame le prix ayant été par elle mal exécuté il ne lui a doit pas la façon ; Par ces motifs = dire la veuve Bernard non recevable en ses demandes, l'en débouter et la condamner aux dépens. Point de droit = Doit-on condamner Bazin à payer à la veuve Bernard la somme de vingt deux francs trente centimes pour travaux de son état ? Ou bien doit on pour les raisons invoquées par Bardin dire la veuve Bernard non recevable en sa demande, l'en débouter ? Que doit-il être statué à l'égard des dépens ?
   </div>
   <div type="judgement">
    Après avoir entendu les parties en leurs demandes et conclusions respectivement et en avoir délibéré conformément à la loi ; Attendu qu'il est constant que Bardin doit à la veuve Bernard la somme de vingt deux francs trente centimes pour travaux exécutés à la tache ; Que Bardin qui allègue de mal façon de ces travaux ne prouve pas et déclare ne plus pouvoir prouver ce qu'il avance ; Attendu en ce qui concerne le départ subit de la veuve Bernard des ateliers de Bardin que ce départ remonte à quinze jours sans que Bardin ait, depuis, fait la moindre démarche pour retenir ou faire rentrer son ouvrière, ce qui est considéré comme un consentement au moins tacite à son départ ; Attendu aussi que Bardin n'a pas comparu devant les Bureaux Particuliers et a par cette non comparution fait perdre du temps à la demanderesse qui en éprouve un préjudice dont il lui doit réparation par la somme de quatre francs ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; condamne Bardin à payer à la veuve Bernard la somme de vingt deux francs trente centimes qu'il lui doit pour salaire, plus une indemnité de quatre francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers la demanderesse à la somme de un franc vingt centimes, et à celle dûe au Trésor Public pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le vingt neuf mots rayés Comme nuls Lecucq coût du présent jugement, la signification d'icelui et ses suites. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire —
   </div>
  </div>
  <div n="10" type="case">
   <opener>
    Dudit jour six Juin —
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur Jean Lardet, ouvrier tisseur, demeurant à Gentilly près Paris, rue de la Comète, numéro cinq ; Demandeur ; Comparant ; D'une part ;
     </span>
     <span type="part2">
      Et Monsieur Bardin fabricant de tissus en plumes, demeurant et domicilié à Paris, Boulevart Saint Jacques, numéro cinquante un ; Défendeur ; Comparant ; D'autre part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait = Par lettres du secrétaire du Conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en dates des Lundi treize et Mardi quatorze Mai mil huit cent soixante dix huit Lardet fit citer Bardin à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Mardi quatorze et Vendredi dix sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit conseil en paiement de primo, cinquante francs pour le salaire de cinq journé pour lui f dix journées par lui lui faites à raison de cinq francs par jour ; Secondo, de vingt deux francs cinquante centimes pour neuf journées à deux francs cinquante centimes du travail de la dame Lardet, sa femme ; Tertio, de celle de sept cents francs restant dûs sur le prix d'une machine par lui faite sur ses ordres. Bardin n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit Cité pour le dit jour six Juin par lettre du secrétaire du conseil en vingt trois Mai mil huit cent soixante dix huit et ajournée au Jeudi six Juin suivant mois. Suivant exploit de Champion, huissier à Paris, en date du vingt sept Mai mil huit cent soixante dix huit, visé pour timbre et Enregistré à Paris, le vingt huit Mai mil huit Cent soixante dix huit, débet deux francs quinze centimes, signé de feuillez Lardin fit citer Bardin à comparaître par devant le dit Conseil de Prud'hommes séant en Bureau Général pour le Jeudi six Juin mil huit cent soixante dix huit pour s'entendre condamner à lui payer la somme de huit cent soixante douze francs cinquante centimes se composant de primo, de cent cinquante francs pour indemnité d'un mois de congé, secondo vingt deux francs cinquate centimes pour solde de journies de la dame Tarset,et de set cents frocs pour prix Et d'une mécunque qui lui a été forrce par lu. A l’appel de la case partet se présenta et conclut à ce qu’il plut au Bureau Général du Conseil lui adjuger le bénifie des conclusions par lui préses dont la citalion exploit de chanmpion, lui près, enregistré et leondamner Bardin aux dépens. De son coté Bardin se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu qu’il n’a pas enfagé Lerder comme ouvrier ; Que s’il et veuu chez lui le st pour y travailler pour son compte à lui Berdin, mois bien pour son profe compte t à une mécuviue qu’il lui a vendue à contition qu’elle fontinse farilement a jec l’arde d’une somme, le qui expligéé commet la dame tordes entre chez lui aménée par son mari qui prétendaint l’employer à cette machine, perte qu'elle duz abondonner le mecanique ne pouvant fonctiner ; attendu ependanit qu à offre payer les vingt deux francs cinquante centimes prodit des neuf journéen de la dine portis oure que celte lore n'oil uis prodtuit ; Par ces motifs - Dire lomn ore vrdit necerlle en ses demandes, l'en débouter et le condamner ux dépens. Pint de troit - Dait-on condamner trédin à payer à erdit la somme de huit cent soirante douze frons cinquate centimes par de la congé d'un mois de torrtet talaire de la dame verder de pretseue ongra parée d’a du et n le que lis ouidiet pur et le e l ue le dtie Lertit non recevableon ses demandes l'endébouter e que de sus te se dui l un deis pus purte le parties en leurs demandes et conclusions respectivement et en avoir et de d pre ille Mrede en in atis ant dene pr e du deu et peur en de e sand eur eu e e de du et en tortie er e e ie purs ser hu tit en e e e a e orer ote e oni e aten u elest prs sn dt lite d e e pusils aeden omi en ent les ei e e e pe e con es les u an e ur de se ete e te dement deprei teuip r et e deds pue an ten pete. pu de u e te de se pu lese qi e E dé a 1835 des vngt deux francs cinquante centimes pour le sataire de la dame Pardr le Conseil n’a qu’à lui en donner aiteor de déclarer en compéteis pour le surplus ; Par ces motifs Le Bureau Général jugeant en premier ressort; donnea à Berstin de l’offre qu’il fait de payer à la jourre du conseil les vingt deux francs cinquante centimes qui lui da réclamés pour salaire de la dame Tardet. Le véutay on compétent pour coumaitre des entrer demandes de tavail qu'il reuvoie devant qui de troit et condamne lar dit u dépens taxés et liquidés enven le Trésor Public, à la soe de deix francs quinze centimes pour le papier timbré l’enregistrement de la citation, conformément à la loi à sept aoux mil huit cent cinquante euce, non compris le cout du présent jugement, la signification d’icelui et ses suites.
   </div>
   <div type="judgement">
    Ainsi jugé les jour mois et an que dessus. Dineu recucg létaire àQudit jour dix ving a Entre Mademoiselle Delasague, fille majeure, ouvrière demeurant à Paris, rue Sant Blonoré, numéro deux cens cinquante sept ; Demanderesse ; Comparant ; D’une par ; t Monsieur Chaput, Cutotiier, demeurant et domiée à Paris, rue de vanmes, numéro six ; Défendeur ; Défelle Dutre part ; Poit de fat - las tetres du peretire de linre de Prud’hommes du Département de la soine pour l’industrier des tissus en dates des vendredi trente un Mai et lundi treis Juin mil huit cent soixante dix huit la demoiselle Wéleve fit citer chaput à comparaître par devant le dit Conseil Prud’hommes sant en Bureont Particuliers les lundi trois et Mardi quatre juin mil huit, cest soixante dix fait pour se conctamner si fairre se pouvait sur la demande qu'elle se convais frrer contre lui devant le dit Conseil en paiement de la somme de Eix deux frans cinquante centimes qu’il lui doit pour salaré ae chapit l'ayar pas comparala cause pt renvogyée les oise Bureau Général du Conseil séant le jeudi six Juin milhi comparante dix huit. Cité par l dit jour li, pui fil ene MA uts secrétaire du Conseil en date du quatre juin mil huit cent soixante dix huit à la réquite de la demoiselle Delevaique chapat ne comparut pas. A l'appel de la cause la demoiselle delevarque le présenta et conclut à ce qu'il plut au Bureau Général du Conseil donner défut contre chapert non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de trente deux francs cinquante centimes qu’il lui doit pour salaire plus telte indemnité qu’il plaira au Conseil fixer pour perte de temps devant les Bureux du Conseil et les dépens.
   </div>
   <div type="pointDeDroit">
    Pont de droit — Dait-on donner défaut contre chapot non comparant en personne pour lui quoique dument appelé et pour le profit adjuger à la demanderelse les conclusion par elle précédemment prises ? Que doit il être statué à l’égard des dépens ? près avoir entendu la demoiselle Delevaique nss demandes et conclusions et en avoir délibéré conformément à la loi; Attendu que la demande de la demoiselle Dele vaique paroint juste et fondée ; Que d'ailleurs elle n'est pas contestée par Chapnt non comparant ni personne pour lu quoique dument appelé ; attendu qu’en ne comparuisant pas devait les Bureaux du Conseil chapat a causé à la demoiselle de le vaique un préjudice de perte de temps, ce qu'il doit éprur ; Par de pentipe. le Brnveu prat l jugen en nere ps ou larticle quarante un du decres du onze Jui mil huit cent el p re en eu te e réseu donne défaut contre Chapat non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; condamne Chapat à payer avec intérêts suivant e et uidte nonean pen t e sei aun en ant le e e t u ten du pu deit tr n e poir en iq u lu, le da deme t en de e ere du de te u e per de tue nir t u ie s pen p pere lele eseq ei q sar de onier uie u e e de paser e e e de onte et preur ante te u uig cin eit nde er ani e purs de pur eu ait lapre nt endes tis p l jur mit ie risill an que dessus. H demait-entéra A Audit jour dix Juin Entre Monsieur Parcors, ouvrier tailleur, demeurant à Cave rue courne fort , numéro quarante trois ; Demandeur ; Comparant D'’une part ; Et Monsieur Aingélà Maitre tailleur s hatit demeurant et domicilié à Paris, Boulevart saint Martie numéro quatre ; Défendeur ; Comparant ; D'autre paryr e Point de fait - Par lettres du secrétaire du Conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus en dates deu Mardi quatorze et Vendredi dit sept Mai mil huit cent soixante dix huit siçon fit citer Engele à comparaître par devant le dit conseil de Prud’homme séant en Bureux Particu les vendredi dix sept et Vendredi vingt quatre eMai mil suit cent soixante dix huit pour se concilier si faire se pouvait pur la demande qu'il entendait former contre lui devant le dit Conseil en paiement de la somme de dix huit francs pour la façon d’iune vêtement. Sengéli n'ayant pas comparu la cause fut renvayée devant le Bureau Général du Conseil séant le jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six juin per lettre du secrétaire du Conseil en date du vingt cinq mai mil huit cent soixante dix huit à la réguate de Fiçon Angeti comparut. A l’appel de la cause, ficois se présenta et conclut à ce qu’il plut au Bureau Général du Conseil dun déde Angéti à lui payer avec intérêts suivant la loi la somme de dix hui francs qu’il lui doit pour salaire et le condamner ax dépens. De bon coté Angéli se présenta et conclut à ce qu'il plut au Bureau Général du conseil attendu que le vêtement du quel Lricois réclame le prix a été nil fait, ; quil etre d'ailleurs entre les moins pour le rataucher ; Par ces moff due Ricon non recevable en sa demande, le recevait reconventionnellement demandeur dire que lorsque le dit Bêtemenit sera rapporté rectifie le prix en ser payé condamner firois aux dépens. Pnt de droit — Doit on condamner cingélui à payer à Perrois la somme de doprns francs pour la façon d'une peerdessns. Ouben dutecn dire Ficon non recevable en sa demande, l'en débouter recevoir cnge li reconventionnellement demandent ; dire que préctableneunt au paiement du prix de dix huit francs deuit, sera tenu de rectifier le Baletot par de ssus qu'il a sitré mains ? Que doit-il être statué à l’épard des dépens Après avoir entendu les parties en leurs demandes et conclusions respectivement et en avoir délibéré conformément à la loi eu attendu qu'il résulte des explications farnies par lu oins a ud i s ité di l du r partiec que le par de pas qui fait l'objet du titnge a été trendu fait par Bicoin le vingt deux avril dernier ; que sil est ce jour entre les moins de Seiçon s’ess qu’angeta la loi à renvoyé àprir plus de trois semaines ; que la raison invaguée par Angelé qui aurait pu être humise à examen dans un de lai apportie ne pout plus l'être aujourd’hui ; Pir ces motifs Le Bureau Général jugeant en dernier retsort ; Condamne ingeli à le precdre pasression du par dessus et à en payer le prix defaçon par dix huit francs, reçoit angere dans la demande reconventionnelle, l'en déboute ; Le condamne aux dépens taxés et liquidés envers le demandeur à la somme de ufronc et à celle dui au Trésor Public, pour le papier timbré de la présente minute, coanformément à la loi du sept Août mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites Ainsi jugé les jour mois et an que dessus. ecusq secrétaire E mou e Leonq dudit jour six cui Eintre apiy courdait Cinsiq aune epiet du ete eri ui ee lete mon ls Demandeur ; Comparant ; D’une part ; L Monpeur iévred trdite. Mite hep e dement e quidte doui co lei de ete nte pasé ux, défard coupet de pasint a pe e e e e te or ae de t e ende eitese dur e renei de repue e pre a pus auet les e et ur e te eleure eu te e e poretr a e en e purs ite de u t e deen e po de p ren de e tes due e en emret en an qur e ci pen de daemar u i à L s LPr huit. Cité pour le dit jour dix juin par lettre du secrétaire du Conseil en date du vingt cinq mai mil huit cent soixante dix huit à préqute de hattrot cr comparut. l’appel de la cause Talleir, se présenta et conclut à ce qudie plut au Bureau Général du Conseil condamner lrcord à laie payer avec intérêts suivant la loi la somme de vingt francon réparation du dommage qu’il lui cansé en nel soumpart pour le douze Mai dernier ann qu’il ene avait, pris l’engugement et le condamner aux dépens. De son coté Brard se présenter et conclut à ce qu'il plut au Bureau Général du Conseil elle qu’il est d’usage qu'un Carron adrepé par Bureau se Placement pour faire ertra le Demonche porte sa cort la vendredi avant le mout ; Que fralliot que prétendque la carte lui aété de livrée trop tors le vendredi pour qu’il lu partet le même jour anrait, pu tant au mans le porter le samedi matin le bonteur, ce qui lui en lévite de faire sa causse de chez lui au Bureau se placement ; Que n’était venu le samedi qu'a près nde et aboors qu'il en avoir demand un autre il sne fut l'ocuper ; Par ces motifs — Dire paltier non recevable en sa demande, l'en débouter comme mal foncéi elle et condamner ax dépens. Pont de trait — Doit -on condamner Ernér à payer à Gaticot la somme de vingt frencs poar Eademité de non éxéccution de convention verbale de travail Ou bien doit-on dire pultiot non recevable en sa demande, l'un débouter ? Que doit il être statué à l’égard des dépens . Lprés d avoir entendu les parties en leurs demandes et conclusions respectivement et en avoir délibéré conformément à la loi ; attendu qu’il est d'usage constant que l'ouvrier envoyé par le Bureau de placement poir faire l’attre du Demonire porte la cost de dit Bureau de vendredi avant cinq heures ; attendu qu'il est constant que aaltcct ou dit -l n’a pu se présenter au domicile ? mois le vendredi le Bureau de Clacement lui ayant donné la tarte trope ; Pard s'esx présenté le samedi aper tors pour quatifier l’inqudte du défendeur et le précantion qu’il a prise de seproier n eemet garan ; Qu'on ce cas pellait n'it pas foire dans sa demande en indemnité ; Pur ces motifs - Le Bureau Général jugeant en dernir lessart ; Et galtrit non recevable en sa demandes l’en detré Le condamne ex dépens envers le Trésor Public pour le papier timbrée de la présente minute, conformément à la loi du segt du oir lu cent cinquante, once, non compris le cout du présent jugement ; qa sigification d’icelu et ses sutes. Cin vuit les jourmis euf laiceu Lecuiq secrétaire nmces M
   </div>
  </div>
  <div n="11" type="case">
   <opener>
    Du dit jour dix Plinau Entré onsieur jandron fabricant chemisier demeurant à Paris, rue du quatte septomtre, numéro quinze Défendeur ou primipal ; Demandeur epposant ; Défaillant ; D’une part ; Le Monseur Wébot cupeur chemisier, demeurant à Pasis, ue Villedo, numéro cinq Demandeur pronpal; Défendeur opposent ; comparant ; D'autre part ; cont de fait — Pur jugement Lendu par défaut par le conseil se Prud’hommes du Département de la Seine pour l’industrie des tissus le onze avril mil huit cent soixante dix huit, enregistré, Lunaron a été condamne à payer à Péot la somme de sept cent quatre vingt cinq trongs de pemitul et les accelure . Enqunte fit signifie à lundron par exploit de cnempron, lui prer a Ruris, en date du huit Mai mil huit cent soixante, dix tuit, 'agate à la réguéte de litetà Suivant exploit de Blnand comprar à Paris, en date du dix Mai mil huit cent paranti dix huit, vigé pour timbre et enregistré à Paris, le ouze Mai mil huit cent sorixmante dix huit. débet deux francs quinze centimes, signé de Pouiller, landion formé opposation à ce jugement et tta Citel à comparaitre par devant le dit Conseil de Prud’homes sige louveiliedt le ut su du e urecln d dix huit pour la voir recevoir opposent à c jugement, lvour dé charger des condamnetions contre lui prenuyé en peisnper et ruce frrires, s'entendre, le dit Pétel, délareer non recevablle uteant er e e lu es on eps p du it e tes dei lt e le e e demenra se dmenvare du ete e er it e e e det t e d e ine cdu pret et erdlit e lun e ue ru e er te pe ele t oesin auprse r e te l e u t eie ae ren ter e d es u erete enden e e e q ir e rei que eme de ense ae e e de ent lu it ue en p de e de tente quee e e e e e on e oaire du d prte e es ue oui l u domnaps pour puti ar de p sree et et e en a e le ue de s ui de de e preer dl s MI q entendu Rotaf en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que les demandes de setet paraissent justes et fondées ; Que d'ailleurs elles ne sont pas contestées par landon non comparant ni personne pour lui quoique demendeur opposat ; Attendu que cette non comparation dit été consilérée comme on atandans l’oppontion . Par es motifs - Le Bureau Général jugeant on fermerer répsort, on la forme mois Landon on de appolétion au jugement rendu contre lui le onze avril dernier senregistré, l'en déboute comme mal fonné en ccelle. Ordonne l’exécution pure et sumple du jugement auquel est oppolétior et condamne landon ax dépens taxés et liquidés en vers Le Trésor Public, à la somme de deux francs quinze centimes pour le papier timbré et l’enregistrement de l’appention, conformément à la loi du sept cox mil huit cent cinquante, ence, non compris la cous du présent jugement, la signification d'icelui et ses suites. Ansi jugé les jour mois et an que dessus. Derucq secrétaire Dmai t d tn a Mudit jour six viiq u
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur Levonte, anvner lapeemntier, demeurant à Boles, au rondipement de Clermont leisa j Demandur, Comperant ; D'une part ;
     </span>
     <span type="part1">
      Et Monpeur Curtet Jeun Marie, ouvrier vailleur d’hobits, demeurant à Paris, passage Montes quien, numéro cinq ; Demander ; Comparant ; D’une part ;
     </span>
     L Monsieur Lacembe,; tute dailleur d’habits, demeurant et domicilié à Paris rue lai sa numéro quinze ; Défendeur ; Comparant ; Dutelurt, Paint de fait - Par lettres du secrétaire du Conseil de srur ha du Département de la Seine pour l’industrie des tissus en dtates des Vendredi trente un Mai et lundi trois juin mil huit cent soixante dix huit, curtet fit citer Lacombe à Comparatre par devant ledit conseil de Prud’homme séant en Bureaux Particuliers les emble trois et Mardi quatre Cin mil huit cent soixante dix huit pour sin concilier à faire se pouvait sur la demande qu’il entendit forixen contre lui devant le dit Conseil en paiement de la somme de Pi de sit es arés nil Mn ec e Dre q u 2 i ié a q i i q du soixante denx francs pour selaire. La coulée n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six pis par lettre du secrétairé en date du quatre juin mil huit ent soixante dix huit à la réquité de Cortet Lecomle comparut; A l'appel de la cause Curet se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner lacole à lui payer avec intérêts suivant la loi la somme de soinante deux francs qu'il lui doit pour prix de travaux de son état et le condonner aux dépens. De son coté Laconbe se présenta et conclut à ce q’il plut au Bureau Général du Conseil ettendu qu'il reconnit devoir la soume de soixante deux francs ; mois attendu qu'an dêtenant le fière qu'il encère entre les moins turtet lui ceuse préjudice Luisque cette frèce n’ayant pa être livrée en temps la cestire pour comple Par ces motifs — Lire Cortet untucevable, en rademand e tavu débouter eile condamner aux dépens. foit de doit - Doit on condamner Lacoute à payer à cortets la somme de soixante de soixante deux froncs contre la remisé d’un ditent qu'il salire à Paton dit en du leis reant lut la demande ; l'en débouter ecevoir Lécombe reonvou tionnellement demandent ; Condamne Tortet a remitre le vêtement en psus eu disitre ou mis d sant au eu du du sent pu tie ueapsite d etito u ell t tent lerse lu de lete de e e e t l peiue en leurs demandes et conclusions repeetivement et encvord et e t te e te ite u ede ur tn du t esur purt la pre des ioise les our are e e e e t due a ler s r se te e dte t e poi i en e p e ten u reit e de u te enpe ente e pe de iq ount quilt eten de ses uee de de e titer duit en ue se andurs deuen der de aun u ant e e reie uar e e t enede tain e ti qu iq e te see inps e Mée minute, Cnformément à la loi du sept aût mil huit cent cinquante rue, non compris le cout du présent jugement, la signification d’iceluit et ses suites.
    </rs>
   </div>
   <div type="judgement">
    Ainsi jugé les jour, mois et an que dessus. anaus Lecusq secrétaire te u dit jour six Piig linre Monsieur Langer, ouvrier tordonner, demeurant à Paris, rue saint Honoré, numéro cent quarante neuf ; Demandeur Comparant ; D’une part ; A Madame veuve Denis, demeurant lais, rue le la Roguette, numéro trente six ; Défenderesse Défaillent ; D'utre part ; Point de fat – Poirant emplot dà Chau hurs, huipser à Paris, en date du vingt neuf Mai mil huit cent sorxante dix, huit, visé por tioibre et enregistré à Paris, le trente en Mai mil huit cent soixante dix huit débet quatre francs cinquante cinq centimes, signé à de Seuilles, Leufer a signifie et soifsé de prè à la veuve Dons, frimo dun jugement endu par le conseil de Prud'hommes du Département de la Seine pour l’industrie des tissus le vingt un ferrier dernier enregistré; portant condamnetion contre le sieur Mllard, Cordonnier, demourant à Paris, a ceta Roquette, numéro trente six de la somme de cent trente trois francs de priniipal es intérêts et dépens liquidés ; Lecondo de la significtion à lui faite de ce jugemeit ; ot par exploit du ministère du dit uipier en date du seize Mad, enregistre ; Lé, D’'un autre jugement rendu sar le même Conseil le vint un Moit mil huit cent soixante dix huit, enregestre, d'éboutant le toir ltard de l’apposstion par ler formé du dit jugement du vingt un fevreer derier le condamner aux décens. CQ de la significtion à lui faite par exploit du tonne hupier en date du vingt cinq avril mil huit cent soixante dix tuit enregistré L? Du homme tant qui lui a été signifié par exploiy du même Loupier, en date du vingt sept avril dernier, enregistre. u ufin de la tontetire de saisse qui a été fate par exploit du ceit puipier endate du vingt quatre Mai courent, enregistrée eeuel lemême exploit donne siguation à la dite dame veuve Moui à comparaître le six Juin mil huit cent soixante dix huit jaut cinq de retevéé à l’adance et par devant Mappouns les meutres cn francs le compet de Prud’homme par l’industrie des tiss puis q u e six neis il les quel sai lene hil ecu t qu'il est creancier de la somme de cent trente trois francs pour prix de travoil et que le conseil par jugement du vingt un fevrer dernier condamne Celtard qu l’en couveait comne patron de l’étotlépement et qui ne serait, que ferant ; Attendu que ce dernier à jeune appention à e jugement et que le conseil l'a déboute de son appention et condame aux dépens par entre jugement de vingt un pars , que par suite de ce socent jugement fit gulement signifié, comme demant lui fit doané et que lorsqu'en voulit prétijuer la saisse des dpet matiliers et merchardon et qurnmpant la Bouttique le sieur ltard se trouvaut installé dans la dite Boulique s'y oppose étendant que les moilles et marchandiser étaeut, la proprité de Madame veuve Denis plntée comme cordonnnère à façon avence de la aquette, nmr trente six; qu'elle qétait imposée et que le loyer était également au nom de la dle dane ; Attendu que la dite dame Denis estpurente du sieur ellard, d'elle esnt en service et n’hibité en acue meme le fond de commet ce de chapurer exploité par le sieur, ellardes que d’ailleurs l’étabtifs enont dans répoudre du selaire des ouvriers etgltant proprrétaire de l'établifement elle duits être trnue au priemer de son la laire dant le dit établissement à profité ; Par ces motifs e taut entrer à de duvré en temps et lieu ententre la dite veuve Donis, déclarer commun à e avec ette es jugement rendut par le conseil de Prud’homme de la Seine pour l’industréie des tissus les vingt en e e e e e e a les leenie la demet e poer de e se fouir e pau t peur cites n e preésir te de el de pome de quen e e e den pe u deu un deu is cete d ape nant e empre ecin eur de dte sen en an que te ere pu de n e u le qu eit te te e eu e u en tense e de p en e nt eue en eu e e e u e prer pu demen de es qur de t ete l praunt pur au e ait e n n entene rprete dea enan de fer ede t e pement e de p den e e de pet en e e e e e e et mrte enre t e pageur en prenier le part ; Donne défaut contre la veuve Douis non comparant à personne pour elle quoique dument appelé. L adjugeant le profit du dit défaut ; Dit commu à la veuve Devis les jugements ren dus par le conseil les vingt un ferrier et ving un Mars mil huit cent soixante dix huit contre ellard, En conséquence ordonne l’exécution de sa jugement aussi bien contre la veuve Denis que contre lauder ensemble, prinmipet intérêts et frais Condamne la veuve Douée aux dépens taxés et liquidés envers le demandeur à la somme d sept francs soixante dix centimes et à celle de quatre francs cinquante cinq centimes envers le Trésor Public pour le papier timbré et l’enregistrement de la critation, conformément à la loi du sept aout mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites. Et vu les articles 435 du code de procédure ciile 27 42 du décret du onze juin 18099 pour signifier à ca défenderesse le présent jugement, commet Chanpion, l'un de ses huipsers audienciers. Ainsi jugé les jour mois et an que dessus. Durois l Lecusq secrétaire Quit jour six quiz Entre Monfieur andre Gugat, ouvrier passementier demeurant à Paris, rue Délettre, numéro douze et quclire Demandeur ; Comparant ; D’une part ; eE Monsieur Caquelt, fabricant papementier, demeurant et domicilié à Paris, bue des Cascusés, nunééro cinquante trois ; Défendeur ; Comprut D'autre part ; Point-de fait — Par lettre du secrétaire qu londe de Prud’hommes du Département de la Seine pour l’industrie des tissus en date du Mardi vingt un Maii mil huit ce parant dix huit crayot fit citer la hur comparaître par devant le jré Conseil de Prud’hommes séant en Bureau Particulier le vendei vingt uatre Mai mil huit cent soixante dix huit pour setisien, nse faure se pavit sur la demendes lel rntieles pouer cesie deans le dit Conseil en faiement de la somme de quatre vin ffnc ue pour indemnité de perte de temps et de renvoi n tompé A l'appel de la cause puret se présenta ea rxpusé le conil is e h lille e nei pet n de son coté taner se présenta et exposa u Conseil que quont il fit attendre Ganot il l’indemnise; que sil l'a conseré tout E à coup sius l’il lui fit un travail necuptette. Le Bnteun Particulier renvoya la cause devant un mimbre du Conseil à ce connaissant Devant ce membre les partis ne purent être conciliées à la cause en cet dot fut renvoyée devant le Bureau Général du Conseil séant le jeudi six juin mil huit cent soixante dix huit. Sénnt, ce memle de coseil Cité pour le dit pour six juin par lettre du secrétaire du conseil en date du vingt sept Mai mil huit cent soixante dex huit à la renuute de huint la quet comparut. A l’appel de la cause ngit le pémmta le et conclut à ce qu’il plut au Bureau Général de Conseil attendu qu'en lui fait ant attendre des matières laquet lui a causé un préjudice de perte de temps dont ià lui doit épusition qu'un le curetient instantemment, nonmbstant la prreusicque lui avoir fite Pahuet père de lai donner un curgement une fuit celui entrois terminé il lui cutse en odre préjéudice pour le uil réparition lui etéralement due Per ces motifis condamner laguet à lài payer avec intérêts suaint la loi la por de qute eit de ponr itle e n qe elesese t landeme aux des e rerle hui t pae aunt e de en ende e enit e e ouerent ce ant e uit doen lens te depr du le u demrantene sepen te u entente pauis et de ure prs et ie a epre pu t unt en enterdete, o ete du peraente t e den lu tee t ue te p e es; ciqui der te il qu tene que nte de en put aper du onte deu puremet mer e e e t qu eu de e de e adums en apre eneie e tur due ei qu de sir de e r de e er adean rand eux er n ei ent uen et es e en en entie ante e n somie aem er e e t la P pl dir sept reuvai etdous mols rayes nulsrepprou Lecucs pe à ayant fut in menvas travail n'y avaix aucun dait ; Qu'ed reitaud même que Caquet, père lui ont promis un chargement ; c qu'il était toujours lacuttotif onu fils de repuser puir qu’il est cesand patron ayant antorité pour diriger son traval, l’expérience fot de son ineapoité sofras à elle seute pour en pastifes T’neote Par ces motifs - Le Bureau Général jugeant, en dernier réport ; Dit Gayot non recevable en ses demandes , l'en déboute, ee la condamne aux dépens envers le Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept aout, mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites. Ainsi jugé les jour, mois et an que dessus. ere ue D l Lecusq secrétaire
   </div>
  </div>
  <div n="12" type="case">
   <opener>
    Du dit jour dix Pinga
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur Pant laguet, ouvrier passementier, demeurant à Paris, rue Grat prolougé, numéro deux ; Demandeur Comparant ; D'une part ;
     </span>
     <span type="part2">
      L Madame Marguerite Cetime Caurant; épouse du sieur Louis Bertail, comme commeradement sous le nom de dame Bouguut, fabricante de paysementite demeurant et domiciliée à Paris, Boulevart de sepastapes, numéro quatre vnt dix huit ; Défenderesse ; Comparant ; D’autre part ;
     </span>
     Ponit, de fait - Suivant xploit de champiin, suipcès à Pariseuns date du premier juin mil huit cent soixante dix huit, visé pour timbe enregistré à Paris, le trois juin mil huit cent sixute dix huit, cen dies ex paur pareite cinge tentioes, leigner duafeite sapit à mocilie et toiféé sapré à la dame Mésheurat, enne Rortel preme d’une jugement rendu par le conseil de Prud'homme du Département de la Seine pour l’industrie des tissus du vingtenc avril dernier, enregistré, portait condamnition à son profit cantre la dame veuve ongeautt, fabricante de lassemens ent Boulevart sétastept, numéro quatre vingt dix huit, le coti francs de princifut pour salaire ed indemnité, plus ces intérêts den dépens. setondu de la signficstion de ce jugement par explioito dit chaznpion, luipsierà en date du nzt duvne dernier uagés tertie du commen dement de payer pais par etre enprisudice du ize dux de ent s tereit, duten de eu de larsse fuite à la maison Bougerants le vingt sept Mai dernier et dans laquelle Madame Bortal aà fat conmme qu’il n'y avair pas de Madame Bougeault et qu'elle édtas la propicétaire de la fabrique de passementerie ; Et par se même exploit à cité le dame Bertail sus nomme et le pour son mori pour la volidilé à comparaître par devant Mépieurs les Président y Dembres comparut le Bureau Général du Conseil de Prud’hommes lu l’industrie des tissus séant en Bureau Général le jeudi six juin mil huit cent soixante dix huit pour attendu qu'il travaille pour le compte des la maisou de fabrique dont elle est profiéitaire ; Que ne pouvant abtenir paiemint des salairer à lui dos il a du l'appelés e devant le conseil de Prud’hommes ; que ne conné saut par le noin qu'elle dit avoir de dame Mertail, il l’a fait citer au nom de dune Bougeault, nom sous lequel elle en génralement et commméuelement compme depuis lind temps tant avant que depuis le décis de Monsieur Bougeaut ; Attendu que sonte nomil a obétenu contre la dite dame run jugement la condamner à lui payer. las ux pour de ti eent est p e fous jugeant t l e du p prdu. aor d en des p pu de i u sier paiement de se part ; Que seulevant lorser en a voula xcuter pravant prer la de de nt pra ten e le ces de dure po lit cent er lur e ome e ie su e e te ten eui eun es puir et pur e en tonlaet tere de en eset du lis ae te e sasemet dan e e e t le sour dun preut e desthuit juemeint itre l due contit sre leee eense an eaent e s e t d reier de de sende demeant pour ut den e demante ce meies ede e e oent n gen e e e ee ei t eu eu e sur aer e e e on puret au e e u te puer ente e ei qu pene rdet u se e deier pesepor ce et de ete qu se e ete u t enc li utie a dermant poen ent que M q o Drdr t somme au dit Caquet ui lui a bien son mis des ichantillens dans le lut d’avoir des commander, mois auquel elle n’avant commandé ; Par ces motifs - Dire laquet non recevable ses demandes, l'en débouter comme mal fondé en rcelle ste condamner aux dépens. tont de troit — Doit on dire applecitte à la dame Gertail le jugement rendu le vingt cinq avril dernier, enregistré, contre la veuve Boureau Gérnale conséquence condamner la dame Marguerite celine Lonvan comme Bertail à payer à Caquet les sommes portées Dat jugement ? Ou bien dait-on dire taquet non recevable en ses demandes, l'en débouter ; Que doit-il être statué à l’égard des frais et dépens ?
    </rs>
   </div>
   <div type="judgement">
    Après avoir entendu les parties en leurs demandes et conclusions respectivement e un avoir délibéré conformément à la loi ; Attendu qu'ile eit constant que la dame Cgeent somme Bortailà accepté de cahuit un ichontitton par lui fait ; lui a donne ces matières, nufrancs pour confectionner des refrences de ce même acpontillon de chargements sur deux metiers aupris de cinquante francs des cent mètres ; que l’exéution de cet engugeant n’a pas été timplié par la same lo jaurent parce qu’elle troiuve plus avantageux de la faire exeiter par un autre ouvrier quine pris que quarante cinq francs des cent mètres, ce qu'il pouvait faire n'ayant eux aucune peure decréation mancun frans d'ichobittonnege ; Qu'et résulte de ce fait indeliés en lui même que caquet à éprouve un préjudice dant la dame lTousen lui doit réparation par la somme de soixante dix francs Attendu que la dame lavarent qui avait en des rapports de travail avoi Cahuet savois, bien que les lettres de conciliation et de citation la visaiens, encore qu'elles fassent adre présse la dame Bougeantt, nom auquet etle répous d’ailleurs comnuilti Qu'n celas elle estpassible de tout les prois qui ont été fots à duision du jugement. Pir ces motifs - Le Bureau Général jugeant en ternier lepart ; Dit applicible à la dame Marguer eline Coureit, somme Bortail, le jugement tendu las le Conseil le vingt cinq avril dernier contre la veuve Bauquuitt ; ordonne l’exécution de ce jugement contre la dite dame Bertaldeu Marguerite le ué Ceurent sisqu’à concusrenre de sois otes froncs de principal; condamne la dame Bertailt éellergt celine Ceurent aix frans et dépens de la première instant a plutat de l'oidlance desant la veuve Boureautt, commé à 'euf, de la présente instn tas et liquidés en vers le demanders e la somme de Cinq francs quatte hunigt quinze centims uit E celle de deux dix frans soixante quinze centimes envers le Trésor Public pour le papier timbré et l'enregistrement de la citation ; Conformément à la loi du sept aout mil huit cent cinquante, ence, non compris le cout du présent jugement, la sinification d’icelui et ses suites . Aini jugé es jour mois et an que dessus. Lecucq secrétaire dmeau
   </div>
  </div>
  <div n="13" type="case">
   <opener>
    Du dit jour dix Jing domant e
   </opener>
   <div type="identificationParties">
    <rs>
     Entre Mademoiselle Désirée Sanson, se disont ouvrière contrière demeurant à Paris, rue Fesnnier, numéro dix Demanderesle ; Comparant ; D’une dort ; t Monsieur et Madame Magnen, le sieur Magnen tant en son nom personnel que pur epaiter et autoriser la dame son épouse, Mantrelte contirre, demeurant et domiciliés, entembrle, le dits épout à Paris, avenie des taris, nméré qatre suit cinq Défendeurs ; Comparant ; D’autre part ; Poit de fit Par lettre du secrétaire du Conseil de Prud’homme du Dépastement de la Seine pour l'industrie des tissus en dates des Mercredi vingt neuf a vendredi trente Mai mil huit cent huit li e t le te de se le e u dapers prde ts tel a sid en ue u Anseu té te eontritat d u umes e les epord s le er en p p t ente unde ete dais poir tetre qunt les e e pre s la o e e en per due mne u deant ens un epe e pe le deu le deur du ti ens ene n en pement dei e e der e paur edu due au de juemne ede t e e e e es lute ede e e que der eun, duedoint e e e e e e en e se e eit e aen e den e eser pr e enqu les Bourrnie de se sonfais ; que si elle lui à enteigne grctutenant le protique de la cuture s'eit qu'elle la veutoir endre expabla de complir l'umploi d’une smme de ciembre au lien de cettedeu borne d’enfans qu'elle avait occupée jusque la ; Que s'elle sa nourrie c’esrt qu'elle prit, l'engagement de lui payer sa nouriter par ena somme de se francs int ciq cntiomes pas jounr qu'en sai elle est sa débitrie et hou se trénière poureni et e ecinq frcès de l’appele devant Monsieur lejage de Pair la su arcondipement qui les ontorisé à détenir sa malle jusque jour ou elle de serapoiquittée envers eur ; Par ces motifs — Le déclarer en conpétut pour comaitre de la demande la demoiselle Sandon, l'en débouter et la condamner aux dépens. Deton coté la demoiselle San son se présenta et conclut à ce qu’il plaut au Bureau Général du conseil attendu que la dame Jourson l’a occuhze pendont cinq mois à des travaux de contire après quoi elle lui delivra un certfiret dant lequel elle est qualitée d’ouvrière conturière ; Par ces motifs — Dire les époux Mégaenu non recevables enteur demante ; les en débouter. Le déclarer compétent, retenir la cause et oedonner qu’il sera explique sur le fand et condamner les époux Magnon aux dépens. Point de troit - Doit-on se déclarer on complent pour comastre de la demande de la demoiselle sanson? Ou bien doit-on de déclarer compétent, retenir la cause et ordonner que les parties seront tenue de s'expliquer sur le fundi pour être statué ce qu'il appartiendeu?? Que doit-il être statué à l'égard es dépens ? ?
    </rs>
   </div>
   <div type="judgement">
    Après avoir entendu les parties en leurs demantrs et conclussons sur le décliniture posé par les époux Mauven et en avoir délibéré conformément à la loi ; Attendu queles eeplications foaries par les parties se part que la dame Moaye n'oit pas maitrepa conturière ; que la demoiselle Sansalle nuné n'est pas ouvrière conterrière ; Que se les époux Magaen s'ont reçue chez euy et ti la dame Magnon lui a enjugné des étements de conture la a danne un certificaton caprits a por deurt l lag loes le pe de u ait e p e somme de chembre qu'elle occupée en ce monent ; Qu'en recosté Conseil n’a pas à conmetre des faits cét de leurs sap parts ; u det ces motifs - Le Bureau Général jugeant en tremier ressort, l déclare n compétent pour commêtre de la demansle le lademoie saeison et la ouvrie devant que de troit ; Condamne le demable Sans on ux dépous envers le Trésor Public, pour le papier timbré prente ille, conformément à la loi du sgt ui muil luiét in ene non comprès le coiut, du présent jugemen q ui ue d’icelui et serpuiltés. Ainsi jugé les jour, mois et an que depas. ecusq serétaire Vergut du dit jour dix vi ntre Monsieur et Madame Potits, le sieur Petit huit en son nom personnel que pour eprriter etutoriser la dame son épouse ouvrière Conturière demeurant ensemble, les dits époux, à Paris, rue du Potian, numéro vingt treit Demandeurs ; Comparant ; D'une part ; Et Monsieur pus Foirser aptécien demeurantà Pains, rue Tris, numéro suite seize ; Défendeur ; Défaillas ; D'autre sart , Pont de fait à Suivent employ de champren lu pier à laris, en date du Vingt huit Mai mil huit let soixante dix huit, visé pour timbre et enregistré à Paris, le vingt nup Mai mil huit cent soixante dix huit déber trois frencs trente cinq centimes, signé saprit la per e pes e e silui doite oen de en te seur du Département de la Sene pour l’industrie du tissus le vingt cinq avril mil huit cent soixante dix huit, enregistré porteu tindemnitions ate de per titene eui en den de setie conturière, de la somme de quarante six francs de pnpl te gu eude ni aug eu t poux jugente e pu eom a e e e our enper e de en l prése e du ei es suite o an tin de n e apres ut en axé dt pe se asseren e eente qul domies dner pus u ue ctise hu etes un de e t nonet ue le sete e e n e te en de ent e d e t e et deratur ent ei en es eps ete soment de ur au de e pre Sar reuvri et quate mits aages nmoils apprenti quinze centimes plus à quatre franc d'indeunité pour temps perdu; attendu que la signification du jugement et le comma dement oit ile faits conformément à la loi ee que la centetire de saisse faute le vingt un Mai dernner Monsieur chailler à cit s’opposer à la same rétendant que tous les otéêts garnépant les dits tient lui ont été lu par Monpeur pales Fénier, aptrien à Pains rue de six, numéro sege, quoique sur la prte de l’appertement de trouve eeployi portant le non de sonnmeller ; attendu que le siour Jules qovrent n't qu’un prété nom et que même s'il était propriétaire de l’étabtifs éneux de conture enistant actuellement, passage la fersien numéro douze il doit être tru au paieme du salaire du pe ouvrières employé dans l’établifsement. Par ces motifs et tousunti à déclaire nltérieuremet, poir déclarer commun avei lui, sieuf juler Foirier le jugement rendu par le conseil le vingt cinq ane dernier, enregistré. L'entendre en conséquece condamner à leur payer la somme de quarante six francs quatre vingt quinze centimes ncoutret on la pitet des condamnations prinoncées, et s'entendre en outre cendmn entans les dépens et frois du jugement de premier instance, et ux francs d’exécution qui l'ont seive et, en outre, Fontandre condamner entans les frois du jugement commen . Al’appel de la canse jule De leur coté les époux Pobit se présentèrent Férier ne comporut pas. et conclurent à ce qu’il plut au Bureau Général du Conseil donner défaut, contre Jiter Vocrier non comparant ni personne pour lui quoique dument appelé et pour le profit leut adjuger le bénmipier d Cconclusions par eux prises dans la citation euploit de chaupsaux huipier, enregistré. Paint de tait — Pait-on donner défaut contre Jules Fvrier non comparant ni personne pour lui quoique due appelé et pour le profit adjuger aux demandeurs les conclusions par ux es précédemment prises ? Que doit-il être statué à l'égard des dépens ? près avoir entendu les époux Potit en leurs demadés et conclusions en avoir délibéré counformément à la loi ; Attendu que les demandersse époux Pabit proipanrt justis et fondées ; Que d'ailleurs elles nevont pas contestées par pales Février non comparant ni personne pour lui quoique dument appelé ; Par ce motifs L Bureau Général jugeant e en premier le part ; Dit commun à pules Février se jugement rendefat le Conseil le vingt cinq uvreil dernie, enregistré, contre les époux ar ou chnelles ; in conséquence ordonne l’execution de le jugement aiquants a pis lour contre te le lo vrer eu ete ur ou lckieller fait pour le principal des condamnetions pu prir e intérêts et frais de pour suites faites à l’arcusion de ce jugement; condomne polur Favier aux dépsouns de la préente eat ie oaie nlégu de anver les demandeures à la somme de cinq franq uai qi cinq centimes et à celle de trois frans trente cinq centimes envers le Trésor Public, pour le papier timbré et l'enregistrement de la citation, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites. Ainsi jugé les jour, mois et an que desus. ecusq secrétaire à Emier D t
   </div>
  </div>
  <div n="1" type="courtHearing">
   Audience Du leudi treize Jng mil huit cent soixante dine huit Siégeuant : Messeurs Bagor, Prud houre Présidant lotoutience, en remplaiement de Mecheure Marienvel et snaud Présitent et Vère président du conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus empechis, Deroyn, Menier Berthemer et Cusse, Prud'hommes epistée de Monsieur e Decucq, brétare du dit Consie Etre épens Poirit, viner papmentier demeurant à Paris, Toulevart de la pgare, mro cent cinquante àn dee ; Demadeur ; Comparant ; D’une part ; Monsieur Dutr fabricant pasementier, demeurant et domclié à det, rue trtize, oummér prt ; Ptatie ples ’eseps ; E if onte e deuit se larde dt lu émin e e e ou Seine pour l’industrie des tissus en dates des Jeudi six et e de pue l e poe oeur cute da pat pus et le cicis demen enten de e e e e e e e te de eperte d it du ten e ui quet e sen t ts pur d t endeate nteme t pou d enes pus qutle ue eu e e e e er e e e e enr ape e desen ueti quige otr cit du cint qu7il du dite relere dumeiet ur de en te iquen e e e p t e se e pe n en anp les ene entisqnti a d e m t en ei se dman a prs eut due ou de pe ur ue des ai su euse é i o r os ? A Li quei Point de droit — Doit-on donner défaut contre Doit on comparant ni personne pour lui quoidue dument appelée ;t pour le profit adouger au enteindeur les conclusions puar lai précédemment prises ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu Régat en ses demendre et conclusions et en avoir délibéré conformément à la loi Attendu que la demande de igar parait juste et fonéée Qque d’ailleurs ele nen pas contestée par Dety n on comparante ni personne pour lui quoique dument appelé ; Attendu qu’il ne comparuépanut pas devant les Bureaux du Conseil Diite à causé à Parat un préjudice de perte de temps, cequ'il doit répurer . Par es motifs - le Bureau Général jugte en dernier repart ; Pu l'article quarante un du decret di conze juin mil huit cent neuf, portant renseuvent por leur conseil de Prud’hommes ; Donne défaut, contre Dieta non comparant ni personne pour lui quoique dument appelé ;Et adjugeant le profit du dit défaut ; condamne Deté à payer avec intérêts suivant la loi à Slgat, la somme de quatante deux francs qu'il lui doit pour salaire, plus une indemnité de six francs pour temps perdu ; Le condamne en outre eux dépns taxés et liquidés envers le demandeur à la somede un franc, et à celle due au Trésor Public, pour le papier timbré de la présente minute , conformément à la loi du sept août mil huit cent cinquante, ence, non compris le cout, du présente jugement la signification d’icelui t ses suites à et vu les entorles 435 du code de procédure civile, 27 et 42 du secret du onze juin 18099 pour sinoifier au défendeur le présent jugement Conmle Cl umpron jun de ses huissies audienciers. Ainsi jugé les jour mois et an que dessus. gagoffiet Lecucq secrétaire Qusi jour cingt siuiq Entre Monsieur Denis Mocke, demeurant à Paris,ue Brzfo numéro vingt deux agistant en nom et comme a demandete nt de la personet des tiens de sa fille mineure Couist, ouvrere cantiriée ; Demandeur ; Comparant ; D'une part ; Mademoiselle Celine Jainq Liéclain, l’atupe contire e demeurant et domiciliée à Paris, rue de la champée ? Pontin ; numéro quinze ; Défenderesse ; Défaillant ; D’autre part ; Pains de fait - Par lettres du secrétaire du Conseil de Prud’hommes du Sépartement de la Seine pour l’industrie des tissus en dates des Mardi quatre et vendredi sept juin mil huit cent soixante dix cent pour se concilier si faire se pouvait sur la demande qu’il entendait former contrerlle devant le dit conseil en paiement de la somme de dix francs trente cinq centimes qu'elle rui doit pour prix de travaux de sa fille louise. La demoiselle Lievain n’ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi treinze jun mil huit cent soixante dix huit Cités pour le dit jour snze juin par lettre du secrétaire du Conseil en date du onze Juin mil huit cent soixante six heaig à la requete de ock la demoiselle Gerain ne comparut pas. Le son coté ole se présenta et conclut, à ce qu’il plut au Bureau Général du Conseil donner défaut contre la demoiselle Térainn non comparante ni personne pour elle quoique dument appelée et pour le profit la condamner à lui payer avec intérêtts sivant la loi aa somme de dix francs trente luiq centimes qu'elle lui daoit pour solde du frix de travau de sa fille vnisi pas, cette indemnité qu'il plaira au Conseil fixer par perté de temps devant la peu e e se rnt eune Negcles Cntre puen eaps tete la deme le tr oin en o prs puesen pur le pi de t n pifi ne ndes au demeide lrte er purse premnen pel pu de de pur uer du te e e e e e enier rete e prde e e t emre le e resq t e des e desipu e u e deme peur qpur em e te e de sen t ps p ur per e ou es a u te resnie ande de e pr ent qut deux ceste den n tes surt nde e nde de e qurate u dte d u se de e i q e e desoe age n e e endem e es pour pur u e e rite purir de lite de e deon le apur e i cncis i e cé C cnt A puste et fondée ; Que dailleur elle n'est pas contestée par la demoiel crevain non comparante ni personne pour elle quoique dament appelé ; Attendu qu’en ne comparusant par devait le Bourer edu Conseil la demoiselle Lrévain a causé à Porte se préjudice de perte de temps, ce qu’il doit réparer ; Par motifs - Le Bureau Général jugeant en dernier repsort ; Dounne défaut contre la demoiselle Léévein on comparante ni personne pour elle quoique dument appelé ; Et adjugeat le profit du dit défaut ; condamne la demoiselle Lrévain payer avec intérêts suivant la loi à Maccy la somme de sex sans trente cinq centimes qu’il lui doit pour prix de travaux des sa fille couisne, plus une indemnité de quatre froncs pour temuse perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de, ue fronc et à celle dui Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept aot mil huit cent cinquante ence non compriu le cout du présent jugement, la signification d’icelui et ses suites. tt vu les articles 435 du code de procder civile, 27 et 42 du decres du onze Juin 18099 pour signifier a la défenderese le présent jugement, commet champion, l'un ler ses huifsiers audienciers. Ainsi jugé les jour mois et an que dessus. Mrasgoffte Lecucq recrétaire e it ui du dit jour Treize Puizan Entre Madame veuve Didier, demeurant à Paris, rue Louge numéro trente lix agissant en nom et comme satrèce n lutelle et légale de sa fille mineure ingele, ouvrière fluriste Demanderetse ; Comparant ; D'une part ; Madime veuve Minée Ségrot, fabricante de fleurs artificilles demeurant et domiciliée à Paris, Boulevart Montmrtr numéro quatorze ; Défenderse ; Défaillant ; D’andesr part ; cnq de fait ) Par lettres du secrétaire du conteil 2u Prud’hommes du Département de la Seine pour l’industrie de tésons en dates des undi trois e Mardi quatre juin mil huit nt soixante dix huit, la dame Didier fit citer la veuve Aune Soyrot à comparaître par devant le dit Conseil de Prud’home de séunt en Bureaux Particuliers les mardi quatre et vendredi sept juin mit huit Cent soixante dix huit, pour se concilier si faire se pouvait sux la demande qu’elle entendit, former contre elle devat le dit Conseil en puement de la somme de quarante franc quelle li doit pour prix de Pa jeur Vnnée Segot n'ayant travaux de sa fille angèle ; pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi liize juin mil huit cent soixente dix huit citée pour le dit jour treize juin par lettre du secrétaire du Conseil en date du heit juin mil huit cent soixante dix huit la réquête de la dame Didier la veuve sgné Trgrit ne comporut pas ni personne pour elle régalièremet. A l’appel de la cause la dame Didier se présenta et conclut à ce us plus a Bureau Général du Conseil donner défaut contre la veuve ine Pagrot, non comparante ni personne pour elle quoique dument appelée et pour le profit la consamnner à lui payer avec intérêts suivant la loi la somme de quarante francs qu'elle lui doit pour prix de travaux de sa fille congte, plu endenité qu'il plaira au Conseil fixer pour perite de temps devant le Bareaux du Conseil et les dépens. Pent de droit - Dit on donner défaut contre la veuve Mirnée Peret non comparante ni personne pour elle quoique dument appelée et pour e profit adjuger atele e e ple lu etens qu e pre e pr e Que doit-il être statué à l'égard des dépens ? Après avor entendu la dame Prdier en ses demander et conclusions e en an slé da prdent cle, doeunte ur d te eu dit purt te pat hre ete de de e eit dener po pe de en e e dene ere du jon eux e eur e e e le oue t e dere en e cine eade seun de en uetle eue e e pede ende e te ee u se dus e e e que q ene u de en e e e ent endt seoi e te des que e e conci inte de p d i ie ile den i de den tess pous e aunr e s qucities en e que e le à R i e L BI5 u d de la i u i D t i ? A dt en date du Mardi vigthui, Mai mil huit cent soixante dix huit renvoi approuvé f Cite Lecc lagettre au Trésor Public pour le papier timbré de la présente minute, conformément à la loi du sent aon mil huit cent cinquante once non compris le cout du présent jugement, la signification t vu les articles 435 du code de d’icelui et ses suites. procédure civile, 27 et 42 du décret du onze juin 1809epou signifier à la défederese le présent jugement, commet coompar l’un de ses huissirs audienciers. Ainsi jugé les jour, mois et an que dessus. csoffe Lecucq serétaire r Audit jour treize Puin ntre Monsieur Jeun craderse, ouvrier tailleur d’habit demeurant et domicilié à Paris, rue des lus, numéro vingt six ; Demandeur ; Comparant ; D'une part ; Et Monsieur restivens aitre tailleur dhabits, demeurant et domicilié à Paris, rue Poinq hronoré, numéro cent trente cent Défendeur ; Défaillante ; D'autre part ; Point de fait - Par lettres du secrétaire du Conseil de Prud’homme du Département de le seime par l'industrie des tissus Clavarie fit citer Thristions à comparaître par devant le dit conseil de Prud’homme s u Bureaux Particuliers le endredi trente un Moai mil huit cent joiante dix huit pour se concilier si faire se pouvaits sur la demande qu’il entendait former contre lui devant le dit conseil en paiement de la somme de neuf francs soixante centimes qu’il lui doit pour sitaire. A l’appel de la case Claverie se présenta et en posa comeudessus. Da santoté Thristinent le présenta, reconnt devoir la somme réclamée et ditedée refuserà la payer percé que le demandeur qui le tavait et nevent pas travailler le vingt sept moi, ce qui lui daute cux préjudice dont il lui dux réparation . Pur l’avis de Bureaux Particulier que Claverie ne tait pas tenur à le prévenir puitque dans cetteindustrie les parties se peuvant quitter non emrent, chaque jour mois encore à toute hence du jour Thistian plauis qu’il paierait le même jour au jour. Cette poé r ne fut pas cence et la cause fut renvoyée devant le oudeil qun le du conseil ting le jendi trige suin mil huit cent prse et aors huit. Cité pour le dit jour tenze juin par lettre du secrétaire du conseil en date du onze jun mil huit cent soixante dix huit à la réquate de Clavarie Boristios ne comparut pas. A l’appel de la cause Claverie se présenta et conclut à ce qu'il plut ai Bureau Général du Conseil donner défaut contre christianne non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui payer avec intérêts suisin la loi la somme de neuf francs soixante centimes qu’il lui doit pour prix de travaux de son état, plus, cette indnité qu'il plaira au Conseil fixer pour perte de temps devant les Bureaux du Conseil et les dépens. Point de droit - Doit-on donner défaut contre Cpristios non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demonder les conclusions par lui précédemment prises ? Que doit-il être statué à l'égarddes dépens ? Après avoir entendu Clavèrie en ses demander et conclusions et en avor délibéré conformément à la loi ; Mttendu que la demande de Claverie en juste et pendée ; que d'ailleurs elle n'it plus contestée par Thiestions non comparant en personne pour lui quoique dument appelé ; attendu que christions à fait perdre du temps à Claverce devant les Bureaux du Conseil, ce qui lui causà un préjudice dont il le dtoit réparation ; Par ces motifs – u Bureau Général jugeant en dernier ressert ; Donne défut contre Threstinent non comparant ni personne pour lui quoique dument appelé pudivant srapadte ie it conte e s se à payer avec intérêts suivant la loi à Aluveice la somme de du our per te ur i peame et oue pe u feimen e e te e e lu e e te ene le puir pitite de e de le u oneil de te u eu e e pour ete por di enie e ep paire u en e eue de e u de s ds pomn e e re eur rei eus eu euppres s dedit en gu denmenenr es lngifle Leng puilite De uns Audi jour treize juin Entre lesioer Moolphe Veedorn, ourier cappren demeurant à Paris, rue Saint Vinolas, numéro vingt ; Demandeur ; comparant ; D'une part ; Et Monsieur Besanvalleti Matre la papser demeurant et domicilié à Paris, rue Car lassal, numéro dir sept ; Défendeur ; Comparant ; audre part ; Pint de fit - Par lettres du secrétaire du Conseil de Prud’homme du Département de la Seme pour l’industrie du tissus en dates des Vendi six et vendredi sept juin mil huit cent soixante dix huit Tinesche fit citer Besenvelle à comparautre par devant le dit conseil de Prud’homme séant en Bureaux Particuliers les vendredi sept et Mardi onze juin mil huit cent soixante dix lui pour se concilier si faire se pouvait sur la demande qu’il entendat ferme contre lui devant le dit conseil en paiement, de la somme de vingt trei francs qu’il lui doit pour sétaire. Besonvelle n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi tronze juin mil huit cent soixante dix huit. Cité pour le dit jour tenze juin par lettre du secrétaire du Conseil en date du huit in mil huit cent soixante dix huit à la réguate de mésche Besavalle comparut . A l’appel de la cause oesch se présenta et conclut à ce qu’il plut au Bureau Général du Conseil condamner Besonvelle à lui payer ave intérêts suivant la loi la somme de vingt trois francs quil lui doit pour salaire et ce condamner aux dépens. Let ontile Tue Beranvelle se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu que mesch sest présente chez lui d’offrant de travailler comme petit ouvrier au pris de cinq francs par jeur ; Que fin du premier jour ayant recoune qu’il ne savait riern du métier il le lui dix ; qu'il dornt lalors et attait de rester pour apprendre ; attendu que donte quatre deurs et demi qu’il est reste vrues chne lui a été d'auvie atilité il ne lui doit ion ; Par ces motifs — Dire vuce se o recevable en sademande, l'en débouter comme nal fonté et uelle et le condamner aux dépens. Paint, de droit — Doiton condamner Besanvelle à payer à Vmesch la somme de vingt trois francs faur quatr auns le atin de travail ? Ou bien deuq en diue Muron nonrecevable en sa demande, l'en débouter ? Que doit-il tretide à l'égard des dépens ? Après avoir entendu les parties en leus demis s et conclusions respectivement et en avoir délibéré conformément la loi ; attendu qu'il et constent que Mausel a erei e chez Besonvelte pendunt quatre jours et deme ; qu'il etre trestais que la de pasier sur Besenelle li qi qu’il ne pouvait préteuve au prix de cinq francs qu’il avaice anmancé vouloir gagnet ; mois il ne pent s’ensuivre de cette déclaratien qu’il l’apoccupe pour rien ; qu'entenait compte de la faiblesse de l’ouvrier le Consil en d’avis qu’'il lui huit, paye deux francs pour sable ; Par ces motifs - Le Bureau Général jugeant en dernier restort ; consanné Besanvalle à payer à Arésch la somme de douze francs et déboute Nresche du surplus de sa demande, condmne Besonvalle aux dépens toxés et liquidés envers le demandeur à la somme de un franc, et à celle due au Trésor lablic, pour le papier timbré de la présente minute, conformément à la loi du sept aout mil huit cent cinquante, ence, non compris le cout du présent juement, la signification d’icelui et ses suites. Ainsi jugé les jour mois et an que dessus Magalli Lecucq secrétaire usi jour crige liuigz e Entre Monsieur Auguste Flessis) ouvrier cordonner, demoirant à paris, rue Camada, numéro six ; Demandeur ; Comparant. mpat ; de te sour lat tort e te se demeurant et domicilié à Paris, rue du faubourg sant Denis, numéro cent quatre hnet dix ; Défendeur ; Comparant par la sieur Charles, son contre maitre, demeurant chez lu aux dur aupere te e p sensen e gue qurele pent e en der due deux pu i l époed det pr e ene e laqerant peredin e p semis ver qua e dit ede de ur e aun e t ue e ete mr de emen u e ente t de de poer de i e se e e e e a i e t ut ue a e e ceties eun esen ser an den prs ement la elles et se eoer t ls la cis D MM it par lui entenslieu : Boré n’ayant pas comparu la cause fut envorée devant le Bureau Général du Conseil ésant le jeudi lui juin mil huit cent friquite die huit. Cité pour le dit jours seuil mots ragis nuil. hraze jun par tettre du sorétaure du conseil ui late des onze juin mil fuit cent soente dix huit à la réquite de Fessis Dauré compret. A l'appel de la cause Pessis de présme Me et concut à ce qu'il plut au Bureau Général du Conseil lev Recu in Paire à lui payer avec intérêts suivant la loi la somme de quarant pours pour li tinie de la semaine de cougé qu'it re Gagelle lui a pas faipé faire comme d’usage et le condamner ex desote Dit son coté Pairé, par Charbes, son mandétaire, se travai et conclut à ce qu'il plut au Bureau Général du Conseil attendu qu'un règlement apposé dans ses atatien prévant les ouvriers qu'ils sne sont pas tenus de donner semaine de congé ni admis à le réclamer à l'haure de leur renvoir Que ce réclement, lorme entre lui et les ouvriers qui l’ait auqs un travaillant eue lui un contret verbal qui detouit l'ata du délai congé. Par ces motifs - Dire Flessis non recevable en sé demande, l'en débouter comme mal fonné celle et le condamner aux dépens. Pint de droit Doit-on condamner Pou à payer à Wlessis la somme de quarante francs pour lui tnir comtte lu de la semaine de congé qul de lui a pas toifré faire ? Ou bien dait-on dire lessis non recevable euse demande, l'en débouter à Que doit-il être statué à l’égard des dépens ? Après avoir entendu les parer en leurs demandes et conclusions respectivement et en avir délitéré conformément à la loi ; Attendu quil est constuatre qu'un réglement d’ételier appectié d'une mineure estensle dan Les ateliers de Paute rent les auvriers fibres de la etes filemant quoir bounr leursemble et résiprogquement le Patron, de les remercier à taite huire ; Que Flesst go déclare avoir en conneilsance, de ce recteames est mal fuit à demander semere de jongé que le dit réglement avait premes out de supfrmer ; Por ces motifs - Le Bureu Général jugeant en dernier repert ; Dit Ptessis non recevable on sa demande, l'on déboute et le condamne aux dépens euversr le Trésor Public, pour le papier timbré de laprése minute, cnformément à la loi du sept aout mil huit ent conquate, ene, non compre le cout le présente resn, cinifation d’ielui et sas sites. ies l u e et et anque dessus. Vagallie Leuicg serétaire E
  </div>
  <div n="14" type="case">
   <opener>
    Du dit jour Treize suin
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur Bourlier Moitre, selfer, demeurant et domicilié à Paris, rue Harg, numéro soixente quatorze ; Demandeur ; Comparant ; D’une part ;
     </span>
     <span type="part2">
      Et Mlonsieur Bupetit, demeurant à Monierf seize et Martre, agissat au nom et comme admins trotur de la ersonne et des biens de son fil nneut constant apprenti Défendeur ; Comparant ; D’autre part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait – Pur lettres du secrétaire du conseil de Prud’homme du Département de la Senme pour l’industrie des tissus en dates des Samedi premier et lundi trois juin mil huit cent soixante dix huit. onr lier fit citer comprtits à comparaître pas devant le dit conseil de Prud’hommes séant en Bureaux Particuliers les Lundi trois et vendredi sept juin mil huit Cent soixante dix huit pour se concilier si jaire se pouvait sur la demande qu’il entendait former contre lui devant le dit Conseil En éxécution de Conventions verbales d’apprentissage au paiement de la somme de deut cent francs à titre de deommage intérêts. Snpelit n'ayant pas comparu la cout fit rentayée devant le Bureau Général du Conseil séant le jour treize jun mil huit cent soixante dix huit. Cité pour le dit jour tinze Juin par lettre du secrétairée du Conseil en date du huit Juin mil huit cent soixante dix tit à la requite de Bouslier upept comparut . A l’appel de la cause Bourlier se présenta et conclut à caqt il plut au Bureau Général du Conseil dire et ordonner que dans le jour du jugement à intervenir epabit sera tru de faire rentrer chez lui son fils constant et de liy fépir pendant une anné qui lui reste à faire sur les trois qui ont été fixées pour la duteeten hui pe t p le u en p purt per du que cet apprentissage pro at demirrelie résolu ence ces condamner cou papit à lui payer avec intérêts suivant la loi la somme de deux Cent francs à titre de dommiges-intérêts et a en en es por le cande ete de pre M et conclut à ce qu’'il plut au Bureau Général du Conseil attendu e de e pe es u es emiene a elie e tete de tent que prse e e p e pet snteur pus ap an seit eme prl u e ene de te rgt u oe de e e e e der e le e e e ou simie ’ept moits ragis uiles le Lruch llée Maq qu Monter à it cit l'ablig peurant hite l’anée restant à faire à payee une somme de trois francs par semane par orter à la nourecter de son apprenti qui resta sun père parté ; mois attendu que Bourlier ne tit pas le nmouvel engagemez en privent, l’unpo des trois francs sur termoils il état en drait de comptes pour pai partie de sa nmurriture il se oit forcée de le reprendre ce quil fit le vingt un avril dernier non par se sonstraire à l’exécuti de t engagement, mois contrant et forcé du six de Bourlier Par ce motifs - Dire Bourlier nonrecevable en ses demandese l'en débouter ; Le recevoir reconventionnellement demandanm dire l'apprentissage de son fils purement etn timplement Vésolu d condamner Bourlier aux dépens. Paint de troit — Doit-on dire qu’il l ne papit sera tenu de faire rentrer son fils chez Bourtier pou cegtiner son apprenpsag par une année se présente, hui et fauté par lui de ce faire dire l'appentissage résolé, en cece condranner capelit à payer au tra Bourlier la somme de deux cents francs à titre de dommages-intérêts ? Ou bien dait in recesa toup . Dire Bourlier non recevabl en sa demone l'en débouter recevoir Papelit reconv entionnelleet demandes dire l’apprentissage doit s'agit purement et simplement résilé Que dott-il être statué à l’égard des dépens ? Ae cnneil très avoir entendu les parties en leurs demandes et conslutio respectivement et en osor délibéré conformément à la loi ttendu qu’il est constant que se mineur Ceapetit à été placé en apprentissage chez Bourlier pour trnq unne avec une rem unération foutature jusse du vingt ferrer dernier, mois décendé à jour ogatore pour une somme de trois francs par semaine ? que cet apprentissage doit recevoir son éxécution bindnet l'année restaut à faire sinon loupept payrà une indemnité que le coutre qui passate la demet moutères d’apprentisae u la somme de sivante Mrnde, cel emil le Bureau Général jugeant en premier ressort ; Dit et ordonée que dans les quatante huit heures à partir de ce jour bapelit sera tenu de faire rentrer sa fils constante dut hout lers pour à melle dnier huitquni pren a pet pal lui de se fice eui l s o ue et cete de pess ; dit que lets apprentissage se ine demeurea résolu lu ce cas, ces anpretis à fréret du etit à payer avecintérêts sur n lui à Burlier la some de cinquante francs lin le domneries intbtle, à cort domne dus ou ln ces ux désous tixés et liquidés en vers le demandeur à la conme de uni frinc fet à celle die u Trésor Public, pour le papier ttimbré de la présente minute, onformement à la loi du sept aout mil huit cent cinquante, ence Fa compris le cout du présent jugement, la significatione d’icclu et ses suites.
   </div>
   <div type="judgement">
    Ainsi jugé les jour mois et an que defaut Eas ptille Lecusq secrétaire
   </div>
  </div>
  <div n="15" type="case">
   <opener>
    Du dit jour trenze Jiin
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monpeur Louis Voubert soupeur d’habités, demeurant à Paris, rue de la bente tir numéro cinquante ; Demandeur ; Comparant ; D'une part ;
     </span>
     Monsieur Purant ; confectionneur d’habits, demeurant et domicilié à Paris, rue croir des Patits chemps, numéro cinquante ; Défendeur ; Comparant ; D'utre part ; Dinq de fait - Par letres du secrétaire du Conseil de Prud'homes du Département de la Seine pour l’industrie des tissus en dates des Mardi quatre et vendredi sept jaunvier mil huit cent soixente dix huit Joulert fpt citer sinant à comparaître par devant le dit conseil de Prud’homme demil au Bureaux Particulier les Vendredi sept et Murdi onze vun mil huit censoixmmmte dix fait sur se concilier si faire se pouvait sur lu demende qu'it entendait former cente lui devant le dit conseil en delai conge domme dusage Denait- n’ayant pas comparu la caute fut renvayée devans le Bureau Général du Conseil séant le jeudi toize juin mil huit cent prbte de li di e le dt es hun sem pr lettre du secrétaire du Conseil en date du onte juin nif huit Cent sixante dix huit à la reate de outant denans aplels; Ail le det dte sudi épaut de eue sut an en et oncsue que det hui e e e e e qui edte pue t e e e fitr de de afur l contilous es re de lui poarseue udits pos l l se e er pl de onos qnr i d it fi t M t de dommages-intérêts et le condamner pans les deur ense dépens. De son coé dement se présenta et conclut à ci qu’il plut au Bureau Général du Conseil attendu qu’il embouche pouler, pour un cop de moins et qu'en sontaires le conseisa à cajournée de quatre francs cinquante centimes de non con mis sonners le prétend ; qu'il la anpe dé poue qu'il étant en capable de bien faire la latijue ; Devait motifs ; Dire Soulert non recevablien su semante l'en déboiter et le condanner ux dépens. Panit, de droit Dait-on dire que Finand sera tenu de recevoir etae lui peutert et de lui continuer pendant le reste du mill de juin du travail de son état de coupeur d’habits enten servant les appointement de cent vingt francs pour le dit mois, sinon et faute par lui de ce faire le condamner à payerau dit Toulert la somme de cent vingt francs à titre de dammaser intrêts ? Ou bien dait-on dire Toulert non recevable ses demandes, l'en débouter ? Que doit-il être statué à l’égard des dépens ?
    </rs>
   </div>
   <div type="judgement">
    Après avoir entendu les parties en lurs demandes et conclusions lespertivement et en avoir délibéré conformément à la loi ; Attendu que les ertiusion fournier par les parties et de l’examon des lisres de sinant ressort aue pubert est resté plus de trois chez simand e qualite de counpeur Fhibits ; que quoique payé par à comptes il était au mois ; Qu'en ce cos il devait prévenut et être prévene en mois à l'avonce conformément à l’asa que reçit cette industrie ; Qrue Finand gur l'a causedie le premier juin pour l'avoir préven le dait recavoir chez lui et lui doit donner du travail ponstant tout ce qui deste à faire de mois de juin en lui payait le mois cen entier ; Par ces mots Le Bureau Général jugeant en dernier lepart ; Diit et deuie que sinant reuvea chez le toutert et lui donnére de travail de son état pendant tait le mois de juin présent vuren lui pupara six peurs par semaine pour se prourer du travail, sinon et faute par Dinand desa conforméere à Cetée décixon ; le condamne dès à présent à payer au dit Poulers le somme de cent vingt francs à titre de dommages-intérêts ; L condamne dant tous les catux dépens taxés et liquides euvriée demandeur à la somme de un franc et à cette due au Trésil Public, pour le papier timbré de la présente minute, conformément à la loi du sept aox mil huit cent cinquante, ence non comptrian at di pepren saqyant, las sopntils. d’calu elestile lure les jour mois et an que dessus Cle as Et fancq etrétaie ce
   </div>
  </div>
  <div n="2" type="courtHearing">
   Audience Jeudi vingt Jng mil huit ceut soixante dix huit siégeant . Messieurs Dinaud, Péalies de la légion d'honneur, vice Prérident du Conseil de Prud’homme du Département de la Seine pour l’industrie des tissus, Tiendant l’olndame n remplacement de Monsieur Martenval Président de dit Conseil, supicté, Esssoy, Larcher Brentry, edhriz ; Pud’hommes assistée de Monsieur secrétaire du dit Conseil. au ee2 Entre Madame Veuve Méniar, ourière fauriste demeurant à Paris, rue de Belleiille, numéro cent sut ; Dumanderese ; Comparant ; D’une part ; L Monseur veuve lorne, demeurant et domiciliée à Paris, rue se feubeure Pain, Denis, numéro seize, agistant comme administrities induuante de la maison pe reprocation et de comme de la dame Tepetème se seur ; Défendeur ; Comparant ; D'autre part, Pont de fait – Der lettres du secrétaire du Conseil de Prud’hommes de Département de la Seine pour l'industrie des tissus en dates deus Vendredi trois et uunti six Mai mil huit cent sixunte dix huit la veuve Moron fa citer deuve Détorme à coumprraître par devant le dit conseil de Prud’hommes poun en Bureaux Particuliers les lundi tese Mardi sept Mai mil huit cent cinute aqfis pu t te e s fait puir ulaet cenitie entendait former contre lui devant le dit conseil en paiement de la soe de seize cent francs pour selde de provision à laison le cinq pour aet ur pes et onternspuen luitue n’ayant pas comparu la case fut renvoyée devant le Bureau prt se e it lu ’enti eil lui et li g tente ix lit e liy le e comme e enl enie et exposa au Conseil qu'elle entra leiy novembre mil huit cent pa t six du e e u elle deie u centre tur le su for p u en un u ente le te e end es ur eu er u prsente ei pu duie soant quea lur pons luit de dapres et coutde six depoit ce die teuite e it r is pas intatire emne ei M jeuve Déferne, de soubité, se présenta et expost au Consil qu’il a payé à la veuve Moson chaque semaine le prisd les journées à sept francs, ausil a versées plus iaus sommes on à compte sur sa provision qu'il offrine et de cinq pour cent sur les bénifices et snon sur le prédeunr de rntert ; Qu'on fu de compte elle en la débitrice et ne sa créancière. ll demandu l’ajournement de la canse par prouver son apartion. Le Bureau Général ajournt a caunse et chargea l'un de ses memtres de l'examet de l’affaire taut au hoint devae de la convention qua ce lui des compte Par lettre du secrétaire du Conseil en date du vingt cinqlaie mil huit Cent soixante dix huit la veuve Mégron fit citer vouve Delarne à comparaître par devant le conseil de drudhom séant en Bureau Générall udi six Juin mil huit cent soixante dix hui pour t'entendre condamner à lui payer la somme de sene cent francs qu’il lui doit à raison la cinq pour leur sur le choffe net ses affaires et trentenr retondamner ax dépens.. A l’appel la veuve Menin se présenta exposa sa demande et conclut à ce que veuve Vélorne fut condamne Pare à lui payer avec intérêts suivant la loi la somme de seize cent francs qu’il lu doit jur prousien à cinq ponr cent par les fairs et le condamner ux dépens. De son coté vouve Détorne se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu que la veuve Mernan ent entrée et restée dans la maison cahitaine au pris de sept francs par jour et cinq pour fent sur les bénifices qus non boulement it a payé chaque semaine le produi des journées, mais à donne des à compte se chiffrans travant somme su parienme de brouvaut à celle qu'il erait de payer. Par e motifs – Pire la veuve Megrandon recevable en sa demande, l'en débouter et la condamner aux dépens. Le Bureau Général aourna au vendi tingt Juin mil huit cent soixante dix huit. LVeude vingt pui pasoue eMleseln seprétite et atirl les quel ls ri ue requierant l’adjudicetion. Vouve Desonne ne comparnt prs. Pungele soit ; Doit en en demn e l les aprsà la veuve Mayor la ome de sixe det faus p sitée u ie pair cent ui t d et e t e po e liun dit- il letre la seune Mésir mart e ue sa demente do rilet tu dit d’ te det fouier ou dien . depre euventel e le e e te e ce stesouns epadement ; compertienes re le nen nl d it ue uns e i o ? De unle charge de l’examon de la cause en son rappert verbal et en avoir délibéré conformément à la loi ; Attendu que de l'audition des parties à la barre du Conseil et de l'axamen le membre du Conseil doms à l’effet de voir la comptetible veuve Lébornée que la demande de la veuve Monrer Ee est juste et fondée. Pur ces motifs - Le Bureau Général jugeant en premier repart  ; Condamne vouve Léforne à payer à la veuve Mégran la somme de seize cent francs qu’elle lui réclame comme soble de pronision sur les offaires de la maison capitime du cinq rovenb mil huit cent soixante treize au jour de sa sortie de condamne en outrer aux dépens taxés et liquidés en vers la demanderesse à la somme de deux francs cinq centimes, et à celle due du Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence non compris le cout du présent jugement la signification d’icelui et ses suites. Ainsi jugé les jour, mois et an que dessus. maie Gs decucq seurétaire ps
  </div>
  <div n="3" type="courtHearing">
   Audience du Jeudi vingt Juin mil huit cet soixantre dix huit iégeannt, V’esseure, Marmenval, Thévalier de la férçon d’honnem, Présideit du Conseil de Prud’hommes du Département de la Seine pour l’industrie du tissus, Larcher, iroy, Monnier, Angitour Bonsher et, Brccc, Prud’hommes apsistés de Monsieur Lecucq secrétaire du dit conseit. Cite onjour e ladaune Dinil, le sieur Déneltant en son nom personnel que par apister et autoriser la dame son étouse ouvrière imentende en fleurs artificielles, demeurant ensemble, à Paris, rue Maudon, numéro dix huit ; Demandeurs ; Comparant ; D'une part ; L Monteur Monnser Cordonares, fatricant, en Marchand de fleurs artificielles demeurant et domicilié à Paris, rue Levonne, numéro vingt un ci devant d actuellement en la même Vile rue veuve Saint Eugustin, numéro trente ; Défendeur; cotilaut ; tute part ; Pouit dé fit - u cete du secrétaire du Conseil de Prud’homes du Département de la Seine pour l’industrie des tissus en date du Mardi ure dumeil ut retient de lat ingen eile prir ies doenver eprte pardaunt le duie Conseil de Prud’homes séant en Bureau Particulier le qudet ente taen lut du prante se li purt on de e pur l pri dn la ee ndtedet pour dt deu e e apour e te prete enppe es pour laremeitie atendur satrit. e deseur se quit ene l prtint e p re u ee e un ere n eu de d t seent les es pere e la eugu es lomisie merdeie l te ent au qur du t i l ne e puser eun ere e e pre que e e quilese eu eu e ne qu us par doznnée, puis en reps pesjout ? Que le mardi quite juin Monnier ayant oulitement, toucédié lal dane Denael, alleu s'envu contrainte de lie faire appelé devant le Conseil auxfins de dommagesintérêts . De vo coté Monnier se présenta et exposa au Conseil qu’il est mevait qu'il ait tougédié la dame Denette e n’a rappelée qu’à l’éxécution de soncatitrnt on la ntine l’erdre de se trouver au mage sin le malie à neuf heureit au bien de dix qu'elle parai fait vou loir actopter. Les parties n’ayant pu étée conciliées la canse fut renvoyée devant le Bueu Général du Conseil séant le jeudi vingt juin mil huit cent suixante dix huit. Cité pour le dit jour vingt juin par lettre du secrétaire du Conseil en date du quatorze Juin mil huit cent soiante dix huit à la riguite des époux Demalle Monnon se présenta et conclut à ce qu'il plut au Bureau Général du Conseil attendu qu'il s’auet, de l'interpretation don Tontree de travauil, d'une part ; D'autre purt e statuer sov me demantes de douze cent francs quand le conseil de Prud’hommes ape conmettre que des affaires dant la demande ent inférieure à a deux Caits francs ; Attendu querre qu’il a peaiti citre les époux senelle devant le tritaral de commerce sont compétent pour conmaitre d'une affaire drcette niture et de cette inparan Par ces motifs — Le délarant n compétent pour connaitre de la demande des époux Denelle les en débouter et les condamner aux dépense De leur coté les époux Denelle se présentèrent et conclurent à ce qu’il plut au Bureau Générals du Conseil attendu que la dame Denelle nt entrée chez Monnier comme louvrière monteuse en fleurs et plarmes,; qu'il s'agit de l'astirfictions d'un contrat de travail et de sommages intérêts suins résuller de son inexécution ; Que dans l'espèce le choffie ’élové qu soit doit être sournis aux juge de prémier onstance ; lu d'autre part l'rtion de Monnier devant le tribanel de commetce était pas tervenme à celle qu'ils ont formée londe lui devant le Conseil ne pert avoir pour etet de chanter la rature de la cause qui ost me de retations d'autère cotun ; Par ces motifs –; Dre Monnier ontre on en sa demande,; l'in dé bouter ; La déclarer, competeur ritezer la cause etordonner qu’il sere passe entre du outecrtire, de Monner pa être apporé et sar le land condamner Mingie aux dépens. Pint de drit ait-en Le déclarer en compétait pour lomaitre de la demende des époux Denelle nenvayet ceux oi devant que de pit luillas pex M M eth t Crtt Meth a tip iis parsiaint Memer apelus cesier Sréger Public reaja et un gois que eul alporie f ecu t E cets reuva pprouvé et A onlr it Ou bin doit-on se déclarer compêtent retenir la cause, r donmer que les parties l’expliquerant sur le faud pour être statué ce après avoir entendu les parties en leurs que de drait – demandes et conclusions respectivement et en avoir délibéré Conformément à la loi, sur le éctinctaire de Monnier, ttendu qu'il est constant que les demandes des époux Dinelle portent sur un contret de travail een conséquences que le conseil doit conmaitre de les demandes par être statué en premier ressot ; Par ces motifs — Dit Monnier non recevable en ses demandes, l'en déboute, retient la cause, or donne que les parties s'enpliquerant et que le conseil statuère en suite sar le mérité de leurs conclusions. L ce mineur Monnier lertire dec lavans faire défaut. de leur coté les époux Denelle contneut à ce qu'il plut au Bureau Général du Conseil donner défat contre Monnier non comparant ni personne pour lui quelque dument appelé et pour le proft le condamner à leur payer avec intérêts suivant la loi la somme de douze centes pris ur somrmilies de oveiles le suend es le condamner aux dépens. tant de dait- Doit-on donner défaut contre Monnier non comparait ni personne pour lui quoique dument appelé et pour le proft adjuger aux du ei tes l l le on lu i pr le e e ir purdint lap satié renselen pus à aer entendu les épous Denelle en leurs demandes et conclusions enier lité la soman l le e t cile uente de et e e u pe te per qr l le e la te e pardemne e de pr e eme e ser conqurate e tie e t ten ene pls uint lentete eneit e la pa e psup permil suit de ou ge paprd ue esun tent de poueme pl adi encoemte ceu t e pee e ldi en en eu e de e en te e le t l so que du ene e de ets ou ete e t de e tretes are e de pr u ant pem l les quint e e aue des de ou de d cpte eu te eu teil se te te e e e e ui de cit e n i que es paur signifier au défendeur le présent jugement, coent chempu Ainsi jugé les jour, mois et l'un de ses huisiers audienciers. an que dessus. aviu àr Le cudf serétaire Qu dit jour vingt sing Entre Monsieur Mathieu, ouvrie tailleur d’habits demeurant à Paris, rue Montranyu numéro neuf ; Demandeur; Comparant ; D'une part ; Et Monsieur Bordeau ; Maite dailleur d’habits, demeurant et domicilié à Paris, rue saint Fonsliqne, numéro deuze ; Défendeur ; Défaillant ; Pautre part ; Point de fait — Par lettres du secrétairn de conseil de Prud’hommes du Département de la Seine pour l'industrie des issus en dates des Mardi quatre et vendredi sept juin mil huit cent soixante dix luit Mathieu fit citer Bordeaux à comparaître par devant le dit Conseil de Prud’hommes séant en Bureux Particuliers les tendredi sept et Mardi onze juin mil huit cent soixante dix huis por se couilier si faire se pouvait sur la demande qu'il entendaiz former contre lui devant ledit Conseil au paiement de la somme de treite neuf francs qu’il lui doit pour salaire . Berdeant n'ayant pas comparu la cause fil uaagée devat le Burau férére du contile leur Chaid vingt juin mil huit cent soixante dix huit. Cité pour le dit jour Cti9 por par titre du serétaire du conteil en bité deu edte joun seil lus ae pirsate de lait à le ouitel oin Bordeair ne comparut pas. A l'appel de la cause Mathieui se présenta et conclut à ceu’il plut au Bureau Général du Contail donner défaut contre Bordeaus, non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui ayet anet ntelite suiveant la loi la some de oet frncs u’il reste lui devoir nayant donné le cinq juin vinq franc sur lante seuf, Juailell duamité e ssaie e le our pis par pertie de temps deuns le Bnraux du contre u dépens. Pois de doit - Doit-on donner de fus letue coisdevur non comparant ni personne pour lui quoique dunen disqpu epepede on dem t prlui pruédement pas à ui dit -il être ain A n ue itée s ils à us 4 t l’égard des dépens ? Après avoir entendu Mathieu en ser demandes et conclusions et en avor délibéré conformément à la oi ; attendu que la demande de Mathié parent juste et fondée ; Que d'ailleurs elle n'est pas contestée par Berdeaux non comparant ni personne pour lui quoique dument appelé ; attendu qu’en ne comparaisant pas levant les Bureaux du Conseil Berdeaux a causé à Mathie un préjudie de perte de temps, ce qu'il doit réparer ; Par ces motifs - Le Bureu Général jugeant en dernier rettert ; Du l’article quarante un qu decret de onze juin mil huit cnt neuf; partait rislement pour les contils de Prud'hommes ; Donne défaut contre Berdeux non comparant ni personne pour lui quoique dument appelé ; Et adjugent e profit du dit défaut ; condamne Bordeant à payer avec fard luvant lelé à Delheu la somede se euf francs qu’il lui doit pour sefaire, plus une indmnité de dix francs par taups perde qu lendemendet susles e liquidés envers le demondeur à la somme de un franc, et à cellee eue au Trésor Public, pour le papieer timbré de la présente minde, la sorément à l le de q ei que lus luse die t ris en ps de de inseunge e perte e e s l et le e ile sur ec peat t e t on ein e es e e e e e ex lui de ta t oer etie à largre e esoie Mavirn à et an que des sus. Leuig recélaue duien en cingq ant e dete e e etie erenteu eede de den ete qa den dirt dementies de t in e one eni e e e te an e eri que m an du ete de de e eme dete et e e ne e e e
  </div>
  <div n="16" type="case">
   <opener>
    Du dit jour vingt Jeuit
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur Ematile Charsé t ouvrier sellier ; demeurant à Paris, pusage Bouchardy, numéro treis; Demandeur ; Comparant ; D’une part ;
     </span>
     <span type="part2">
      E Monsieur Balendier, autre prenoor de travaux de silernie, demeurant nt àParis, rue de tafoyette, numéro deut cent quatante trois ; Défendeur ; Comparant ; D’autre part ;
     </span>
     <span type="part2">
      ande Monsieur crousselle Maite Geltier, demeurant à Paris rue de lafayette, numéro cent trenteneuf ; Défendeur ; Comparant. Anssi d’autre part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait ur lettres du secrétaire du Conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus en dtates des samedi huit Juin mil huit cent soixnte dix huit, Charrier fit citer Balegdrer à comparaître par devant le dit conseil de Prud’hommes séant en Bureux Particulier se le Mardi onze juin mil huit ces soixante dix huit para se concilier si faire se pouvait sur la demande qu’il entendait suse former contre lui devant le dit conseil en paiement de la somme de trente six francs pour indemnité de temps perdu. A l’appel de la cause Cérrier se présenta et en posa comme à dessas. De son coté Balerdier se présente il retonnet qu’après avoir donné du travail pendant pate me lonaire à Blerer lu et pes pars donner enure il le pa venir Chaque jour de la durane mais qu’ayant lui même été abosé par prasselle sonitatron e e edemen en le e rdes. qu e par l e t s tendes dein et dandil prer t ondit u dt lui Mai ente e pr t e lete lun dre dte a aon eu e te e les en eur pur en t e ne t poir rande eu ete deit re t e e e et enene ape ur e den ant le se eue a cem espse u deme pur deure des oue den drer ne e tente en e te ememre e quele ade dente is d e te de é doe en la I5x trois mots tayés oils aux Lecuiq se per De son coté trousselle se présenta et exposa au Conseil que non seulement il n'a pas embouche Charrier mois qu’il ne la seulement jamais vu chez lui. Le Bureau Particulier fut d'avis que Bulerdier qui reconnails aà avoir causé du perter de temps devait payer à Chévrier une indemnité qu’il fixa à vingt cinq francs et en qugea charrier à retirer sa demande envers trousselle qu’il ne jugea pas responsable. Aurrier ayant déclaré persister dans toutes ses demandes la cause fut renvoyée devant le Bureau Général du Conseil séant en Bureau Général lejoudi vingt Juin mil huit cent soixante dix huit. Cités pour le dit jour n dt jue par lettre du secrétaire du conseil en date du quatorze ju mil huit cent soixante dix huit à la réguite des Chabrel Malendier et troup elle comparurent. A lappel de la cause Chavrier se présenta et conclut à ce qu'il plut au Bureau Général du conseil condamner solidairement Balessier est trousselle à lui payer avec intérêts suivant la loi la somme de trente fit francs pour indemnité de perte de temps et les condamner solidairement aux dépens. Poton, coté Balesdier se présenta et conclut à ce qu’il plus a Bureau Général du Conseil lui donner cite des les erves qu’il fait de résiiter à Troussille les sommes auxquelles le Conseil turair devoir le condamner envers le demandeur; De son coté Trousselle se présenta et conclut à ce qu'il plut au Bureau Général du Conseil le déclarer nin respinsibe erlemettre pors de causé duns dépens. sont de droit doit on condamner Baleydier et ousselle solidairement à payer à Charrier la somme de trente six francs à titre de réparction du préjudice pu'lils lui cause sienment ; Dot on donner cité à chartie de Baley dier des resorves qu’il fait de repiter à Troissielle les sommes auxquelles le pourris être condamné envers Chavror à Pait on dire truflle nou responsible et le mettre trort de couse Doit Que doit-il être statué à l’égard des dépens ? Apréès evorte. entreavu les parties en leurs demandes et concfusions respectivement et en avoir délibéré conformément à la loi ; Attendu qu'el qui coucerne troupelle qu’il est acquis aux débité ate confié à Balegaier de s travaux pour être exécutes à la tatte et à des prix détermints ; que le fait davaint at refé au dit Belrysier des ouvriers, tefait fit à pevier se paut le end re uni tes eunver esprs ut deraites de son entrepreneur ; Eonce qui conderiet elgde d ls it eute attendu que charrier à déclaré à la barre du Conseil que Ballortier était son aperié de fait et non son patron ; que à le dit Balordier était en nom c'est que au seut pollediert un local et que s'il touctut une avant part sar les prix da façon d’était unsquement pour payer le prix de lalocations es pourvoir à de momes dépenses argentes ; Que ce fait acaires Charvier devant en déviduellement supporter le part des dommages qui rappent la sicé n’a rienc reclaner à sa rendier, son tollégène, éttent comme lui ; Par ces motifs Le Bureau Général jugeant en dernier réport ; Dit Trouselle non responsable, te met purs de cause ; Dit Charrier non recevable en sa demande envers Balesodier l'en déboute et le condamne ux dépens envers le Trésor Publie, pour le papier timbré de la présente minute,; conformément à la loi du sept tour mil huit cent cinquante ence, non compris le cout du présent jugement, la signification d’icelui et ses suites.
   </div>
   <div type="judgement">
    Ainsi jugé les jour mois et an que dessus. Lecuig secrétaire eaeveir e Quit jour vingt ing Entre Monseur Voiter Decroëq, ouvrier teltiers umér tote euf demeurant à Paris, rue Demandeur ; Comparant ; D’une part ; Et prima dendier tipe et pour eleur l setie emnt e de e e e ene eu qeat eu e e enes aeante euper cepits de tu ires dune du det inqe etie un tre e eteil n deu aeier au es de te de t ir de e e e e pus ser t e e esen e pu et em e te r aupe por e t es denses de e e ie hunt e ent u guse n e en pur de e is Moni Conseil en paiement de la somme de trente six francs pour réparation du dommage qu’il lui a cause n la promettae du travail qu’il ne lui donna pas. A l’appel de la cause Lecrorqse présenta et exposa comme cn dessus. De son cote Baledier se présenta et exposa au Conseil qu'après avoir occupé Decroëz afsey régulièrement pouttant une se maner il le fit veuir chague pour de la semane suivante pousoint chaque jour paureur lui donner du travail qui lui était proms par tronpel son Patron mois cellaci ayant trouvé à faire faire à plus bas prir, ce qu’il lui destine il ne put on donner n'en ayant pas. Pa cemanent Devron, repuit la parale pour due que trousselle l'ayant embourset chez lui et l'ayant atrepe à Balerdier était responsable de pertes de temps qu'il avait éprouvées et demandu la remiselle la cause pour le pouvair aitionner. la cause e cet état fut renvoyée au vendredi quatorze Juin mil huit cet soixate dix huit. Cités pour le dit jour quatorze Juin par lettres du secrétaire du conseil à la reguate de Decroëq alesdier et troupelle comparurent. A l’appel de la cause Decroëq se présenta et affirma que tropselle l’avait embourtie et achrepé à Bagdier qui lui donna du travail sont sa rerommendation tropselle nia avoir imbouche secroëq et dit même ne l'avoir jamais vucher lui. Le Bureau Particulier fut d’avis que Baleririer qui resonnat avoir fait perdre à Decroëqune sormaine lui promettait du travail devait lui payer une indemnité qu’il fixa à la somme de vingt cinq francs à il tengagea Decroëg à retirer sa demande en ce qui concerne trousselle qu’il ne crot pas respontible Decroëy ayant déclaré persister dans ses demandes le cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi vingt Juin mil huit cent loixnte dix huit Cités pour le dit jour vingt Juin par lettres du secrétaire du Conseil en date du quatorze Juin mil huit cent faisine dixhuit à la requete de secrous Balerdier et pronssill comparurent. A l’appel de la cause Decrot à se présenta et conclut à ce qu’il plut a Bureau Général du Conseil condamner solidairement Balerdier et Trousselle à lui pay avec intérêts suivant la loi la somme de trente six francs pa réparation du dommage qu’il a éprouve par le chemaie de toute une semaine et les condamner solidairement eaer dépens. De son coté Bolgdier se présente et conclut égul plut au Bureau Général du Conseil lui donne vacte des i V ligé l u resorsesqu’il fait de repiter à tronsselle ces sommes ourouilles age le Conseil croirait devor le condamner. De son coté tapelle se présenta et conclut à ce qu’il plut au Bureau Général dmuaaner du Conseil le dire non responsable et le mettre puis de causé suns dépens.
   </div>
   <div type="pointDeDroit">
    Point de droit — Doit-on condamner Baloudier et troupelle solidairement à payer à Decrocq la somme de trente six francs pour indemnité de chamage Dait on donner aite à Balegdier des reservis qu’il fait eel de répete à tronselle les sommes axmelles il aurait été condamne ? Dait-on dire trousselle non recevable et le mettre pors de cause ? Que doit-il être statué à l’égard et des dépens ? Après avoir entendu les parties en leurs demandes e et conclusions respectivement et en avoir délibéré conformément e à la loi ; Attendu en ce qui conterne cropelle qu'il est acqust aux déboté quel a confié à Balerdier de s travaux pour être à exécités à la taihe et à des prix de termins ; que le fait par oupselle devoir adrehé des ouvriers au di Wolerdier, refait fut il acquit, ne peut prendre responsable en ces ouvriers du fait de son entreprenent ; Cresé qui conterne Baleydier; attendu que Decroya déclaré à la berredu Conseil que Baley dier était son aprcée et non son datronds que e Bales dier était sulen nom ses que sul il pas sidant anlocal ; Que si le dit Balesdier aprélevé sur les prix de façon une avant part avait portige d’était anquement par payer les tavoes et res le paes ue miner fournitures ; que ce fuit acquis Decroëq devant in dividuellement sapporter la part des dommages qui etteiuent la socete vislé rien à réclamer à Baler dier, Sourellegue, attent some cinq pet cesmuitte le sire hant puant donietrs, Cit ouete pe i quel le demie pus lasile, et douden en tent demande envers Balendier l'en déboute et le condamne eux dus minte deu cite pu e e te sa preeane omen desqou entererant juige es trer uain les p e te sesin ei purs lesoul eu ses eu n esi e aerui et reuig serétai L E
   </div>
  </div>
  <div n="17" type="case">
   <opener>
    Du dit jour vingtuin
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur Toales Mégeont, ouvrier Poller demeurant à Paris, rue des paissonniers, numéro seize ; Demandeur ; Comparant ; D'une part ;
     </span>
     <span type="part2">
      <span type="part1">
       E prima elonsieur Bales u entrepreneur de travaux de telterie, demeurant à larise rue de Lafayette, numéro deux cent quarante trois ; Demandeur ; comparant ; D'une part ;
      </span>
      Ctecand, elaupeur Trouselie Maitre sellier, demeurant et domicilié à Paris, rue de Lafayette, numéro cent trente neuf ; Défendeur ; Comparant ; causoi d'autre part ;
     </span>
     Oout de fait - Par lettre du secrétaire du Conseil de Prud’homme du Département de la Seine pour l’industrie des tissus en date du samedi huit Juin mil huit cent soixante dix Mignoul fit citer Bales,jour à comparaître par devant le dit Conseil de Prud’hommes séant en Bureau Particulier le Mardi onze juin mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendu former contre lui devant le dit Conseil en paiement de la somme de trente six francs pour réparation du dommage qu’il lui a cause fir des pertes de temps à l’appel de la cause Mogeaul se présenta et exposa comme ndessus. De son coté Baleydier se présenta et reconnut avoir employé Migeaul opt et régulièrement pendant toute une semaine et l'avair fait venir chaque jour de la deuxrème ponsant pouvour lui donner dé travail son Patron, trousselle, lui en ayant promis; mais celui ayant trouvé à faire travailler à des prix de duits ne lui on donna pas. Mle moment Mégeaul repris la parate pour exposer que troisselle l'ayant embouihe et a dripe à Balesdier devait être responsable de son chamage .Il demando àe l’actionner en responsabilité. La cause en ret état fut renvoyée au Bureau Particulier du vendredi quatorze du même mois. Cités pour le dit jour Balesdier et trousselle comparurent. A llappel de la cause Migeail se présenta enposa comme tle huit Jain et offerma que trousselle l'avair enlere et envoyéet Balesdier avec l’ordre de le recevait, lu tronsselle ia avoir embauché le demandeur et décara ne l’avoir même jamais ve cher lui . le Bureau pParticulier fut d'avis que Balerdier reconnaisaut avor fit chener Migesl pendant toute une somane le faisant, venir chez lui chaque jour il lui devait suf une indemnité qu’il fixa à vingt cinq francs. Wendeg Migneul à retirer sa demande envers tronsselle qu’il de jugea pas être responsable ; Migeaul ayant déclaré persister dans ses demandes la cause fut renvayée devant le Bureau Général du Conseil séant le jeudi vingt Juin mil huit cent soixante dix huit. Cités pour le dit jour vingt Juin par lettres du secrétaire du Conseil en date du quatorze Juin mil huit Cent soixante dix huit à la réquate de Migeant Balegihier et tousselle comparurent. A l’appel de la causé Migeoul se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner solidairement Balendier et Traisselle à lui payer avec intérêts suivant la loi la somme de trente six francs à titre de réparation du dommage qu’il àéprouvé par du chomage de leur fait et les condamner ux dépens. De son coté Balerdier se présenta et conclut à ce qu’il plut au Bureau Général du Conseil lui donner cité des reserves qu’il fait de répiter à tronsselle les sommes ausquelles il croirait devoir le condamner. De son coté dmame tronpselle se présenta et conclut à ce qu’il plut au Bureau Général du Conseil le dire non responsable et le mettre chers de causé, sans dépens.
    </rs>
   </div>
   <div type="pointDeDroit">
    Point de droit — Doit -on condamner Baleydier et trausselte solidairement à payer à Mégeanl la somme de trente six francs pour indemnité de chemage ; Dait-on donner acte à Balesdier des résorves qu’il fit de répéter à Cromptelle les sommes ax quelles et arait été condlamné à Dait on dire tropelle nun respousible et le mettre port de causé ? Qu ditit M être statué à l’égard des dépens ?
   </div>
   <div type="judgement">
    Après avoir entendu les parties en leurs demandes et conclusions respectivement dem anr silé la soméent lelis Meneau qu tacane compele e du peour eis el adu du travail à Balerdier pour être exenités à la toite eta des poar deni, que les devie parsemes dit Balesvier, afit fat i prouvé, ne puit le rendre desponsible erde emie er dete ntomn cons ant ou e e de e le u étire de eis e e ite deant e quentu pere e e e en es our e l e e e e eie oefaute m p t de doment paur pr le er e et our u parse ate nenr deus, ou prme eu ene t it, Ai devant in deréduillement sapporter la part ses dommages qui fraphent la souile n'arien à reclamer à Balais son talleque attend comme lui ; Par ces motifs Le Bureau Général jugeant en dernier report ; Dits Trouft elle non responsable, le met sers de conte Dit Mégean nonrecevable en sa demande envers Balesdier, son callegue, attent comme, l'en déboute ei te condamne ux dépens anvers le Trésor Public pur le papier timbré de la présente minute, conformément à la loi du sept aot mil huit cent cinquante rence non compris le cout du présent jugement, la signuigées d'icelui et ses suites. Ainsi jugé les jour mai et en que dessus. Decraveu Leincg secrétaire rant
   </div>
  </div>
  <div n="18" type="case">
   <opener>
    Du dit jour vingt rnz
   </opener>
   <div type="identificationParties">
    <rs>
     entre Monsieur Benort Lébart, ouvrier sellier; demeurant à Paris, rue des tctuset saint Martin, numéroi vingt trois ; Demandeur ; Comparant ; D’une part ; primo ; Monsieur Balerdier, entretreneur de travaiux de tellerie demeurant à Paris, rue de Lafayette Guméro deux cent quarante trois. Aafendeur comparant ; D’autre part ; L serando , Monsieur Trousselle, Maitre sellier, demeurant et domicilié à Paris, rue de rapayette, numéro cent trente neuf ; Défenreur ; comparant ; Quulis d’autre part ;
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait — Fri lettre du secrétaire du conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus en date du farmes huit juin mil huit cent soixante dix huit Lebont fit et tere Balerdier à comparaître part devant le dit Conseil de Pondi hommes séant en Bureau Particulier le mardi onze juin mil huit cent soixante dix huit pour se concilier si faire si pouvait sur la demande qu’il entendait former cont lui devant le dit conseil en paiement de la somme de Trentesix francs pour ruparation du dommagef lui a causé en le faisoit chamer taute une Lemander là l’appel de la cause le bert se présenta et exposa teu cidissus. De son coté Balesoier se présenta M exposa au conseil qu’après avor ocupe de bert asser régulièrement pendant une se maine quoiqu’il l’ant ni occuper de vantage, et que penvant la deuxième il l’e fat revinir chaque jour lui promettait de travaul comptaut sur la promepe qui lui était chaque jour fuite par trousselle, son Patron, et qu’il ne tunt pas ayant travé à faire travailler à des près enférieurs à libre mement Lebert repis, la pirate pour dire que trousselle l'ayant mtauché et adrepé à Batesdier il demandant à le citer comme responsable. La cause en ce étt fut renvoyée devant le Bureau Particulier du Conseil séant le vendredi quatorze Juin mil huit cent soixante dix eup Cités pour le dit jour quatorze juin par lettre du secréaaire du Conseil à la requete de retert Ba lerdior es trousselle comparurent.. A l’appel de la cause le bert se présenta rappelu la demande envers Balerdier et demandà la responsabilité de Troufs elle comme l'ayant embourte et adrefié au dit Balégdier , tront selle de son coté de présenta et exposa au conseil qu’il n’a mellement embaurtie déberr, que de plus il ne l’a jamais vai chez lui. Le Bureau Particulier fut d’avis que Balesrer reconnaisant avon fait perdre du temps à Le bart lui devais une indemnité de vingt cinq francs, et ne trouvait pas motif arendre topselle respousable il enqadea Le pert à retirer lu demande envers celui à Lebers ayant déclaré parsister dans ses derindes la cause fut renvoyée devant le Bureau Général du Conseil séant de jeudi vingt Juin mil huit cent soixante dix tuit Cités pour le dit jour vingt juin par tettres du secrétaire du Conseil en date du quatorze juin mil huit cent soixante dix huit à la réquite de lebeit Balegdier et trousselle Comparurent. A l'appel de la cause le bort se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner solidairement Baley dier e troup elle à lui payer avec intérêts suivant la loi la sonme dde diu ue e e ue etese t lemit prde prur se poer cait on pome es en de e en er lsent s senbile pl e de du tin le t saou Burea Graral du Conseil lui donneraite des réserves e l e e pat l domen e le deu u demen p te s E un mit jusée noff Ak Lécule Da le it it il de son coté, se présenta et conclut à ce qu’il plut au Bureau Général du Conseil le déclrer non lesponsable et le metir rois de cause dons dépiens. Pnt de droit Doit-on condamner Bolesdier et trousselle à payer à Le bers le somme de trente six francs pour léparation du tort qu'ils lui ont causé par pertes de temps ? Dcit on donevaulle Balerdier des reserves qu’il fait de repeter à trons sette les sommes aux quelles il pourroy être condamne par la Conseil ? Dait-on dire trous selle non responsabelie le mettre pors de causé ? Que doit-il être statué à l’égard des dépens ?
   </div>
   <div type="judgement">
    Après avoir entendu les parties en leurs demandes et conclusions les pertivement et en avoir délibéré conformément à la loi ; aAttendu qu’il est acquis eux débots que troufs elle à confié à Baleyier de s travaux pour être exeuités à la toche et à des prix déteminés ; que le fait par Trouls elle d’avair la drepé des ouvriers au dit Balerdier, cefut fit il acguis, ne pourrait le rendre resonsable vis à vis de les ouvriers du fait de son estrepreneur à leur égard ur en ce qui concerte Balesdier ; attendu que Lebert à déclaré à la baire du Conseil que Balesdier était son apraiée et non son patron ; que s'il était seul en norte s'est que seul il pepsédat un locat ; que s’il prétéveir une avant part sur les prix de façans d'était uniquement pour payer les loyers du local et pourvoir au paiement de memes dépenses communés ; Que cepuis arquis Lbars sta rous à réclamer à Balesidier son allègue ettente comme lui par les dommanes qus fraphent l’apruition et dont chaque membre dos saphort et la part en déveduellement ; Par ces motifs - Le Bureau Général jugeant en dernier retsort ; Dit Troulelle non responsiable emet lurs de causé, dit de bert non recevable centime demande envers Séont Balesdier, l'en déboute et le Condamne aux dépens envers le Trésor Public, pour le papiei timbré de la présente minte, conformément à la loi du sept Aui mil huit cent cinquante, ence, non compris le cout drus présent jugement, la signification d’iceli et ses suites. Ainsi jugé les jour mois et un que dessus. ecrevin e Lecucg secrétaire t
   </div>
  </div>
  <div n="19" type="case">
   <opener>
    Du dit jour vingt suin
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Mosieur Porcaille à hustive, auvnier tellier, demeurant à Paris, rue du Levroge, numéro vingt cinq lis ; Demandeur ; Comparant ; D’une part ;
     </span>
     <span type="part2">
      Et primo Monsieur Bélegoier, ttre preneur de travaux de teltere demeurant à Paris, rue de lafigette numéro dex cent quatante trois ; Défendeur ; Comparat ; Dtautre part ; Et Monfieur Trousselle, Mautre ellies demeurant à Paris, rue se lafagete, numéro cent trente neuf ; Défendeur ; Comparant tupsi d’autre part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait - Par lettre du secrétairé du Conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus en date du Samedi huit Juin mil huit cent soixante dix huit Couaille fit citer Balesdier à comparaître pardevant le dit conseil de Prud’hommes séant en Bureau Particulier le Mardi inz juin mil hui cent soixante dix hui pour se concilier si faire se paivait sur la demande qu’il entendit former contre lui devant le dit conseil en paiemet de la somme de trente six francs pour réparation du tort quil lui a causé en sefabsant chiner toute une semaine A l’appel de la cause ouaille se présenta et exposé cume e dessus. De son coté Balesdier se présenta et expos a du Conseil qu'il employa Camille pendant toute une semaine cpez renulaement quoiqu’il oit pupent étre l’oucher plus se le travail avair été plus atondant ; Que peudant la deuxième semaine il le ft, veur chague jour experait aup chaque pour lui donner du tavail quatr tes pour poali que onsele suilie ayant trouvé à fare faire à meilleur morché ne lui donna rien quoique l'ayant ajourné de jour en jour e de le pomd suilte sur la perde pour dire que Trousselle l'avant intouthé et ed’russe à Véberne dot responsable du fix de ce dernier et demande la remise de la cause pour l'actirnner dugen teleue en téle dit duites appeler Balerdor et truselle devait le Bureau de eite e ten ue jugent mis. Cits de la cause Suailfe se présenta, repit, sa demandes pre d ure der teuit e de de ae le e p e des pour de t de pu s uratesan se présenta et exposa au Conseil qu’il n’a pi mi emboucher souaille un l’adresserà Balendier puisqu’il ne l’a samais vu n chez lui ni ailloure Le Bureau Particulier pt davis nue puisque Ba les dure reconné par avax fait chemer ille il lui devait payer une indemnité qu’il fixa à la somme de vingt cinq frntc quand à trousselle il qe le sout pas responsable de engagea ouaille à abousonner sa demande à seu EardeCouaille ayant déclaré persister dans sa demoinde la cause fut renvoyée devant le Bureau Général du Conseil deant le jeudi vingt Juin mil huit cent soixante dix huit Cités pour le dit jour nit juin par lettres du secrétaire du Conseil en date du quatorze juin mil huit cent soixante e trait à la réguate de souaille Balegotier et Troussitte comparurent. A l'appel de la cause coneille se présenta et conclut à ce qu’il plut au Bureau Général du Conseil condamner solidairemen, Balestier et Coupelle à lui payer avec intérêts suivant la loi la somme de trente six francs pour répiration du dommaze qu’il a’etrouse par le chomage de taute une semiaine et les condamner solidairement aux dépens. De son coté Balerdier se présent et conclut à ce quilt plut au Bureau Général du Conseil lui donner aîte des résorver qu’il fait de résiiter à Troup ete les sommes auxocellet le conseil croirair devor le condamner proupselle, de son coté, conclut à ce qu’il plut au Bureau Général du conseil le dire non respontable et le matte lorr de cause sons dépens. Rais de troit — Doit-on condamner Baley dier etcroupelle à payer à Ponaille la sommed trente six francs pour indemnité de perte detemps ? Dait on donner aité à Balesdier des reésorves qu'il fait de régiten à trousselle les sommes auxquelles il aurait été condamne par le conseil ; Doit on dite trousselle non responsible le mettre trors de cause ; que doit-il être statué à l’égard des dépens ?
   </div>
   <div type="judgement">
    Après avoir entendu les parties en leurs et conclusions respectivement et en avoir délibéré Conformément à la loi ; Attendu que ce qui loncerne Trantelle qu’il refert des débats q’’il a confié à Béerdier des travix par être faits à la toitre eu des pris determinés ; qqu’dl fait pur trousselle d'avoir atreté à Bales dois dit ouvriers, le fit fit il acquis, ne pourrait le rendre envers e ouvriers responsable du fit de son athetrenent, Cnre condame Balepier; atendu que vuille Pittre dou e i e d un u di M Barre du Conseil que Baleydier était son apaice et nonton Patron ? Que si Bales dier était seul en nomc est que seul il pasés aun local il que s'il a prélevé sur le prix de façon une avant part Wéled anrquement pour payer les loyers du sdif local et aussi pourvoir au paiement de meues fournitirer. Qu’en ce fait acquis douaille devant en déviduellement lapporter de part, des dommages qui atteigent la loceté n’aren a réclamer à Balesdier, son colléque, attent comne lui ; Par ces motifs - Le Bureau Général jugeant en dernier réport ; n ce qui concerne Trouptelle le z non responsable et le methirs La cause ; Dit sonclle non recevable en la demande envers Baleydier, l'en déboute et le condamne aux dépens envers le Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept aout mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites. Ainsi jugé les jour, mois et an que dessus. Lecucq secrétaire avee .Ausi jour vint ciniq Entre Monsieur Boulain Auelphe, ouvrier letties, demerant à Pais, rue beremtf, numéro soixante dix Demandeur ; Comparant ; D’une part ;Mosieur Bélerdier, entretreneur de travaux dépelarie, de mourant à Pain, rue de lafayette, numéro deux cent quarante trois ; Défendeur ; Comparant ; D’autre part ; Lt se de de sur Consele, rte d e pome à Paris, rue de Calayette, numéro cant trente neuf Déléjant Consant; Crs ntiepus lit ptis u loitete a el e se la e doie d entie et euig en de e dous elle de onr les su et lu pent pes duti lesir le ur de sart some prs te e e dueiten e ei de la det n tes ponde q ues ute a de t e e e aur cte eu es e per et en urs pus e entre de quele causé en le faisaunt chaner tute rne semaine. A l’appel de la cause Poulin se présenta et exposa comme et dessus. De son coté Balejuer se présenta et exposa au conseil qu’il a ccupé Poulain after régulièrement pendant une semaine ; qu’en suite I le fis nenir tont les jours pousait pouvait lui on donner josqué ce la loi était promis par Trousselle, son Patron ; mais que le dit Crousselle ayant truvé à faire executer se travoux à des pris de tuits le fit, vamement attendre. Pare moment Poulain repris ca parale pour dire que pousselle l'ayant embauché et adressé à Bas ue devait être responsable qu’il a subl et demandasue la cause fut ajournée, pour le faire appelés E respon sabilité. La cause en et état fut ajournée a Juudi à vendredi quatorze Juin même mois. Cités pour le dit jour quatorze Juin par lettres du secrétaire du Conseil les détendeus comparurent. A l'appel de la cause Voulain rappela sa demande en vers Bales te tterma que Trousselle l'avait embouché et adressir li Boleydier son entrepreneur et demandu qu’a aison de ces faits il fut tenu comme responsable de l'indemnité demande à son entrepreneur. Lé Bureau Particulier fut davis nue Balesdier reonnaipa avoir fut chomer Poulai pendant tuné semaine il lui devait une indemnité qu’il fixa à vingt cinq francs ; Quanda tous selle il ne le crut pas repous du 4 en garea Poulain à aton donner à l’égard de Coupelle Poulain ayat déclaré persister dans sa demande la cause fut renvoyée devant le Bureau Généra du Conseil séant le Jeudi vingt Juin mil huit cent soixante dix huit. Cités pour le dit jour vingt juin par lettres du secrétaire du Conseil en dates du quatorze Juin mil huit cent soixante dix hui à la requite de Poulain Balesjon et trousselle Comprrurent. A l’appel de la case Poulain se présenta et conclut à ce qu’il plut au Bureau Général du Conseil condamner lolidairement Boleydor et trousselle à lui payer avec intérêts suivant la loi la somme de trente six francs à titre de répuration du dommase qu'il a éprouvé par Comage de toute cne semaine et les condamne solidairement ux dépens. De son coté Baley dox se présenta et conclut à ce qu’il plut au Bureau Général ufl inq mi une ; l de s 2 nsil que e e eu A aeuig de i auls ? A du Conseil lui donner aite des resarves qu’il fait de répiter à Trousselle les sommes auxquelles le Conseil eurai devoir le condamner envers coulain ; trousselle, de son coté, se présenta et conclut à ce qu’il plur au Bureau Général du Conseil le déclrer noreomble ar responsable envers Poulain et le mettre pers de cause dans dépens ? Paint de troit — Doit-on condamner Balendier et troups elle solidairement à payer à Poulain la somme de trente six francs pour l’indemnitér de temps de chamage; Doit on donner cité à Bolesdier des réserves qu’il fait de étarter à troupselle les sommes euxquelles il pourrait être condamne Daitaou dire rousselle non responsable et le mettre pors de causé ? Que dit-il être tatué à l’égard des dépens ? Après avoir entendu les parties en leurs demandes et conclusions respertivement et en csoir délibéré conformément à la loi ; Attendu qu’il eit acvis ux débetsque troupelle à confié à Bébersi des travaux pour être executés à la toitre et à des peis de termines ; Qu le fis par trouselle d'avoir accetté de onvents à l’atrder, apis les it poup la larer le rendre responsable envers ces ouvriers des aitert de tete fremte à la dauese enaer tere le der ud ue su les tilté le uire su titire qe silu e les eu drenl litne tie e e e u ei e t fasier en lete et eu s l rél se pus on purs seiles e prix de façon avant portire ee ne fit qu'inquement dur ui lite l e mn pareten cinse e lus seli de e en e e le ier nu el u te en lu se le em e quie ce de l r e t e d n en le eue aure eu re sa commnt le hur t e e t e de es le ent prser e e e rde men te ua due areden e te es u ant soir et prs e e te se poeur edt e e l ten u e e aur rspu e e eu d er edereveure demerait u e t u atese Leuigerétine r Dudi jour vingt Ping fu Entre Maisieur Fentane Consegdier, ontretreneu de travaux de elterie, demeurant à Paris, rue de Lafay édu uméro tent deix cit quarante troit ; Demandeur; Comparant ; D'une part ; Et Monsieur Trousselle e Maitre sollier demeurant et domicilié à Paris rue de lapaette, numéro cont trente neuf ; Défendeur ; Comparant ; D’autre part ; Point de fait - Par lettre du secrétaire du Conseil de Prud’hommes du Déurtenent de la seme pour l’industrie des tissus en date du moisi ouze juin mil huit cent soixante dix huit Baregdier fit citte trousselle à comparaître par devant le dit conseil de Prud’hommes séant en Bureau Particulier le vendredi quatorze Juin mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait fermen contre lui devant le dit conseil en paiement de la somme de cent ving francs pour indemnité de chomage et en quratre des indemnités qu'il pourrait être condamné à payer à treire, ouvriers qu’il a intrainés dans le même case A l’appel de la cause Borerdor se présenta et exposa uu Conseil que suivant convention verbale en date du vingt leai dernier troutselle s'eit engagé à lui donner les travaux à des Conditions de terminées nun seulemeant pour lui mois encore pour autant douvriers qu'il en pourrait trouvert ; Que le dit Trousselle qui l’accutra pendant une première semaine et trure ouvrièers emiboustrés à cet offet trouvant à fuire parer ces travaux à des prir reduits de les lui dama tros quoique le reit à chaque les demain, ce qui fait qu’il a non veulemre perdu son temps ; mois en cole fait perdre clui à d’avriers qui lui réclament des dommages-intérêts. De Venntate Tronsselle se présenta et exposa au Conseil qu’il ne i et Pis engage à donner du travail à Bales dier qelan sur etq mésure de ses besoins et non a toubon plaisir et par quantités, fres ; que s’il n’on donna que peuà la soin det aue n’était pas propté pour les livraissons à lui avait conseil d'ajourner eu qu'encore les apprêts étirent en retird, enta cis il n’était tenu à reuni envers Balesdier. Le teueau Particlier n’ayant pa concilier les parties la cause fut renvoyée devant le Bureau Général du Conseil séant le huddi vingt ui mil huit cent soixnte dix huit. Cité pour le dit jour mi jui par lettre du secrétaire du Conseil en date du mattrné juin mil huit Cent soixante dix huit à la réquitte de iteger 5 i Il u papelle Comparut . A l’appel de la cause Raleglier se présenta et roctut à ce qu'il plut au Bureau Général du Conseil attendu que troupel qui devait lui donner du ravail pour lu et ses ouvriers l’atupe conte une semane lans lui en donner et, chose plus auve l’afit veur chaque jour pour le remettre au tendemin ; qu'il ut constant que troupelle lui a causé un dommage dont il lui doit réparution Par ces motifs – Condaane Tousselle à lui payer avec intérêts suivant la loi la somme de cent vingt francs de dommages intérêts et le condamner aux dépens. De son oté Ausselle se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu qu'en acceptaut de Balerdier ses frir de façon il ne pitvis à vis de lui d’outre eugugement que de la domse des travaux à exécuter au fur et a mésire de ses besons et par quatités qu'il jugerait ; Qu'avit eu le soin de certains erti leuil les fit coupersappreter et les lui donna le dout et enpaye le prix ; Par ces motifs — Dire Ba legdor nonréccvalte en sa demande, l'en débouter et le condamner aux dépens. tont de droit — Doit-on condamner Troupelle à payer à Walensier la somme de cent vingt francs pour l’induniser du temps de couage Conquel il l’a otistrent par son fait à Paben dit -ooun dire du de e e dt endemnit ourl e e noer pet tre s’atué à rare du it àl peur deveir de parté à las ler de eu ese prisant ren avoir délibéré conformément à la loi ; attendu qu'il est constant que Baleydier s'engageant le vingt mai dernier à traviller pour da pete de a dis et e dede eseutie de di perdéster e ugit du jun cnee e pare en n es et foui p en i e n lu e t t e deiar sape quete u sa teusen de lure leq i au e te e des sutie uete sus sesg et eu et on e rdeer pasden de en de eit et an premen a s liur eue e e t de tinet ent e t pouir eni re es quendene eu e e per de ietr eu en e e Duriern e reurq usétire 1 Mudience jeudi vingt set soug mil huit cent sixante dix huit siégeant. Dessieurs Brand Théalier de la Ségon d’homnler dure Président du Conseil de Prud’homme du Département de la Seine pour l’industrie des E Tissus, président l’audence en remplacement de Monsieur Marienvat, Trerideut du dit Conseil imperte Carrony, Léforge Blanchiz et Margier Prul’hommes asistes le Monsieur Lecusq, secrétaire du dit Conseil. Entre Monseur Ponnet demetant à Paris, rue du Boulo, numéro dix sept, agissant à nom et comme administreteur de la personne et des biens pe la ville mineure Margueite, ouvrière ; Demandeur Comparant ; D’une part ; L Monsieur Varner, Maitre ailleur d’hotit, demeurant et domicilié à Paris, rue Montmartre, numéro cent surante, six; Défendeur ; D’éfailleus ; D'autre part ; Poir de fit Par lettres du secrétaire du conseil de Prud’homme du Département de la Seine pour l’industrie des tissus en dates des Vendredi sept e mardi onze juin mil huit cent soixante dix huit Ponet fit citer Vagrer a de sute purie t de le er der eu den eil e t e lated n enilier Apaus peuise le part quage cenis s lit lu e e e e pour le atis u t u e it en de por aente t u te de er ure ue de e ten q e suti ee de en e e u rceton et enie e e prs leus deu e de e e de ende e e i eu de it e n pur due cengetie ui nt amn c onor is D o Paguer fit citer Vlagyer à comparaise pardevant le dit conseil de Prud’hommes séait en Bureau Général lendi vingt sixx juin mil huit cent sixante dix huit pouir s'entendre condamne lui payer avec intérêts suivant la loi la somme de quatre vingt deux francs cinquante centimes qu’il lui doit corpris de travaux de sa fille Margueute, plus, tette indemnité qu’i plasra au Conseil fixer pour perte de temts devant les Bureaux du Conseil et reslégiens. A l’appel de la canse Mlagner ne comparut pas. De son coté Poinner le présenta et conclut à ce qu’il plut au Bureau Général du Conseil donner défaut contre Bacirer non comparu ni personne pour lui quoique dument appelé et pour ce profit reui adjuger le bénifice des conclusions par lui préses dant la citation exploit de champrou, tempsede enregistré.
   </div>
   <div type="pointDeDroit">
    Point de droit — fait-on donner défaut contre Vagner non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur conclusions par lui précédeumment prises ? Que doit il enre statué à l'égard les dépens ? Après avoir entendu Poiger en ses demandes et conclusions et en avoir délibéré conforméent à la loi Mndu que les demandes de oiguet pasursa justes et fondées ; Que d'ailleurs lles ne sont pas confestée par Vaprer non comparant ni personne pour lui quoique dudent l'appelé ; Attendu qu'en ne comparupant pas devant les Bureux du Conseil lagner a cause à Vorgney u présender de perte de temps, ce qu’il doit réparer ; Par ces mols e Buraun enret saint condemner atsort ; Done dépaur contre agner non comparant ni personne pour l quoique dument appelé ; Et adjugeant le proft du dit dé faute; condamne Clagner à payer avec intérêts suivant la loi à Peigner la somme de quatre vingt deux francs cinquante cent qusil lui dit pour lire de trtix de sa file de ron ins plus une indemnisté de neuf francs pour temps perdu d condamne en outre aux dépens trrois et ligudés envers le demender e lle somme de deuve forens t t e det our quante ca timoes avrirs le Treur Publi pour le ppie dut tinttrée et l'enreustrement de la citation la sormement li de sept du mil hui det oinquante en ces pon tompré cted preut oue a cine e te erie e e le a de lu dil es pre de lesadeante en te te e nt inque liu pe ent lent por prmiene pe épourx lunde et hapes. C eer jugé les jour mois et an que dessus. Lecug secrétaire dmanie e
   </div>
  </div>
  <div n="20" type="case">
   <opener>
    Du dit jour vingt det sinz
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur et Madame ol cseur Cil tant en son nom personnel que pour apsister et autoriser la dame son épouse, ouvrière qu'eltière, demeurant ensemble les dits éhoux, à Paris, rue sainte Marthe, numéro trente quatre ; Demandeur ; Comparant ; D'une part ;
     </span>
     <span type="part2">
      Et Monsieur et Madame Combier, le sour combraytant en son nom personnel que pa aister et autoriser la dame son épouse Goletrère demeurant ensemble les tits éhout; à Pans, rue saint Meur, numéro deux cent dix ; Défndeurs ; Défaillants ; D’autre part ;
     </span>
    </rs>
   </div>
   <div type="pointDeFait">
    Point de fait - Par lettres du secrétaire du Conseil de Prud’homme du Département de la erme pour l'indstrie des tissus en dates des lundi dix apretende det luis doveil le es purte qui du pour Cihl par seur le vaire contie la parte par devant le dit conseil de Prud’homme s an Bureaux atintes le vrit ei ui t unt t en e ps nil huit cent soixante dix huit pour se conclu si faire se pouvait son le derentete Cinseur puin ce tersein le dit conseil en paiement de la somme de enze francs pour purt luit cende l de oere e pet e prig ent u ciles e de e e eni quent é p agfe e e ce t enie are ue e et e le ue d anter t du e pus aen o l ar tente ea de te e que pu eni e pdseus an tere ei u e quei e u ne apen de unt t eil unt e e te é Bel, plus tette indmnité qu'il plaira au Conseil fixer pour perte de temps devant les Bureaux du Conseil et les dépens ? Pouix de droit — Doit-on donner défaut contre es époux Conmlarion non comparants ni personne por eux quoique dument apelés pour le profit adjuger aux demandeurs les conclusions par eux précédemment prises ; Que doit-il être statué à l'égardde dépens ?
   </div>
   <div type="judgement">
    Après avoir entendu les époux Bel en leurs demandes et conclusions et en avoir délibéré conformément à la loi. Mltende que la demander des époux Wel posus juste et fendée; que d’ailleurs elle nt'est lus contestée par les époux Comproy non comparut ni personne pour eux quoique dument appelés ; attendu qu’en e comparusant pas devant les Bureaux du Conseil les époux Compal ont causé aux épeus ief un préjudice de perte de temps, le qu'ils doivent réparer ; Par ces motifs u Bureau Général jugeant en dernier repart ; au tarticle quarante un du décret du oize quigt mil huit cent neuf, portant réslement pour les conseils de Prud’homme Donne depau contre les époux Cambror non comparant ni personne pour ux quoique dument appelés ; Et adjugeant le profit du dit défaut ; condamne les époux Cambray à payer avec intérêts suivant la loi ax époux Rel la somme de ente francs u’il leur doivent pour prix de travaux de la dame Gel, prusane indemnité de quatre francs pour temps perdu ; Les condamne r outre aux dépens taxés et liquidés envers les demandeurs à la soe de un franc, et à celle due au Trésor Public, pour le papier timbré de la présente minute, conformément, à la loi du sept aout mil hui cent cinquante ence, non compris le cout du présent jugement, la signifiecction d’icelui et ses suites. Et vu les articles 435 du Codede procédure civile, 27 et 42 du decret du onze juin 18e9, pour signifier aux défendeurs le présent jugement, conmet. Chompran, l'uin de ses huisiers audienciers. Ansi jugé les jour mois et un que dessus. Jeurg iétre  Dineanx Ausit jour vingt cent Sing a ntre etenser ertordaure Polarée e sour Colrs le e tr omparent u por apales la deme due part mire par dom e de e es u e si ete i e vingt ; Demandeurs ; Comparant ; D’une part ; E Machenr Madame Gorguard, le sour Torgardtant en son nom personnel que pour apister la dame son épouse fabricnte de fleurs artificielles, demeurant et domiciliés à Paris, rue des deux torter sant Jonveur, numéro vingt huit ; Défendeurs ; Défaillant ; D’untre part ; Point de fait - Par lettre du secrétaire du Conseil de Prud’hommes du Département de la Seine pour l’industrie des Tissus en date du Lundi dix sept Juin mil huit Cent soixante dix huit les époux Citer pres citer les époux soiguard à comparaitre par devant le dit Conseil de Prud’hommes hui en Bureau Particulier le mardi dix huit juin mil huit cent soixante dix huit pour se concilier si jaire se pouvait sus la demande qu’ils entendaiens former contre eux devant le dit Conseil en paiemet de cent quarante trois francs qu'ils leur doivent pour prix de travaux de la dame Citer. A l’appel de la cause les époux Piter se présentèrent et exposstant comme dessus. De on coté orgon se présenta et reconnait devant la somme déclamée et fis, l’engagement de la payer par quinze francs le vingt deux Juin et ingt francs tant let quinze jurs, le saqmedi à le vingt deux juin les époux Gergmard ne poyeant prasut la causé en cet étot fut renvoyée devant le Bureau Général du Conseil séant le Judi vingt sept i mil huit cent soixante dix huit. Cité pour le dit jour vingt sept juin par lettre du Ginétaire du Conseil en date du vingt cinq juin mil huit cent soixante, dex luis pur tarout de l qurt coitit e gore demin ne comprmes pour e e l er le eoue Citer se présentèrent et conclurent à ce qu’il plut au Bureau prte se tarat sui u ent le genent qu ante pre e onden s se per e rf te la deme e lem ent ene e pe t e pu ile ou e e te e our eren e e eux aun e et en etite eende re de u tegen de au per t pu en en pesr audepeur a e coux e e e es e dem e de en uete etant a toemnité apee es de demnet que den esente eupre en eneique n Ees E D i ui is in e M t ni personne pour eui quoique dument appelés ; Attenda que e les époux Forshgard ont fait perdre du temps aux chaue iter devant les Bureaux du Conseil, ce qui leur cantei préjudice dont ils leur doivent réparation ; Par ces motis Le Bureau Général jugeant en dernier report, ou bartule quarante un du décres du onze juin mil huit cent neufer portant regument pour les conseil de Prud’hommes ; Done défaut contre les époux Gorguard non comparants ni personne pour eui quoique dument, appelés ; t adjugeant le profit dedit défaut condamne les époux Lorguiarda payer avec intérêts suivant la loi ax époux Céter la some de cent quarante trois francs quits leur doivent pour prix de trevux de la dame Poter, plus une indmnité de deux trancs pour temps perdu Les condamne en outre aux dépens taxés et liquidés envers les demandeurs à la somede suivait cinq centimes, et à celle due au Trésor Public pour le papier timbré de la présente minute, conformément à la loi du sept coute mil huit cent cinquante, ence non compris le cout du présent jugement, la signifiation d’icelui et ses suites. Et vules articles 435 du code de procédure civile 27 et 42 du secret du onze juin 1809, pour signifier aux défendeurs le présent jugement Comet Chompron, l'un de ses tempsiérs audienciers a jugé les jour mois et an que dessus. Lecucq térnétaireammeuoil
   </div>
  </div>
  <div n="21" type="case">
   <opener>
    Du dit jour vingt sejt Puinu
   </opener>
   <div type="identificationParties">
    <rs>
     <span type="part1">
      Entre Monsieur Charles Fétemett, ouvrier tordonner peurs à Paris, rue Paint souveur, numéro vingt huit ; Demanders comparant ; D'une part ;
     </span>
     Et Monsieur Gavail, Mntre cordonnier, demeurant et domicilié à Paris, rue Vayevir numéro quatre ; Défendeur ; Défaillant ;D’autre hart ; ores de fait – Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l’industrie des tissus en dates des Lundi trois et Mardi quatre juin mil huit cent sixante dix huit étemill fit citer Lavail à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les mordi quel et vendredi sept juin mil huit, cent soixante dix huit pour se concilier si faire se pouvait sur la demande qusil entendu former contre lui devant le dit conseil en paiement de la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour lalaire . Lavail n'ayant pas comprru la cause ft renvayée devant le Bureau Général du conseil séant le jeudi vingt et ajournée au putte Vingt sept juin mil huit cent soixante six temp Cité pour le dit suivant exploif de champron, luipier à Paris, en date du vingt un juin mil huit cent soixante dix huit, visé pour timbre et enregistré à Paris, le vingt deux Juin mil huit cent soirante dix huit, etmitte fit citer ravail à comparaître par devant le dit Conseil de Prud’hommes séant en Bureau Général le jeudi vingt sept juin mil huit cent soixante dix lui pour s'entendre condamner u lui payer avec intérêts suivant la loi la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour salaire plus, telte indemnité qu'il plaira au Conseil frer pour perte de temps devant les Bureaux du Conseil et les dépens. A l'’appel de la cause Lavail ne comparut pas. De son coté schunt se présenta et conclutz à ce qu’il plut au Bureau Général du Conseil donner défau contre Lavail non comparant ni personn pour lui quoique dumet appelé et pour le profit lui adjuger le bénifice des conclusions par lui prises dans le citation en plois de Champour, lui par enregistré. Poin de troit — Doit-on donner défaut contre lavail non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précédemment prises ? Que doit-il etre statué à l’égard des dépens ?
    </rs>
   </div>
   <div type="judgement">
    Après avoir entendu létimelte en ses demandes et conclusions et en avor délibéré conformément à la loi ; Mttendu que la demande sihuett purai juste t fondée ; que d'ailleurs elle n'est pas contestée par lavail non comparant ni personne pour lui quoique dument appelé ; attendu qu'en ne comparipant jes devant les Bureux du Conseil Lavail a causé à Btmill un préjudice de perte de temps, ce qu’il doit réparer. Par ces motif - Le Bureau Général jugeant ente e n comn da d it ente tue en lre t par me teu pé de pur e pprecit et i el u en es pr le oe uet cinre es soir de e e senr indemnité de nuf francs pour temps pardu ; le condamne unr e de tis e d en t lerie se épeur la premie ue e er i uil it é franc quire centimes envens le Trésor Public, pour le papier. timbré et l’enregistrement de la ctation, conformément à la loi sept aoux mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d'icelui et ses suites. t vu les articles 435 du code de procédure civile, 27 et 42 du decres, du onze juin 18099 pour signifier au défendeur le prémier jugement , Comme Rompi, l’un de ses huipars audienir Aiusi jugé les jour moois et an que dessus. Deneur Lecuuq secrétaire Audit jour vingt set sinig Entre Mademoiselle Maria Lesqune, ouvrière brodeute demeurant à Paris, rue Mongé, numéro cent huit ; Demandeur ; comparant ; D'une part ; Et Monsieur es Madame Coureille le sieur Courelle tant en son nom personnelque pour apristedt autoriser la dame son épouse brodeuse, demeurant et domecili onsemble, les dits époux, à Paris, rue du faubourg Montmestre numéro vingt huit ; Défendeur ; Comparant ; D’autre part , cn de fait - Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l’industrieu Tissus en date des Jendi vingt trois et vendredi vingt quit Mai mil huit cent soixante dix huit la demoiselle Lequine fit citer les époux Courcelle à comparaître par devant lesdit Conseil de Prud’homme séant en Bureaux Particuliers les Vendredi ving quatre et lundi vingt sept Mai mil huit cent saixante dil huit pour se concilier si faire se pouvait sur la demande qu’elle entendai former contre eux devant le dit Conseil en priéement de la somme de vingt cinq francs pour prix de travanx le trocdie A l'appel de la cause le vingt sept Mai les époux Conreil n'ayant pas comparu le vingt quatre la demoiselle Lguran, se présenta et exposa cou eu deseus. De leur Cite les époux Conrielles se présentèrent, drens que le prix du Gravail bien la trit de six francs contigusiéte mal fit et rendrer lardivement entendaus ne rien payer. La demanderees épara que son travail était nreplachablement faist LBureau Particulier veulent être fixé sur le mérite du Traral et sur sa valeur Vénble renvoya la cause à V’eroameil t qu Di i membre du conseil à ce comaitsant. Les parties n'avant pu ctre conciliée, la cause fut renvoyée devant le Bureau Général du Conseil séanz le jeudi vingt sept juin mil huit cent soixante dix huit. Ctés pour le dit jour vingt sept Juin par lettre du secrétaire du Conseil en date du vingt un juin mil huit cent loixante dix huit à la réquete de la demoiselle Lequine les époux Courcelles comparurent. A l’appel de la cause le demoiselle Semine se présenta et conclut à ce qu’il plut au Bureau Général du Conseil condamner les époux Courcelles à lui payer avec intérêts suivant la loi la somme de vingt cinq francs pour travaux de broderies et les condanner ux dépens. De leur coté les époux Courcelles se présentèrant et conclurent à ceguil plut à Bureau Général du Conseil attendu que la semoiselle Legune lui arendu un travail mal fait et en acceptable par la loie qu'elle a employée Qu'on tant cas, ce travail fut il acreptable le prix n’envaidrauit que six francs et non vingt cinq ur ces motifs — Dire la demoiselle Leonne non recevable en sa demande, l'en débouter comme mal fondée en celle et la condamner aux dépens. Paint de drot — Doit-on condamner les époux Courcelles à payer dà la demoiselle Léquine la somme de vingt cinq francs pour travaux de brochrre ? Oubion doit-on dire la demoiselle Lequine ; onrécemble en ses demandes, l'en déboutes ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu les parties en leurs demandes et conclusions respectivement et en avou délibéré conformément à la loi ; attendu qu’il est constant que la soie employée e travail de brodrie qui fait l'objit du titige est nne soie mathinte ; Mais attendu que cet en couvenments pouvait être cité par ls épous Courcelles en chaisissant et fournipsant et mimes la toie du bien don taser le soin à l’ouvrière qui ne puit certeimeent par avoir la même, connailsence qu’eut et oui ne doit pas enconser une semblabli responsabilité ; attendu eu ce qui donverne le prix du travait qui le Conseil qui à legilement quce lavrier d’apprenition bestime à due francs , Eor ces motifs – Le dpen r te jour an deuer t s lu sante n lanre l eu ile u tete e a lire alrende i7 pui qu pae t sux dte ies d dit den it e mne tende a pent dou qeme lrsae t de e e ter deu deniont a deme tep le mrdt pure u la tie u e e d pumnt ape et esprqute enet non compris le cout du présent jugement, la signification d'icelui Ainsi jugé les jour, mois et an que dessus. et ses suites. derurg seuctaire Dnaurlte du dit jour vingt sept sing Entre Monsieur Dauté, fabricans de tissus, demeurant Qu et domicilié à Paris, rue saint paire, numéro vingt Défendeur au tirmipal ; Demandeur appotant ; Comparaite D'une part ; Et Madame Délatiace, ouvrière, demantant à antenit le Maudouin oise ; Demanderesse au princépar Défenderesse ; opprosent ; Comparant ; D'autre part ; Paint le aux termes d'un fait- Par jugement rendu par défaut par le conseil de Prud’home pouvoir sons devante du Département de la Seine pour l'industrie des tissuste neouf pasé en date du anthuil le houdun, du Ma mil huit cent soixante dix huit, enregistré ; Dendé à étter premier Mi mil condamné à payer à la dame Défatrace la somme de trois cent huit cent soixante soixente quinze frons de princépat et les accilsire: d jugement dix huit, enregttrée fut Simifié à Fandit par exploit de Ginaad, uipier ; en dit reuve epreuvdu dix neuf Juin mil huit cent soixante dix huit, enregistré à la ruguite de Dauilé. suivant en péloit de cham prin huipare à lain en date du vingt Juin mil huit cent soixante dix traid udisé pour titre elenregistré à Paris, le vingt un juin mil Devai huit cent soixante dix huit, détes deux francs quinze centimes spur de Siuilles ; Daudé forme apposition à A jugement et Citre les époux Délahacé à comparaître par devant le dit Conseil de Prud’homme dént en Bureau Général le jeudi vingt sept juin mil huit cent soixante dix huit pour le voir de cevoir opposant à le gugeme le voir, de charger des condamnations contre dux prosoncées en principal et accepaires, s'entendre lus dix époux Délalice déclarer non recevable eu leurs demandeu, les en débouter; Cone mal fondés en celles, et s'entendre condamner ux dépens de la première comme de la présente inistance. A l’appel de la cause Dande se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu que la dame Délahayé n’et pas auveiit dans l'acaption du mot ; qu'elle entreprenense de travaux de fet execiter par dtentres ouvrières à Monteuil le hondamne ses ouirons ; Qu'en outre elle lui achate la soie qu'elle faut M or o I t pour n et avi les façons et a forfait ; que le conseil n’était pas compateut pour lomuitre des différent entre les fabricaute et les entrehreneus de travaux à lerhuit, d'ut à tort que les époux Delcheye aun près contre lui le neuf Mai dernier en jugement par défaut Par ces motifs — Dire le jugent nul et de mil uffes ; Ptatuant à mouveu Le déclrer n compétent pour tommêtre de la demande des époux Délataye, les renvoyer devant jui de droit et les condamner aux dépent de la première comme de la présente inistance. De son coté la dame Détatoge se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu qu’elle n’est ni marchon de ni fabricant; qu’elle n’a fait avec Dandé auun oite de commerte Que si elle ne ceute pas seule les trovuy qui lui sont confiés per cmperté à Dau de puisqu'elle en suite responsable Bois avis de lui de l'ixeution de cestranailr ; Par ces motifs Dire Doudé, nonrecevable en vondéblinatoire, se déclarer competent, rétenir la cause et ordonner qu’il sera procéde à l’examen de sa demande pour être statué sur le fautà Pants de droit - Doit on se déclarer en compétent pour conaitre de la demande des éour Délatoye il renvayer la cause devaut qui de drait ? Ou bien doit-on se déclarer confétent retenir la cause et ordonner que les parties s'entliqueran pour être statué au fand. Que doit-il être statué à l'égarddes dépens ? Après avoir entendu les parties en leurs demandes ete conclusions respectivement et en avoir délibéré conformément à la loi ; Attendu que des explications fournies par les partiese que la dame Delahoya quoiqu’oupput des ouvrirres pour l’éxcution de travaux qui lui roit fournis par dans é n’est pas monis vis t sis de cela a une ouvrière prtinee du conseil de Prud’hommes puisqu’elle ne furait avei le travail que la soce que est é la brodeuse ce qu'et le fle au contirier au à la conturière ; que cette fourniture et e e r p le con der een de préte us e pent edue preut dmit lur puastur qui de pr e e pit edt peu e t e re desenite on et du n t ee eis de en e de en tur ete em e se e seut endere e un que lut e du l te e nten ingq en eit journée par être reprise le jendi quatre puillet mil huit cnt seinente dix huit. Ainsi jugé les jour mois et an que depis Leniig ecrétare L Dunaiof
   </div>
  </div>
 </div>
</body>
```



### 5.5 Contenu du fichier XML prêt à être enregistré"

```py
complete_tree = make_tei_container(titles[-1])
complete_tree.text_blob.append(tree.extract())
complete_tree = clean_up(complete_tree)
complete_tree = find_dates(complete_tree)
complete_tree.text_blob.name = "text"
ready_text = fix_date_tags(complete_tree.prettify())

print(ready_text)
```
Output
```xml 
<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>
     AD75 D1U10 405 Prud'hommes 1878 - juin - Transcription
    </title>
    <editor>
     Alix Chagué
    </editor>
    <respStmt>
     <name>
      Alix Chagué
     </name>
     <resp>
      Gestion des données, encodage de la structure logique
     </resp>
    </respStmt>
    <respStmt>
     <name>
      Victoria Le Fourner
     </name>
     <resp>
      Encodage de la structure des documents
     </resp>
    </respStmt>
    <respStmt>
     <name>
      Kevin Champougny
     </name>
     <resp>
      Correction de la transcription et prise de vues
     </resp>
    </respStmt>
   </titleStmt>
   <publicationStmt>
    <p>
     Time Us (2017-2020) : http://timeusage.paris.inria.fr/mediawiki/index.php/Accueil
    </p>
   </publicationStmt>
   <sourceDesc>
    <p>
     No description
    </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc>
    <p>
     TIME US est un projet ANR dont le but est de reconstituer les rémunérations et les budgets temps des travailleur⋅ses du textile dans quatre villes industrielles française (Lille, Paris, Lyon, Marseille) dans une perspective européenne et de longue durée. Il réunit une équipe pluridisciplinaire d'historiens des techniques, de l'économie et du travail, des spécialistes du traitement automatique des langues et des sociologues spécialistes des budgets familiaux. Il vise à donner des clés pour comprendre le gender gap en analysant les mutations du travail et la répartition du temps et des tâches au sein des ménages pendant la première industrialisation. Pour ce faire, le projet met en place une action de transcription et d'annotation de documents d'archives datés de la fin du XVIIe au début du XXe siècle.
    </p>
   </projectDesc>
   <editorialDecl>
    <p>
     Les transcriptions et leur annotations sont réalisées à l'aide de la plate-forme Transkribus.
    </p>
    <p>
     Les transcriptions ont été transférées importées dans eScriptorium (escriptorium.inria.fr) et la validité de leur segmentation controlée manuellement.
    </p>
   </editorialDecl>
  </encodingDesc>
  <revisionDesc>
   <change type="Created">
    2018-12-19T23:03:09.528+01:00
   </change>
   <change type="LastChange">
    2019-01-27T15:58:22.906+01:00
   </change>
   <change type="toeScriptorium">
    2021-01
   </change>
   <change type="ToTEI">
    2021-10-25T16:36:56.605520+2:00
   </change>
  </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <div type="manuscrit">
    <div n="0" type="courtHearing">
     <p>
      Audience
     </p>
    </div>
    <div n="0" type="case">
     <opener>
      du <date>Jeudi six Juin mil huit cent soixante dix huit.</date> Siégeaient : Messieurs Pinaud, Chevalier de la Légion d'honneur, vice Président du conseil de Prud'homme du Département de la Seine pour l'Industrie des tissus, Lerroy, Larcher, Platiau, Broutin, Godfrin et Meslien, Prud'hommes assistés de Monsieur Lecucq, secrétaire dudit conseil
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur et Madame Lablanche, le sieur Lablanche tant en son nom personnel que pour assister et autoriser la dame son épouse ouvrière passementière, demeurant ensemble, les dits époux, à Paris, rue Besfroid, numéro dix ; Demandeur ; Comparant ; D'une part ;
        </span>
        <span type="part2">
         et Monsieur Nisson, fabricant passementier, demeurant et domicilié à Paris, rue Rambuteau, numéro trente huit ; Défendeur ; Défaillant ; D'autre part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la Seine pour l'industrie des tissus en dates des Lundi vingt et Mardi vingt un Mai mil huit cent soixante dix huit les époux Lablanche firent citer nisson à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particulier les Mardi Vingt un et vendredi vingt trois Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'ils entendaient former contre lui devant le dit conseil en paiement de la somme de douze francs pour prix de travaux de la dame Lablanche. A l'appel de la cause le vingt trois Mai mil, nisson n'ayant pas comparu le vingt un, la dame Lablanche se présenta et exposa comme cidessous. De son coté Nisson se présenta et exposa au Conseil que la dame Lablanche qui avait entrepris un travail qu'elle devait coudre le vendredi l'obligea à l'aller chercher chez elle le Lendemain samedi ; pour ce fait il entendit, Quoique le travail fut bien exécuté, ne payer que moitié prix de façon . Les parties n'ayant pu être conciliées la cause fut renvoyée devant le Bureau Général du Conseil séant le Jeudi six Juin mil huit Cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt cinq Mai mil huit cent soixante dix huit à la requête des époux Lablanche quatre mots rayés nuls Lecucq Nisson ne comparut pas. A l'appel de la cause les époux Lablanche se présentèrent et conclurent à ce qu'il plut au Bureau Général du Conseil donner défaut contre Nisson non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à leur payer avec intérêts suivant la loi la somme de douze francs qu'il leur doit pour prix de travaux de la dame Lablanche, plus, telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du Conseil et les dépens
      </p>
     </div>
     <div type="pointDeDroit">
      <p>
       Point de droit = Doit-on donner défaut contre Nisson non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui adjuger aux demandeurs les conclusions par eux précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu les époux Lablanche en leurs demande et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande des époux Lablanche parait juste et fondée ; Que d'ailleurs elle n'est plus contestée par Nisson non comparant ni personne pour lui quoique dument appelé ; Attendu que Nisson a fait perdre du temps aux époux Lablanche devant les Bureaux du conseil, ce qui leur cause un préjudice dont il leur doit réparation Par ces motifs = Le Bureau Général jugeant en dernier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf portant règlement pour les Conseils de Prud'hommes, Donne défaut contre Nisson non comparant ni personne pour lui quoique dument appelé ; Et adjugeant profit du dit défaut ; Condamne Nisson à payer avec intérêts suivant la loi aux époux Lablanche la somme de douze francs qu'il leur doit pour prix de travaux de la dame Lablanche, plus une indemnité de deux francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers les demandeurs à la somme de un franc, et à celle due au Trésor Public, pour le papier timbré de la présente minute, Conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement, la signification d'icelui et ses suites. Et vu les articles 437 du code de procédure civile, 27 et 42 du décret du onze Juin 1809, pour signifier au défendeur le présent jugement, Commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et que dessus Lecucq secrétaire
      </p>
     </div>
    </div>
    <div n="1" type="case">
     <opener>
      Du dit jour six Juin —
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur Gutz Willer, ouvrier cordonnier, demeurant à Paris, rue de Jouy, impasse Guepin, numéro quatre ; Demandeur ; Comparant ; D'une part ;
        </span>
        <span type="part2">
         Et Monsieur Caillier Maître cordonnier, demeurant et domicilié à Paris, rue Notre dame de Nazareth, numéro soixante dix ; Défendeur ; Défaillant ; D'autre part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait = Par lettre du secrétaire du Conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en date du Mardi vingt un Mai mil huit cent soixante dix huit Gutz willer fit citer Caillier à comparaître par devant le dit Conseil de Prud'hommes séant en Bureau Particulier le Vendredi vingt quatre Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit conseil en remboursement de cinquante centimes pour pareille somme qu'il a payée au Bureau de placement qui l'adressa chez lui au moyen d'une carte et en celle de cinq francs pour perte de temps. A l'appel de la cause Gutz Willer se présenta et exposa au conseil que s'étant présenté au Bureau de placement pour y demander en carte fut envoyé à Caillier qui ne le reçut pas prétendant qu'il arrivait alors que la place était prises ; or, il résulte d'une clause qui régit les Bureaux de placement que le cas échéant le Patron rembourse le prix de la carte qui est de cinquante centimes et l'ouvrier n'a alors qu'à se retirer ; Mais Caillier s'est refusé à rembourser et fut cause qu'il perdait sa journée. De son coté Caillier se présenta et exposa au Conseil qu'il a en effet demandé au Bureau de Placement des ouvriers cordonniers un ouvrier, mais ce Bureau le lui envoyant près cinq semaines rien détonnant qu'il n'en ait plus besoin au jour de l'envoi. C'est pourquoi il s'opposa au remboursement du coût de la carte. Le Bureau Particulier fut d'avis que Caillier qui reconnut n'avoir pas contremandé la demande au Bureau d'un ouvrier aurait dû rembourser le coût de la caste, que s'étant refusé à le faire il devait les cinquante centimes et une indemnité de temps perdu qu'il fixa à quatre francs. Sur le refus de Caillier de se rendre à l'avis du Bureau Particulier la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt quatre Mai mil huit Cent soixante dix huit à la requête de Gutzwiller Caillier ne comparut pas. A l'appel de la cause Gutzwiller se présenta et Conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Caillier non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de quatre francs cinquante Centimes se composant de celle de cinquante centimes pour le prix d'une carte émanant du Bureau de Paiement et de celle de quatr francs pour indemnité du temps qu'il a fait perdre devant les Bureaux du conseil et les dépens.
      </p>
     </div>
     <div type="pointDeDroit">
      <p>
       Point de droit = Doit-on donner défaut contre Caillier non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu Gutzwiller en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande de Gutzwiller parait juste et fondée ; Que d'ailleurs elle n'est plus contestée par Caillier non comparant ni personne pour lui quoique dument appelé ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant règlement po les Conseils de Prud'hommes ; Donne défaut conter Caillier non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Caillier à payer avec intérêts suivant la loi à Gutzwiller la somme de quatre francs cinquante centimes pour remboursement d'un coût d'un carte de Bureau de Placement et indemnité de temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de soixante cinq centimes, et à celle dûe au Trésor Public pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante non compris le coût du présent jugement, la signification d'icelui et ses suites. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire
      </p>
     </div>
    </div>
    <div n="2" type="case">
     <opener>
      Du dit jour six Juin
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur et Madame Poitier, le sieur Poitier tant en son nom personnel que pour assister et autoriser la dame son épouse ouvrière brodeuse, demeurant ensemble, les dits époux, à Paris, rue Beauregard, numéro neuf Demandeurs ; Comparant ; D'une part ;
        </span>
        <span type="part2">
         Et Monsieur Dieu fabricant de broderies, demeurant et domicilié à Paris, rue Turbigo, numéro cinquante trois ; Défendeur ; Défaillant ; D'autre part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait = Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l'industrie des Tissus en dates des Mardi vingt un et Vendredi Vingt quatre Mai mil huit cent soixante dix huit les époux Poitier firent citer Dieu à comparaître par devant ledit conseil de Prud'hommes séant en Bureaux Particuliers les vendredi vingt quatre et Mardi vingt huit Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'ils entendaient former contre lui devant le dit conseil en paiement de la somme de vingt quatre francs qu'il leur doit pour prix de travaux de la dame Poitier. Dieu n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt neuf Mais mil huit cent soixante dix huit à la requête des époux Poitier Dieu ne comparut pas. A l'appel de la cause les époux Poitier se présentèrent et conclurent à ce qu'il plut a Bureau Général du conseil donner défaut contre Dieu non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à leur payer avec intérêts suivant la loi la somme de vingt quatre francs qu'il leur doit pour prix de travaux de la dame Poirier, plus telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du conseil et les dépens.
      </p>
     </div>
     <div type="pointDeDroit">
      <p>
       Point de droit = Doit-on donner défaut contre Dieu non comparant ni personne pour lui quoique ce dument appelé et pour le profit adjuger aux demandeurs les conclusions pour eux précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu les époux Poitier en leurs demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande des époux Poitier parait juste et fondée ; Que d'ailleurs elle n'est pas contestée par Dieu non comparant ni personne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du conseil Dieu a causé aux époux Poitier un préjudice de perte de temps, ce qu'il doit réparer ; Pour ces motifs = Le Bureau Général jugeant en dernier ressort ; Donne défaut contre Dieu non comparant ni personne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du conseil Dieu a causé aux époux Poitiers un préjudice de perte de temps, ce qu'il doit réparer ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; Donne défaut contre Dieu non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Dieu à payer avec intérêts suivant la loi aux époux Poirier la somme de vingt quatre francs qu'il leur doit pour prix de travaux de la dame Poirier, plus une indemnité de quatre francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers les demandeurs à la somme de un franc, et à celle dûe au Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement, la signification d'icelui et ses suites. Et vu les articles 437 du code de procèdure civile, 27 et 42 du décret du onze Juin 1804, pour signifier au défendeur le présent jugement, commet champion, l'un de ses huissiers- audienciers. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire
      </p>
     </div>
    </div>
    <div n="3" type="case">
     <opener>
      Dudit jour six Juin —
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur Afchain, (Edouard) ouvrier passementier, demeurant à Paris, rue des cendriers, numéro trente six ; Demandeur ; Comparant ; D'une part ;
        </span>
        <span type="part2">
         Et Monsieur Bailleux, fabricant passementier, demeurant et domicilié à Paris, rue d'angoulême, numéro cinquante ; Défendeur ; Défaillant ; D'autre part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la Seine pour l'industrie des tissus en dates des Mercredi vingt deux et vendredi vingt quatre Mai mil huit cent soixante dix huit Afchain fit citer Bailleux à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi vingt quatre et Lundi vingt sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit conseil en paiement de la somme de cinquante huit francs cinquante centimes qu'il lui doit pour salaire et en celle de vingt sept francs cinquante centimes pour perte de temps par la faute Bailleux n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du trente un Mais mil huit cent soixante dix huit à la requête d'Afchain Bailleux ne comparant pas. A l'appel de la cause Afchain se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé et pour le profit le condamne à lui payer avec intérêts suivant la loi la somme de cinquante huit francs cinquante centimes qu'il lui doit pour salaire et celle de vingt sept francs cinquante centimes pour temps perdu et le condamner aux dépens.
      </p>
     </div>
     <div type="pointDeDroit">
      <p>
       Point de droit = Doit-on donner défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions pour lui précedemment prises ? Que doit-il être statué à l'égard des dépens ?
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu Afchain en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande d'Afchain parait juste et fondée ; Que d'ailleurs elle n'est pas contestée par Bailleux non comparant ni personne pour lu Quoique dument appelé ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant règlement pour les conseils de Prud'hommes ; Donne défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit dépens ; condamne Bailleux à payer avec intérêts suivant la loi à Afchain la somme de cinquante huit francs cinquante centimes qu'il lui doit pour salaire, puis celle de vingt sept francs cinquante centimes pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe au Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement, la signification d'icelui et ses suites. Et vu les articles 437 du code de procèdure civile, 27 et 42 du décret du onze Juin 1809, pour signifier au défendeur le présent jugement, commet champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire
      </p>
     </div>
    </div>
    <div n="4" type="case">
     <opener>
      Dudit jour six Juin
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur Zephir Dignoire, ouvrier passementier, demeurant à Paris, Cité Popincourt, numéro six ; Demandeur ; Comparant ; D'une part ;
        </span>
        <span type="part2">
         Et Monsieur Bailleux, fabricant passementier, demeurant et domicilié à Paris, rue d'Angoulême, numéro cinquante ; Défendeur ; Défaillant ; D'autre part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en dates des Mercredi vingt deux et vendredi vingt quatre Mai mil huit cent soixante dx huit Dignoire fit citer Bailleux à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi vingt quatre et Lundi sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit conseil en paiement de la somme de cinq cent soixante treize francs cinquante centimes qu'il lui doit pour salaire. Bailleux n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt neuf Mai mil huit cent soixante dix huit à la requête de Dignoire Bailleux ne comparut pas. A l'appel de la cause Dignoire se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de cinq cent soixante treize francs cinquante centimes qu'il lui doit pour salaire, plus, telle indemnité qu'il plaira au Conseil fixer pour perte de temps devant les Bureaux du Conseil et les dépens, ordonner l'exécution provisoire du payement à intervenir, nonobstant appel et sans qu'il soit besoin par lui de fournir caution, conformément à l'article quatorze de la loi du premier Juin mil huit cent cinquante trois.
      </p>
     </div>
     <div type="pointDeDroit">
      <p>
       Point de droit = Doit-on donner défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu Dignoire en ses demandes et Conclusions et en avoir délibéré conformément à la loi ; Attendu que les demandes de Dignoire paraissent justes et fondées ; Que d'ailleurs elles ne sont pas contestée par Bailleux non comparant ni \trsonne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du conseil Bailleux a causé à Dignoire un préjudice de perte de temps, ce qu'il doit réparer ; Par ces motifs = Le Bureau Général jugeant en premier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant règlement pour les Conseils de Prud'hommes ; Donne défaut contre Bailleux non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Bailleux à payer avec intérêts suivant la loi à Dignoire la somme de Cinq cent soixante treize francs cinquante centimes qu'il lui doit pour salaire, plus une indemnité de six francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe au Trésor Public, pour la papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante ; ence, non compris le coût du présent jugement, la signification d'icelui et ses suites ; Ordonne l'exécution provisoire du présent jugement, nonobstant appel et sans qu'il soit besoin par le demandeur de fournir caution, conformément à l'article quatorze du premier Juin mil huit cent cinquante trois. Et vu les articles 437 du code de procèdure civile, 27 et 42 du décret du onze Juin mil huit cent neuf, pour signifier au défendeur le présent jugement commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire
      </p>
     </div>
    </div>
    <div n="5" type="case">
     <opener>
      Dudit jour six Juin —
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur Jules Omnès, ouvrier implanteur, demeurant à Paris, rue Quicampoix, numéro quatre ; Demandeur ; comparant ; D'une part ;
        </span>
        <span type="part2">
         Et Monsieur Pagès, Maître coiffeur, demeurant et domicilié à Paris rue de Rennes, numéro soixante cinq ; Défendeur ; Défaillant ; D'autre part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en dates des Mercredi vingt deux et vendredi vingt quatre Mai mil huit cent soixante dix huit Omnès fit citer Pagès à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi vingt quatre et Lundi vingt sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit Conseil en paiement de la somme de six francs cinquante centimes qu'il lui doit pour salaire. Pagès n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt neuf Mai mil huit cent soixante dix huit à la requête de Omnès Pagès ne comparut pas. A l'appel de la cause Omnès se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Pagès non Comparant ni personne pour lui quoique dument appelé pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de six francs cinquante centimes qu'il lui doit pour salaire, plus, telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du conseil et les dépens.
      </p>
     </div>
     <div type="pointDeDroit">
      <p>
       Point de droit = Doit-on donner défaut contre Pagès non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions pour lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu Omnès en ses demandes et Conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande d'Omnès parait juste et fondée ; Que d'ailleurs elle n'est pas contestée par Pagès non comparant ni personne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du Conseil Pagès a causé à Omnès en préjudice de perte de temps, ce qu'il doit réparer ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; Vu l'article quarante du décret du onze Juin mil huit cent neuf, portant règlement pour les conseils de Prud'hommes ; Donne défaut contre Pagès non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Pagès à payer avec intérêts suivant la loi à Omnès la somme de six francs cinquante Centimes qu'il lui doit pour salaire, plus une indemnité de six francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe un Trésor Public, pour le papier timbré de la présente minute, Conformément à la loi du sept août mil cent cinquante, ence non compris le coût du présent jugement, la signification d'icelui et ses suites. Et vu les articles quatre cent trente cinq du code de procèdure civile, vingt sept et quarante deux du décret du onze Juin mil huit cent neuf, portant réglement vu d'allemagne numéro cent quarante pour les conseils pour signifier au défendeur le présent jugement, Commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus Lecucq secrétaire —
      </p>
     </div>
    </div>
    <div n="6" type="case">
     <opener>
      Dudit jour six Juin
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur Jean Baptiste Richard, demeurant à rue de Flandre, 28 à Paris\tagissant au nom et comme administrateur de la personne et des biens de son fils mineur Victor, ouvrier en apprêts sur Etoffes ; Demandeur ; Comparant ; D'une part ;
        </span>
        <span type="part2">
         Et Monsieur Lecrique et Compagnie, Maîtres appréteurs d'Etoffes, demeurant et domiciliés à Paris, quarante trois rue Riquet et rue de Tanger ; Défendeurs ; Défaillant ; D'autre part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en dates des Mercredi vingt deux et Vendredi vingt quatre Mai mil huit cent soixante dix huit Richard, ès noms et qualités qu'il agit, fit citer Lecrique et Compagnie à comparaître par devant le dit Conseil de Prud'hommes séant en Bureaux Particuliers les _ Vendredi vingt quatre et Lundi vingt sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre eux devant le dit conseil en paiement de la somme de sept francs qu'il lui doit pour prix de travaux de son fils Victor. Lecrique et compagnie n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cités pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt Neuf Mai mil huit cent soixante dix huit à la requête de Richard Lecrique et compagnie ne comparurent pas. A l'appel de la cause Richard se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Lecrique et cie non comparant ni personne pour eux quoique dument appelés et pour le profit les condamner solidairement à lui payer avec intérêts suivant la loi la somme de sept francs qu'ils lui doivent pour salaire de son fils victor et les condamner aux dépens.
      </p>
     </div>
     <div type="pointDeDroit">
      <p>
       Point de droit = Doit-on donner défaut contre Lecrique et compagnie non comparants ni personne pour eux quoique dument appelés et pour le profit adjuger au demandeur les conclusions par lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu Richard en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que la demande de Richard parait juste et fondée ; Que d'ailleurs elle n'est pas contestée par Lecrique et compagnie non comparant ni personne pour eux quoique dument appelés ; Attendu qu'en ne comparaissant pas devant les Bureaux du Conseil Lecrique et Compagnie ont causé à Richard un préjudice de perte de temps, ce qu'ils doivent réparer ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; Vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant réglement pour les conseil de Prud'hommes ; Donne défaut contre Lecrique et Compagnie non comparants ni personne pour eux quoique dument appelés ; Et adjugeant le profit du dit défaut ; Condamne Lecrique et Compagnie à payer avec intérêts suiva la loi à Richard la somme de sept francs qu'il lui doit pour salaire de son fils victor, plus une indemnité de six francs pour temps perdu ; Les condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe au Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement, la signification d'icelui et ses suites ; Et vu les articles 435 du code de procédure civile, 27 et 42 du décret du onze Juin 1809, pour signifier au défendeur le présent jugement, Commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus Lecucq secrétaire —
      </p>
     </div>
    </div>
    <div n="7" type="case">
     <opener>
      Dudit jour six Juin —
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur Bréchot, ouvrier Galochier, demeurant à Paris, rue de Meaux, numéro trente sept, passage de la Brie ; Demandeur ; Comparant ; D'une part ;
        </span>
        Et Monsieur Vermérot, fabricant de Galoches, demeurant et domicilié à Paris deux mots rayés nuls Lecucq —
       </rs>
      </p>
     </div>
    </div>
    <div n="8" type="case">
     <opener>
      Dudit jour six Juin
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur — Emile Pilloy, ouvrier Bourrelier, demeurant à Paris, rue de Cotte, numéro seize ; Demandeur ; Comparant ; D'une part ;
        </span>
        <span type="part2">
         Et Monsieur Prevost père, Maître Bourrelier, demeurant et domicilié à Paris, Montreuil sous Bois, près Paris, rue du Pré, numéro trois ; Défendeur ; Défaillant ; D'autre part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait = Par lettres du secretaire du Conseil de Prud'hommes du Département de la seine pour l'Industrie des Tissus en dates des Mercredi vingt deux et Vendredi vingt quatre Mai mil huit cent soixante dix huit Pilloy fit citer Prevost à comparaître par devant le dit Conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi Vingt quatre et Lundi vingt sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit Conseil en paiement de la somme de Cinquante sept francs se composant de trente neuf francs pour prix de soixante dix huit heures à cinquante centimes et de dix huit francs pour travaux à la tache. Prevost n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du vingt neuf Mai mil huit Cent soixante dix huit à la requête de Pilloy Prevost ne comparut pas. A l'appel de la cause Pilloy se présenta et conclut à ce qu'il plut au Bureau Général du conseil donner défaut contre Prevost non comparant ni personne pour lui quoique dûment appelé et pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de cinquante sept francs qu'il lui doit pour salaire, plus, telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du conseil et les dépens.
      </p>
     </div>
     <div type="pointDeDroit">
      <p>
       Point de droit = Doit-on donner défaut contre Prevost non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précèdemment prises ? Que doit-il être statué à l'égard des dépens ?
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu Pilloy en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que les demandes de Pilloy paraissent justes et fondées ; Que d'ailleurs elles ne sont pas contestées par Prévost non comparant ni personne pour lui quoique dument appelé ; Attendu qu'en ne comparaissant pas devant les Bureaux du Conseil Prevost a causé à Pilloy un préjudice de perte de temps, ce qu'il doit réparer ; Sur ces motifs = Le Bureau Général jugeant en dernier ressort ; vu l'article quarante un du décret du onze Juin mil huit cent neuf, portant règlement pour les conseils de Prud'hommes ; \tDonne défaut contre Prévost non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; Condamne Prévost à payer avec intérêts suivant la loi à Pilloy la somme de cinquante sept francs qu'il lui doit pour salaire, plus une indemnité de six francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de un franc, et à celle dûe au Trésor Public pour le prix timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le coût du présent jugement la signification d'icelui et ses suites. Et vu les articles 435 du Code de procèdure civile, 27 et 42 du décret du onze Juin 1809, pour signifier au défendeur le présent jugement, Commet Champion, l'un de ses huissiers audienciers. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire —
      </p>
     </div>
    </div>
    <div n="9" type="case">
     <opener>
      Dudit jour six Juin —
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Madame veuve Bernard, ouvrière demeurant à Joinville le Pont, près Paris, rue du canal, numéro onze ; Demanderesse ; Comparant ; D'une part ;
        </span>
        <span type="part1">
         Et Monsieur Bardin, fabricant plumassier, demeurant et domicilié à Joinville le Pont, rue des Réservoirs ; Défendeur ; Comparant ; D'une part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait = Par lettres du secrétaire du conseil de Prud'hommes du Département de la seine pour l'industrie des Tissus en dates des mardi vingt huit et Vendredi trente un Mai mil huit cent soixante dix huit la veuve Bernard fit citer Bardin comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Vendredi trente un Mai et Lundi trois Juin mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'elle entendait former contre lui devant le dit Conseil en paiement de la somme de vingt deux francs trente centimes qu'il lui doit pour salaire. Bardin n'ayant pas comparut la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six Juin par lettre du secrétaire du conseil en date du trois Juin mil huit cent soixante dix huit à la requête de la veuve Bernard Bardin comparut. A l'appel de la cause la veuve Bernard se présenta et conclut à ce qu'il plut au Bureau Général du conseil condamner à lui payer avec intérêts suivant la loi la somme de vingt deux francs trente centimes qu'il lui doit pour salaire, plus, telle indemnité qu'il plaira au conseil fixer pour perte de temps devant les Bureaux du conseil et les dépens.
      </p>
     </div>
     <div type="pointDeDroit">
      <p>
       Point de droits De son coté Bardin se présenta et conclut à ce qu'il plut au Bureau Général du conseil attendu que la veuve Bernard a quitté ses ateliers sans faire la huitaine d'usage ; Que d'ailleurs le travail dont elle reclame le prix ayant été par elle mal exécuté il ne lui a doit pas la façon ; Par ces motifs = dire la veuve Bernard non recevable en ses demandes, l'en débouter et la condamner aux dépens. Point de droit = Doit-on condamner Bazin à payer à la veuve Bernard la somme de vingt deux francs trente centimes pour travaux de son état ? Ou bien doit on pour les raisons invoquées par Bardin dire la veuve Bernard non recevable en sa demande, l'en débouter ? Que doit-il être statué à l'égard des dépens ?
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu les parties en leurs demandes et conclusions respectivement et en avoir délibéré conformément à la loi ; Attendu qu'il est constant que Bardin doit à la veuve Bernard la somme de vingt deux francs trente centimes pour travaux exécutés à la tache ; Que Bardin qui allègue de mal façon de ces travaux ne prouve pas et déclare ne plus pouvoir prouver ce qu'il avance ; Attendu en ce qui concerne le départ subit de la veuve Bernard des ateliers de Bardin que ce départ remonte à quinze jours sans que Bardin ait, depuis, fait la moindre démarche pour retenir ou faire rentrer son ouvrière, ce qui est considéré comme un consentement au moins tacite à son départ ; Attendu aussi que Bardin n'a pas comparu devant les Bureaux Particuliers et a par cette non comparution fait perdre du temps à la demanderesse qui en éprouve un préjudice dont il lui doit réparation par la somme de quatre francs ; Par ces motifs = Le Bureau Général jugeant en dernier ressort ; condamne Bardin à payer à la veuve Bernard la somme de vingt deux francs trente centimes qu'il lui doit pour salaire, plus une indemnité de quatre francs pour temps perdu ; Le condamne en outre aux dépens taxés et liquidés envers la demanderesse à la somme de un franc vingt centimes, et à celle dûe au Trésor Public pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le vingt neuf mots rayés Comme nuls Lecucq coût du présent jugement, la signification d'icelui et ses suites. Ainsi jugé les jour mois et an que dessus. Lecucq secrétaire —
      </p>
     </div>
    </div>
    <div n="10" type="case">
     <opener>
      Dudit jour six Juin —
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur Jean Lardet, ouvrier tisseur, demeurant à Gentilly près Paris, rue de la Comète, numéro cinq ; Demandeur ; Comparant ; D'une part ;
        </span>
        <span type="part2">
         Et Monsieur Bardin fabricant de tissus en plumes, demeurant et domicilié à Paris, Boulevart Saint Jacques, numéro cinquante un ; Défendeur ; Comparant ; D'autre part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait = Par lettres du secrétaire du Conseil de Prud'hommes du Département de la seine pour l'industrie des tissus en dates des Lundi treize et Mardi quatorze Mai mil huit cent soixante dix huit Lardet fit citer Bardin à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les Mardi quatorze et Vendredi dix sept Mai mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait former contre lui devant le dit conseil en paiement de primo, cinquante francs pour le salaire de cinq journé pour lui f dix journées par lui lui faites à raison de cinq francs par jour ; Secondo, de vingt deux francs cinquante centimes pour neuf journées à deux francs cinquante centimes du travail de la dame Lardet, sa femme ; Tertio, de celle de sept cents francs restant dûs sur le prix d'une machine par lui faite sur ses ordres. Bardin n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du conseil séant le Jeudi six Juin mil huit cent soixante dix huit Cité pour le dit jour six Juin par lettre du secrétaire du conseil en vingt trois Mai mil huit cent soixante dix huit et ajournée au Jeudi six Juin suivant mois. Suivant exploit de Champion, huissier à Paris, en date du vingt sept Mai mil huit cent soixante dix huit, visé pour timbre et Enregistré à Paris, le vingt huit Mai mil huit Cent soixante dix huit, débet deux francs quinze centimes, signé de feuillez Lardin fit citer Bardin à comparaître par devant le dit Conseil de Prud'hommes séant en Bureau Général pour le Jeudi six Juin mil huit cent soixante dix huit pour s'entendre condamner à lui payer la somme de huit cent soixante douze francs cinquante centimes se composant de primo, de cent cinquante francs pour indemnité d'un mois de congé, secondo vingt deux francs cinquate centimes pour solde de journies de la dame Tarset,et de set cents frocs pour prix Et d'une mécunque qui lui a été forrce par lu. A l’appel de la case partet se présenta et conclut à ce qu’il plut au Bureau Général du Conseil lui adjuger le bénifie des conclusions par lui préses dont la citalion exploit de chanmpion, lui près, enregistré et leondamner Bardin aux dépens. De son coté Bardin se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu qu’il n’a pas enfagé Lerder comme ouvrier ; Que s’il et veuu chez lui le st pour y travailler pour son compte à lui Berdin, mois bien pour son profe compte t à une mécuviue qu’il lui a vendue à contition qu’elle fontinse farilement a jec l’arde d’une somme, le qui expligéé commet la dame tordes entre chez lui aménée par son mari qui prétendaint l’employer à cette machine, perte qu'elle duz abondonner le mecanique ne pouvant fonctiner ; attendu ependanit qu à offre payer les vingt deux francs cinquante centimes prodit des neuf journéen de la dine portis oure que celte lore n'oil uis prodtuit ; Par ces motifs - Dire lomn ore vrdit necerlle en ses demandes, l'en débouter et le condamner ux dépens. Pint de troit - Dait-on condamner trédin à payer à erdit la somme de huit cent soirante douze frons cinquate centimes par de la congé d'un mois de torrtet talaire de la dame verder de pretseue ongra parée d’a du et n le que lis ouidiet pur et le e l ue le dtie Lertit non recevableon ses demandes l'endébouter e que de sus te se dui l un deis pus purte le parties en leurs demandes et conclusions respectivement et en avoir et de d pre ille Mrede en in atis ant dene pr e du deu et peur en de e sand eur eu e e de du et en tortie er e e ie purs ser hu tit en e e e a e orer ote e oni e aten u elest prs sn dt lite d e e pusils aeden omi en ent les ei e e e pe e con es les u an e ur de se ete e te dement deprei teuip r et e deds pue an ten pete. pu de u e te de se pu lese qi e E dé a 1835 des vngt deux francs cinquante centimes pour le sataire de la dame Pardr le Conseil n’a qu’à lui en donner aiteor de déclarer en compéteis pour le surplus ; Par ces motifs Le Bureau Général jugeant en premier ressort; donnea à Berstin de l’offre qu’il fait de payer à la jourre du conseil les vingt deux francs cinquante centimes qui lui da réclamés pour salaire de la dame Tardet. Le véutay on compétent pour coumaitre des entrer demandes de tavail qu'il reuvoie devant qui de troit et condamne lar dit u dépens taxés et liquidés enven le Trésor Public, à la soe de deix francs quinze centimes pour le papier timbré l’enregistrement de la citation, conformément à la loi à sept aoux mil huit cent cinquante euce, non compris le cout du présent jugement, la signification d’icelui et ses suites.
      </p>
     </div>
     <div type="judgement">
      <p>
       Ainsi jugé les jour mois et an que dessus. Dineu recucg létaire àQudit jour dix ving a Entre Mademoiselle Delasague, fille majeure, ouvrière demeurant à Paris, rue Sant Blonoré, numéro deux cens cinquante sept ; Demanderesse ; Comparant ; D’une par ; t Monsieur Chaput, Cutotiier, demeurant et domiée à Paris, rue de vanmes, numéro six ; Défendeur ; Défelle Dutre part ; Poit de fat - las tetres du peretire de linre de Prud’hommes du Département de la soine pour l’industrier des tissus en dates des vendredi trente un Mai et lundi treis Juin mil huit cent soixante dix huit la demoiselle Wéleve fit citer chaput à comparaître par devant le dit Conseil Prud’hommes sant en Bureont Particuliers les lundi trois et Mardi quatre juin mil huit, cest soixante dix fait pour se conctamner si fairre se pouvait sur la demande qu'elle se convais frrer contre lui devant le dit Conseil en paiement de la somme de Eix deux frans cinquante centimes qu’il lui doit pour salaré ae chapit l'ayar pas comparala cause pt renvogyée les oise Bureau Général du Conseil séant le jeudi six Juin milhi comparante dix huit. Cité par l dit jour li, pui fil ene MA uts secrétaire du Conseil en date du quatre juin mil huit cent soixante dix huit à la réquite de la demoiselle Delevaique chapat ne comparut pas. A l'appel de la cause la demoiselle delevarque le présenta et conclut à ce qu'il plut au Bureau Général du Conseil donner défut contre chapert non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui payer avec intérêts suivant la loi la somme de trente deux francs cinquante centimes qu’il lui doit pour salaire plus telte indemnité qu’il plaira au Conseil fixer pour perte de temps devant les Bureux du Conseil et les dépens.
      </p>
     </div>
     <div type="pointDeDroit">
      <p>
       Pont de droit — Dait-on donner défaut contre chapot non comparant en personne pour lui quoique dument appelé et pour le profit adjuger à la demanderelse les conclusion par elle précédemment prises ? Que doit il être statué à l’égard des dépens ? près avoir entendu la demoiselle Delevaique nss demandes et conclusions et en avoir délibéré conformément à la loi; Attendu que la demande de la demoiselle Dele vaique paroint juste et fondée ; Que d'ailleurs elle n'est pas contestée par Chapnt non comparant ni personne pour lu quoique dument appelé ; attendu qu’en ne comparuisant pas devait les Bureaux du Conseil chapat a causé à la demoiselle de le vaique un préjudice de perte de temps, ce qu'il doit éprur ; Par de pentipe. le Brnveu prat l jugen en nere ps ou larticle quarante un du decres du onze Jui mil huit cent el p re en eu te e réseu donne défaut contre Chapat non comparant ni personne pour lui quoique dument appelé ; Et adjugeant le profit du dit défaut ; condamne Chapat à payer avec intérêts suivant e et uidte nonean pen t e sei aun en ant le e e t u ten du pu deit tr n e poir en iq u lu, le da deme t en de e ere du de te u e per de tue nir t u ie s pen p pere lele eseq ei q sar de onier uie u e e de paser e e e de onte et preur ante te u uig cin eit nde er ani e purs de pur eu ait lapre nt endes tis p l jur mit ie risill an que dessus. H demait-entéra A Audit jour dix Juin Entre Monsieur Parcors, ouvrier tailleur, demeurant à Cave rue courne fort , numéro quarante trois ; Demandeur ; Comparant D'’une part ; Et Monsieur Aingélà Maitre tailleur s hatit demeurant et domicilié à Paris, Boulevart saint Martie numéro quatre ; Défendeur ; Comparant ; D'autre paryr e Point de fait - Par lettres du secrétaire du Conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus en dates deu Mardi quatorze et Vendredi dit sept Mai mil huit cent soixante dix huit siçon fit citer Engele à comparaître par devant le dit conseil de Prud’homme séant en Bureux Particu les vendredi dix sept et Vendredi vingt quatre eMai mil suit cent soixante dix huit pour se concilier si faire se pouvait pur la demande qu'il entendait former contre lui devant le dit Conseil en paiement de la somme de dix huit francs pour la façon d’iune vêtement. Sengéli n'ayant pas comparu la cause fut renvayée devant le Bureau Général du Conseil séant le jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six juin per lettre du secrétaire du Conseil en date du vingt cinq mai mil huit cent soixante dix huit à la réguate de Fiçon Angeti comparut. A l’appel de la cause, ficois se présenta et conclut à ce qu’il plut au Bureau Général du Conseil dun déde Angéti à lui payer avec intérêts suivant la loi la somme de dix hui francs qu’il lui doit pour salaire et le condamner ax dépens. De bon coté Angéli se présenta et conclut à ce qu'il plut au Bureau Général du conseil attendu que le vêtement du quel Lricois réclame le prix a été nil fait, ; quil etre d'ailleurs entre les moins pour le rataucher ; Par ces moff due Ricon non recevable en sa demande, le recevait reconventionnellement demandeur dire que lorsque le dit Bêtemenit sera rapporté rectifie le prix en ser payé condamner firois aux dépens. Pnt de droit — Doit on condamner cingélui à payer à Perrois la somme de doprns francs pour la façon d'une peerdessns. Ouben dutecn dire Ficon non recevable en sa demande, l'en débouter recevoir cnge li reconventionnellement demandent ; dire que préctableneunt au paiement du prix de dix huit francs deuit, sera tenu de rectifier le Baletot par de ssus qu'il a sitré mains ? Que doit-il être statué à l’épard des dépens Après avoir entendu les parties en leurs demandes et conclusions respectivement et en avoir délibéré conformément à la loi eu attendu qu'il résulte des explications farnies par lu oins a ud i s ité di l du r partiec que le par de pas qui fait l'objet du titnge a été trendu fait par Bicoin le vingt deux avril dernier ; que sil est ce jour entre les moins de Seiçon s’ess qu’angeta la loi à renvoyé àprir plus de trois semaines ; que la raison invaguée par Angelé qui aurait pu être humise à examen dans un de lai apportie ne pout plus l'être aujourd’hui ; Pir ces motifs Le Bureau Général jugeant en dernier retsort ; Condamne ingeli à le precdre pasression du par dessus et à en payer le prix defaçon par dix huit francs, reçoit angere dans la demande reconventionnelle, l'en déboute ; Le condamne aux dépens taxés et liquidés envers le demandeur à la somme de ufronc et à celle dui au Trésor Public, pour le papier timbré de la présente minute, coanformément à la loi du sept Août mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites Ainsi jugé les jour mois et an que dessus. ecusq secrétaire E mou e Leonq dudit jour six cui Eintre apiy courdait Cinsiq aune epiet du ete eri ui ee lete mon ls Demandeur ; Comparant ; D’une part ; L Monpeur iévred trdite. Mite hep e dement e quidte doui co lei de ete nte pasé ux, défard coupet de pasint a pe e e e e te or ae de t e ende eitese dur e renei de repue e pre a pus auet les e et ur e te eleure eu te e e poretr a e en e purs ite de u t e deen e po de p ren de e tes due e en emret en an qur e ci pen de daemar u i à L s LPr huit. Cité pour le dit jour dix juin par lettre du secrétaire du Conseil en date du vingt cinq mai mil huit cent soixante dix huit à préqute de hattrot cr comparut. l’appel de la cause Talleir, se présenta et conclut à ce qudie plut au Bureau Général du Conseil condamner lrcord à laie payer avec intérêts suivant la loi la somme de vingt francon réparation du dommage qu’il lui cansé en nel soumpart pour le douze Mai dernier ann qu’il ene avait, pris l’engugement et le condamner aux dépens. De son coté Brard se présenter et conclut à ce qu'il plut au Bureau Général du Conseil elle qu’il est d’usage qu'un Carron adrepé par Bureau se Placement pour faire ertra le Demonche porte sa cort la vendredi avant le mout ; Que fralliot que prétendque la carte lui aété de livrée trop tors le vendredi pour qu’il lu partet le même jour anrait, pu tant au mans le porter le samedi matin le bonteur, ce qui lui en lévite de faire sa causse de chez lui au Bureau se placement ; Que n’était venu le samedi qu'a près nde et aboors qu'il en avoir demand un autre il sne fut l'ocuper ; Par ces motifs — Dire paltier non recevable en sa demande, l'en débouter comme mal foncéi elle et condamner ax dépens. Pont de trait — Doit -on condamner Ernér à payer à Gaticot la somme de vingt frencs poar Eademité de non éxéccution de convention verbale de travail Ou bien doit-on dire pultiot non recevable en sa demande, l'un débouter ? Que doit il être statué à l’égard des dépens . Lprés d avoir entendu les parties en leurs demandes et conclusions respectivement et en avoir délibéré conformément à la loi ; attendu qu’il est d'usage constant que l'ouvrier envoyé par le Bureau de placement poir faire l’attre du Demonire porte la cost de dit Bureau de vendredi avant cinq heures ; attendu qu'il est constant que aaltcct ou dit -l n’a pu se présenter au domicile ? mois le vendredi le Bureau de Clacement lui ayant donné la tarte trope ; Pard s'esx présenté le samedi aper tors pour quatifier l’inqudte du défendeur et le précantion qu’il a prise de seproier n eemet garan ; Qu'on ce cas pellait n'it pas foire dans sa demande en indemnité ; Pur ces motifs - Le Bureau Général jugeant en dernir lessart ; Et galtrit non recevable en sa demandes l’en detré Le condamne ex dépens envers le Trésor Public pour le papier timbrée de la présente minute, conformément à la loi du segt du oir lu cent cinquante, once, non compris le cout du présent jugement ; qa sigification d’icelu et ses sutes. Cin vuit les jourmis euf laiceu Lecuiq secrétaire nmces M
      </p>
     </div>
    </div>
    <div n="11" type="case">
     <opener>
      Du dit jour dix Plinau Entré onsieur jandron fabricant chemisier demeurant à Paris, rue du quatte septomtre, numéro quinze Défendeur ou primipal ; Demandeur epposant ; Défaillant ; D’une part ; Le Monseur Wébot cupeur chemisier, demeurant à Pasis, ue Villedo, numéro cinq Demandeur pronpal; Défendeur opposent ; comparant ; D'autre part ; cont de fait — Pur jugement Lendu par défaut par le conseil se Prud’hommes du Département de la Seine pour l’industrie des tissus le onze avril mil huit cent soixante dix huit, enregistré, Lunaron a été condamne à payer à Péot la somme de sept cent quatre vingt cinq trongs de pemitul et les accelure . Enqunte fit signifie à lundron par exploit de cnempron, lui prer a Ruris, en date du huit Mai mil huit cent soixante, dix tuit, 'agate à la réguéte de litetà Suivant exploit de Blnand comprar à Paris, en date du dix Mai mil huit cent paranti dix huit, vigé pour timbre et enregistré à Paris, le ouze Mai mil huit cent sorixmante dix huit. débet deux francs quinze centimes, signé de Pouiller, landion formé opposation à ce jugement et tta Citel à comparaitre par devant le dit Conseil de Prud’homes sige louveiliedt le ut su du e urecln d dix huit pour la voir recevoir opposent à c jugement, lvour dé charger des condamnetions contre lui prenuyé en peisnper et ruce frrires, s'entendre, le dit Pétel, délareer non recevablle uteant er e e lu es on eps p du it e tes dei lt e le e e demenra se dmenvare du ete e er it e e e det t e d e ine cdu pret et erdlit e lun e ue ru e er te pe ele t oesin auprse r e te l e u t eie ae ren ter e d es u erete enden e e e q ir e rei que eme de ense ae e e de ent lu it ue en p de e de tente quee e e e e e on e oaire du d prte e es ue oui l u domnaps pour puti ar de p sree et et e en a e le ue de s ui de de e preer dl s MI q entendu Rotaf en ses demandes et conclusions et en avoir délibéré conformément à la loi ; Attendu que les demandes de setet paraissent justes et fondées ; Que d'ailleurs elles ne sont pas contestées par landon non comparant ni personne pour lui quoique demendeur opposat ; Attendu que cette non comparation dit été consilérée comme on atandans l’oppontion . Par es motifs - Le Bureau Général jugeant on fermerer répsort, on la forme mois Landon on de appolétion au jugement rendu contre lui le onze avril dernier senregistré, l'en déboute comme mal fonné en ccelle. Ordonne l’exécution pure et sumple du jugement auquel est oppolétior et condamne landon ax dépens taxés et liquidés en vers Le Trésor Public, à la somme de deux francs quinze centimes pour le papier timbré et l’enregistrement de l’appention, conformément à la loi du sept cox mil huit cent cinquante, ence, non compris la cous du présent jugement, la signification d'icelui et ses suites. Ansi jugé les jour mois et an que dessus. Derucq secrétaire Dmai t d tn a Mudit jour six viiq u
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur Levonte, anvner lapeemntier, demeurant à Boles, au rondipement de Clermont leisa j Demandur, Comperant ; D'une part ;
        </span>
        <span type="part1">
         Et Monpeur Curtet Jeun Marie, ouvrier vailleur d’hobits, demeurant à Paris, passage Montes quien, numéro cinq ; Demander ; Comparant ; D’une part ;
        </span>
        L Monsieur Lacembe,; tute dailleur d’habits, demeurant et domicilié à Paris rue lai sa numéro quinze ; Défendeur ; Comparant ; Dutelurt, Paint de fait - Par lettres du secrétaire du Conseil de srur ha du Département de la Seine pour l’industrie des tissus en dtates des Vendredi trente un Mai et lundi trois juin mil huit cent soixante dix huit, curtet fit citer Lacombe à Comparatre par devant ledit conseil de Prud’homme séant en Bureaux Particuliers les emble trois et Mardi quatre Cin mil huit cent soixante dix huit pour sin concilier à faire se pouvait sur la demande qu’il entendit forixen contre lui devant le dit Conseil en paiement de la somme de Pi de sit es arés nil Mn ec e Dre q u 2 i ié a q i i q du soixante denx francs pour selaire. La coulée n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant jeudi six Juin mil huit cent soixante dix huit. Cité pour le dit jour six pis par lettre du secrétairé en date du quatre juin mil huit ent soixante dix huit à la réquité de Cortet Lecomle comparut; A l'appel de la cause Curet se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner lacole à lui payer avec intérêts suivant la loi la somme de soinante deux francs qu'il lui doit pour prix de travaux de son état et le condonner aux dépens. De son coté Laconbe se présenta et conclut à ce q’il plut au Bureau Général du Conseil ettendu qu'il reconnit devoir la soume de soixante deux francs ; mois attendu qu'an dêtenant le fière qu'il encère entre les moins turtet lui ceuse préjudice Luisque cette frèce n’ayant pa être livrée en temps la cestire pour comple Par ces motifs — Lire Cortet untucevable, en rademand e tavu débouter eile condamner aux dépens. foit de doit - Doit on condamner Lacoute à payer à cortets la somme de soixante de soixante deux froncs contre la remisé d’un ditent qu'il salire à Paton dit en du leis reant lut la demande ; l'en débouter ecevoir Lécombe reonvou tionnellement demandent ; Condamne Tortet a remitre le vêtement en psus eu disitre ou mis d sant au eu du du sent pu tie ueapsite d etito u ell t tent lerse lu de lete de e e e t l peiue en leurs demandes et conclusions repeetivement et encvord et e t te e te ite u ede ur tn du t esur purt la pre des ioise les our are e e e e t due a ler s r se te e dte t e poi i en e p e ten u reit e de u te enpe ente e pe de iq ount quilt eten de ses uee de de e titer duit en ue se andurs deuen der de aun u ant e e reie uar e e t enede tain e ti qu iq e te see inps e Mée minute, Cnformément à la loi du sept aût mil huit cent cinquante rue, non compris le cout du présent jugement, la signification d’iceluit et ses suites.
       </rs>
      </p>
     </div>
     <div type="judgement">
      <p>
       Ainsi jugé les jour, mois et an que dessus. anaus Lecusq secrétaire te u dit jour six Piig linre Monsieur Langer, ouvrier tordonner, demeurant à Paris, rue saint Honoré, numéro cent quarante neuf ; Demandeur Comparant ; D’une part ; A Madame veuve Denis, demeurant lais, rue le la Roguette, numéro trente six ; Défenderesse Défaillent ; D'utre part ; Point de fat – Poirant emplot dà Chau hurs, huipser à Paris, en date du vingt neuf Mai mil huit cent sorxante dix, huit, visé por tioibre et enregistré à Paris, le trente en Mai mil huit cent soixante dix huit débet quatre francs cinquante cinq centimes, signé à de Seuilles, Leufer a signifie et soifsé de prè à la veuve Dons, frimo dun jugement endu par le conseil de Prud'hommes du Département de la Seine pour l’industrie des tissus le vingt un ferrier dernier enregistré; portant condamnetion contre le sieur Mllard, Cordonnier, demourant à Paris, a ceta Roquette, numéro trente six de la somme de cent trente trois francs de priniipal es intérêts et dépens liquidés ; Lecondo de la significtion à lui faite de ce jugemeit ; ot par exploit du ministère du dit uipier en date du seize Mad, enregistre ; Lé, D’'un autre jugement rendu sar le même Conseil le vint un Moit mil huit cent soixante dix huit, enregestre, d'éboutant le toir ltard de l’apposstion par ler formé du dit jugement du vingt un fevreer derier le condamner aux décens. CQ de la significtion à lui faite par exploit du tonne hupier en date du vingt cinq avril mil huit cent soixante dix tuit enregistré L? Du homme tant qui lui a été signifié par exploiy du même Loupier, en date du vingt sept avril dernier, enregistre. u ufin de la tontetire de saisse qui a été fate par exploit du ceit puipier endate du vingt quatre Mai courent, enregistrée eeuel lemême exploit donne siguation à la dite dame veuve Moui à comparaître le six Juin mil huit cent soixante dix huit jaut cinq de retevéé à l’adance et par devant Mappouns les meutres cn francs le compet de Prud’homme par l’industrie des tiss puis q u e six neis il les quel sai lene hil ecu t qu'il est creancier de la somme de cent trente trois francs pour prix de travoil et que le conseil par jugement du vingt un fevrer dernier condamne Celtard qu l’en couveait comne patron de l’étotlépement et qui ne serait, que ferant ; Attendu que ce dernier à jeune appention à e jugement et que le conseil l'a déboute de son appention et condame aux dépens par entre jugement de vingt un pars , que par suite de ce socent jugement fit gulement signifié, comme demant lui fit doané et que lorsqu'en voulit prétijuer la saisse des dpet matiliers et merchardon et qurnmpant la Bouttique le sieur ltard se trouvaut installé dans la dite Boulique s'y oppose étendant que les moilles et marchandiser étaeut, la proprité de Madame veuve Denis plntée comme cordonnnère à façon avence de la aquette, nmr trente six; qu'elle qétait imposée et que le loyer était également au nom de la dle dane ; Attendu que la dite dame Denis estpurente du sieur ellard, d'elle esnt en service et n’hibité en acue meme le fond de commet ce de chapurer exploité par le sieur, ellardes que d’ailleurs l’étabtifs enont dans répoudre du selaire des ouvriers etgltant proprrétaire de l'établifement elle duits être trnue au priemer de son la laire dant le dit établissement à profité ; Par ces motifs e taut entrer à de duvré en temps et lieu ententre la dite veuve Donis, déclarer commun à e avec ette es jugement rendut par le conseil de Prud’homme de la Seine pour l’industréie des tissus les vingt en e e e e e e a les leenie la demet e poer de e se fouir e pau t peur cites n e preésir te de el de pome de quen e e e den pe u deu un deu is cete d ape nant e empre ecin eur de dte sen en an que te ere pu de n e u le qu eit te te e eu e u en tense e de p en e nt eue en eu e e e u e prer pu demen de es qur de t ete l praunt pur au e ait e n n entene rprete dea enan de fer ede t e pement e de p den e e de pet en e e e e e e et mrte enre t e pageur en prenier le part ; Donne défaut contre la veuve Douis non comparant à personne pour elle quoique dument appelé. L adjugeant le profit du dit défaut ; Dit commu à la veuve Devis les jugements ren dus par le conseil les vingt un ferrier et ving un Mars mil huit cent soixante dix huit contre ellard, En conséquence ordonne l’exécution de sa jugement aussi bien contre la veuve Denis que contre lauder ensemble, prinmipet intérêts et frais Condamne la veuve Douée aux dépens taxés et liquidés envers le demandeur à la somme d sept francs soixante dix centimes et à celle de quatre francs cinquante cinq centimes envers le Trésor Public pour le papier timbré et l’enregistrement de la critation, conformément à la loi du sept aout mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites. Et vu les articles 435 du code de procédure ciile 27 42 du décret du onze juin 18099 pour signifier à ca défenderesse le présent jugement, commet Chanpion, l'un de ses huipsers audienciers. Ainsi jugé les jour mois et an que dessus. Durois l Lecusq secrétaire Quit jour six quiz Entre Monfieur andre Gugat, ouvrier passementier demeurant à Paris, rue Délettre, numéro douze et quclire Demandeur ; Comparant ; D’une part ; eE Monsieur Caquelt, fabricant papementier, demeurant et domicilié à Paris, bue des Cascusés, nunééro cinquante trois ; Défendeur ; Comprut D'autre part ; Point-de fait — Par lettre du secrétaire qu londe de Prud’hommes du Département de la Seine pour l’industrie des tissus en date du Mardi vingt un Maii mil huit ce parant dix huit crayot fit citer la hur comparaître par devant le jré Conseil de Prud’hommes séant en Bureau Particulier le vendei vingt uatre Mai mil huit cent soixante dix huit pour setisien, nse faure se pavit sur la demendes lel rntieles pouer cesie deans le dit Conseil en faiement de la somme de quatre vin ffnc ue pour indemnité de perte de temps et de renvoi n tompé A l'appel de la cause puret se présenta ea rxpusé le conil is e h lille e nei pet n de son coté taner se présenta et exposa u Conseil que quont il fit attendre Ganot il l’indemnise; que sil l'a conseré tout E à coup sius l’il lui fit un travail necuptette. Le Bnteun Particulier renvoya la cause devant un mimbre du Conseil à ce connaissant Devant ce membre les partis ne purent être conciliées à la cause en cet dot fut renvoyée devant le Bureau Général du Conseil séant le jeudi six juin mil huit cent soixante dix huit. Sénnt, ce memle de coseil Cité pour le dit pour six juin par lettre du secrétaire du conseil en date du vingt sept Mai mil huit cent soixante dex huit à la renuute de huint la quet comparut. A l’appel de la cause ngit le pémmta le et conclut à ce qu’il plut au Bureau Général de Conseil attendu qu'en lui fait ant attendre des matières laquet lui a causé un préjudice de perte de temps dont ià lui doit épusition qu'un le curetient instantemment, nonmbstant la prreusicque lui avoir fite Pahuet père de lai donner un curgement une fuit celui entrois terminé il lui cutse en odre préjéudice pour le uil réparition lui etéralement due Per ces motifis condamner laguet à lài payer avec intérêts suaint la loi la por de qute eit de ponr itle e n qe elesese t landeme aux des e rerle hui t pae aunt e de en ende e enit e e ouerent ce ant e uit doen lens te depr du le u demrantene sepen te u entente pauis et de ure prs et ie a epre pu t unt en enterdete, o ete du peraente t e den lu tee t ue te p e es; ciqui der te il qu tene que nte de en put aper du onte deu puremet mer e e e t qu eu de e de e adums en apre eneie e tur due ei qu de sir de e r de e er adean rand eux er n ei ent uen et es e en en entie ante e n somie aem er e e t la P pl dir sept reuvai etdous mols rayes nulsrepprou Lecucs pe à ayant fut in menvas travail n'y avaix aucun dait ; Qu'ed reitaud même que Caquet, père lui ont promis un chargement ; c qu'il était toujours lacuttotif onu fils de repuser puir qu’il est cesand patron ayant antorité pour diriger son traval, l’expérience fot de son ineapoité sofras à elle seute pour en pastifes T’neote Par ces motifs - Le Bureau Général jugeant, en dernier réport ; Dit Gayot non recevable en ses demandes , l'en déboute, ee la condamne aux dépens envers le Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept aout, mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites. Ainsi jugé les jour, mois et an que dessus. ere ue D l Lecusq secrétaire
      </p>
     </div>
    </div>
    <div n="12" type="case">
     <opener>
      Du dit jour dix Pinga
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur Pant laguet, ouvrier passementier, demeurant à Paris, rue Grat prolougé, numéro deux ; Demandeur Comparant ; D'une part ;
        </span>
        <span type="part2">
         L Madame Marguerite Cetime Caurant; épouse du sieur Louis Bertail, comme commeradement sous le nom de dame Bouguut, fabricante de paysementite demeurant et domiciliée à Paris, Boulevart de sepastapes, numéro quatre vnt dix huit ; Défenderesse ; Comparant ; D’autre part ;
        </span>
        Ponit, de fait - Suivant xploit de champiin, suipcès à Pariseuns date du premier juin mil huit cent soixante dix huit, visé pour timbe enregistré à Paris, le trois juin mil huit cent sixute dix huit, cen dies ex paur pareite cinge tentioes, leigner duafeite sapit à mocilie et toiféé sapré à la dame Mésheurat, enne Rortel preme d’une jugement rendu par le conseil de Prud'homme du Département de la Seine pour l’industrie des tissus du vingtenc avril dernier, enregistré, portait condamnition à son profit cantre la dame veuve ongeautt, fabricante de lassemens ent Boulevart sétastept, numéro quatre vingt dix huit, le coti francs de princifut pour salaire ed indemnité, plus ces intérêts den dépens. setondu de la signficstion de ce jugement par explioito dit chaznpion, luipsierà en date du nzt duvne dernier uagés tertie du commen dement de payer pais par etre enprisudice du ize dux de ent s tereit, duten de eu de larsse fuite à la maison Bougerants le vingt sept Mai dernier et dans laquelle Madame Bortal aà fat conmme qu’il n'y avair pas de Madame Bougeault et qu'elle édtas la propicétaire de la fabrique de passementerie ; Et par se même exploit à cité le dame Bertail sus nomme et le pour son mori pour la volidilé à comparaître par devant Mépieurs les Président y Dembres comparut le Bureau Général du Conseil de Prud’hommes lu l’industrie des tissus séant en Bureau Général le jeudi six juin mil huit cent soixante dix huit pour attendu qu'il travaille pour le compte des la maisou de fabrique dont elle est profiéitaire ; Que ne pouvant abtenir paiemint des salairer à lui dos il a du l'appelés e devant le conseil de Prud’hommes ; que ne conné saut par le noin qu'elle dit avoir de dame Mertail, il l’a fait citer au nom de dune Bougeault, nom sous lequel elle en génralement et commméuelement compme depuis lind temps tant avant que depuis le décis de Monsieur Bougeaut ; Attendu que sonte nomil a obétenu contre la dite dame run jugement la condamner à lui payer. las ux pour de ti eent est p e fous jugeant t l e du p prdu. aor d en des p pu de i u sier paiement de se part ; Que seulevant lorser en a voula xcuter pravant prer la de de nt pra ten e le ces de dure po lit cent er lur e ome e ie su e e te ten eui eun es puir et pur e en tonlaet tere de en eset du lis ae te e sasemet dan e e e t le sour dun preut e desthuit juemeint itre l due contit sre leee eense an eaent e s e t d reier de de sende demeant pour ut den e demante ce meies ede e e oent n gen e e e ee ei t eu eu e sur aer e e e on puret au e e u te puer ente e ei qu pene rdet u se e deier pesepor ce et de ete qu se e ete u t enc li utie a dermant poen ent que M q o Drdr t somme au dit Caquet ui lui a bien son mis des ichantillens dans le lut d’avoir des commander, mois auquel elle n’avant commandé ; Par ces motifs - Dire laquet non recevable ses demandes, l'en débouter comme mal fondé en rcelle ste condamner aux dépens. tont de troit — Doit on dire applecitte à la dame Gertail le jugement rendu le vingt cinq avril dernier, enregistré, contre la veuve Boureau Gérnale conséquence condamner la dame Marguerite celine Lonvan comme Bertail à payer à Caquet les sommes portées Dat jugement ? Ou bien dait-on dire taquet non recevable en ses demandes, l'en débouter ; Que doit-il être statué à l’égard des frais et dépens ?
       </rs>
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu les parties en leurs demandes et conclusions respectivement e un avoir délibéré conformément à la loi ; Attendu qu'ile eit constant que la dame Cgeent somme Bortailà accepté de cahuit un ichontitton par lui fait ; lui a donne ces matières, nufrancs pour confectionner des refrences de ce même acpontillon de chargements sur deux metiers aupris de cinquante francs des cent mètres ; que l’exéution de cet engugeant n’a pas été timplié par la same lo jaurent parce qu’elle troiuve plus avantageux de la faire exeiter par un autre ouvrier quine pris que quarante cinq francs des cent mètres, ce qu'il pouvait faire n'ayant eux aucune peure decréation mancun frans d'ichobittonnege ; Qu'et résulte de ce fait indeliés en lui même que caquet à éprouve un préjudice dant la dame lTousen lui doit réparation par la somme de soixante dix francs Attendu que la dame lavarent qui avait en des rapports de travail avoi Cahuet savois, bien que les lettres de conciliation et de citation la visaiens, encore qu'elles fassent adre présse la dame Bougeantt, nom auquet etle répous d’ailleurs comnuilti Qu'n celas elle estpassible de tout les prois qui ont été fots à duision du jugement. Pir ces motifs - Le Bureau Général jugeant en ternier lepart ; Dit applicible à la dame Marguer eline Coureit, somme Bortail, le jugement tendu las le Conseil le vingt cinq avril dernier contre la veuve Bauquuitt ; ordonne l’exécution de ce jugement contre la dite dame Bertaldeu Marguerite le ué Ceurent sisqu’à concusrenre de sois otes froncs de principal; condamne la dame Bertailt éellergt celine Ceurent aix frans et dépens de la première instant a plutat de l'oidlance desant la veuve Boureautt, commé à 'euf, de la présente instn tas et liquidés en vers le demanders e la somme de Cinq francs quatte hunigt quinze centims uit E celle de deux dix frans soixante quinze centimes envers le Trésor Public pour le papier timbré et l'enregistrement de la citation ; Conformément à la loi du sept aout mil huit cent cinquante, ence, non compris le cout du présent jugement, la sinification d’icelui et ses suites . Aini jugé es jour mois et an que dessus. Lecucq secrétaire dmeau
      </p>
     </div>
    </div>
    <div n="13" type="case">
     <opener>
      Du dit jour dix Jing domant e
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        Entre Mademoiselle Désirée Sanson, se disont ouvrière contrière demeurant à Paris, rue Fesnnier, numéro dix Demanderesle ; Comparant ; D’une dort ; t Monsieur et Madame Magnen, le sieur Magnen tant en son nom personnel que pur epaiter et autoriser la dame son épouse, Mantrelte contirre, demeurant et domiciliés, entembrle, le dits épout à Paris, avenie des taris, nméré qatre suit cinq Défendeurs ; Comparant ; D’autre part ; Poit de fit Par lettre du secrétaire du Conseil de Prud’homme du Dépastement de la Seine pour l'industrie des tissus en dates des Mercredi vingt neuf a vendredi trente Mai mil huit cent huit li e t le te de se le e u dapers prde ts tel a sid en ue u Anseu té te eontritat d u umes e les epord s le er en p p t ente unde ete dais poir tetre qunt les e e pre s la o e e en per due mne u deant ens un epe e pe le deu le deur du ti ens ene n en pement dei e e der e paur edu due au de juemne ede t e e e e es lute ede e e que der eun, duedoint e e e e e e en e se e eit e aen e den e eser pr e enqu les Bourrnie de se sonfais ; que si elle lui à enteigne grctutenant le protique de la cuture s'eit qu'elle la veutoir endre expabla de complir l'umploi d’une smme de ciembre au lien de cettedeu borne d’enfans qu'elle avait occupée jusque la ; Que s'elle sa nourrie c’esrt qu'elle prit, l'engagement de lui payer sa nouriter par ena somme de se francs int ciq cntiomes pas jounr qu'en sai elle est sa débitrie et hou se trénière poureni et e ecinq frcès de l’appele devant Monsieur lejage de Pair la su arcondipement qui les ontorisé à détenir sa malle jusque jour ou elle de serapoiquittée envers eur ; Par ces motifs — Le déclarer en conpétut pour comaitre de la demande la demoiselle Sandon, l'en débouter et la condamner aux dépens. Deton coté la demoiselle San son se présenta et conclut à ce qu’il plaut au Bureau Général du conseil attendu que la dame Jourson l’a occuhze pendont cinq mois à des travaux de contire après quoi elle lui delivra un certfiret dant lequel elle est qualitée d’ouvrière conturière ; Par ces motifs — Dire les époux Mégaenu non recevables enteur demante ; les en débouter. Le déclarer compétent, retenir la cause et oedonner qu’il sera explique sur le fand et condamner les époux Magnon aux dépens. Point de troit - Doit-on se déclarer on complent pour comastre de la demande de la demoiselle sanson? Ou bien doit-on de déclarer compétent, retenir la cause et ordonner que les parties seront tenue de s'expliquer sur le fundi pour être statué ce qu'il appartiendeu?? Que doit-il être statué à l'égard es dépens ? ?
       </rs>
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu les parties en leurs demantrs et conclussons sur le décliniture posé par les époux Mauven et en avoir délibéré conformément à la loi ; Attendu queles eeplications foaries par les parties se part que la dame Moaye n'oit pas maitrepa conturière ; que la demoiselle Sansalle nuné n'est pas ouvrière conterrière ; Que se les époux Magaen s'ont reçue chez euy et ti la dame Magnon lui a enjugné des étements de conture la a danne un certificaton caprits a por deurt l lag loes le pe de u ait e p e somme de chembre qu'elle occupée en ce monent ; Qu'en recosté Conseil n’a pas à conmetre des faits cét de leurs sap parts ; u det ces motifs - Le Bureau Général jugeant en tremier ressort, l déclare n compétent pour commêtre de la demansle le lademoie saeison et la ouvrie devant que de troit ; Condamne le demable Sans on ux dépous envers le Trésor Public, pour le papier timbré prente ille, conformément à la loi du sgt ui muil luiét in ene non comprès le coiut, du présent jugemen q ui ue d’icelui et serpuiltés. Ainsi jugé les jour, mois et an que depas. ecusq serétaire Vergut du dit jour dix vi ntre Monsieur et Madame Potits, le sieur Petit huit en son nom personnel que pour eprriter etutoriser la dame son épouse ouvrière Conturière demeurant ensemble, les dits époux, à Paris, rue du Potian, numéro vingt treit Demandeurs ; Comparant ; D'une part ; Et Monsieur pus Foirser aptécien demeurantà Pains, rue Tris, numéro suite seize ; Défendeur ; Défaillas ; D'autre sart , Pont de fait à Suivent employ de champren lu pier à laris, en date du Vingt huit Mai mil huit let soixante dix huit, visé pour timbre et enregistré à Paris, le vingt nup Mai mil huit cent soixante dix huit déber trois frencs trente cinq centimes, signé saprit la per e pes e e silui doite oen de en te seur du Département de la Sene pour l’industrie du tissus le vingt cinq avril mil huit cent soixante dix huit, enregistré porteu tindemnitions ate de per titene eui en den de setie conturière, de la somme de quarante six francs de pnpl te gu eude ni aug eu t poux jugente e pu eom a e e e our enper e de en l prése e du ei es suite o an tin de n e apres ut en axé dt pe se asseren e eente qul domies dner pus u ue ctise hu etes un de e t nonet ue le sete e e n e te en de ent e d e t e et deratur ent ei en es eps ete soment de ur au de e pre Sar reuvri et quate mits aages nmoils apprenti quinze centimes plus à quatre franc d'indeunité pour temps perdu; attendu que la signification du jugement et le comma dement oit ile faits conformément à la loi ee que la centetire de saisse faute le vingt un Mai dernner Monsieur chailler à cit s’opposer à la same rétendant que tous les otéêts garnépant les dits tient lui ont été lu par Monpeur pales Fénier, aptrien à Pains rue de six, numéro sege, quoique sur la prte de l’appertement de trouve eeployi portant le non de sonnmeller ; attendu que le siour Jules qovrent n't qu’un prété nom et que même s'il était propriétaire de l’étabtifs éneux de conture enistant actuellement, passage la fersien numéro douze il doit être tru au paieme du salaire du pe ouvrières employé dans l’établifsement. Par ces motifs et tousunti à déclaire nltérieuremet, poir déclarer commun avei lui, sieuf juler Foirier le jugement rendu par le conseil le vingt cinq ane dernier, enregistré. L'entendre en conséquece condamner à leur payer la somme de quarante six francs quatre vingt quinze centimes ncoutret on la pitet des condamnations prinoncées, et s'entendre en outre cendmn entans les dépens et frois du jugement de premier instance, et ux francs d’exécution qui l'ont seive et, en outre, Fontandre condamner entans les frois du jugement commen . Al’appel de la canse jule De leur coté les époux Pobit se présentèrent Férier ne comporut pas. et conclurent à ce qu’il plut au Bureau Général du Conseil donner défaut, contre Jiter Vocrier non comparant ni personne pour lui quoique dument appelé et pour le profit leut adjuger le bénmipier d Cconclusions par eux prises dans la citation euploit de chaupsaux huipier, enregistré. Paint de tait — Pait-on donner défaut contre Jules Fvrier non comparant ni personne pour lui quoique due appelé et pour le profit adjuger aux demandeurs les conclusions par ux es précédemment prises ? Que doit-il être statué à l'égard des dépens ? près avoir entendu les époux Potit en leurs demadés et conclusions en avoir délibéré counformément à la loi ; Attendu que les demandersse époux Pabit proipanrt justis et fondées ; Que d'ailleurs elles nevont pas contestées par pales Février non comparant ni personne pour lui quoique dument appelé ; Par ce motifs L Bureau Général jugeant e en premier le part ; Dit commun à pules Février se jugement rendefat le Conseil le vingt cinq uvreil dernie, enregistré, contre les époux ar ou chnelles ; in conséquence ordonne l’execution de le jugement aiquants a pis lour contre te le lo vrer eu ete ur ou lckieller fait pour le principal des condamnetions pu prir e intérêts et frais de pour suites faites à l’arcusion de ce jugement; condomne polur Favier aux dépsouns de la préente eat ie oaie nlégu de anver les demandeures à la somme de cinq franq uai qi cinq centimes et à celle de trois frans trente cinq centimes envers le Trésor Public, pour le papier timbré et l'enregistrement de la citation, conformément à la loi du sept août mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites. Ainsi jugé les jour, mois et an que desus. ecusq secrétaire à Emier D t
      </p>
     </div>
    </div>
    <div n="1" type="courtHearing">
     <p>
      Audience Du leudi treize Jng mil huit cent soixante dine huit Siégeuant : Messeurs Bagor, Prud houre Présidant lotoutience, en remplaiement de Mecheure Marienvel et snaud Présitent et Vère président du conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus empechis, Deroyn, Menier Berthemer et Cusse, Prud'hommes epistée de Monsieur e Decucq, brétare du dit Consie Etre épens Poirit, viner papmentier demeurant à Paris, Toulevart de la pgare, mro cent cinquante àn dee ; Demadeur ; Comparant ; D’une part ; Monsieur Dutr fabricant pasementier, demeurant et domclié à det, rue trtize, oummér prt ; Ptatie ples ’eseps ; E if onte e deuit se larde dt lu émin e e e ou Seine pour l’industrie des tissus en dates des Jeudi six et e de pue l e poe oeur cute da pat pus et le cicis demen enten de e e e e e e e te de eperte d it du ten e ui quet e sen t ts pur d t endeate nteme t pou d enes pus qutle ue eu e e e e er e e e e enr ape e desen ueti quige otr cit du cint qu7il du dite relere dumeiet ur de en te iquen e e e p t e se e pe n en anp les ene entisqnti a d e m t en ei se dman a prs eut due ou de pe ur ue des ai su euse é i o r os ? A Li quei Point de droit — Doit-on donner défaut contre Doit on comparant ni personne pour lui quoidue dument appelée ;t pour le profit adouger au enteindeur les conclusions puar lai précédemment prises ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu Régat en ses demendre et conclusions et en avoir délibéré conformément à la loi Attendu que la demande de igar parait juste et fonéée Qque d’ailleurs ele nen pas contestée par Dety n on comparante ni personne pour lui quoique dument appelé ; Attendu qu’il ne comparuépanut pas devant les Bureaux du Conseil Diite à causé à Parat un préjudice de perte de temps, cequ'il doit répurer . Par es motifs - le Bureau Général jugte en dernier repart ; Pu l'article quarante un du decret di conze juin mil huit cent neuf, portant renseuvent por leur conseil de Prud’hommes ; Donne défaut, contre Dieta non comparant ni personne pour lui quoique dument appelé ;Et adjugeant le profit du dit défaut ; condamne Deté à payer avec intérêts suivant la loi à Slgat, la somme de quatante deux francs qu'il lui doit pour salaire, plus une indemnité de six francs pour temps perdu ; Le condamne en outre eux dépns taxés et liquidés envers le demandeur à la somede un franc, et à celle due au Trésor Public, pour le papier timbré de la présente minute , conformément à la loi du sept août mil huit cent cinquante, ence, non compris le cout, du présente jugement la signification d’icelui t ses suites à et vu les entorles 435 du code de procédure civile, 27 et 42 du secret du onze juin 18099 pour sinoifier au défendeur le présent jugement Conmle Cl umpron jun de ses huissies audienciers. Ainsi jugé les jour mois et an que dessus. gagoffiet Lecucq secrétaire Qusi jour cingt siuiq Entre Monsieur Denis Mocke, demeurant à Paris,ue Brzfo numéro vingt deux agistant en nom et comme a demandete nt de la personet des tiens de sa fille mineure Couist, ouvrere cantiriée ; Demandeur ; Comparant ; D'une part ; Mademoiselle Celine Jainq Liéclain, l’atupe contire e demeurant et domiciliée à Paris, rue de la champée ? Pontin ; numéro quinze ; Défenderesse ; Défaillant ; D’autre part ; Pains de fait - Par lettres du secrétaire du Conseil de Prud’hommes du Sépartement de la Seine pour l’industrie des tissus en dates des Mardi quatre et vendredi sept juin mil huit cent soixante dix cent pour se concilier si faire se pouvait sur la demande qu’il entendait former contrerlle devant le dit conseil en paiement de la somme de dix francs trente cinq centimes qu'elle rui doit pour prix de travaux de sa fille louise. La demoiselle Lievain n’ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi treinze jun mil huit cent soixante dix huit Cités pour le dit jour snze juin par lettre du secrétaire du Conseil en date du onze Juin mil huit cent soixante six heaig à la requete de ock la demoiselle Gerain ne comparut pas. Le son coté ole se présenta et conclut, à ce qu’il plut au Bureau Général du Conseil donner défaut contre la demoiselle Térainn non comparante ni personne pour elle quoique dument appelée et pour le profit la condamner à lui payer avec intérêtts sivant la loi aa somme de dix francs trente luiq centimes qu'elle lui daoit pour solde du frix de travau de sa fille vnisi pas, cette indemnité qu'il plaira au Conseil fixer par perté de temps devant la peu e e se rnt eune Negcles Cntre puen eaps tete la deme le tr oin en o prs puesen pur le pi de t n pifi ne ndes au demeide lrte er purse premnen pel pu de de pur uer du te e e e e e enier rete e prde e e t emre le e resq t e des e desipu e u e deme peur qpur em e te e de sen t ps p ur per e ou es a u te resnie ande de e pr ent qut deux ceste den n tes surt nde e nde de e qurate u dte d u se de e i q e e desoe age n e e endem e es pour pur u e e rite purir de lite de e deon le apur e i cncis i e cé C cnt A puste et fondée ; Que dailleur elle n'est pas contestée par la demoiel crevain non comparante ni personne pour elle quoique dament appelé ; Attendu qu’en ne comparusant par devait le Bourer edu Conseil la demoiselle Lrévain a causé à Porte se préjudice de perte de temps, ce qu’il doit réparer ; Par motifs - Le Bureau Général jugeant en dernier repsort ; Dounne défaut contre la demoiselle Léévein on comparante ni personne pour elle quoique dument appelé ; Et adjugeat le profit du dit défaut ; condamne la demoiselle Lrévain payer avec intérêts suivant la loi à Maccy la somme de sex sans trente cinq centimes qu’il lui doit pour prix de travaux des sa fille couisne, plus une indemnité de quatre froncs pour temuse perdu ; Le condamne en outre aux dépens taxés et liquidés envers le demandeur à la somme de, ue fronc et à celle dui Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept aot mil huit cent cinquante ence non compriu le cout du présent jugement, la signification d’icelui et ses suites. tt vu les articles 435 du code de procder civile, 27 et 42 du decres du onze Juin 18099 pour signifier a la défenderese le présent jugement, commet champion, l'un ler ses huifsiers audienciers. Ainsi jugé les jour mois et an que dessus. Mrasgoffte Lecucq recrétaire e it ui du dit jour Treize Puizan Entre Madame veuve Didier, demeurant à Paris, rue Louge numéro trente lix agissant en nom et comme satrèce n lutelle et légale de sa fille mineure ingele, ouvrière fluriste Demanderetse ; Comparant ; D'une part ; Madime veuve Minée Ségrot, fabricante de fleurs artificilles demeurant et domiciliée à Paris, Boulevart Montmrtr numéro quatorze ; Défenderse ; Défaillant ; D’andesr part ; cnq de fait ) Par lettres du secrétaire du conteil 2u Prud’hommes du Département de la Seine pour l’industrie de tésons en dates des undi trois e Mardi quatre juin mil huit nt soixante dix huit, la dame Didier fit citer la veuve Aune Soyrot à comparaître par devant le dit Conseil de Prud’home de séunt en Bureaux Particuliers les mardi quatre et vendredi sept juin mit huit Cent soixante dix huit, pour se concilier si faire se pouvait sux la demande qu’elle entendit, former contre elle devat le dit Conseil en puement de la somme de quarante franc quelle li doit pour prix de Pa jeur Vnnée Segot n'ayant travaux de sa fille angèle ; pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi liize juin mil huit cent soixente dix huit citée pour le dit jour treize juin par lettre du secrétaire du Conseil en date du heit juin mil huit cent soixante dix huit la réquête de la dame Didier la veuve sgné Trgrit ne comporut pas ni personne pour elle régalièremet. A l’appel de la cause la dame Didier se présenta et conclut à ce us plus a Bureau Général du Conseil donner défaut contre la veuve ine Pagrot, non comparante ni personne pour elle quoique dument appelée et pour le profit la consamnner à lui payer avec intérêts suivant la loi la somme de quarante francs qu'elle lui doit pour prix de travaux de sa fille congte, plu endenité qu'il plaira au Conseil fixer pour perite de temps devant le Bareaux du Conseil et les dépens. Pent de droit - Dit on donner défaut contre la veuve Mirnée Peret non comparante ni personne pour elle quoique dument appelée et pour e profit adjuger atele e e ple lu etens qu e pre e pr e Que doit-il être statué à l'égard des dépens ? Après avor entendu la dame Prdier en ses demander et conclusions e en an slé da prdent cle, doeunte ur d te eu dit purt te pat hre ete de de e eit dener po pe de en e e dene ere du jon eux e eur e e e le oue t e dere en e cine eade seun de en uetle eue e e pede ende e te ee u se dus e e e que q ene u de en e e e ent endt seoi e te des que e e conci inte de p d i ie ile den i de den tess pous e aunr e s qucities en e que e le à R i e L BI5 u d de la i u i D t i ? A dt en date du Mardi vigthui, Mai mil huit cent soixante dix huit renvoi approuvé f Cite Lecc lagettre au Trésor Public pour le papier timbré de la présente minute, conformément à la loi du sent aon mil huit cent cinquante once non compris le cout du présent jugement, la signification t vu les articles 435 du code de d’icelui et ses suites. procédure civile, 27 et 42 du décret du onze juin 1809epou signifier à la défederese le présent jugement, commet coompar l’un de ses huissirs audienciers. Ainsi jugé les jour, mois et an que dessus. csoffe Lecucq serétaire r Audit jour treize Puin ntre Monsieur Jeun craderse, ouvrier tailleur d’habit demeurant et domicilié à Paris, rue des lus, numéro vingt six ; Demandeur ; Comparant ; D'une part ; Et Monsieur restivens aitre tailleur dhabits, demeurant et domicilié à Paris, rue Poinq hronoré, numéro cent trente cent Défendeur ; Défaillante ; D'autre part ; Point de fait - Par lettres du secrétaire du Conseil de Prud’homme du Département de le seime par l'industrie des tissus Clavarie fit citer Thristions à comparaître par devant le dit conseil de Prud’homme s u Bureaux Particuliers le endredi trente un Moai mil huit cent joiante dix huit pour se concilier si faire se pouvaits sur la demande qu’il entendait former contre lui devant le dit conseil en paiement de la somme de neuf francs soixante centimes qu’il lui doit pour sitaire. A l’appel de la case Claverie se présenta et en posa comeudessus. Da santoté Thristinent le présenta, reconnt devoir la somme réclamée et ditedée refuserà la payer percé que le demandeur qui le tavait et nevent pas travailler le vingt sept moi, ce qui lui daute cux préjudice dont il lui dux réparation . Pur l’avis de Bureaux Particulier que Claverie ne tait pas tenur à le prévenir puitque dans cetteindustrie les parties se peuvant quitter non emrent, chaque jour mois encore à toute hence du jour Thistian plauis qu’il paierait le même jour au jour. Cette poé r ne fut pas cence et la cause fut renvoyée devant le oudeil qun le du conseil ting le jendi trige suin mil huit cent prse et aors huit. Cité pour le dit jour tenze juin par lettre du secrétaire du conseil en date du onze jun mil huit cent soixante dix huit à la réquate de Clavarie Boristios ne comparut pas. A l’appel de la cause Claverie se présenta et conclut à ce qu'il plut ai Bureau Général du Conseil donner défaut contre christianne non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui payer avec intérêts suisin la loi la somme de neuf francs soixante centimes qu’il lui doit pour prix de travaux de son état, plus, cette indnité qu'il plaira au Conseil fixer pour perte de temps devant les Bureaux du Conseil et les dépens. Point de droit - Doit-on donner défaut contre Cpristios non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demonder les conclusions par lui précédemment prises ? Que doit-il être statué à l'égarddes dépens ? Après avoir entendu Clavèrie en ses demander et conclusions et en avor délibéré conformément à la loi ; Mttendu que la demande de Claverie en juste et pendée ; que d'ailleurs elle n'it plus contestée par Thiestions non comparant en personne pour lui quoique dument appelé ; attendu que christions à fait perdre du temps à Claverce devant les Bureaux du Conseil, ce qui lui causà un préjudice dont il le dtoit réparation ; Par ces motifs – u Bureau Général jugeant en dernier ressert ; Donne défut contre Threstinent non comparant ni personne pour lui quoique dument appelé pudivant srapadte ie it conte e s se à payer avec intérêts suivant la loi à Aluveice la somme de du our per te ur i peame et oue pe u feimen e e te e e lu e e te ene le puir pitite de e de le u oneil de te u eu e e pour ete por di enie e ep paire u en e eue de e u de s ds pomn e e re eur rei eus eu euppres s dedit en gu denmenenr es lngifle Leng puilite De uns Audi jour treize juin Entre lesioer Moolphe Veedorn, ourier cappren demeurant à Paris, rue Saint Vinolas, numéro vingt ; Demandeur ; comparant ; D'une part ; Et Monsieur Besanvalleti Matre la papser demeurant et domicilié à Paris, rue Car lassal, numéro dir sept ; Défendeur ; Comparant ; audre part ; Pint de fit - Par lettres du secrétaire du Conseil de Prud’homme du Département de la Seme pour l’industrie du tissus en dates des Vendi six et vendredi sept juin mil huit cent soixante dix huit Tinesche fit citer Besenvelle à comparautre par devant le dit conseil de Prud’homme séant en Bureaux Particuliers les vendredi sept et Mardi onze juin mil huit cent soixante dix lui pour se concilier si faire se pouvait sur la demande qu’il entendat ferme contre lui devant le dit conseil en paiement, de la somme de vingt trei francs qu’il lui doit pour sétaire. Besonvelle n'ayant pas comparu la cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi tronze juin mil huit cent soixante dix huit. Cité pour le dit jour tenze juin par lettre du secrétaire du Conseil en date du huit in mil huit cent soixante dix huit à la réguate de mésche Besavalle comparut . A l’appel de la cause oesch se présenta et conclut à ce qu’il plut au Bureau Général du Conseil condamner Besonvelle à lui payer ave intérêts suivant la loi la somme de vingt trois francs quil lui doit pour salaire et ce condamner aux dépens. Let ontile Tue Beranvelle se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu que mesch sest présente chez lui d’offrant de travailler comme petit ouvrier au pris de cinq francs par jeur ; Que fin du premier jour ayant recoune qu’il ne savait riern du métier il le lui dix ; qu'il dornt lalors et attait de rester pour apprendre ; attendu que donte quatre deurs et demi qu’il est reste vrues chne lui a été d'auvie atilité il ne lui doit ion ; Par ces motifs — Dire vuce se o recevable en sademande, l'en débouter comme nal fonté et uelle et le condamner aux dépens. Paint, de droit — Doiton condamner Besanvelle à payer à Vmesch la somme de vingt trois francs faur quatr auns le atin de travail ? Ou bien deuq en diue Muron nonrecevable en sa demande, l'en débouter ? Que doit-il tretide à l'égard des dépens ? Après avoir entendu les parties en leus demis s et conclusions respectivement et en avoir délibéré conformément la loi ; attendu qu'il et constent que Mausel a erei e chez Besonvelte pendunt quatre jours et deme ; qu'il etre trestais que la de pasier sur Besenelle li qi qu’il ne pouvait préteuve au prix de cinq francs qu’il avaice anmancé vouloir gagnet ; mois il ne pent s’ensuivre de cette déclaratien qu’il l’apoccupe pour rien ; qu'entenait compte de la faiblesse de l’ouvrier le Consil en d’avis qu’'il lui huit, paye deux francs pour sable ; Par ces motifs - Le Bureau Général jugeant en dernier restort ; consanné Besanvalle à payer à Arésch la somme de douze francs et déboute Nresche du surplus de sa demande, condmne Besonvalle aux dépens toxés et liquidés envers le demandeur à la somme de un franc, et à celle due au Trésor lablic, pour le papier timbré de la présente minute, conformément à la loi du sept aout mil huit cent cinquante, ence, non compris le cout du présent juement, la signification d’icelui et ses suites. Ainsi jugé les jour mois et an que dessus Magalli Lecucq secrétaire usi jour crige liuigz e Entre Monsieur Auguste Flessis) ouvrier cordonner, demoirant à paris, rue Camada, numéro six ; Demandeur ; Comparant. mpat ; de te sour lat tort e te se demeurant et domicilié à Paris, rue du faubourg sant Denis, numéro cent quatre hnet dix ; Défendeur ; Comparant par la sieur Charles, son contre maitre, demeurant chez lu aux dur aupere te e p sensen e gue qurele pent e en der due deux pu i l époed det pr e ene e laqerant peredin e p semis ver qua e dit ede de ur e aun e t ue e ete mr de emen u e ente t de de poer de i e se e e e e a i e t ut ue a e e ceties eun esen ser an den prs ement la elles et se eoer t ls la cis D MM it par lui entenslieu : Boré n’ayant pas comparu la cause fut envorée devant le Bureau Général du Conseil ésant le jeudi lui juin mil huit cent friquite die huit. Cité pour le dit jours seuil mots ragis nuil. hraze jun par tettre du sorétaure du conseil ui late des onze juin mil fuit cent soente dix huit à la réquite de Fessis Dauré compret. A l'appel de la cause Pessis de présme Me et concut à ce qu'il plut au Bureau Général du Conseil lev Recu in Paire à lui payer avec intérêts suivant la loi la somme de quarant pours pour li tinie de la semaine de cougé qu'it re Gagelle lui a pas faipé faire comme d’usage et le condamner ex desote Dit son coté Pairé, par Charbes, son mandétaire, se travai et conclut à ce qu'il plut au Bureau Général du Conseil attendu qu'un règlement apposé dans ses atatien prévant les ouvriers qu'ils sne sont pas tenus de donner semaine de congé ni admis à le réclamer à l'haure de leur renvoir Que ce réclement, lorme entre lui et les ouvriers qui l’ait auqs un travaillant eue lui un contret verbal qui detouit l'ata du délai congé. Par ces motifs - Dire Flessis non recevable en sé demande, l'en débouter comme mal fonné celle et le condamner aux dépens. Pint de droit Doit-on condamner Pou à payer à Wlessis la somme de quarante francs pour lui tnir comtte lu de la semaine de congé qul de lui a pas toifré faire ? Ou bien dait-on dire lessis non recevable euse demande, l'en débouter à Que doit-il être statué à l’égard des dépens ? Après avoir entendu les parer en leurs demandes et conclusions respectivement et en avir délitéré conformément à la loi ; Attendu quil est constuatre qu'un réglement d’ételier appectié d'une mineure estensle dan Les ateliers de Paute rent les auvriers fibres de la etes filemant quoir bounr leursemble et résiprogquement le Patron, de les remercier à taite huire ; Que Flesst go déclare avoir en conneilsance, de ce recteames est mal fuit à demander semere de jongé que le dit réglement avait premes out de supfrmer ; Por ces motifs - Le Bureu Général jugeant en dernier repert ; Dit Ptessis non recevable on sa demande, l'on déboute et le condamne aux dépens euversr le Trésor Public, pour le papier timbré de laprése minute, cnformément à la loi du sept aout mil huit ent conquate, ene, non compre le cout le présente resn, cinifation d’ielui et sas sites. ies l u e et et anque dessus. Vagallie Leuicg serétaire E
     </p>
    </div>
    <div n="14" type="case">
     <opener>
      Du dit jour Treize suin
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur Bourlier Moitre, selfer, demeurant et domicilié à Paris, rue Harg, numéro soixente quatorze ; Demandeur ; Comparant ; D’une part ;
        </span>
        <span type="part2">
         Et Mlonsieur Bupetit, demeurant à Monierf seize et Martre, agissat au nom et comme admins trotur de la ersonne et des biens de son fil nneut constant apprenti Défendeur ; Comparant ; D’autre part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait – Pur lettres du secrétaire du conseil de Prud’homme du Département de la Senme pour l’industrie des tissus en dates des Samedi premier et lundi trois juin mil huit cent soixante dix huit. onr lier fit citer comprtits à comparaître pas devant le dit conseil de Prud’hommes séant en Bureaux Particuliers les Lundi trois et vendredi sept juin mil huit Cent soixante dix huit pour se concilier si jaire se pouvait sur la demande qu’il entendait former contre lui devant le dit Conseil En éxécution de Conventions verbales d’apprentissage au paiement de la somme de deut cent francs à titre de deommage intérêts. Snpelit n'ayant pas comparu la cout fit rentayée devant le Bureau Général du Conseil séant le jour treize jun mil huit cent soixante dix huit. Cité pour le dit jour tinze Juin par lettre du secrétairée du Conseil en date du huit Juin mil huit cent soixante dix tit à la requite de Bouslier upept comparut . A l’appel de la cause Bourlier se présenta et conclut à caqt il plut au Bureau Général du Conseil dire et ordonner que dans le jour du jugement à intervenir epabit sera tru de faire rentrer chez lui son fils constant et de liy fépir pendant une anné qui lui reste à faire sur les trois qui ont été fixées pour la duteeten hui pe t p le u en p purt per du que cet apprentissage pro at demirrelie résolu ence ces condamner cou papit à lui payer avec intérêts suivant la loi la somme de deux Cent francs à titre de dommiges-intérêts et a en en es por le cande ete de pre M et conclut à ce qu’'il plut au Bureau Général du Conseil attendu e de e pe es u es emiene a elie e tete de tent que prse e e p e pet snteur pus ap an seit eme prl u e ene de te rgt u oe de e e e e der e le e e e ou simie ’ept moits ragis uiles le Lruch llée Maq qu Monter à it cit l'ablig peurant hite l’anée restant à faire à payee une somme de trois francs par semane par orter à la nourecter de son apprenti qui resta sun père parté ; mois attendu que Bourlier ne tit pas le nmouvel engagemez en privent, l’unpo des trois francs sur termoils il état en drait de comptes pour pai partie de sa nmurriture il se oit forcée de le reprendre ce quil fit le vingt un avril dernier non par se sonstraire à l’exécuti de t engagement, mois contrant et forcé du six de Bourlier Par ce motifs - Dire Bourlier nonrecevable en ses demandese l'en débouter ; Le recevoir reconventionnellement demandanm dire l'apprentissage de son fils purement etn timplement Vésolu d condamner Bourlier aux dépens. Paint de troit — Doit-on dire qu’il l ne papit sera tenu de faire rentrer son fils chez Bourtier pou cegtiner son apprenpsag par une année se présente, hui et fauté par lui de ce faire dire l'appentissage résolé, en cece condranner capelit à payer au tra Bourlier la somme de deux cents francs à titre de dommages-intérêts ? Ou bien dait in recesa toup . Dire Bourlier non recevabl en sa demone l'en débouter recevoir Papelit reconv entionnelleet demandes dire l’apprentissage doit s'agit purement et simplement résilé Que dott-il être statué à l’égard des dépens ? Ae cnneil très avoir entendu les parties en leurs demandes et conslutio respectivement et en osor délibéré conformément à la loi ttendu qu’il est constant que se mineur Ceapetit à été placé en apprentissage chez Bourlier pour trnq unne avec une rem unération foutature jusse du vingt ferrer dernier, mois décendé à jour ogatore pour une somme de trois francs par semaine ? que cet apprentissage doit recevoir son éxécution bindnet l'année restaut à faire sinon loupept payrà une indemnité que le coutre qui passate la demet moutères d’apprentisae u la somme de sivante Mrnde, cel emil le Bureau Général jugeant en premier ressort ; Dit et ordonée que dans les quatante huit heures à partir de ce jour bapelit sera tenu de faire rentrer sa fils constante dut hout lers pour à melle dnier huitquni pren a pet pal lui de se fice eui l s o ue et cete de pess ; dit que lets apprentissage se ine demeurea résolu lu ce cas, ces anpretis à fréret du etit à payer avecintérêts sur n lui à Burlier la some de cinquante francs lin le domneries intbtle, à cort domne dus ou ln ces ux désous tixés et liquidés en vers le demandeur à la conme de uni frinc fet à celle die u Trésor Public, pour le papier ttimbré de la présente minute, onformement à la loi du sept aout mil huit cent cinquante, ence Fa compris le cout du présent jugement, la significatione d’icclu et ses suites.
      </p>
     </div>
     <div type="judgement">
      <p>
       Ainsi jugé les jour mois et an que defaut Eas ptille Lecusq secrétaire
      </p>
     </div>
    </div>
    <div n="15" type="case">
     <opener>
      Du dit jour trenze Jiin
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monpeur Louis Voubert soupeur d’habités, demeurant à Paris, rue de la bente tir numéro cinquante ; Demandeur ; Comparant ; D'une part ;
        </span>
        Monsieur Purant ; confectionneur d’habits, demeurant et domicilié à Paris, rue croir des Patits chemps, numéro cinquante ; Défendeur ; Comparant ; D'utre part ; Dinq de fait - Par letres du secrétaire du Conseil de Prud'homes du Département de la Seine pour l’industrie des tissus en dates des Mardi quatre et vendredi sept jaunvier mil huit cent soixente dix huit Joulert fpt citer sinant à comparaître par devant le dit conseil de Prud’homme demil au Bureaux Particulier les Vendredi sept et Murdi onze vun mil huit censoixmmmte dix fait sur se concilier si faire se pouvait sur lu demende qu'it entendait former cente lui devant le dit conseil en delai conge domme dusage Denait- n’ayant pas comparu la caute fut renvayée devans le Bureau Général du Conseil séant le jeudi toize juin mil huit cent prbte de li di e le dt es hun sem pr lettre du secrétaire du Conseil en date du onte juin nif huit Cent sixante dix huit à la reate de outant denans aplels; Ail le det dte sudi épaut de eue sut an en et oncsue que det hui e e e e e qui edte pue t e e e fitr de de afur l contilous es re de lui poarseue udits pos l l se e er pl de onos qnr i d it fi t M t de dommages-intérêts et le condamner pans les deur ense dépens. De son coé dement se présenta et conclut à ci qu’il plut au Bureau Général du Conseil attendu qu’il embouche pouler, pour un cop de moins et qu'en sontaires le conseisa à cajournée de quatre francs cinquante centimes de non con mis sonners le prétend ; qu'il la anpe dé poue qu'il étant en capable de bien faire la latijue ; Devait motifs ; Dire Soulert non recevablien su semante l'en déboiter et le condanner ux dépens. Panit, de droit Dait-on dire que Finand sera tenu de recevoir etae lui peutert et de lui continuer pendant le reste du mill de juin du travail de son état de coupeur d’habits enten servant les appointement de cent vingt francs pour le dit mois, sinon et faute par lui de ce faire le condamner à payerau dit Toulert la somme de cent vingt francs à titre de dammaser intrêts ? Ou bien dait-on dire Toulert non recevable ses demandes, l'en débouter ? Que doit-il être statué à l’égard des dépens ?
       </rs>
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu les parties en lurs demandes et conclusions lespertivement et en avoir délibéré conformément à la loi ; Attendu que les ertiusion fournier par les parties et de l’examon des lisres de sinant ressort aue pubert est resté plus de trois chez simand e qualite de counpeur Fhibits ; que quoique payé par à comptes il était au mois ; Qu'en ce cos il devait prévenut et être prévene en mois à l'avonce conformément à l’asa que reçit cette industrie ; Qrue Finand gur l'a causedie le premier juin pour l'avoir préven le dait recavoir chez lui et lui doit donner du travail ponstant tout ce qui deste à faire de mois de juin en lui payait le mois cen entier ; Par ces mots Le Bureau Général jugeant en dernier lepart ; Diit et deuie que sinant reuvea chez le toutert et lui donnére de travail de son état pendant tait le mois de juin présent vuren lui pupara six peurs par semaine pour se prourer du travail, sinon et faute par Dinand desa conforméere à Cetée décixon ; le condamne dès à présent à payer au dit Poulers le somme de cent vingt francs à titre de dommages-intérêts ; L condamne dant tous les catux dépens taxés et liquides euvriée demandeur à la somme de un franc et à cette due au Trésil Public, pour le papier timbré de la présente minute, conformément à la loi du sept aox mil huit cent cinquante, ence non comptrian at di pepren saqyant, las sopntils. d’calu elestile lure les jour mois et an que dessus Cle as Et fancq etrétaie ce
      </p>
     </div>
    </div>
    <div n="2" type="courtHearing">
     <p>
      Audience Jeudi vingt Jng mil huit ceut soixante dix huit siégeant . Messieurs Dinaud, Péalies de la légion d'honneur, vice Prérident du Conseil de Prud’homme du Département de la Seine pour l’industrie des tissus, Tiendant l’olndame n remplacement de Monsieur Martenval Président de dit Conseil, supicté, Esssoy, Larcher Brentry, edhriz ; Pud’hommes assistée de Monsieur secrétaire du dit Conseil. au ee2 Entre Madame Veuve Méniar, ourière fauriste demeurant à Paris, rue de Belleiille, numéro cent sut ; Dumanderese ; Comparant ; D’une part ; L Monseur veuve lorne, demeurant et domiciliée à Paris, rue se feubeure Pain, Denis, numéro seize, agistant comme administrities induuante de la maison pe reprocation et de comme de la dame Tepetème se seur ; Défendeur ; Comparant ; D'autre part, Pont de fait – Der lettres du secrétaire du Conseil de Prud’hommes de Département de la Seine pour l'industrie des tissus en dates deus Vendredi trois et uunti six Mai mil huit cent sixunte dix huit la veuve Moron fa citer deuve Détorme à coumprraître par devant le dit conseil de Prud’hommes poun en Bureaux Particuliers les lundi tese Mardi sept Mai mil huit cent cinute aqfis pu t te e s fait puir ulaet cenitie entendait former contre lui devant le dit conseil en paiement de la soe de seize cent francs pour selde de provision à laison le cinq pour aet ur pes et onternspuen luitue n’ayant pas comparu la case fut renvoyée devant le Bureau prt se e it lu ’enti eil lui et li g tente ix lit e liy le e comme e enl enie et exposa au Conseil qu'elle entra leiy novembre mil huit cent pa t six du e e u elle deie u centre tur le su for p u en un u ente le te e end es ur eu er u prsente ei pu duie soant quea lur pons luit de dapres et coutde six depoit ce die teuite e it r is pas intatire emne ei M jeuve Déferne, de soubité, se présenta et expost au Consil qu’il a payé à la veuve Moson chaque semaine le prisd les journées à sept francs, ausil a versées plus iaus sommes on à compte sur sa provision qu'il offrine et de cinq pour cent sur les bénifices et snon sur le prédeunr de rntert ; Qu'on fu de compte elle en la débitrice et ne sa créancière. ll demandu l’ajournement de la canse par prouver son apartion. Le Bureau Général ajournt a caunse et chargea l'un de ses memtres de l'examet de l’affaire taut au hoint devae de la convention qua ce lui des compte Par lettre du secrétaire du Conseil en date du vingt cinqlaie mil huit Cent soixante dix huit la veuve Mégron fit citer vouve Delarne à comparaître par devant le conseil de drudhom séant en Bureau Générall udi six Juin mil huit cent soixante dix hui pour t'entendre condamner à lui payer la somme de sene cent francs qu’il lui doit à raison la cinq pour leur sur le choffe net ses affaires et trentenr retondamner ax dépens.. A l’appel la veuve Menin se présenta exposa sa demande et conclut à ce que veuve Vélorne fut condamne Pare à lui payer avec intérêts suivant la loi la somme de seize cent francs qu’il lu doit jur prousien à cinq ponr cent par les fairs et le condamner ux dépens. De son coté vouve Détorne se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu que la veuve Mernan ent entrée et restée dans la maison cahitaine au pris de sept francs par jour et cinq pour fent sur les bénifices qus non boulement it a payé chaque semaine le produi des journées, mais à donne des à compte se chiffrans travant somme su parienme de brouvaut à celle qu'il erait de payer. Par e motifs – Pire la veuve Megrandon recevable en sa demande, l'en débouter et la condamner aux dépens. Le Bureau Général aourna au vendi tingt Juin mil huit cent soixante dix huit. LVeude vingt pui pasoue eMleseln seprétite et atirl les quel ls ri ue requierant l’adjudicetion. Vouve Desonne ne comparnt prs. Pungele soit ; Doit en en demn e l les aprsà la veuve Mayor la ome de sixe det faus p sitée u ie pair cent ui t d et e t e po e liun dit- il letre la seune Mésir mart e ue sa demente do rilet tu dit d’ te det fouier ou dien . depre euventel e le e e te e ce stesouns epadement ; compertienes re le nen nl d it ue uns e i o ? De unle charge de l’examon de la cause en son rappert verbal et en avoir délibéré conformément à la loi ; Attendu que de l'audition des parties à la barre du Conseil et de l'axamen le membre du Conseil doms à l’effet de voir la comptetible veuve Lébornée que la demande de la veuve Monrer Ee est juste et fondée. Pur ces motifs - Le Bureau Général jugeant en premier repart  ; Condamne vouve Léforne à payer à la veuve Mégran la somme de seize cent francs qu’elle lui réclame comme soble de pronision sur les offaires de la maison capitime du cinq rovenb mil huit cent soixante treize au jour de sa sortie de condamne en outrer aux dépens taxés et liquidés en vers la demanderesse à la somme de deux francs cinq centimes, et à celle due du Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept août mil huit cent cinquante, ence non compris le cout du présent jugement la signification d’icelui et ses suites. Ainsi jugé les jour, mois et an que dessus. maie Gs decucq seurétaire ps
     </p>
    </div>
    <div n="3" type="courtHearing">
     <p>
      Audience du Jeudi vingt Juin mil huit cet soixantre dix huit iégeannt, V’esseure, Marmenval, Thévalier de la férçon d’honnem, Présideit du Conseil de Prud’hommes du Département de la Seine pour l’industrie du tissus, Larcher, iroy, Monnier, Angitour Bonsher et, Brccc, Prud’hommes apsistés de Monsieur Lecucq secrétaire du dit conseit. Cite onjour e ladaune Dinil, le sieur Déneltant en son nom personnel que par apister et autoriser la dame son étouse ouvrière imentende en fleurs artificielles, demeurant ensemble, à Paris, rue Maudon, numéro dix huit ; Demandeurs ; Comparant ; D'une part ; L Monteur Monnser Cordonares, fatricant, en Marchand de fleurs artificielles demeurant et domicilié à Paris, rue Levonne, numéro vingt un ci devant d actuellement en la même Vile rue veuve Saint Eugustin, numéro trente ; Défendeur; cotilaut ; tute part ; Pouit dé fit - u cete du secrétaire du Conseil de Prud’homes du Département de la Seine pour l’industrie des tissus en date du Mardi ure dumeil ut retient de lat ingen eile prir ies doenver eprte pardaunt le duie Conseil de Prud’homes séant en Bureau Particulier le qudet ente taen lut du prante se li purt on de e pur l pri dn la ee ndtedet pour dt deu e e apour e te prete enppe es pour laremeitie atendur satrit. e deseur se quit ene l prtint e p re u ee e un ere n eu de d t seent les es pere e la eugu es lomisie merdeie l te ent au qur du t i l ne e puser eun ere e e pre que e e quilese eu eu e ne qu us par doznnée, puis en reps pesjout ? Que le mardi quite juin Monnier ayant oulitement, toucédié lal dane Denael, alleu s'envu contrainte de lie faire appelé devant le Conseil auxfins de dommagesintérêts . De vo coté Monnier se présenta et exposa au Conseil qu’il est mevait qu'il ait tougédié la dame Denette e n’a rappelée qu’à l’éxécution de soncatitrnt on la ntine l’erdre de se trouver au mage sin le malie à neuf heureit au bien de dix qu'elle parai fait vou loir actopter. Les parties n’ayant pu étée conciliées la canse fut renvoyée devant le Bueu Général du Conseil séant le jeudi vingt juin mil huit cent suixante dix huit. Cité pour le dit jour vingt juin par lettre du secrétaire du Conseil en date du quatorze Juin mil huit cent soiante dix huit à la riguite des époux Demalle Monnon se présenta et conclut à ce qu'il plut au Bureau Général du Conseil attendu qu'il s’auet, de l'interpretation don Tontree de travauil, d'une part ; D'autre purt e statuer sov me demantes de douze cent francs quand le conseil de Prud’hommes ape conmettre que des affaires dant la demande ent inférieure à a deux Caits francs ; Attendu querre qu’il a peaiti citre les époux senelle devant le tritaral de commerce sont compétent pour conmaitre d'une affaire drcette niture et de cette inparan Par ces motifs — Le délarant n compétent pour connaitre de la demande des époux Denelle les en débouter et les condamner aux dépense De leur coté les époux Denelle se présentèrent et conclurent à ce qu’il plut au Bureau Générals du Conseil attendu que la dame Denelle nt entrée chez Monnier comme louvrière monteuse en fleurs et plarmes,; qu'il s'agit de l'astirfictions d'un contrat de travail et de sommages intérêts suins résuller de son inexécution ; Que dans l'espèce le choffie ’élové qu soit doit être sournis aux juge de prémier onstance ; lu d'autre part l'rtion de Monnier devant le tribanel de commetce était pas tervenme à celle qu'ils ont formée londe lui devant le Conseil ne pert avoir pour etet de chanter la rature de la cause qui ost me de retations d'autère cotun ; Par ces motifs –; Dre Monnier ontre on en sa demande,; l'in dé bouter ; La déclarer, competeur ritezer la cause etordonner qu’il sere passe entre du outecrtire, de Monner pa être apporé et sar le land condamner Mingie aux dépens. Pint de drit ait-en Le déclarer en compétait pour lomaitre de la demende des époux Denelle nenvayet ceux oi devant que de pit luillas pex M M eth t Crtt Meth a tip iis parsiaint Memer apelus cesier Sréger Public reaja et un gois que eul alporie f ecu t E cets reuva pprouvé et A onlr it Ou bin doit-on se déclarer compêtent retenir la cause, r donmer que les parties l’expliquerant sur le faud pour être statué ce après avoir entendu les parties en leurs que de drait – demandes et conclusions respectivement et en avoir délibéré Conformément à la loi, sur le éctinctaire de Monnier, ttendu qu'il est constant que les demandes des époux Dinelle portent sur un contret de travail een conséquences que le conseil doit conmaitre de les demandes par être statué en premier ressot ; Par ces motifs — Dit Monnier non recevable en ses demandes, l'en déboute, retient la cause, or donne que les parties s'enpliquerant et que le conseil statuère en suite sar le mérité de leurs conclusions. L ce mineur Monnier lertire dec lavans faire défaut. de leur coté les époux Denelle contneut à ce qu'il plut au Bureau Général du Conseil donner défat contre Monnier non comparant ni personne pour lui quelque dument appelé et pour le proft le condamner à leur payer avec intérêts suivant la loi la somme de douze centes pris ur somrmilies de oveiles le suend es le condamner aux dépens. tant de dait- Doit-on donner défaut contre Monnier non comparait ni personne pour lui quoique dument appelé et pour le proft adjuger aux du ei tes l l le on lu i pr le e e ir purdint lap satié renselen pus à aer entendu les épous Denelle en leurs demandes et conclusions enier lité la soman l le e t cile uente de et e e u pe te per qr l le e la te e pardemne e de pr e eme e ser conqurate e tie e t ten ene pls uint lentete eneit e la pa e psup permil suit de ou ge paprd ue esun tent de poueme pl adi encoemte ceu t e pee e ldi en en eu e de e en te e le t l so que du ene e de ets ou ete e t de e tretes are e de pr u ant pem l les quint e e aue des de ou de d cpte eu te eu teil se te te e e e e ui de cit e n i que es paur signifier au défendeur le présent jugement, coent chempu Ainsi jugé les jour, mois et l'un de ses huisiers audienciers. an que dessus. aviu àr Le cudf serétaire Qu dit jour vingt sing Entre Monsieur Mathieu, ouvrie tailleur d’habits demeurant à Paris, rue Montranyu numéro neuf ; Demandeur; Comparant ; D'une part ; Et Monsieur Bordeau ; Maite dailleur d’habits, demeurant et domicilié à Paris, rue saint Fonsliqne, numéro deuze ; Défendeur ; Défaillant ; Pautre part ; Point de fait — Par lettres du secrétairn de conseil de Prud’hommes du Département de la Seine pour l'industrie des issus en dates des Mardi quatre et vendredi sept juin mil huit cent soixante dix luit Mathieu fit citer Bordeaux à comparaître par devant le dit Conseil de Prud’hommes séant en Bureux Particuliers les tendredi sept et Mardi onze juin mil huit cent soixante dix huis por se couilier si faire se pouvait sur la demande qu'il entendaiz former contre lui devant ledit Conseil au paiement de la somme de treite neuf francs qu’il lui doit pour salaire . Berdeant n'ayant pas comparu la cause fil uaagée devat le Burau férére du contile leur Chaid vingt juin mil huit cent soixante dix huit. Cité pour le dit jour Cti9 por par titre du serétaire du conteil en bité deu edte joun seil lus ae pirsate de lait à le ouitel oin Bordeair ne comparut pas. A l'appel de la cause Mathieui se présenta et conclut à ceu’il plut au Bureau Général du Contail donner défaut contre Bordeaus, non comparant ni personne pour lui quoique dument appelé et pour le profit le condamner à lui ayet anet ntelite suiveant la loi la some de oet frncs u’il reste lui devoir nayant donné le cinq juin vinq franc sur lante seuf, Juailell duamité e ssaie e le our pis par pertie de temps deuns le Bnraux du contre u dépens. Pois de doit - Doit-on donner de fus letue coisdevur non comparant ni personne pour lui quoique dunen disqpu epepede on dem t prlui pruédement pas à ui dit -il être ain A n ue itée s ils à us 4 t l’égard des dépens ? Après avoir entendu Mathieu en ser demandes et conclusions et en avor délibéré conformément à la oi ; attendu que la demande de Mathié parent juste et fondée ; Que d'ailleurs elle n'est pas contestée par Berdeaux non comparant ni personne pour lui quoique dument appelé ; attendu qu’en ne comparaisant pas levant les Bureaux du Conseil Berdeaux a causé à Mathie un préjudie de perte de temps, ce qu'il doit réparer ; Par ces motifs - Le Bureu Général jugeant en dernier rettert ; Du l’article quarante un qu decret de onze juin mil huit cnt neuf; partait rislement pour les contils de Prud'hommes ; Donne défaut contre Berdeux non comparant ni personne pour lui quoique dument appelé ; Et adjugent e profit du dit défaut ; condamne Bordeant à payer avec fard luvant lelé à Delheu la somede se euf francs qu’il lui doit pour sefaire, plus une indmnité de dix francs par taups perde qu lendemendet susles e liquidés envers le demondeur à la somme de un franc, et à cellee eue au Trésor Public, pour le papieer timbré de la présente minde, la sorément à l le de q ei que lus luse die t ris en ps de de inseunge e perte e e s l et le e ile sur ec peat t e t on ein e es e e e e e ex lui de ta t oer etie à largre e esoie Mavirn à et an que des sus. Leuig recélaue duien en cingq ant e dete e e etie erenteu eede de den ete qa den dirt dementies de t in e one eni e e e te an e eri que m an du ete de de e eme dete et e e ne e e e
     </p>
    </div>
    <div n="16" type="case">
     <opener>
      Du dit jour vingt Jeuit
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur Ematile Charsé t ouvrier sellier ; demeurant à Paris, pusage Bouchardy, numéro treis; Demandeur ; Comparant ; D’une part ;
        </span>
        <span type="part2">
         E Monsieur Balendier, autre prenoor de travaux de silernie, demeurant nt àParis, rue de tafoyette, numéro deut cent quatante trois ; Défendeur ; Comparant ; D’autre part ;
        </span>
        <span type="part2">
         ande Monsieur crousselle Maite Geltier, demeurant à Paris rue de lafayette, numéro cent trenteneuf ; Défendeur ; Comparant. Anssi d’autre part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait ur lettres du secrétaire du Conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus en dtates des samedi huit Juin mil huit cent soixnte dix huit, Charrier fit citer Balegdrer à comparaître par devant le dit conseil de Prud’hommes séant en Bureux Particulier se le Mardi onze juin mil huit ces soixante dix huit para se concilier si faire se pouvait sur la demande qu’il entendait suse former contre lui devant le dit conseil en paiement de la somme de trente six francs pour indemnité de temps perdu. A l’appel de la cause Cérrier se présenta et en posa comme à dessas. De son coté Balerdier se présente il retonnet qu’après avoir donné du travail pendant pate me lonaire à Blerer lu et pes pars donner enure il le pa venir Chaque jour de la durane mais qu’ayant lui même été abosé par prasselle sonitatron e e edemen en le e rdes. qu e par l e t s tendes dein et dandil prer t ondit u dt lui Mai ente e pr t e lete lun dre dte a aon eu e te e les en eur pur en t e ne t poir rande eu ete deit re t e e e et enene ape ur e den ant le se eue a cem espse u deme pur deure des oue den drer ne e tente en e te ememre e quele ade dente is d e te de é doe en la I5x trois mots tayés oils aux Lecuiq se per De son coté trousselle se présenta et exposa au Conseil que non seulement il n'a pas embouche Charrier mois qu’il ne la seulement jamais vu chez lui. Le Bureau Particulier fut d'avis que Bulerdier qui reconnails aà avoir causé du perter de temps devait payer à Chévrier une indemnité qu’il fixa à vingt cinq francs et en qugea charrier à retirer sa demande envers trousselle qu’il ne jugea pas responsable. Aurrier ayant déclaré persister dans toutes ses demandes la cause fut renvoyée devant le Bureau Général du Conseil séant en Bureau Général lejoudi vingt Juin mil huit cent soixante dix huit. Cités pour le dit jour n dt jue par lettre du secrétaire du conseil en date du quatorze ju mil huit cent soixante dix huit à la réguite des Chabrel Malendier et troup elle comparurent. A lappel de la cause Chavrier se présenta et conclut à ce qu'il plut au Bureau Général du conseil condamner solidairement Balessier est trousselle à lui payer avec intérêts suivant la loi la somme de trente fit francs pour indemnité de perte de temps et les condamner solidairement aux dépens. Poton, coté Balesdier se présenta et conclut à ce qu’il plus a Bureau Général du Conseil lui donner cite des les erves qu’il fait de résiiter à Troussille les sommes auxquelles le Conseil turair devoir le condamner envers le demandeur; De son coté Trousselle se présenta et conclut à ce qu'il plut au Bureau Général du Conseil le déclarer nin respinsibe erlemettre pors de causé duns dépens. sont de droit doit on condamner Baleydier et ousselle solidairement à payer à Charrier la somme de trente six francs à titre de réparction du préjudice pu'lils lui cause sienment ; Dot on donner cité à chartie de Baley dier des resorves qu’il fait de repiter à Troissielle les sommes auxquelles le pourris être condamné envers Chavror à Pait on dire truflle nou responsible et le mettre trort de couse Doit Que doit-il être statué à l’égard des dépens ? Apréès evorte. entreavu les parties en leurs demandes et concfusions respectivement et en avoir délibéré conformément à la loi ; Attendu qu'el qui coucerne troupelle qu’il est acquis aux débité ate confié à Balegaier de s travaux pour être exécutes à la tatte et à des prix détermints ; que le fait davaint at refé au dit Belrysier des ouvriers, tefait fit à pevier se paut le end re uni tes eunver esprs ut deraites de son entrepreneur ; Eonce qui conderiet elgde d ls it eute attendu que charrier à déclaré à la barre du Conseil que Ballortier était son aperié de fait et non son patron ; que à le dit Balordier était en nom c'est que au seut pollediert un local et que s'il touctut une avant part sar les prix da façon d’était unsquement pour payer le prix de lalocations es pourvoir à de momes dépenses argentes ; Que ce fait acaires Charvier devant en déviduellement supporter le part des dommages qui rappent la sicé n’a rienc reclaner à sa rendier, son tollégène, éttent comme lui ; Par ces motifs Le Bureau Général jugeant en dernier réport ; Dit Trouselle non responsable, te met purs de cause ; Dit Charrier non recevable en sa demande envers Balesodier l'en déboute et le condamne ux dépens envers le Trésor Publie, pour le papier timbré de la présente minute,; conformément à la loi du sept tour mil huit cent cinquante ence, non compris le cout du présent jugement, la signification d’icelui et ses suites.
      </p>
     </div>
     <div type="judgement">
      <p>
       Ainsi jugé les jour mois et an que dessus. Lecuig secrétaire eaeveir e Quit jour vingt ing Entre Monseur Voiter Decroëq, ouvrier teltiers umér tote euf demeurant à Paris, rue Demandeur ; Comparant ; D’une part ; Et prima dendier tipe et pour eleur l setie emnt e de e e e ene eu qeat eu e e enes aeante euper cepits de tu ires dune du det inqe etie un tre e eteil n deu aeier au es de te de t ir de e e e e pus ser t e e esen e pu et em e te r aupe por e t es denses de e e ie hunt e ent u guse n e en pur de e is Moni Conseil en paiement de la somme de trente six francs pour réparation du dommage qu’il lui a cause n la promettae du travail qu’il ne lui donna pas. A l’appel de la cause Lecrorqse présenta et exposa comme cn dessus. De son cote Baledier se présenta et exposa au Conseil qu'après avoir occupé Decroëz afsey régulièrement pouttant une se maner il le fit veuir chague pour de la semane suivante pousoint chaque jour paureur lui donner du travail qui lui était proms par tronpel son Patron mois cellaci ayant trouvé à faire faire à plus bas prir, ce qu’il lui destine il ne put on donner n'en ayant pas. Pa cemanent Devron, repuit la parale pour due que trousselle l'ayant embourset chez lui et l'ayant atrepe à Balerdier était responsable de pertes de temps qu'il avait éprouvées et demandu la remiselle la cause pour le pouvair aitionner. la cause e cet état fut renvoyée au vendredi quatorze Juin mil huit cet soixate dix huit. Cités pour le dit jour quatorze Juin par lettres du secrétaire du conseil à la reguate de Decroëq alesdier et troupelle comparurent. A l’appel de la cause Decroëq se présenta et affirma que tropselle l’avait embourtie et achrepé à Bagdier qui lui donna du travail sont sa rerommendation tropselle nia avoir imbouche secroëq et dit même ne l'avoir jamais vucher lui. Le Bureau Particulier fut d’avis que Baleririer qui resonnat avoir fait perdre à Decroëqune sormaine lui promettait du travail devait lui payer une indemnité qu’il fixa à la somme de vingt cinq francs à il tengagea Decroëg à retirer sa demande en ce qui concerne trousselle qu’il ne crot pas respontible Decroëy ayant déclaré persister dans ses demandes le cause fut renvoyée devant le Bureau Général du Conseil séant le jeudi vingt Juin mil huit cent loixnte dix huit Cités pour le dit jour vingt Juin par lettres du secrétaire du Conseil en date du quatorze Juin mil huit cent faisine dixhuit à la requete de secrous Balerdier et pronssill comparurent. A l’appel de la cause Decrot à se présenta et conclut à ce qu’il plut a Bureau Général du Conseil condamner solidairement Balerdier et Trousselle à lui pay avec intérêts suivant la loi la somme de trente six francs pa réparation du dommage qu’il a éprouve par le chemaie de toute une semaine et les condamner solidairement eaer dépens. De son coté Bolgdier se présente et conclut égul plut au Bureau Général du Conseil lui donne vacte des i V ligé l u resorsesqu’il fait de repiter à tronsselle ces sommes ourouilles age le Conseil croirait devor le condamner. De son coté tapelle se présenta et conclut à ce qu’il plut au Bureau Général dmuaaner du Conseil le dire non responsable et le mettre puis de causé suns dépens.
      </p>
     </div>
     <div type="pointDeDroit">
      <p>
       Point de droit — Doit-on condamner Baloudier et troupelle solidairement à payer à Decrocq la somme de trente six francs pour indemnité de chamage Dait on donner aite à Balegdier des reservis qu’il fait eel de répete à tronselle les sommes axmelles il aurait été condamne ? Dait-on dire trousselle non recevable et le mettre pors de cause ? Que doit-il être statué à l’égard et des dépens ? Après avoir entendu les parties en leurs demandes e et conclusions respectivement et en avoir délibéré conformément e à la loi ; Attendu en ce qui conterne cropelle qu'il est acqust aux déboté quel a confié à Balerdier de s travaux pour être à exécités à la taihe et à des prix de termins ; que le fait par oupselle devoir adrehé des ouvriers au di Wolerdier, refait fut il acquit, ne peut prendre responsable en ces ouvriers du fait de son entreprenent ; Cresé qui conterne Baleydier; attendu que Decroya déclaré à la berredu Conseil que Baley dier était son aprcée et non son datronds que e Bales dier était sulen nom ses que sul il pas sidant anlocal ; Que si le dit Balesdier aprélevé sur les prix de façon une avant part avait portige d’était anquement par payer les tavoes et res le paes ue miner fournitures ; que ce fuit acquis Decroëq devant in dividuellement sapporter la part des dommages qui etteiuent la socete vislé rien à réclamer à Baler dier, Sourellegue, attent some cinq pet cesmuitte le sire hant puant donietrs, Cit ouete pe i quel le demie pus lasile, et douden en tent demande envers Balendier l'en déboute et le condamne eux dus minte deu cite pu e e te sa preeane omen desqou entererant juige es trer uain les p e te sesin ei purs lesoul eu ses eu n esi e aerui et reuig serétai L E
      </p>
     </div>
    </div>
    <div n="17" type="case">
     <opener>
      Du dit jour vingtuin
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur Toales Mégeont, ouvrier Poller demeurant à Paris, rue des paissonniers, numéro seize ; Demandeur ; Comparant ; D'une part ;
        </span>
        <span type="part2">
         <span type="part1">
          E prima elonsieur Bales u entrepreneur de travaux de telterie, demeurant à larise rue de Lafayette, numéro deux cent quarante trois ; Demandeur ; comparant ; D'une part ;
         </span>
         Ctecand, elaupeur Trouselie Maitre sellier, demeurant et domicilié à Paris, rue de Lafayette, numéro cent trente neuf ; Défendeur ; Comparant ; causoi d'autre part ;
        </span>
        Oout de fait - Par lettre du secrétaire du Conseil de Prud’homme du Département de la Seine pour l’industrie des tissus en date du samedi huit Juin mil huit cent soixante dix Mignoul fit citer Bales,jour à comparaître par devant le dit Conseil de Prud’hommes séant en Bureau Particulier le Mardi onze juin mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendu former contre lui devant le dit Conseil en paiement de la somme de trente six francs pour réparation du dommage qu’il lui a cause fir des pertes de temps à l’appel de la cause Mogeaul se présenta et exposa comme ndessus. De son coté Baleydier se présenta et reconnut avoir employé Migeaul opt et régulièrement pendant toute une semaine et l'avair fait venir chaque jour de la deuxrème ponsant pouvour lui donner dé travail son Patron, trousselle, lui en ayant promis; mais celui ayant trouvé à faire travailler à des prix de duits ne lui on donna pas. Mle moment Mégeaul repris la parate pour exposer que troisselle l'ayant embouihe et a dripe à Balesdier devait être responsable de son chamage .Il demando àe l’actionner en responsabilité. La cause en ret état fut renvoyée au Bureau Particulier du vendredi quatorze du même mois. Cités pour le dit jour Balesdier et trousselle comparurent. A llappel de la cause Migeail se présenta enposa comme tle huit Jain et offerma que trousselle l'avair enlere et envoyéet Balesdier avec l’ordre de le recevait, lu tronsselle ia avoir embauché le demandeur et décara ne l’avoir même jamais ve cher lui . le Bureau pParticulier fut d'avis que Balerdier reconnaisaut avor fit chener Migesl pendant toute une somane le faisant, venir chez lui chaque jour il lui devait suf une indemnité qu’il fixa à vingt cinq francs. Wendeg Migneul à retirer sa demande envers tronsselle qu’il de jugea pas être responsable ; Migeaul ayant déclaré persister dans ses demandes la cause fut renvayée devant le Bureau Général du Conseil séant le jeudi vingt Juin mil huit cent soixante dix huit. Cités pour le dit jour vingt Juin par lettres du secrétaire du Conseil en date du quatorze Juin mil huit Cent soixante dix huit à la réquate de Migeant Balegihier et tousselle comparurent. A l’appel de la causé Migeoul se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner solidairement Balendier et Traisselle à lui payer avec intérêts suivant la loi la somme de trente six francs à titre de réparation du dommage qu’il àéprouvé par du chomage de leur fait et les condamner ux dépens. De son coté Balerdier se présenta et conclut à ce qu’il plut au Bureau Général du Conseil lui donner cité des reserves qu’il fait de répiter à tronsselle les sommes ausquelles il croirait devoir le condamner. De son coté dmame tronpselle se présenta et conclut à ce qu’il plut au Bureau Général du Conseil le dire non responsable et le mettre chers de causé, sans dépens.
       </rs>
      </p>
     </div>
     <div type="pointDeDroit">
      <p>
       Point de droit — Doit -on condamner Baleydier et trausselte solidairement à payer à Mégeanl la somme de trente six francs pour indemnité de chemage ; Dait-on donner acte à Balesdier des résorves qu’il fit de répéter à Cromptelle les sommes ax quelles et arait été condlamné à Dait on dire tropelle nun respousible et le mettre port de causé ? Qu ditit M être statué à l’égard des dépens ?
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu les parties en leurs demandes et conclusions respectivement dem anr silé la soméent lelis Meneau qu tacane compele e du peour eis el adu du travail à Balerdier pour être exenités à la toite eta des poar deni, que les devie parsemes dit Balesvier, afit fat i prouvé, ne puit le rendre desponsible erde emie er dete ntomn cons ant ou e e de e le u étire de eis e e ite deant e quentu pere e e e en es our e l e e e e eie oefaute m p t de doment paur pr le er e et our u parse ate nenr deus, ou prme eu ene t it, Ai devant in deréduillement sapporter la part ses dommages qui fraphent la souile n'arien à reclamer à Balais son talleque attend comme lui ; Par ces motifs Le Bureau Général jugeant en dernier report ; Dits Trouft elle non responsable, le met sers de conte Dit Mégean nonrecevable en sa demande envers Balesdier, son callegue, attent comme, l'en déboute ei te condamne ux dépens anvers le Trésor Public pur le papier timbré de la présente minute, conformément à la loi du sept aot mil huit cent cinquante rence non compris le cout du présent jugement, la signuigées d'icelui et ses suites. Ainsi jugé les jour mai et en que dessus. Decraveu Leincg secrétaire rant
      </p>
     </div>
    </div>
    <div n="18" type="case">
     <opener>
      Du dit jour vingt rnz
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        entre Monsieur Benort Lébart, ouvrier sellier; demeurant à Paris, rue des tctuset saint Martin, numéroi vingt trois ; Demandeur ; Comparant ; D’une part ; primo ; Monsieur Balerdier, entretreneur de travaiux de tellerie demeurant à Paris, rue de Lafayette Guméro deux cent quarante trois. Aafendeur comparant ; D’autre part ; L serando , Monsieur Trousselle, Maitre sellier, demeurant et domicilié à Paris, rue de rapayette, numéro cent trente neuf ; Défenreur ; comparant ; Quulis d’autre part ;
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait — Fri lettre du secrétaire du conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus en date du farmes huit juin mil huit cent soixante dix huit Lebont fit et tere Balerdier à comparaître part devant le dit Conseil de Pondi hommes séant en Bureau Particulier le mardi onze juin mil huit cent soixante dix huit pour se concilier si faire si pouvait sur la demande qu’il entendait former cont lui devant le dit conseil en paiement de la somme de Trentesix francs pour ruparation du dommagef lui a causé en le faisoit chamer taute une Lemander là l’appel de la cause le bert se présenta et exposa teu cidissus. De son coté Balesoier se présenta M exposa au conseil qu’après avor ocupe de bert asser régulièrement pendant une se maine quoiqu’il l’ant ni occuper de vantage, et que penvant la deuxième il l’e fat revinir chaque jour lui promettait de travaul comptaut sur la promepe qui lui était chaque jour fuite par trousselle, son Patron, et qu’il ne tunt pas ayant travé à faire travailler à des près enférieurs à libre mement Lebert repis, la pirate pour dire que trousselle l'ayant mtauché et adrepé à Batesdier il demandant à le citer comme responsable. La cause en ce étt fut renvoyée devant le Bureau Particulier du Conseil séant le vendredi quatorze Juin mil huit cent soixante dix eup Cités pour le dit jour quatorze juin par lettre du secréaaire du Conseil à la requete de retert Ba lerdior es trousselle comparurent.. A l’appel de la cause le bert se présenta rappelu la demande envers Balerdier et demandà la responsabilité de Troufs elle comme l'ayant embourte et adrefié au dit Balégdier , tront selle de son coté de présenta et exposa au conseil qu’il n’a mellement embaurtie déberr, que de plus il ne l’a jamais vai chez lui. Le Bureau Particulier fut d’avis que Balesrer reconnaisant avon fait perdre du temps à Le bart lui devais une indemnité de vingt cinq francs, et ne trouvait pas motif arendre topselle respousable il enqadea Le pert à retirer lu demande envers celui à Lebers ayant déclaré parsister dans ses derindes la cause fut renvoyée devant le Bureau Général du Conseil séant de jeudi vingt Juin mil huit cent soixante dix tuit Cités pour le dit jour vingt juin par tettres du secrétaire du Conseil en date du quatorze juin mil huit cent soixante dix huit à la réquite de lebeit Balegdier et trousselle Comparurent. A l'appel de la cause le bort se présenta et conclut à ce qu'il plut au Bureau Général du Conseil condamner solidairement Baley dier e troup elle à lui payer avec intérêts suivant la loi la sonme dde diu ue e e ue etese t lemit prde prur se poer cait on pome es en de e en er lsent s senbile pl e de du tin le t saou Burea Graral du Conseil lui donneraite des réserves e l e e pat l domen e le deu u demen p te s E un mit jusée noff Ak Lécule Da le it it il de son coté, se présenta et conclut à ce qu’il plut au Bureau Général du Conseil le déclrer non lesponsable et le metir rois de cause dons dépiens. Pnt de droit Doit-on condamner Bolesdier et trousselle à payer à Le bers le somme de trente six francs pour léparation du tort qu'ils lui ont causé par pertes de temps ? Dcit on donevaulle Balerdier des reserves qu’il fait de repeter à trons sette les sommes aux quelles il pourroy être condamne par la Conseil ? Dait-on dire trous selle non responsabelie le mettre pors de causé ? Que doit-il être statué à l’égard des dépens ?
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu les parties en leurs demandes et conclusions les pertivement et en avoir délibéré conformément à la loi ; aAttendu qu’il est acquis eux débots que troufs elle à confié à Baleyier de s travaux pour être exeuités à la toche et à des prix déteminés ; que le fait par Trouls elle d’avair la drepé des ouvriers au dit Balerdier, cefut fit il acguis, ne pourrait le rendre resonsable vis à vis de les ouvriers du fait de son estrepreneur à leur égard ur en ce qui concerte Balesdier ; attendu que Lebert à déclaré à la baire du Conseil que Balesdier était son apraiée et non son patron ; que s'il était seul en norte s'est que seul il pepsédat un locat ; que s’il prétéveir une avant part sur les prix de façans d'était uniquement pour payer les loyers du local et pourvoir au paiement de memes dépenses communés ; Que cepuis arquis Lbars sta rous à réclamer à Balesidier son allègue ettente comme lui par les dommanes qus fraphent l’apruition et dont chaque membre dos saphort et la part en déveduellement ; Par ces motifs - Le Bureau Général jugeant en dernier retsort ; Dit Troulelle non responsiable emet lurs de causé, dit de bert non recevable centime demande envers Séont Balesdier, l'en déboute et le Condamne aux dépens envers le Trésor Public, pour le papiei timbré de la présente minte, conformément à la loi du sept Aui mil huit cent cinquante, ence, non compris le cout drus présent jugement, la signification d’iceli et ses suites. Ainsi jugé les jour mois et un que dessus. ecrevin e Lecucg secrétaire t
      </p>
     </div>
    </div>
    <div n="19" type="case">
     <opener>
      Du dit jour vingt suin
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Mosieur Porcaille à hustive, auvnier tellier, demeurant à Paris, rue du Levroge, numéro vingt cinq lis ; Demandeur ; Comparant ; D’une part ;
        </span>
        <span type="part2">
         Et primo Monsieur Bélegoier, ttre preneur de travaux de teltere demeurant à Paris, rue de lafigette numéro dex cent quatante trois ; Défendeur ; Comparat ; Dtautre part ; Et Monfieur Trousselle, Mautre ellies demeurant à Paris, rue se lafagete, numéro cent trente neuf ; Défendeur ; Comparant tupsi d’autre part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait - Par lettre du secrétairé du Conseil de Prud’hommes du Département de la Seine pour l’industrie des tissus en date du Samedi huit Juin mil huit cent soixante dix huit Couaille fit citer Balesdier à comparaître pardevant le dit conseil de Prud’hommes séant en Bureau Particulier le Mardi inz juin mil hui cent soixante dix hui pour se concilier si faire se paivait sur la demande qu’il entendit former contre lui devant le dit conseil en paiemet de la somme de trente six francs pour réparation du tort quil lui a causé en sefabsant chiner toute une semaine A l’appel de la cause ouaille se présenta et exposé cume e dessus. De son coté Balesdier se présenta et expos a du Conseil qu'il employa Camille pendant toute une semaine cpez renulaement quoiqu’il oit pupent étre l’oucher plus se le travail avair été plus atondant ; Que peudant la deuxième semaine il le ft, veur chague jour experait aup chaque pour lui donner du tavail quatr tes pour poali que onsele suilie ayant trouvé à fare faire à meilleur morché ne lui donna rien quoique l'ayant ajourné de jour en jour e de le pomd suilte sur la perde pour dire que Trousselle l'avant intouthé et ed’russe à Véberne dot responsable du fix de ce dernier et demande la remise de la cause pour l'actirnner dugen teleue en téle dit duites appeler Balerdor et truselle devait le Bureau de eite e ten ue jugent mis. Cits de la cause Suailfe se présenta, repit, sa demandes pre d ure der teuit e de de ae le e p e des pour de t de pu s uratesan se présenta et exposa au Conseil qu’il n’a pi mi emboucher souaille un l’adresserà Balendier puisqu’il ne l’a samais vu n chez lui ni ailloure Le Bureau Particulier pt davis nue puisque Ba les dure reconné par avax fait chemer ille il lui devait payer une indemnité qu’il fixa à la somme de vingt cinq frntc quand à trousselle il qe le sout pas responsable de engagea ouaille à abousonner sa demande à seu EardeCouaille ayant déclaré persister dans sa demoinde la cause fut renvoyée devant le Bureau Général du Conseil deant le jeudi vingt Juin mil huit cent soixante dix huit Cités pour le dit jour nit juin par lettres du secrétaire du Conseil en date du quatorze juin mil huit cent soixante e trait à la réguate de souaille Balegotier et Troussitte comparurent. A l'appel de la cause coneille se présenta et conclut à ce qu’il plut au Bureau Général du Conseil condamner solidairemen, Balestier et Coupelle à lui payer avec intérêts suivant la loi la somme de trente six francs pour répiration du dommaze qu’il a’etrouse par le chomage de taute une semiaine et les condamner solidairement aux dépens. De son coté Balerdier se présent et conclut à ce quilt plut au Bureau Général du Conseil lui donner aîte des résorver qu’il fait de résiiter à Troup ete les sommes auxocellet le conseil croirair devor le condamner proupselle, de son coté, conclut à ce qu’il plut au Bureau Général du conseil le dire non respontable et le matte lorr de cause sons dépens. Rais de troit — Doit-on condamner Baley dier etcroupelle à payer à Ponaille la sommed trente six francs pour indemnité de perte detemps ? Dait on donner aité à Balesdier des reésorves qu'il fait de régiten à trousselle les sommes auxquelles il aurait été condamne par le conseil ; Doit on dite trousselle non responsible le mettre trors de cause ; que doit-il être statué à l’égard des dépens ?
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu les parties en leurs et conclusions respectivement et en avoir délibéré Conformément à la loi ; Attendu que ce qui loncerne Trantelle qu’il refert des débats q’’il a confié à Béerdier des travix par être faits à la toitre eu des pris determinés ; qqu’dl fait pur trousselle d'avoir atreté à Bales dois dit ouvriers, le fit fit il acquis, ne pourrait le rendre envers e ouvriers responsable du fit de son athetrenent, Cnre condame Balepier; atendu que vuille Pittre dou e i e d un u di M Barre du Conseil que Baleydier était son apaice et nonton Patron ? Que si Bales dier était seul en nomc est que seul il pasés aun local il que s'il a prélevé sur le prix de façon une avant part Wéled anrquement pour payer les loyers du sdif local et aussi pourvoir au paiement de meues fournitirer. Qu’en ce fait acquis douaille devant en déviduellement lapporter de part, des dommages qui atteigent la loceté n’aren a réclamer à Balesdier, son colléque, attent comne lui ; Par ces motifs - Le Bureau Général jugeant en dernier réport ; n ce qui concerne Trouptelle le z non responsable et le methirs La cause ; Dit sonclle non recevable en la demande envers Baleydier, l'en déboute et le condamne aux dépens envers le Trésor Public, pour le papier timbré de la présente minute, conformément à la loi du sept aout mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d’icelui et ses suites. Ainsi jugé les jour, mois et an que dessus. Lecucq secrétaire avee .Ausi jour vint ciniq Entre Monsieur Boulain Auelphe, ouvrier letties, demerant à Pais, rue beremtf, numéro soixante dix Demandeur ; Comparant ; D’une part ;Mosieur Bélerdier, entretreneur de travaux dépelarie, de mourant à Pain, rue de lafayette, numéro deux cent quarante trois ; Défendeur ; Comparant ; D’autre part ; Lt se de de sur Consele, rte d e pome à Paris, rue de Calayette, numéro cant trente neuf Déléjant Consant; Crs ntiepus lit ptis u loitete a el e se la e doie d entie et euig en de e dous elle de onr les su et lu pent pes duti lesir le ur de sart some prs te e e dueiten e ei de la det n tes ponde q ues ute a de t e e e aur cte eu es e per et en urs pus e entre de quele causé en le faisaunt chaner tute rne semaine. A l’appel de la cause Poulin se présenta et exposa comme et dessus. De son coté Balejuer se présenta et exposa au conseil qu’il a ccupé Poulain after régulièrement pendant une semaine ; qu’en suite I le fis nenir tont les jours pousait pouvait lui on donner josqué ce la loi était promis par Trousselle, son Patron ; mais que le dit Crousselle ayant truvé à faire executer se travoux à des pris de tuits le fit, vamement attendre. Pare moment Poulain repris ca parale pour dire que pousselle l'ayant embauché et adressé à Bas ue devait être responsable qu’il a subl et demandasue la cause fut ajournée, pour le faire appelés E respon sabilité. La cause en et état fut ajournée a Juudi à vendredi quatorze Juin même mois. Cités pour le dit jour quatorze Juin par lettres du secrétaire du Conseil les détendeus comparurent. A l'appel de la cause Voulain rappela sa demande en vers Bales te tterma que Trousselle l'avait embouché et adressir li Boleydier son entrepreneur et demandu qu’a aison de ces faits il fut tenu comme responsable de l'indemnité demande à son entrepreneur. Lé Bureau Particulier fut davis nue Balesdier reonnaipa avoir fut chomer Poulai pendant tuné semaine il lui devait une indemnité qu’il fixa à vingt cinq francs ; Quanda tous selle il ne le crut pas repous du 4 en garea Poulain à aton donner à l’égard de Coupelle Poulain ayat déclaré persister dans sa demande la cause fut renvoyée devant le Bureau Généra du Conseil séant le Jeudi vingt Juin mil huit cent soixante dix huit. Cités pour le dit jour vingt juin par lettres du secrétaire du Conseil en dates du quatorze Juin mil huit cent soixante dix hui à la requite de Poulain Balesjon et trousselle Comprrurent. A l’appel de la case Poulain se présenta et conclut à ce qu’il plut au Bureau Général du Conseil condamner lolidairement Boleydor et trousselle à lui payer avec intérêts suivant la loi la somme de trente six francs à titre de répuration du dommase qu'il a éprouvé par Comage de toute cne semaine et les condamne solidairement ux dépens. De son coté Baley dox se présenta et conclut à ce qu’il plut au Bureau Général ufl inq mi une ; l de s 2 nsil que e e eu A aeuig de i auls ? A du Conseil lui donner aite des resarves qu’il fait de répiter à Trousselle les sommes auxquelles le Conseil eurai devoir le condamner envers coulain ; trousselle, de son coté, se présenta et conclut à ce qu’il plur au Bureau Général du Conseil le déclrer noreomble ar responsable envers Poulain et le mettre pers de cause dans dépens ? Paint de troit — Doit-on condamner Balendier et troups elle solidairement à payer à Poulain la somme de trente six francs pour l’indemnitér de temps de chamage; Doit on donner cité à Bolesdier des réserves qu’il fait de étarter à troupselle les sommes euxquelles il pourrait être condamne Daitaou dire rousselle non responsable et le mettre pors de causé ? Que dit-il être tatué à l’égard des dépens ? Après avoir entendu les parties en leurs demandes et conclusions respertivement et en csoir délibéré conformément à la loi ; Attendu qu’il eit acvis ux débetsque troupelle à confié à Bébersi des travaux pour être executés à la toitre et à des peis de termines ; Qu le fis par trouselle d'avoir accetté de onvents à l’atrder, apis les it poup la larer le rendre responsable envers ces ouvriers des aitert de tete fremte à la dauese enaer tere le der ud ue su les tilté le uire su titire qe silu e les eu drenl litne tie e e e u ei e t fasier en lete et eu s l rél se pus on purs seiles e prix de façon avant portire ee ne fit qu'inquement dur ui lite l e mn pareten cinse e lus seli de e en e e le ier nu el u te en lu se le em e quie ce de l r e t e d n en le eue aure eu re sa commnt le hur t e e t e de es le ent prser e e e rde men te ua due areden e te es u ant soir et prs e e te se poeur edt e e l ten u e e aur rspu e e eu d er edereveure demerait u e t u atese Leuigerétine r Dudi jour vingt Ping fu Entre Maisieur Fentane Consegdier, ontretreneu de travaux de elterie, demeurant à Paris, rue de Lafay édu uméro tent deix cit quarante troit ; Demandeur; Comparant ; D'une part ; Et Monsieur Trousselle e Maitre sollier demeurant et domicilié à Paris rue de lapaette, numéro cont trente neuf ; Défendeur ; Comparant ; D’autre part ; Point de fait - Par lettre du secrétaire du Conseil de Prud’hommes du Déurtenent de la seme pour l’industrie des tissus en date du moisi ouze juin mil huit cent soixante dix huit Baregdier fit citte trousselle à comparaître par devant le dit conseil de Prud’hommes séant en Bureau Particulier le vendredi quatorze Juin mil huit cent soixante dix huit pour se concilier si faire se pouvait sur la demande qu'il entendait fermen contre lui devant le dit conseil en paiement de la somme de cent ving francs pour indemnité de chomage et en quratre des indemnités qu'il pourrait être condamné à payer à treire, ouvriers qu’il a intrainés dans le même case A l’appel de la cause Borerdor se présenta et exposa uu Conseil que suivant convention verbale en date du vingt leai dernier troutselle s'eit engagé à lui donner les travaux à des Conditions de terminées nun seulemeant pour lui mois encore pour autant douvriers qu'il en pourrait trouvert ; Que le dit Trousselle qui l’accutra pendant une première semaine et trure ouvrièers emiboustrés à cet offet trouvant à fuire parer ces travaux à des prir reduits de les lui dama tros quoique le reit à chaque les demain, ce qui fait qu’il a non veulemre perdu son temps ; mois en cole fait perdre clui à d’avriers qui lui réclament des dommages-intérêts. De Venntate Tronsselle se présenta et exposa au Conseil qu’il ne i et Pis engage à donner du travail à Bales dier qelan sur etq mésure de ses besoins et non a toubon plaisir et par quantités, fres ; que s’il n’on donna que peuà la soin det aue n’était pas propté pour les livraissons à lui avait conseil d'ajourner eu qu'encore les apprêts étirent en retird, enta cis il n’était tenu à reuni envers Balesdier. Le teueau Particlier n’ayant pa concilier les parties la cause fut renvoyée devant le Bureau Général du Conseil séant le huddi vingt ui mil huit cent soixnte dix huit. Cité pour le dit jour mi jui par lettre du secrétaire du Conseil en date du mattrné juin mil huit Cent soixante dix huit à la réquitte de iteger 5 i Il u papelle Comparut . A l’appel de la cause Raleglier se présenta et roctut à ce qu'il plut au Bureau Général du Conseil attendu que troupel qui devait lui donner du ravail pour lu et ses ouvriers l’atupe conte une semane lans lui en donner et, chose plus auve l’afit veur chaque jour pour le remettre au tendemin ; qu'il ut constant que troupelle lui a causé un dommage dont il lui doit réparution Par ces motifs – Condaane Tousselle à lui payer avec intérêts suivant la loi la somme de cent vingt francs de dommages intérêts et le condamner aux dépens. De son oté Ausselle se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu qu'en acceptaut de Balerdier ses frir de façon il ne pitvis à vis de lui d’outre eugugement que de la domse des travaux à exécuter au fur et a mésire de ses besons et par quatités qu'il jugerait ; Qu'avit eu le soin de certains erti leuil les fit coupersappreter et les lui donna le dout et enpaye le prix ; Par ces motifs — Dire Ba legdor nonréccvalte en sa demande, l'en débouter et le condamner aux dépens. tont de droit — Doit-on condamner Troupelle à payer à Walensier la somme de cent vingt francs pour l’induniser du temps de couage Conquel il l’a otistrent par son fait à Paben dit -ooun dire du de e e dt endemnit ourl e e noer pet tre s’atué à rare du it àl peur deveir de parté à las ler de eu ese prisant ren avoir délibéré conformément à la loi ; attendu qu'il est constant que Baleydier s'engageant le vingt mai dernier à traviller pour da pete de a dis et e dede eseutie de di perdéster e ugit du jun cnee e pare en n es et foui p en i e n lu e t t e deiar sape quete u sa teusen de lure leq i au e te e des sutie uete sus sesg et eu et on e rdeer pasden de en de eit et an premen a s liur eue e e t de tinet ent e t pouir eni re es quendene eu e e per de ietr eu en e e Duriern e reurq usétire 1 Mudience jeudi vingt set soug mil huit cent sixante dix huit siégeant. Dessieurs Brand Théalier de la Ségon d’homnler dure Président du Conseil de Prud’homme du Département de la Seine pour l’industrie des E Tissus, président l’audence en remplacement de Monsieur Marienvat, Trerideut du dit Conseil imperte Carrony, Léforge Blanchiz et Margier Prul’hommes asistes le Monsieur Lecusq, secrétaire du dit Conseil. Entre Monseur Ponnet demetant à Paris, rue du Boulo, numéro dix sept, agissant à nom et comme administreteur de la personne et des biens pe la ville mineure Margueite, ouvrière ; Demandeur Comparant ; D’une part ; L Monsieur Varner, Maitre ailleur d’hotit, demeurant et domicilié à Paris, rue Montmartre, numéro cent surante, six; Défendeur ; D’éfailleus ; D'autre part ; Poir de fit Par lettres du secrétaire du conseil de Prud’homme du Département de la Seine pour l’industrie des tissus en dates des Vendredi sept e mardi onze juin mil huit cent soixante dix huit Ponet fit citer Vagrer a de sute purie t de le er der eu den eil e t e lated n enilier Apaus peuise le part quage cenis s lit lu e e e e pour le atis u t u e it en de por aente t u te de er ure ue de e ten q e suti ee de en e e u rceton et enie e e prs leus deu e de e e de ende e e i eu de it e n pur due cengetie ui nt amn c onor is D o Paguer fit citer Vlagyer à comparaise pardevant le dit conseil de Prud’hommes séait en Bureau Général lendi vingt sixx juin mil huit cent sixante dix huit pouir s'entendre condamne lui payer avec intérêts suivant la loi la somme de quatre vingt deux francs cinquante centimes qu’il lui doit corpris de travaux de sa fille Margueute, plus, tette indemnité qu’i plasra au Conseil fixer pour perte de temts devant les Bureaux du Conseil et reslégiens. A l’appel de la canse Mlagner ne comparut pas. De son coté Poinner le présenta et conclut à ce qu’il plut au Bureau Général du Conseil donner défaut contre Bacirer non comparu ni personne pour lui quoique dument appelé et pour ce profit reui adjuger le bénifice des conclusions par lui préses dant la citation exploit de champrou, tempsede enregistré.
      </p>
     </div>
     <div type="pointDeDroit">
      <p>
       Point de droit — fait-on donner défaut contre Vagner non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur conclusions par lui précédeumment prises ? Que doit il enre statué à l'égard les dépens ? Après avoir entendu Poiger en ses demandes et conclusions et en avoir délibéré conforméent à la loi Mndu que les demandes de oiguet pasursa justes et fondées ; Que d'ailleurs lles ne sont pas confestée par Vaprer non comparant ni personne pour lui quoique dudent l'appelé ; Attendu qu'en ne comparupant pas devant les Bureux du Conseil lagner a cause à Vorgney u présender de perte de temps, ce qu’il doit réparer ; Par ces mols e Buraun enret saint condemner atsort ; Done dépaur contre agner non comparant ni personne pour l quoique dument appelé ; Et adjugeant le proft du dit dé faute; condamne Clagner à payer avec intérêts suivant la loi à Peigner la somme de quatre vingt deux francs cinquante cent qusil lui dit pour lire de trtix de sa file de ron ins plus une indemnisté de neuf francs pour temps perdu d condamne en outre aux dépens trrois et ligudés envers le demender e lle somme de deuve forens t t e det our quante ca timoes avrirs le Treur Publi pour le ppie dut tinttrée et l'enreustrement de la citation la sormement li de sept du mil hui det oinquante en ces pon tompré cted preut oue a cine e te erie e e le a de lu dil es pre de lesadeante en te te e nt inque liu pe ent lent por prmiene pe épourx lunde et hapes. C eer jugé les jour mois et an que dessus. Lecug secrétaire dmanie e
      </p>
     </div>
    </div>
    <div n="20" type="case">
     <opener>
      Du dit jour vingt det sinz
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur et Madame ol cseur Cil tant en son nom personnel que pour apsister et autoriser la dame son épouse, ouvrière qu'eltière, demeurant ensemble les dits éhoux, à Paris, rue sainte Marthe, numéro trente quatre ; Demandeur ; Comparant ; D'une part ;
        </span>
        <span type="part2">
         Et Monsieur et Madame Combier, le sour combraytant en son nom personnel que pa aister et autoriser la dame son épouse Goletrère demeurant ensemble les tits éhout; à Pans, rue saint Meur, numéro deux cent dix ; Défndeurs ; Défaillants ; D’autre part ;
        </span>
       </rs>
      </p>
     </div>
     <div type="pointDeFait">
      <p>
       Point de fait - Par lettres du secrétaire du Conseil de Prud’homme du Département de la erme pour l'indstrie des tissus en dates des lundi dix apretende det luis doveil le es purte qui du pour Cihl par seur le vaire contie la parte par devant le dit conseil de Prud’homme s an Bureaux atintes le vrit ei ui t unt t en e ps nil huit cent soixante dix huit pour se conclu si faire se pouvait son le derentete Cinseur puin ce tersein le dit conseil en paiement de la somme de enze francs pour purt luit cende l de oere e pet e prig ent u ciles e de e e eni quent é p agfe e e ce t enie are ue e et e le ue d anter t du e pus aen o l ar tente ea de te e que pu eni e pdseus an tere ei u e quei e u ne apen de unt t eil unt e e te é Bel, plus tette indmnité qu'il plaira au Conseil fixer pour perte de temps devant les Bureaux du Conseil et les dépens ? Pouix de droit — Doit-on donner défaut contre es époux Conmlarion non comparants ni personne por eux quoique dument apelés pour le profit adjuger aux demandeurs les conclusions par eux précédemment prises ; Que doit-il être statué à l'égardde dépens ?
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu les époux Bel en leurs demandes et conclusions et en avoir délibéré conformément à la loi. Mltende que la demander des époux Wel posus juste et fendée; que d’ailleurs elle nt'est lus contestée par les époux Comproy non comparut ni personne pour eux quoique dument appelés ; attendu qu’en e comparusant pas devant les Bureaux du Conseil les époux Compal ont causé aux épeus ief un préjudice de perte de temps, le qu'ils doivent réparer ; Par ces motifs u Bureau Général jugeant en dernier repart ; au tarticle quarante un du décret du oize quigt mil huit cent neuf, portant réslement pour les conseils de Prud’homme Donne depau contre les époux Cambror non comparant ni personne pour ux quoique dument appelés ; Et adjugeant le profit du dit défaut ; condamne les époux Cambray à payer avec intérêts suivant la loi ax époux Rel la somme de ente francs u’il leur doivent pour prix de travaux de la dame Gel, prusane indemnité de quatre francs pour temps perdu ; Les condamne r outre aux dépens taxés et liquidés envers les demandeurs à la soe de un franc, et à celle due au Trésor Public, pour le papier timbré de la présente minute, conformément, à la loi du sept aout mil hui cent cinquante ence, non compris le cout du présent jugement, la signifiecction d’icelui et ses suites. Et vu les articles 435 du Codede procédure civile, 27 et 42 du decret du onze juin 18e9, pour signifier aux défendeurs le présent jugement, conmet. Chompran, l'uin de ses huisiers audienciers. Ansi jugé les jour mois et un que dessus. Jeurg iétre  Dineanx Ausit jour vingt cent Sing a ntre etenser ertordaure Polarée e sour Colrs le e tr omparent u por apales la deme due part mire par dom e de e es u e si ete i e vingt ; Demandeurs ; Comparant ; D’une part ; E Machenr Madame Gorguard, le sour Torgardtant en son nom personnel que pour apister la dame son épouse fabricnte de fleurs artificielles, demeurant et domiciliés à Paris, rue des deux torter sant Jonveur, numéro vingt huit ; Défendeurs ; Défaillant ; D’untre part ; Point de fait - Par lettre du secrétaire du Conseil de Prud’hommes du Département de la Seine pour l’industrie des Tissus en date du Lundi dix sept Juin mil huit Cent soixante dix huit les époux Citer pres citer les époux soiguard à comparaitre par devant le dit Conseil de Prud’hommes hui en Bureau Particulier le mardi dix huit juin mil huit cent soixante dix huit pour se concilier si jaire se pouvait sus la demande qu’ils entendaiens former contre eux devant le dit Conseil en paiemet de cent quarante trois francs qu'ils leur doivent pour prix de travaux de la dame Citer. A l’appel de la cause les époux Piter se présentèrent et exposstant comme dessus. De on coté orgon se présenta et reconnait devant la somme déclamée et fis, l’engagement de la payer par quinze francs le vingt deux Juin et ingt francs tant let quinze jurs, le saqmedi à le vingt deux juin les époux Gergmard ne poyeant prasut la causé en cet étot fut renvoyée devant le Bureau Général du Conseil séant le Judi vingt sept i mil huit cent soixante dix huit. Cité pour le dit jour vingt sept juin par lettre du Ginétaire du Conseil en date du vingt cinq juin mil huit cent soixante, dex luis pur tarout de l qurt coitit e gore demin ne comprmes pour e e l er le eoue Citer se présentèrent et conclurent à ce qu’il plut au Bureau prte se tarat sui u ent le genent qu ante pre e onden s se per e rf te la deme e lem ent ene e pe t e pu ile ou e e te e our eren e e eux aun e et en etite eende re de u tegen de au per t pu en en pesr audepeur a e coux e e e es e dem e de en uete etant a toemnité apee es de demnet que den esente eupre en eneique n Ees E D i ui is in e M t ni personne pour eui quoique dument appelés ; Attenda que e les époux Forshgard ont fait perdre du temps aux chaue iter devant les Bureaux du Conseil, ce qui leur cantei préjudice dont ils leur doivent réparation ; Par ces motis Le Bureau Général jugeant en dernier report, ou bartule quarante un du décres du onze juin mil huit cent neufer portant regument pour les conseil de Prud’hommes ; Done défaut contre les époux Gorguard non comparants ni personne pour eui quoique dument, appelés ; t adjugeant le profit dedit défaut condamne les époux Lorguiarda payer avec intérêts suivant la loi ax époux Céter la some de cent quarante trois francs quits leur doivent pour prix de trevux de la dame Poter, plus une indmnité de deux trancs pour temps perdu Les condamne en outre aux dépens taxés et liquidés envers les demandeurs à la somede suivait cinq centimes, et à celle due au Trésor Public pour le papier timbré de la présente minute, conformément à la loi du sept coute mil huit cent cinquante, ence non compris le cout du présent jugement, la signifiation d’icelui et ses suites. Et vules articles 435 du code de procédure civile 27 et 42 du secret du onze juin 1809, pour signifier aux défendeurs le présent jugement Comet Chompron, l'un de ses tempsiérs audienciers a jugé les jour mois et an que dessus. Lecucq térnétaireammeuoil
      </p>
     </div>
    </div>
    <div n="21" type="case">
     <opener>
      Du dit jour vingt sejt Puinu
     </opener>
     <div type="identificationParties">
      <p>
       <rs>
        <span type="part1">
         Entre Monsieur Charles Fétemett, ouvrier tordonner peurs à Paris, rue Paint souveur, numéro vingt huit ; Demanders comparant ; D'une part ;
        </span>
        Et Monsieur Gavail, Mntre cordonnier, demeurant et domicilié à Paris, rue Vayevir numéro quatre ; Défendeur ; Défaillant ;D’autre hart ; ores de fait – Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l’industrie des tissus en dates des Lundi trois et Mardi quatre juin mil huit cent sixante dix huit étemill fit citer Lavail à comparaître par devant le dit conseil de Prud'hommes séant en Bureaux Particuliers les mordi quel et vendredi sept juin mil huit, cent soixante dix huit pour se concilier si faire se pouvait sur la demande qusil entendu former contre lui devant le dit conseil en paiement de la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour lalaire . Lavail n'ayant pas comprru la cause ft renvayée devant le Bureau Général du conseil séant le jeudi vingt et ajournée au putte Vingt sept juin mil huit cent soixante six temp Cité pour le dit suivant exploif de champron, luipier à Paris, en date du vingt un juin mil huit cent soixante dix huit, visé pour timbre et enregistré à Paris, le vingt deux Juin mil huit cent soirante dix huit, etmitte fit citer ravail à comparaître par devant le dit Conseil de Prud’hommes séant en Bureau Général le jeudi vingt sept juin mil huit cent soixante dix lui pour s'entendre condamner u lui payer avec intérêts suivant la loi la somme de cinquante deux francs soixante quinze centimes qu’il lui doit pour salaire plus, telte indemnité qu'il plaira au Conseil frer pour perte de temps devant les Bureaux du Conseil et les dépens. A l'’appel de la cause Lavail ne comparut pas. De son coté schunt se présenta et conclutz à ce qu’il plut au Bureau Général du Conseil donner défau contre Lavail non comparant ni personn pour lui quoique dumet appelé et pour le profit lui adjuger le bénifice des conclusions par lui prises dans le citation en plois de Champour, lui par enregistré. Poin de troit — Doit-on donner défaut contre lavail non comparant ni personne pour lui quoique dument appelé et pour le profit adjuger au demandeur les conclusions par lui précédemment prises ? Que doit-il etre statué à l’égard des dépens ?
       </rs>
      </p>
     </div>
     <div type="judgement">
      <p>
       Après avoir entendu létimelte en ses demandes et conclusions et en avor délibéré conformément à la loi ; Mttendu que la demande sihuett purai juste t fondée ; que d'ailleurs elle n'est pas contestée par lavail non comparant ni personne pour lui quoique dument appelé ; attendu qu'en ne comparipant jes devant les Bureux du Conseil Lavail a causé à Btmill un préjudice de perte de temps, ce qu’il doit réparer. Par ces motif - Le Bureau Général jugeant ente e n comn da d it ente tue en lre t par me teu pé de pur e pprecit et i el u en es pr le oe uet cinre es soir de e e senr indemnité de nuf francs pour temps pardu ; le condamne unr e de tis e d en t lerie se épeur la premie ue e er i uil it é franc quire centimes envens le Trésor Public, pour le papier. timbré et l’enregistrement de la ctation, conformément à la loi sept aoux mil huit cent cinquante, ence, non compris le cout du présent jugement, la signification d'icelui et ses suites. t vu les articles 435 du code de procédure civile, 27 et 42 du decres, du onze juin 18099 pour signifier au défendeur le prémier jugement , Comme Rompi, l’un de ses huipars audienir Aiusi jugé les jour moois et an que dessus. Deneur Lecuuq secrétaire Audit jour vingt set sinig Entre Mademoiselle Maria Lesqune, ouvrière brodeute demeurant à Paris, rue Mongé, numéro cent huit ; Demandeur ; comparant ; D'une part ; Et Monsieur es Madame Coureille le sieur Courelle tant en son nom personnelque pour apristedt autoriser la dame son épouse brodeuse, demeurant et domecili onsemble, les dits époux, à Paris, rue du faubourg Montmestre numéro vingt huit ; Défendeur ; Comparant ; D’autre part , cn de fait - Par lettres du secrétaire du Conseil de Prud'hommes du Département de la Seine pour l’industrieu Tissus en date des Jendi vingt trois et vendredi vingt quit Mai mil huit cent soixante dix huit la demoiselle Lequine fit citer les époux Courcelle à comparaître par devant lesdit Conseil de Prud’homme séant en Bureaux Particuliers les Vendredi ving quatre et lundi vingt sept Mai mil huit cent saixante dil huit pour se concilier si faire se pouvait sur la demande qu’elle entendai former contre eux devant le dit Conseil en priéement de la somme de vingt cinq francs pour prix de travanx le trocdie A l'appel de la cause le vingt sept Mai les époux Conreil n'ayant pas comparu le vingt quatre la demoiselle Lguran, se présenta et exposa cou eu deseus. De leur Cite les époux Conrielles se présentèrent, drens que le prix du Gravail bien la trit de six francs contigusiéte mal fit et rendrer lardivement entendaus ne rien payer. La demanderees épara que son travail était nreplachablement faist LBureau Particulier veulent être fixé sur le mérite du Traral et sur sa valeur Vénble renvoya la cause à V’eroameil t qu Di i membre du conseil à ce comaitsant. Les parties n'avant pu ctre conciliée, la cause fut renvoyée devant le Bureau Général du Conseil séanz le jeudi vingt sept juin mil huit cent soixante dix huit. Ctés pour le dit jour vingt sept Juin par lettre du secrétaire du Conseil en date du vingt un juin mil huit cent loixante dix huit à la réquete de la demoiselle Lequine les époux Courcelles comparurent. A l’appel de la cause le demoiselle Semine se présenta et conclut à ce qu’il plut au Bureau Général du Conseil condamner les époux Courcelles à lui payer avec intérêts suivant la loi la somme de vingt cinq francs pour travaux de broderies et les condanner ux dépens. De leur coté les époux Courcelles se présentèrant et conclurent à ceguil plut à Bureau Général du Conseil attendu que la semoiselle Legune lui arendu un travail mal fait et en acceptable par la loie qu'elle a employée Qu'on tant cas, ce travail fut il acreptable le prix n’envaidrauit que six francs et non vingt cinq ur ces motifs — Dire la demoiselle Leonne non recevable en sa demande, l'en débouter comme mal fondée en celle et la condamner aux dépens. Paint de drot — Doit-on condamner les époux Courcelles à payer dà la demoiselle Léquine la somme de vingt cinq francs pour travaux de brochrre ? Oubion doit-on dire la demoiselle Lequine ; onrécemble en ses demandes, l'en déboutes ? Que doit-il être statué à l'égard des dépens ? Après avoir entendu les parties en leurs demandes et conclusions respectivement et en avou délibéré conformément à la loi ; attendu qu’il est constant que la soie employée e travail de brodrie qui fait l'objit du titige est nne soie mathinte ; Mais attendu que cet en couvenments pouvait être cité par ls épous Courcelles en chaisissant et fournipsant et mimes la toie du bien don taser le soin à l’ouvrière qui ne puit certeimeent par avoir la même, connailsence qu’eut et oui ne doit pas enconser une semblabli responsabilité ; attendu eu ce qui donverne le prix du travait qui le Conseil qui à legilement quce lavrier d’apprenition bestime à due francs , Eor ces motifs – Le dpen r te jour an deuer t s lu sante n lanre l eu ile u tete e a lire alrende i7 pui qu pae t sux dte ies d dit den it e mne tende a pent dou qeme lrsae t de e e ter deu deniont a deme tep le mrdt pure u la tie u e e d pumnt ape et esprqute enet non compris le cout du présent jugement, la signification d'icelui Ainsi jugé les jour, mois et an que dessus. et ses suites. derurg seuctaire Dnaurlte du dit jour vingt sept sing Entre Monsieur Dauté, fabricans de tissus, demeurant Qu et domicilié à Paris, rue saint paire, numéro vingt Défendeur au tirmipal ; Demandeur appotant ; Comparaite D'une part ; Et Madame Délatiace, ouvrière, demantant à antenit le Maudouin oise ; Demanderesse au princépar Défenderesse ; opprosent ; Comparant ; D'autre part ; Paint le aux termes d'un fait- Par jugement rendu par défaut par le conseil de Prud’home pouvoir sons devante du Département de la Seine pour l'industrie des tissuste neouf pasé en date du anthuil le houdun, du Ma mil huit cent soixante dix huit, enregistré ; Dendé à étter premier Mi mil condamné à payer à la dame Défatrace la somme de trois cent huit cent soixante soixente quinze frons de princépat et les accilsire: d jugement dix huit, enregttrée fut Simifié à Fandit par exploit de Ginaad, uipier ; en dit reuve epreuvdu dix neuf Juin mil huit cent soixante dix huit, enregistré à la ruguite de Dauilé. suivant en péloit de cham prin huipare à lain en date du vingt Juin mil huit cent soixante dix traid udisé pour titre elenregistré à Paris, le vingt un juin mil Devai huit cent soixante dix huit, détes deux francs quinze centimes spur de Siuilles ; Daudé forme apposition à A jugement et Citre les époux Délahacé à comparaître par devant le dit Conseil de Prud’homme dént en Bureau Général le jeudi vingt sept juin mil huit cent soixante dix huit pour le voir de cevoir opposant à le gugeme le voir, de charger des condamnations contre dux prosoncées en principal et accepaires, s'entendre lus dix époux Délalice déclarer non recevable eu leurs demandeu, les en débouter; Cone mal fondés en celles, et s'entendre condamner ux dépens de la première comme de la présente inistance. A l’appel de la cause Dande se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu que la dame Délahayé n’et pas auveiit dans l'acaption du mot ; qu'elle entreprenense de travaux de fet execiter par dtentres ouvrières à Monteuil le hondamne ses ouirons ; Qu'en outre elle lui achate la soie qu'elle faut M or o I t pour n et avi les façons et a forfait ; que le conseil n’était pas compateut pour lomuitre des différent entre les fabricaute et les entrehreneus de travaux à lerhuit, d'ut à tort que les époux Delcheye aun près contre lui le neuf Mai dernier en jugement par défaut Par ces motifs — Dire le jugent nul et de mil uffes ; Ptatuant à mouveu Le déclrer n compétent pour tommêtre de la demande des époux Délataye, les renvoyer devant jui de droit et les condamner aux dépent de la première comme de la présente inistance. De son coté la dame Détatoge se présenta et conclut à ce qu’il plut au Bureau Général du Conseil attendu qu’elle n’est ni marchon de ni fabricant; qu’elle n’a fait avec Dandé auun oite de commerte Que si elle ne ceute pas seule les trovuy qui lui sont confiés per cmperté à Dau de puisqu'elle en suite responsable Bois avis de lui de l'ixeution de cestranailr ; Par ces motifs Dire Doudé, nonrecevable en vondéblinatoire, se déclarer competent, rétenir la cause et ordonner qu’il sera procéde à l’examen de sa demande pour être statué sur le fautà Pants de droit - Doit on se déclarer en compétent pour conaitre de la demande des éour Délatoye il renvayer la cause devaut qui de drait ? Ou bien doit-on se déclarer confétent retenir la cause et ordonner que les parties s'entliqueran pour être statué au fand. Que doit-il être statué à l'égarddes dépens ? Après avoir entendu les parties en leurs demandes ete conclusions respectivement et en avoir délibéré conformément à la loi ; Attendu que des explications fournies par les partiese que la dame Delahoya quoiqu’oupput des ouvrirres pour l’éxcution de travaux qui lui roit fournis par dans é n’est pas monis vis t sis de cela a une ouvrière prtinee du conseil de Prud’hommes puisqu’elle ne furait avei le travail que la soce que est é la brodeuse ce qu'et le fle au contirier au à la conturière ; que cette fourniture et e e r p le con der een de préte us e pent edue preut dmit lur puastur qui de pr e e pit edt peu e t e re desenite on et du n t ee eis de en e de en tur ete em e se e seut endere e un que lut e du l te e nten ingq en eit journée par être reprise le jendi quatre puillet mil huit cnt seinente dix huit. Ainsi jugé les jour mois et an que depis Leniig ecrétare L Dunaiof
      </p>
     </div>
    </div>
   </div>
  </body>
 </text>
</TEI>
```
